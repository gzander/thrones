var tab_count=0;
var keywords_array=new Array();
var libraries_array=new Array();
var keywords_metadata_array=new Array();
var libraries_metadata_array=new Array();
var decorators=new Array();
var lib_selected=false;
var selected_lib_name='';

//-------------------------------
var flatThreadsArray=new Array();
var threads_level=-1;

function uniqueHash(s)
{
    if(s!='undefined')
        return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);  
    else
        return  Math.floor(Math.random()*1000);
}

var Base64 = {
   _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
   //метод для кодировки в base64 на javascript 
    encode : function (input) {
      var output = "";
      //alert(input);
      var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
      var i = 0;
      input = Base64._utf8_encode(input);
         while (i < input.length) {
       chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);
       enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;
       if( isNaN(chr2) ) {
           enc3 = enc4 = 64;
        }else if( isNaN(chr3) ){
          enc4 = 64;
        }
       output = output +
        this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
        this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
     }
      return output;
    },
 
   //метод для раскодировки из base64 
    decode : function (input) {
      var output = "";
      var chr1, chr2, chr3;
      var enc1, enc2, enc3, enc4;
      var i = 0;
     input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
     while (i < input.length) {
       enc1 = this._keyStr.indexOf(input.charAt(i++));
        enc2 = this._keyStr.indexOf(input.charAt(i++));
        enc3 = this._keyStr.indexOf(input.charAt(i++));
        enc4 = this._keyStr.indexOf(input.charAt(i++));
       chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;
       output = output + String.fromCharCode(chr1);
       if( enc3 != 64 ){
          output = output + String.fromCharCode(chr2);
        }
        if( enc4 != 64 ) {
          output = output + String.fromCharCode(chr3);
        }
   }
   output = Base64._utf8_decode(output);
     return output;
   },
   // метод для кодировки в utf8 
    _utf8_encode : function (string) {
      string = string.replace(/\r\n/g,"\n");
      var utftext = "";
      for (var n = 0; n < string.length; n++) {
        var c = string.charCodeAt(n);
       if( c < 128 ){
          utftext += String.fromCharCode(c);
        }else if( (c > 127) && (c < 2048) ){
          utftext += String.fromCharCode((c >> 6) | 192);
          utftext += String.fromCharCode((c & 63) | 128);
        }else {
          utftext += String.fromCharCode((c >> 12) | 224);
          utftext += String.fromCharCode(((c >> 6) & 63) | 128);
          utftext += String.fromCharCode((c & 63) | 128);
        }
     }
      return utftext;
 
    },
 
    //метод для раскодировки из urf8 
    _utf8_decode : function (utftext) {
      var string = "";
      var i = 0;
      var c = c1 = c2 = 0;
      while( i < utftext.length ){
        c = utftext.charCodeAt(i);
       if (c < 128) {
          string += String.fromCharCode(c);
          i++;
        }else if( (c > 191) && (c < 224) ) {
          c2 = utftext.charCodeAt(i+1);
          string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
          i += 2;
        }else {
          c2 = utftext.charCodeAt(i+1);
          c3 = utftext.charCodeAt(i+2);
          string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
          i += 3;
        }
     }
     return string;
    }
 }
 
function dismissTab(obj)
{
    //alert($(obj).nextAll("a").text());
    $($(obj).nextAll("a").attr("href")).remove();
    $(obj).parent("li").remove();
}
 
function callCreateLibrary()
{
    $('#createLibraryModal').modal();
}
 
function fillCategoriesSelect()
{
    $.ajax
        ({
            data: "action=getCategories",
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                //console.log(msg);
                var options = eval('(' + msg + ')'); // myJson is your data variable

                var length = options.categories.length;
                console.log(length);
                $('#script_category_select').empty();
                for(var j = 0; j < length; j++)
                {
                    var newOption = $('<option/>');
                    //newOption.attr('text', options.categories[j].name);
                    newOption.html(options.categories[j].name);
                    newOption.attr('value', options.categories[j].id); // fixed typo
                    $('#script_category_select').append(newOption);
                    console.log(newOption);
                }
            },
            complete: function (XMLHttpRequest, textStatus){}
       });
}

function fillLibrariesSelect()
{
    $.ajax
        ({
            data: "action=getLibraries",
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                console.log(msg);
                var options = eval('(' + msg + ')'); // myJson is your data variable
                
                var length = options.libraries.length;
                console.log(length);
               
                
                $('.script_library_select').each(function(i, block) {
                    //alert('suka');
                                                                        var newDefaultOption = $('<option/>');
                                                                        //newOption.attr('text', options.categories[j].name);
                                                                        newDefaultOption.html("Main library");
                                                                        newDefaultOption.attr('value', ""); // fixed typo
                                                                        $(block).empty();
                                                                        $(block).append(newDefaultOption);
                                                                    });  
                
                $('.script_library_select').each(function(i, block) {
                                                                        for(var j = 0; j < length; j++)
                                                                        {
                                                                            var newOption = $('<option/>');
                                                                            //newOption.attr('text', options.categories[j].name);
                                                                            newOption.html(options.libraries[j]);
                                                                            newOption.attr('value', options.libraries[j]); // fixed typo
                                                                            $(block).append(newOption);
                                                                            console.log(newOption);
                                                                        }
                                                                    });

            },
            complete: function (XMLHttpRequest, textStatus){}
       });
}

function fillCategoriesTabs(obj)
{
    $.ajax
        ({
            data: {action:'getCategories', library_name: $('#library_library_select').val()},
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                $('#library_tabs_content').empty();
                console.log(msg);
                var options = eval('(' + msg + ')'); // myJson is your data variable

                var length = options.categories.length;
                console.log(length);
                $('#library_tabs').empty();
                for(var j = 0; j < length; j++)
                {
                    /*var newOption = $('<option/>');
                    //newOption.attr('text', options.categories[j].name);
                    newOption.html(options.categories[j].name);
                    newOption.attr('value', options.categories[j].id); // fixed typo
                    $('#script_category_select').append(newOption);
                    console.log(newOption);*/
                    
                    $('#library_tabs').append('<li'+(j==0?' class="active"':'')+'><a href="#libtab_'+options.categories[j].id+'" data-toggle="tab"><img src="'+options.categories[j].image+'"/>'/*+options.categories[j].name*/+'</a></li>');
                    $('#library_tabs_content').append('<div class="tab-pane '+(j==0?'active':'')+'" id="libtab_'+options.categories[j].id+'"><table class="table table-striped" id="liblist_'+options.categories[j].id+'"></table></div>');
                    for(var k=0; k<options.categories[j].functions.length;k++)
                    {
                        //console.log(('#liblist_'+options.categories[j].id));
                        $('#liblist_'+options.categories[j].id).append('<tr onclick="zoomSource(this);" ondblclick="zoomScript(this, \''+$('#library_library_select').val()+'\');"><td class="spec"><b>'+options.categories[j].functions[k].spec+'</b></td><td>'+options.categories[j].functions[k].desc+'</td><td class="source"><input type="hidden" value="'+options.categories[j].functions[k].body_base64+'"></td></tr>');
                    }
                }
            },
            complete: function (XMLHttpRequest, textStatus){}
       });
}

function fillInterfaces()
{
    $.ajax
        ({
            data: "action=getInterfaces",
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                $('#interfaces-list').empty();
                console.log(msg);
                var options = eval('(' + msg + ')'); // myJson is your data variable

                var length = options.interfaces.length;
                console.log(length);
                for(var j = 0; j < length; j++)
                {

                    $($('#interfaces-list').append($('<tr style="cursor: pointer;" onclick="zoomInterface(this);"><td>'+options.interfaces[j].prefix+'</td><td>'+options.interfaces[j].type+'</td><td><button class="btn btn-warning" value="Autogenerate methods" onclick="autoGenerate(\''+options.interfaces[j].prefix+'\')">Autogenerate methods</button></td></tr>').data('properties',options.interfaces[j].params)));//+'<a href="#libtab_'+options.categories[j].id+'" data-toggle="tab"><img src="'+options.categories[j].image+'"/>'/*+options.categories[j].name*/+'</a></li>');
                }
            },
            complete: function (XMLHttpRequest, textStatus){}
       });
}

function zoomInterface(obj)
{
    //alert($(obj).children(".source").children("input").val());
    $('#interface-details').empty();
    for(var j = 0; $(obj).data('properties').length; j++)
    {
        $('#interface-details').append('<tr><td>'+$(obj).data('properties')[j].name+'</td><td>'+$(obj).data('properties')[j].value+'</td></tr>');
    }
}

function autoGenerate(prefix)
{
    /*fillCategoriesSelect();
    fillLibrariesSelect();
    $('#saveScriptModal').modal();
    $('#target_save').val('autogenerated');
    $("#save-script-desc").hide();
    $("#save_for").val(prefix);*/
    $.ajax
        ({
            data: {action: 'autoGenerate', scriptBody: prefix},
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                var response=eval('('+msg+')');
                //setTimeout(updateKeyWords, 1000);
                //$(source).parent().parent().parent().children(".script-identifier").val(response.id);
                //setTimeout(getConsole, 1000);
                //setTimeout(requestParams, 1000);
            },
            complete: function (XMLHttpRequest, textStatus){}
       });
}

function zoomSource(obj)
{
    //alert($(obj).children(".source").children("input").val());
    $('#library_details').empty();
    $('#library_details').html(Base64.decode($(obj).children(".source").children("input").val()));
    //hljs.highlightBlock($('#library_details'));
    $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });
}

function zoomScript(obj, library)
{
    var library_prefix='';
    if(library.length>0)
    {
        library_prefix=library+'.';
    }
    //alert($(obj).children(".source").children("input").val());
    createScriptTab(library_prefix+$(obj).children(".spec").children("b").text(), Base64.decode($(obj).children(".source").children("input").val()));
    //$('#library_details').html(Base64.decode($(obj).children(".source").children("input").val()));
    //hljs.highlightBlock($('#library_details'));
}

function setParams()
{
    $.ajax
        ({
            data: "action=sendAction&urlParams="+Base64.encode($('#urlParams').val())+"&signin_session="+$('#signin_session').val()
                +"&signin_userid="+$('#signin_userid').val()+"&method="+$('#method').val()+
                "&signInAuthSeed="+Base64._utf8_encode($('#signInAuthSeed').val())+
                "&signInAuthKey="+$('#signInAuthKey').val(),
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                //setTimeout(requestParams, 1000);
            },
            complete: function (XMLHttpRequest, textStatus){}
       });
}

function setSession()
{
    $.ajax
        ({
            data: "action=startSession&"+"signin_session="+$('#signin_session').val()
                +"&signin_userid="+$('#signin_userid').val()+
                "&signInAuthSeed="+Base64.encode($('#signInAuthSeed').val())+
                "&signInAuthKey="+$('#signInAuthKey').val(),
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                //setTimeout(requestParams, 1000);
            },
            complete: function (XMLHttpRequest, textStatus){}
       });
}

function getConsole()
{
    $.ajax
        ({
            data: {action:"getMessages"},
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                //console.log(msg);
                //alert(msg);
                var response=eval('('+msg+')');
                for(var i=0;i<response.processes.length;i++)
                {
                    for(var j=0;j<response.processes[i].messages.length;j++)
                    {
                        var message=response.processes[i].messages[j];
                        //alert(msg);
                        if(message.type==0) // console message
                        {
                            pushMessageTo(response.processes[i].id,response.processes[i].messages[j]);
                        }
                        else if(message.type==2)
                        {
                            requestParams(response.processes[i].id,response.processes[i].messages[j]);
                            pushMessageTo(response.processes[i].id,response.processes[i].messages[j]);
                        }
                        else if(message.type==4)
                        {
                            alert(message.text);
                        }
                    }
                }
                
                for(var i=0;i<response.core_messages.length;i++)
                {
                    var message=response.core_messages[i];
                    if(message.type==0) // console message
                    {
                        pushMessageTo(null,response.processes[i].messages[j]);
                    }
                    else if(message.type==2)
                    {
                        requestParams(null,response.processes[i].messages[j]);
                        pushMessageTo(null,response.processes[i].messages[j]);
                    }
                    else if(message.type==4)
                    {
                        alert(message.text);
                    }
                    else if(message.type==5)
                    {
                        createScriptTab(message.title,message.text);
                    }
                }
                
            },
            complete: function (XMLHttpRequest, textStatus){setTimeout(getConsole, 1000);}
       });
}

function requestParams(process_identifier, message)
{
    var params=message.params.params;
    var scriptBody=$('input').filter(function() { return this.value == process_identifier && $(this).hasClass('script-identifier'); }).parent().children('.tab-content').children('.script-pane').children('div').children(".script-body").children("label").children("textarea").val();
    var script_hash=uniqueHash(scriptBody);
    //alert(script_hash);
    if(script_hash==$("#request_for_hash").val())
    {
        //alert($("#params-modal-inside").children().length);
    }
    else
    {
        $("#request_for_hash").val(script_hash);
    }
    $("#request_for").val(process_identifier);
    
    var params_cached_str=$('input').filter(function() { return this.value == process_identifier && $(this).hasClass('script-identifier'); }).parent().children('.params-cache').val();
    //alert(params_cached_str);
    
    var params_cached;
    if(params_cached_str.length>0)
        params_cached=eval('('+params_cached_str+')').params;
    
    
    $("#params-modal-inside").empty();
    
    //alert(typeof params_cached);
    if(typeof params_cached == 'object')
    {
        for(var i=0; i<params.length; i++)
        {
            if(params[i].type==0)
            {
                var placeholder='';
                for(var j=0; j<params_cached.length; j++)
                {
                    if(params_cached[j].name==params[i].name)
                        placeholder=params_cached[j].value;
                }
                $("#params-modal-inside").append('<div><span class="common-label">'+params[i].name+':</span><input class="input-large value-input" type="text" name="'+params[i].name+'" value="'+params[i].value+'" placeholder="'+placeholder+'"><span class="common-label">don\'t ask again</span><input class="remember-input" type="checkbox" name="'+params[i].name+'" '+(params[i].remember?'checked':'')+'"></div>');
            }

        }
    }
    else
    {
        for(var i=0; i<params.length; i++)
        {
            if(params[i].type==0)
            {
                $("#params-modal-inside").append('<div><span class="common-label">'+params[i].name+':</span><input class="input-large value-input" type="text" name="'+params[i].name+'" value="'+params[i].value+'"><span class="common-label">don\'t ask again</span><input class="remember-input" type="checkbox" name="'+params[i].name+'" '+(params[i].remember?'checked':'')+'"></div>');
            }

        }
    }
    $('#paramsModal').modal();
}

function sendParams()
{
    var params=new Object();
    params.params = new Array();
    $.each(
            $("#params-modal-inside").children("div"),
            function( i, val ) 
                                {
                                    var param=new Object();
                                    param.name=$(val).children(".value-input").attr('name');
                                    if($(val).children(".value-input").val().length>0)
                                    {
                                        param.value=$(val).children(".value-input").val();
                                    }
                                    else
                                    {
                                        param.value=$(val).children(".value-input").attr('placeholder');
                                    }
                                    param.remember=($(val).children(".remember-input").attr('checked')=='checked');
                                    params.params[i]=param;
                                }
          );
              
     var targetTabCache=$($('input').filter(function() { return this.value == $("#request_for").val() && $(this).hasClass('script-identifier'); }).parent().children('.params-cache'));
     targetTabCache.val(deserialize(params));
     $.ajax
        ({
            data: {action:"setParams",params:deserialize(params),id:$("#request_for").val()},
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                var response=eval('('+msg+')');
                console.log(msg);
                //setTimeout(requestParams, 1000);
            },
            complete: function (XMLHttpRequest, textStatus){}
       });
}

function pushMessageTo(id, message)
{
    $.each($(".script-identifier"),function( i, val ) 
    {
        if($(val).val()==id)
        {
            var added_message=$("<div>"+message.text+"</div>");
            if(message.level==2)
            {
                added_message.addClass("warning-message");
            }
            else if(message.level==3)
            {
                added_message.addClass("error-message");
            }
            else
            {
                if(message.type==2)
                    added_message.addClass("success-message");
            }
            
            $(val).parent().children(".tab-content").children(".console-pane").children("div").children(".console-output").append(added_message);
        }
    });
    
}

function runScript(source, level)
{
    var script_body=$(source).parent().children("div").children(".script-body").children("label").children("textarea").val();
    $.ajax
        ({
            data: {action:"runScript",scriptBody:script_body,mode:level},
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                var response=eval('('+msg+')');
                $(source).parent().parent().parent().children(".script-identifier").val(response.id);
                $(source).parent().parent().parent().children(".script-hash").val(uniqueHash(script_body));
                //setTimeout(requestParams, 1000);
            },
            complete: function (XMLHttpRequest, textStatus){}
       });
}

function callSaveModal(source)
{
    var script_body=$(source).parent().children("div").children(".script-body").children("label").children("textarea").val();
    
    try {
            if(typeof  eval("("+script_body+")") === "function")
            {
                console.log(script_body);
                $("#save_for").val(Base64.encode(script_body));
                fillCategoriesSelect();
                fillLibrariesSelect();
                $('#saveScriptModal').modal();
                $('#target_save').val='single';
                $("#save-script-desc").show();
            }
            else
            {
                alert("Such a shame! Script isn't function.");
            } 
        } 
        catch (e) 
        {
            if (e instanceof SyntaxError) 
            {
                alert(e.message);
            } 
            else 
            {
                throw( e );
            }
        }
}

function saveLibrary()
{
    $.ajax
        ({
            data: {action: 'saveLibrary', library_name: $("#save-library-desc").val()},
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                var response=eval('('+msg+')');
                if(response.state==0)
                {
                    fillLibrariesSelect();
                }
            },
            complete: function (XMLHttpRequest, textStatus){}
       });
}

function saveScript()
{
    $.ajax
        ({
            data: {action: 'saveScript', scriptBody: $("#save_for").val(), desc:$("#save-script-desc").val(), textcomplete:$("#save-script-auto").attr('checked')=='checked'?"yes":"no", category:$('#script_category_select').val(), library:$('#script_library_select').val()},
            beforeSend: function (XMLHttpRequest){},
            success: function(msg)
            {
                var response=eval('('+msg+')');
                setTimeout(updateKeyWords, 1000);
                //$(source).parent().parent().parent().children(".script-identifier").val(response.id);
                //setTimeout(getConsole, 1000);
                //setTimeout(requestParams, 1000);
            },
            complete: function (XMLHttpRequest, textStatus){}
       });
}

function selectAllRemember(check)
{
    $.each(
            $("#params-modal-inside").children("div"),
            function( i, val ) 
                                {
                                    if($(check).attr('checked')=='checked')
                                        $(val).children(".remember-input").attr('checked',true);
                                    else
                                        $(val).children(".remember-input").attr('checked',false);
                                }
          );
}


$(document).ready(function () 
{
    $.ajaxSetup
    (
        {
            type: "POST",
            url: ""
        }
    );
        
    updateKeyWords();
    setTimeout(getConsole, 1000);
    $("#example-basic").treetable({ expandable: true });
    fillCategoriesTabs();
    fillLibrariesSelect();
    fillInterfaces();
});

function updateProcesses()
{
    $.ajax
    ({
        data: "action=getThreads",
        beforeSend: function (XMLHttpRequest){},
        success: function(msg)
        {
            console.log(msg);
            var response=eval('('+msg+')');
            flatThreadsArray=new Array();
            threads_level=0;
            expandThreadElement(response, threads_level, -1)
            var threads_table='';
            /*
             *<tr data-tt-id="1">
                <td>Node 1: Click on the icon in front of me to expand this branch.</td>
                <td>I live in the second column.</td>
              </tr>
              <tr data-tt-id="1.1" data-tt-parent-id="1">
                <td>Node 1.1: Look, I am a table row <em>and</em> I am part of a tree!</td>
                <td>Interesting.</td>
              </tr>
             **/
            $('#threads-list').empty();
            for(var i=0;i<flatThreadsArray.length;i++)
            {
                threads_table+='<tr data-tt-id="'+flatThreadsArray[i].level+'" data-tt-parent-id="'+flatThreadsArray[i].parent+'"><td>'+flatThreadsArray[i].thread+'</td><td>'+flatThreadsArray[i].value+'</td></tr>';
            }
            $('#threads-list').append(threads_table);
            $("#threads-list").treetable({ expandable: true },true);
            console.log(threads_table);
        },
        complete: function (XMLHttpRequest, textStatus){}
   });
}

function updateJustProcesses()
{
    $.ajax
    ({
        data: "action=getJustThreads",
        beforeSend: function (XMLHttpRequest){},
        success: function(msg)
        {
            console.log(msg);
            var response=eval('('+msg+')');
            flatThreadsArray=new Array();
            threads_level=0;
            expandThreadElement(response, threads_level, -1)
            var threads_table='';
            /*
             *<tr data-tt-id="1">
                <td>Node 1: Click on the icon in front of me to expand this branch.</td>
                <td>I live in the second column.</td>
              </tr>
              <tr data-tt-id="1.1" data-tt-parent-id="1">
                <td>Node 1.1: Look, I am a table row <em>and</em> I am part of a tree!</td>
                <td>Interesting.</td>
              </tr>
             **/
            $('#threads-list').empty();
            for(var i=0;i<flatThreadsArray.length;i++)
            {
                threads_table+='<tr data-tt-id="'+flatThreadsArray[i].level+'" data-tt-parent-id="'+flatThreadsArray[i].parent+'"><td>'+flatThreadsArray[i].thread+'</td><td>'+flatThreadsArray[i].value+'</td></tr>';
            }
            $('#threads-list').append(threads_table);
            $("#threads-list").treetable({ expandable: true },true);
            console.log(threads_table);
        },
        complete: function (XMLHttpRequest, textStatus){}
   });
}

function expandThreadElement(node, level, parent)
{
    threads_level++;
    var pt=threads_level;
    var threads_array=node.threads;
    flatThreadsArray.push({level:threads_level,thread:('thread #'+node.id), value:'', parent: parent, type: 'thread'});
    for(var i=0;i<threads_array.length;i++)
    {
        expandThreadElement(threads_array[i], threads_level, pt)
    }
    
    if(node.variables)
    {
        var variables_array=node.variables;
        for(var j=0;j<variables_array.length;j++)
        {
            expandVariableElementX(variables_array[j].name,variables_array[j].value,threads_level, pt);
        }
    }
}

function expandVariableElementX(node_name, node_value, level, parent)
{
    threads_level++;
    var pt=threads_level;
    console.log('variable '+node_name);
    if(node_value instanceof Object)
    {
        console.log('object!');
        flatThreadsArray.push({level:threads_level,thread:node_name, value:'', parent: parent, type: 'object'});
        var keys = Object.keys(node_value)
        for(var i=0;i<keys.length;i++)
        {
            var obj=node_value[keys[i]];
            console.log('key:'+keys[i]);
            expandVariableElementX(keys[i],obj,threads_level,pt);
        }
    }
    else if(node_value instanceof Array)
    {
        console.log('array!');
        flatThreadsArray.push({level:threads_level,thread:node_name, value:'', parent: parent, type: 'array'});
        for(var j=0;j<node_value.length;j++)
        {
            expandVariableElementX('['+j+']',node_value[j],threads_level,pt);
        }
    }
    else
    {
        console.log('simple! ');
        flatThreadsArray.push({level:threads_level,thread:node_name, value:node_value, parent: parent, type: 'variable'});
    }
}

function expandVariableElement(node, level, parent)
{
    threads_level++;
    var pt=threads_level;
    console.log('variable '+node.name);
    if(node.value instanceof Object)
    {
        console.log('object!');
        flatThreadsArray.push({level:threads_level,thread:node.name, value:'', parent: parent});
        var keys = Object.keys(node.value)
        for(var i=0;i<keys.length;i++)
        {
            var obj=node.value[keys[i]];
            console.log('key:'+keys[i]);
            expandVariableElement(obj,threads_level,pt);
        }
    }
    else if(node.value instanceof Array)
    {
        console.log('array!');
        flatThreadsArray.push({level:threads_level,thread:node.name, value:'', parent: parent});
        for(var j=0;j<node.value.length;j++)
        {
            expandVariableElement(node.value[j],threads_level,pt);
        }
    }
    else
    {
        console.log('simple! ');
        flatThreadsArray.push({level:threads_level,thread:node.name, value:node.value, parent: parent});
    }
}

function updateKeyWords()
{
    $.ajax
    ({
        data: "action=getKeywords",
        beforeSend: function (XMLHttpRequest){},
        success: function(msg)
        {
            console.log(msg);
            var response=eval('('+msg+')');
            
            keywords_array=response.keywords;
            keywords_metadata_array=response.metadata;
            libraries_array=response.libraries;
            libraries_metadata_array=response.libraries_metadata;
        },
        complete: function (XMLHttpRequest, textStatus){}
   });
}

function getKeyWordMetadata(keyword)
{
   for(var j=0;j<keywords_metadata_array.length;j++)
   {
       if(keywords_metadata_array[j].keyword==keyword)
       {
           return keywords_metadata_array[j];
       }
   }
   
   return {keyword: keyword, category_name: '',category_desc: '', desc: 'no description', category_image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACs0lEQVR42pWWu2tTURzHv0PMu02btCFpUy22xUdTi7XRqKhptdpGqYoILkJHRwc3FRxcRAQRqoP/hlZ0Ef8BR50UFwfBwUFBRYjfk3xPe3q5J4/Ah3Bf53u+v9e9aDQa6AR/EbKDxEg8hJiuR0Kf73Jxs1CSpEk/GXDo1/mk7osGxToJxPRwHxkkQ6RAimREmONhkpVgUs9F2oo4Aimz24/A6nfgTvN28RW49xa4xuuTZJyUSF7u0nIVaSdiLCefc4G/wEt38SC/gVf3gRXev1diRblKW0c+F/FHwOg/4EU7AZerwCKfK5PdEhpU6KJeF9+ANbvAH2DjMXClApzjtSWycgO4/gVYt/e8B+7y/GEJjSt0/c2whYg0c+G6uAmc4bmjZIEskwvkErnsuuHxKTJP9pDRTTceERPPnJI5RWZJVYsYJ3WyOs3EW4GfwBu0NlOVm12qxrQvXCntoqgY7ydzclPTYvXPwFMrsgHcRiucx8kBhcwrYpovQTKq/5LK1AgdNDtdAy7+Al5bgU/AM+OMnJXITCeRbT2iBBqhCbJvnblw82DcSOA8OU2OaENjCnnK1yd2lKQVNiM09oRl6gp8AB5o8bpcnFBYpxTqgWZUOoyVuEaKaa7iD+ChFXgH3NLCy/qvqYSnnVD1hZawpwjMjgqui/lWly+JmopiVvnbcmGi0sUUtkVQCPSErbJFJXtO/VGSi5R3rHhCltG0ndBubSkvKA8VJ0x53d9+QPpEAk5OiqoTphEVSqrjqO9S5JgcHTKlrQ4ftsnu9c0Y16DLB0QqmlMzIS6ivYrYWTbkzLKyRkdZyd65bep2+/r1zLKCwjKpxe1bsRB8UfUq4jZlTguWtPuSjnObjce+6OlrxRGJOh8UWYUur/+szifDvlS6EgnkJuF8FmWcz6FEWJgs/wHoNvy1hXthmgAAAABJRU5ErkJggg==', is_library:false};
}


function getLibraryMetadata(keyword)
{
   for(var j=0;j<libraries_metadata_array.length;j++)
   {
       if(libraries_metadata_array[j].name==keyword)
       {
           return libraries_metadata_array[j];
       }
   }
   
   return {keyword: keyword, category_name: '',category_desc: '', desc: 'no description', category_image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACs0lEQVR42pWWu2tTURzHv0PMu02btCFpUy22xUdTi7XRqKhptdpGqYoILkJHRwc3FRxcRAQRqoP/hlZ0Ef8BR50UFwfBwUFBRYjfk3xPe3q5J4/Ah3Bf53u+v9e9aDQa6AR/EbKDxEg8hJiuR0Kf73Jxs1CSpEk/GXDo1/mk7osGxToJxPRwHxkkQ6RAimREmONhkpVgUs9F2oo4Aimz24/A6nfgTvN28RW49xa4xuuTZJyUSF7u0nIVaSdiLCefc4G/wEt38SC/gVf3gRXev1diRblKW0c+F/FHwOg/4EU7AZerwCKfK5PdEhpU6KJeF9+ANbvAH2DjMXClApzjtSWycgO4/gVYt/e8B+7y/GEJjSt0/c2whYg0c+G6uAmc4bmjZIEskwvkErnsuuHxKTJP9pDRTTceERPPnJI5RWZJVYsYJ3WyOs3EW4GfwBu0NlOVm12qxrQvXCntoqgY7ydzclPTYvXPwFMrsgHcRiucx8kBhcwrYpovQTKq/5LK1AgdNDtdAy7+Al5bgU/AM+OMnJXITCeRbT2iBBqhCbJvnblw82DcSOA8OU2OaENjCnnK1yd2lKQVNiM09oRl6gp8AB5o8bpcnFBYpxTqgWZUOoyVuEaKaa7iD+ChFXgH3NLCy/qvqYSnnVD1hZawpwjMjgqui/lWly+JmopiVvnbcmGi0sUUtkVQCPSErbJFJXtO/VGSi5R3rHhCltG0ndBubSkvKA8VJ0x53d9+QPpEAk5OiqoTphEVSqrjqO9S5JgcHTKlrQ4ftsnu9c0Y16DLB0QqmlMzIS6ivYrYWTbkzLKyRkdZyd65bep2+/r1zLKCwjKpxe1bsRB8UfUq4jZlTguWtPuSjnObjce+6OlrxRGJOh8UWYUur/+szifDvlS6EgnkJuF8FmWcz6FEWJgs/wHoNvy1hXthmgAAAABJRU5ErkJggg==', is_library:true};
}

function getKeywordDatabase(lib)
{
    if(lib.length>0)
    {
        console.log('library: '+lib);
        for(var k=0;k<keywords_metadata_array.length;k++)
        {
            console.log('check '+keywords_metadata_array[k].keyword+' by '+lib);
            if(keywords_metadata_array[k].keyword==lib)
            {
                 console.log('found! ');
                return keywords_metadata_array[k].methods;
            }
        }
        return keywords_metadata_array;
    }
    else
    {
        console.log('not library '+lib);
        return keywords_metadata_array;
    }
}

function processStrongMatch(words,term)
{
    for(var i=0; i< words.length;i++)
    {
       if(words[i].keyword==term && words[i].is_library)
       {
           lib_selected=true;
           selected_lib_name=term;
           var terms=new Array();
           terms[0]=term;
           return terms;
       }
    }
    
    lib_selected=false;
    selected_lib_name='';
    
    return $.map(
                    words, 
                    function (word) 
                    {
                        //alert(word);
                        return word.keyword.indexOf(term) === 0 ? word : null;
                        //return word;
                    }
                 )
    //return null;
}

function createScriptTab(script_name, script_body)
{
    var new_tab=$("#script_tab_template").clone().appendTo("#nav_tabs");
    tab_count++;
    $(new_tab).children("a").attr("href","#tab"+tab_count);
    if(!script_name)
    {
//        $(new_tab).children("a").text("href","#tab"+tab_count);//
    }
    else
    {
        $(new_tab).children("a").text(script_name);
    }
    
    $(new_tab).attr("id","");
    $(new_tab).show();
    
    
    var new_tab_body=$("#script_tab_body_template").clone().appendTo("#nav_tabs_content");
    $(new_tab_body).attr("id","tab"+tab_count);
    $(new_tab_body).css("display","");
    //alert($(new_tab_body).children("div").children(".tab-content").children(".script-pane").children("div").children(".script-body").attr("class"));
    var textarea = $(new_tab_body).children("div").children(".tab-content").children(".script-pane").children("div").children(".script-body")[0];
    if(!script_body)
    {
        textarea.value = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    }
    else
    {
        textarea.value = script_body;
    }
    
    // start the decorator
    decorator = new TextareaDecorator( textarea, parser );
    decorators.push(decorator);
    $(textarea).textcomplete([
                                 { // tech companies
                                    words: keywords_array,
                                    match:/([\.]\w*)/,
                                        //\b(\w{2,})/, 
                                        //\b(\w{2,})\.|$/,
                                    search: function (term, callback) 
                                            {
                                                //alert(term+' '+selected_lib_name);
                                                var words=lib_selected?getKeywordDatabase(selected_lib_name):keywords_metadata_array;
                                                //alert(words.length);
                                                callback(
                                                            $.map(
                                                                    words, 
                                                                    function (word) 
                                                                    {
                                                                        //alert(word);
                                                                        var term_killed_dot=term.replace('.','');
                                                                        if(term_killed_dot.length>0)
                                                                            return word.keyword.indexOf(term_killed_dot) === 0 ? word : null;
                                                                        else
                                                                            return word;
                                                                    }
                                                                 )
                                                        );
                                            },
                                    template: function (value) 
                                    {
                                        var key_metadata=value;//getKeyWordMetadata(value);
                                        if(key_metadata.category_image != undefined)
                                            return '<img src="'+key_metadata.category_image+'"/><strong>' + value.keyword+'('+value.params+')' + '</strong>  - ' + key_metadata.category_desc+'<br>';
                                        else
                                            return '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACs0lEQVR42pWWu2tTURzHv0PMu02btCFpUy22xUdTi7XRqKhptdpGqYoILkJHRwc3FRxcRAQRqoP/hlZ0Ef8BR50UFwfBwUFBRYjfk3xPe3q5J4/Ah3Bf53u+v9e9aDQa6AR/EbKDxEg8hJiuR0Kf73Jxs1CSpEk/GXDo1/mk7osGxToJxPRwHxkkQ6RAimREmONhkpVgUs9F2oo4Aimz24/A6nfgTvN28RW49xa4xuuTZJyUSF7u0nIVaSdiLCefc4G/wEt38SC/gVf3gRXev1diRblKW0c+F/FHwOg/4EU7AZerwCKfK5PdEhpU6KJeF9+ANbvAH2DjMXClApzjtSWycgO4/gVYt/e8B+7y/GEJjSt0/c2whYg0c+G6uAmc4bmjZIEskwvkErnsuuHxKTJP9pDRTTceERPPnJI5RWZJVYsYJ3WyOs3EW4GfwBu0NlOVm12qxrQvXCntoqgY7ydzclPTYvXPwFMrsgHcRiucx8kBhcwrYpovQTKq/5LK1AgdNDtdAy7+Al5bgU/AM+OMnJXITCeRbT2iBBqhCbJvnblw82DcSOA8OU2OaENjCnnK1yd2lKQVNiM09oRl6gp8AB5o8bpcnFBYpxTqgWZUOoyVuEaKaa7iD+ChFXgH3NLCy/qvqYSnnVD1hZawpwjMjgqui/lWly+JmopiVvnbcmGi0sUUtkVQCPSErbJFJXtO/VGSi5R3rHhCltG0ndBubSkvKA8VJ0x53d9+QPpEAk5OiqoTphEVSqrjqO9S5JgcHTKlrQ4ftsnu9c0Y16DLB0QqmlMzIS6ivYrYWTbkzLKyRkdZyd65bep2+/r1zLKCwjKpxe1bsRB8UfUq4jZlTguWtPuSjnObjce+6OlrxRGJOh8UWYUur/+szifDvlS6EgnkJuF8FmWcz6FEWJgs/wHoNvy1hXthmgAAAABJRU5ErkJggg=="/><strong>' + value + '</strong>  - ' + key_metadata.category_desc+'<br>';
                                    },
                                    index: 1,
                                    replace: function (word) 
                                    {
                                        //var key_metadata=getKeyWordMetadata(word);
                                        //alert(word);
                                        /*if(key_metadata.is_library)
                                        {
                                            selected_lib_name=word;
                                            lib_selected=true;
                                        }*/
                                        setTimeout(updateDecorators, 200);
                                        return '.'+word.keyword+'('+word.params+')';
                                    }
                                },
                                { // tech companies
                                    words: keywords_array,
                                    match:/\b(\w{2,})/,
                                        //\b(\w{2,})/, 
                                        //\b(\w{2,})\.|$/,
                                    search: function (term, callback) 
                                            {
                                                var words=keywords_metadata_array;
                                                //alert(keywords_metadata_array.length);
                                                callback(
                                                            processStrongMatch(words,term)
                                                        );
                                            },
                                    template: function (value) 
                                    {
                                        var key_metadata=value;//getKeyWordMetadata(value);
                                        if(key_metadata.category_image != undefined)
                                            return '<img src="'+key_metadata.category_image+'"/><strong>' + value.keyword + '</strong>  - ' + key_metadata.category_desc+'<br>';
                                        else
                                            return '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACs0lEQVR42pWWu2tTURzHv0PMu02btCFpUy22xUdTi7XRqKhptdpGqYoILkJHRwc3FRxcRAQRqoP/hlZ0Ef8BR50UFwfBwUFBRYjfk3xPe3q5J4/Ah3Bf53u+v9e9aDQa6AR/EbKDxEg8hJiuR0Kf73Jxs1CSpEk/GXDo1/mk7osGxToJxPRwHxkkQ6RAimREmONhkpVgUs9F2oo4Aimz24/A6nfgTvN28RW49xa4xuuTZJyUSF7u0nIVaSdiLCefc4G/wEt38SC/gVf3gRXev1diRblKW0c+F/FHwOg/4EU7AZerwCKfK5PdEhpU6KJeF9+ANbvAH2DjMXClApzjtSWycgO4/gVYt/e8B+7y/GEJjSt0/c2whYg0c+G6uAmc4bmjZIEskwvkErnsuuHxKTJP9pDRTTceERPPnJI5RWZJVYsYJ3WyOs3EW4GfwBu0NlOVm12qxrQvXCntoqgY7ydzclPTYvXPwFMrsgHcRiucx8kBhcwrYpovQTKq/5LK1AgdNDtdAy7+Al5bgU/AM+OMnJXITCeRbT2iBBqhCbJvnblw82DcSOA8OU2OaENjCnnK1yd2lKQVNiM09oRl6gp8AB5o8bpcnFBYpxTqgWZUOoyVuEaKaa7iD+ChFXgH3NLCy/qvqYSnnVD1hZawpwjMjgqui/lWly+JmopiVvnbcmGi0sUUtkVQCPSErbJFJXtO/VGSi5R3rHhCltG0ndBubSkvKA8VJ0x53d9+QPpEAk5OiqoTphEVSqrjqO9S5JgcHTKlrQ4ftsnu9c0Y16DLB0QqmlMzIS6ivYrYWTbkzLKyRkdZyd65bep2+/r1zLKCwjKpxe1bsRB8UfUq4jZlTguWtPuSjnObjce+6OlrxRGJOh8UWYUur/+szifDvlS6EgnkJuF8FmWcz6FEWJgs/wHoNvy1hXthmgAAAABJRU5ErkJggg=="/><strong>' + value + '</strong>  - ' + key_metadata.category_desc+'<br>';
                                    },
                                    index: 1,
                                    replace: function (word) 
                                    {
                                        if(word.is_library)
                                        {
                                            lib_selected=true;
                                            selected_lib_name=word.keyword;
                                            //alert('library! '+selected_lib_name);
                                        }
                                        else
                                        {
                                            lib_selected=false;
                                            selected_lib_name='';
                                        }
                                        setTimeout(updateDecorators, 200);
                                        return word.keyword;
                                    }
                                }
                               
                                
                            ]);
}

function updateDecorators()
{
    for(var zzzzi=0;zzzzi<decorators.length;zzzzi++)
    {
        decorators[zzzzi].update();
    }
}

function toggleTabs(btn)
{
    var index=-1; 
    $.each($(btn).parent().children(),function( i, val ) 
    {
        if(!$(val).hasClass("btn-inverse"))
        {
            $(val).addClass("btn-inverse");
            index=i;
        }
        else
            $(val).removeClass("btn-inverse");
    });
    
    $.each($(btn).parent().parent().children(".tab-content").children(),function( i, val ) 
    {
        if(i==index)
            $(val).show();
        else
            $(val).hide();
    });
}

var parser = new Parser({
                            whitespace: /\s+/,
                            comment: /\/\*([^\*]|\*[^\/])*(\*\/?)?|(\/\/|#)[^\r\n]*/,
                            string: /"(\\.|[^"\r\n])*"?|'(\\.|[^'\r\n])*'?/,
                            number: /0x[\dA-Fa-f]+|-?(\d+\.?\d*|\.\d+)/,
                            keyword: /(and|as|case|catch|class|const|def|delete|die|do|else|elseif|esac|exit|extends|false|fi|finally|for|foreach|function|global|if|new|null|or|private|protected|public|published|resource|return|self|static|struct|switch|then|this|throw|true|try|var|void|while|xor)(?!\w|=)/,
                            variable: /[\$\%\@](\->|\w)+(?!\w)|\${\w*}?/,
                            define: /[$A-Z_a-z0-9]+/,
                            op: /[\+\-\*\/=<>!]=?|[\(\)\{\}\[\]\.\|]/,
                            other: /\S+/
                         }
                        );
                        
function  deserialize(obj) 
{
    var t = typeof (obj);
    if (t != "object" || obj === null) 
    {  // simple data type\n 
        if (t == "string") 
            obj = '"'+obj+'"';
        return String(obj);
    } 
    else 
    {  
        // recurse array or object 
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) 
        { 
            v = obj[n]; 
            t = typeof(v);  
            if (t == "string") 
                v = '"'+v+'"'; 
            else if (t == "object" && v !== null)
                v = deserialize(v);  
            json.push((arr ? "" : '"' + n + '":') + String(v)); 
        }  
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}"); 
    }
}
//window.hijs = '.script-body-formatted';