/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.file.core;

import ru.wnfx.scriptfighter.http.core.*;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.*;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.HttpsURLConnection;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.utils.Base64;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import ru.wnfx.scriptfighter.utils.StringUtils;


/**
 *
 * @author sergk
 */
public class FileInterface  extends Library
{
    private List<File> list_of_files;
    private List<File> list_of_dirs; 
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(Http.class);
    private String file_path;
    String name_filter;
    String file_encoding;
    String content_filter;
    String extentions;
    int file_limit;
    int file_offset;
    File dir;
    protected PropertyResourceBundle rb;
    boolean is_directory;
    
    private void init()
    {
        prefix=rb.getString("interface.prefix");
        desc="File API";
        file_path = rb.getString("file.path");
        name_filter = rb.getString("file.name_filter");
        content_filter = rb.getString("file.content_filter");
        file_encoding = rb.getString("file.encoding");
        try
        {
        file_limit = Integer.parseInt(rb.getString("file.limit"));
        file_offset = Integer.parseInt(rb.getString("file.offset"));
        }
        catch(Exception ex)
        {
            file_limit = 0;
            file_offset = 0;
        }
        extentions = rb.getString("file.extentions");
        
        dir = new File(file_path);
        is_directory = dir.isDirectory();
    }
    
    public FileInterface(CommonEngine g, PropertyResourceBundle rb)
    {
        this.rb=rb;
        init();
        list_of_files = new ArrayList();
        list_of_dirs = new ArrayList();
                
        if(g!=null)
        {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="", desc="gets file or directory")
    public Object load()
    {
        init();
        
        List<String> paths=new ArrayList();
        if(is_directory)
        {
            File[] files = dir.listFiles(new LimitedFilenameFilter(name_filter, extentions, content_filter, file_encoding, file_limit, file_offset));
            for(File f:files)
            {
                if(f.isFile())
                {
                    list_of_files.add(f);
                }
                else
                {
                    list_of_dirs.add(f);
                }
            }
            
        }
        else
        {
            list_of_files.add(dir);
        }
        
        for(File f:list_of_files)
        {
            paths.add(f.getAbsolutePath());
        }
        
        return ObjectUtils.listToNativeArray(paths);
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="", desc="gets file or directory")
    public FileInterface prepare()
    {
        init();
        
        if(is_directory)
        {
            File[] files = dir.listFiles(new LimitedFilenameFilter(name_filter, extentions, content_filter, file_encoding, file_limit, file_offset));
            for(File f:files)
            {
                if(f.isFile())
                {
                    list_of_files.add(f);
                }
                else
                {
                    list_of_dirs.add(f);
                }
            }
            
        }
        else
        {
            list_of_files.add(dir);
        }
        
        
        return this;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String extention", desc="gets file or directory")
    public String getRandomFile(String extention)
    {
        String result=null;
        if(is_directory)
        {
            String filename=file_path+System.getProperty("file.separator")+java.util.UUID.randomUUID().toString().replace("-", "");
            if(extention!=null)
            {
                if(extention.length()>0)
                {
                    filename+="."+extention.replace(".", "");
                }
            }
            result=filename;
            file_path=filename;
        }
        return result;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String extention", desc="gets file or directory")
    public FileInterface asRandomFile(String extention)
    {
        if(is_directory)
        {
            String filename=file_path+System.getProperty("file.separator")+java.util.UUID.randomUUID().toString().replace("-", "");
            if(extention!=null)
            {
                if(extention.length()>0)
                {
                    filename+="."+extention.replace(".", "");
                }
            }
            file_path=filename;
            is_directory=(new File(file_path)).isDirectory();
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String extention", desc="gets file or directory")
    public FileInterface asRandomFileSuffix(String extention, String suffix)
    {
        if(is_directory)
        {
            String filename=file_path+System.getProperty("file.separator")+java.util.UUID.randomUUID().toString().replace("-", "");
            if(extention!=null)
            {
                if(extention.length()>0)
                {
                    filename+=suffix+"."+extention.replace(".", "");
                }
            }
            file_path=filename;
            is_directory=(new File(file_path)).isDirectory();
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String extention", desc="gets file or directory")
    public FileInterface asFile(String fname)
    {
        if(is_directory)
        {
            String filename=file_path+System.getProperty("file.separator")+fname;

            file_path=filename;
            File f = new File(file_path);
            f.getParentFile().mkdirs(); 
            try {
                f.createNewFile();
            } catch (IOException ex) {
                LOG.error("Error creating file: "+file_path+", caused by: "+ex.getMessage());
            }
            is_directory=false;//(new File(file_path)).isDirectory();
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String extention", desc="gets file or directory")
    public FileInterface asAbsoluteFile(String fname)
    {
        file_path=fname;
        is_directory = false;        
        return this;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String content", desc="gets file or directory")
    public FileInterface putFileBase64(String content)
    {
        if(!is_directory)
        {
            byte dearr[] = Base64.decodeBytes(content);
            File file = new File(file_path);
            try
            {
                FileOutputStream fos = new FileOutputStream(file); 
                fos.write(dearr); 
                fos.close();
            }
            catch(Exception ex)
            {
                LOG.error("Error saving file: "+file_path+", caused by: "+ex.getMessage());
            }
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String content", desc="gets file or directory")
    public FileInterface putFileString(String content)
    {
        if(!is_directory)
        {
            File file = new File(file_path);
            try
            {
                FileOutputStream fos = new FileOutputStream(file); 
                fos.write(content.getBytes()); 
                fos.close();
            }
            catch(Exception ex)
            {
                LOG.error("Error saving file: "+file_path+", caused by: "+ex.getMessage());
            }
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String content", desc="gets file or directory")
    public FileInterface putCSV(Object content)
    {
        if(!is_directory)
        {
            File file = new File(file_path);
            try
            {
                FileOutputStream fos = new FileOutputStream(file); 
                fos.write(StringUtils.listToCSV((List<Map<?,?>>)ObjectUtils.convert(content)).getBytes("Cp1251")); 
                fos.close();
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
                LOG.error("Error saving file: "+file_path+", caused by: "+ex.getMessage());
            }
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String content", desc="gets file or directory")
    public Object getCSV()
    {
        List<Map<String, Object>> content = new ArrayList();
        if(!is_directory)
        {
            File file = new File(file_path);
            try
            {
                int count = 0;
                
                try
                {
                    BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "Cp1251"));
                    String line = "";
                    String str = br.readLine();
                    String [] keys = str.split(";");
                    while ((line = br.readLine()) != null) {
                        Map<String, Object> tmp = new HashMap();
                        String[] values = line.split(";");
                        for(int i=0; i<keys.length;i++)
                        {
                            if(i<values.length)
                            {
                                tmp.put(keys[i], values[i]);
                            }
                        }
                        content.add(tmp);
                    }
                    
                } catch (Exception e) {
                  //Some error logging
                    LOG.error("Error saving file: "+file_path+", caused by: "+e.getMessage());
                }
                return ObjectUtils.convertNative(content);
            }
            catch(Exception ex)
            {
                LOG.error("Error saving file: "+file_path+", caused by: "+ex.getMessage());
            }
        }
        
        return content;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String content", desc="gets file or directory")
    public String getFileName()
    {
        if(!is_directory)
        {
            File file = new File(file_path);
            return file.getName();
        }
        
        return null;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String content", desc="gets file or directory")
    public String getFullFileName()
    {
        if(!is_directory)
        {
            File file = new File(file_path);
            try
            {
                return file.getCanonicalFile().getCanonicalPath();
            }
            catch(Exception Ex)
            {
                LOG.error("Error getting file name: "+file_path+", caused by: "+Ex.getMessage());
            }
        }
        
        return null;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String content", desc="gets file or directory")
    public String getPath()
    {
        File file = new File(file_path);
        try
        {
            return file.getCanonicalFile().getCanonicalPath();
        }
        catch(Exception Ex)
        {
            LOG.error("Error getting file name: "+file_path+", caused by: "+Ex.getMessage());
        }
        
        return null;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String content", desc="gets file or directory")
    public FileInterface get(String filename)
    {
        if(is_directory)
        {
            for(File f:list_of_files)
            {
                if(f.getName().equalsIgnoreCase(filename))
                {
                    file_path += System.getProperty("file.separator")+f.getName();
                    break;
                }
            }
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="", desc="gets file or directory")
    public String getFileBase64()
    {
        String encoded=null;
        if(!is_directory)
        {
            File file = new File(file_path);
            byte[] bytes=null;
            try {
                bytes = loadFile(file);
            } catch (IOException ex) {
                Logger.getLogger(FileInterface.class.getName()).log(Level.SEVERE, null, ex);
            }
            encoded = Base64.encode(bytes);
        }
        else
        {
            File file = new File(file_path);
            if(file.isDirectory())
                return null;
            else
            {
                byte[] bytes=null;
                try {
                    bytes = loadFile(file);
                } catch (IOException ex) {
                    Logger.getLogger(FileInterface.class.getName()).log(Level.SEVERE, null, ex);
                }
                encoded = Base64.encode(bytes);
            }
        }
        
        return encoded;
    }
    
    
    private static byte[] loadFile(File file) throws IOException 
    {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
               && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }
    
}
