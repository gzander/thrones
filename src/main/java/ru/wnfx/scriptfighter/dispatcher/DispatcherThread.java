/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.dispatcher;

/**
 *
 * @author sergk
 */
import ru.wnfx.scriptfighter.engine.core.ThreadsHolder;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import java.io.*;
import java.nio.*;
import java.net.*;
import java.util.Arrays;
//import java.util.concurrent.ConcurrentLinkedQueue;
//import java.util.concurrent.ArrayBlockingQueue;
import com.google.gson.*;
import com.google.gson.stream.*;
import org.apache.log4j.Logger;
import ru.wnfx.scriptfighter.utils.MyBase64;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.UUID.*;
import java.util.ArrayList;
import ru.wnfx.scriptfighter.engine.core.manager.EnginePool;
import ru.wnfx.scriptfighter.engine.messaging.Event;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;


/**
 *
 * @author sergk
 */
public class DispatcherThread implements Runnable 
{
    public static final Logger LOG=Logger.getLogger(DispatcherThread.class);

     private volatile boolean stopped;
     private volatile boolean paused;
     //public LimitedMessageQueue events_queue;
     public volatile ArrayBlockingQueue events_queue;
     //ArrayList<ScriptThread> threads;
     
     public DispatcherThread() 
     {
         Thread t = new Thread(this);
         EnginePool ep=EnginePool.initInstance();
         this.events_queue = ep.events_queue;
         t.start();
         
         //this.threads=threads;
     }

    @Override
     public void run() 
     {
         try 
         {
             for(;;)
             {
                 Event e=(Event)events_queue.take();
                 if(e != null)
                 {
                     for(Scriptable sc: ThreadsHolder.getInstance().getThreads())
                     {
                        /*if(sc.incoming_events_queue.size()>64)//setting
                        {
                            Event e_deleted=(Event)sc.incoming_events_queue.poll();
                            e_deleted=null;
                            //sc.g.log("Events queue overflow, "+e_deleted.toString()+" was deleted");
                        }*/
                        sc.incoming_events_queue.offer(e);
                        //sc.g.log("Event received: "+e.toString());
                     }
                 }
             }
         } 
         catch (Exception e) 
         {
             LOG.error(e.getMessage());
             //g.error("Ошибка на верхнем уровне потока скрипта: "+e.getMessage());
         } 
     }
}