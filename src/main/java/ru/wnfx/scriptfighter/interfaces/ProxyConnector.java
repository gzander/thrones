/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import org.littleshoot.proxy.HttpFilters;
import org.littleshoot.proxy.HttpFiltersAdapter;
import org.littleshoot.proxy.HttpFiltersSourceAdapter;
import org.littleshoot.proxy.HttpProxyServer;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;
import ru.wnfx.scriptfighter.engine.core.BasicScript;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.messaging.AlertMessage;
import ru.wnfx.scriptfighter.engine.messaging.Event;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;

import javax.script.ScriptException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

//import java.util.concurrent.ConcurrentLinkedQueue;


/**
 *
 * @author sergk
 */
public class ProxyConnector extends Connector
{

    @Override
    public void autoGenerate(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10) {
        message_queue.add(new AlertMessage("Automatic method generaion for interface type: "+type+" not supported yet"));
    }
    
    public class HttpPropertyFilter
    {
        public String property_name;
        public String property_value;
    }
    
    public class HttpFilter
    {
        public String name;
        public boolean request;
        public boolean response;
        public String method;
        public String host;
        public String response_code;
        public boolean param;
        public String request_body;
        public String response_body;
        public ArrayList<HttpPropertyFilter> request_properties;
        public ArrayList<HttpPropertyFilter> response_properties;
        public ArrayList<HttpPropertyFilter> request_params;
        
        public HttpFilter()
        {
            name="";
            request=false;
            response=false;
            method="";
            host="";
            response_code="";
            param=false;
            request_body="";
            response_body="";
        }
        
        public boolean compare(String filter_condition, String source)
        {
            boolean result=false;
            String condition_without_escape=filter_condition.replace("%", "").toLowerCase();
            
            if(filter_condition!=null)
            {
                if(filter_condition.startsWith("%")&&!filter_condition.endsWith("%"))
                {
                    result=source.toLowerCase().endsWith(condition_without_escape);
                }
                else if(!filter_condition.startsWith("%")&&filter_condition.endsWith("%"))
                {
                    result=source.toLowerCase().startsWith(condition_without_escape);
                }
                else if(filter_condition.startsWith("%")&&filter_condition.endsWith("%"))
                {
                    result=source.toLowerCase().contains(condition_without_escape);
                }
                else
                {
                    result=source.toLowerCase().equalsIgnoreCase(condition_without_escape);
                }
            }
            
            return result;
        }
        
        
        public boolean ApplyFilter(HttpObject httpObject)
        {
            boolean result=false;
            if (httpObject instanceof HttpRequest && request) 
            {
                 HttpRequest httpRequest = (HttpRequest) httpObject;
                 result=compare(method, httpRequest.getMethod().name())||compare(host, httpRequest.getUri());
                 
                 if(!result)
                 {
                    for(HttpPropertyFilter hf: request_properties)
                    {
                        for(Entry<String,String> e: httpRequest.headers().entries())
                        {
                            if(e.getKey().equalsIgnoreCase(hf.property_name))
                            {
                                result=result?true:compare(hf.property_value, e.getValue());
                            }
                        }
                    }
                 }
            }
            else if(httpObject instanceof HttpResponse && response)
            {
                HttpResponse httpResponse = (HttpResponse) httpObject;
                result=compare(response_code, String.valueOf(httpResponse.getStatus().code()));
            }
            
            return result;
        }
    }
    
    
    private ArrayList<HttpFilter> filters;
    private String auth_secret;
    private String url_mask;
    public String URL;
    private PropertyResourceBundle rb;
    private Map<String, String> request_properties;
    private String request_method;
    public String port;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(WebConnector.class);
    
    public ProxyConnector(String propFileName, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, ArrayBlockingQueue event_queue)
    {
        super(propFileName, message_queue, commands_queue, event_queue);
        request_properties=new HashMap<String, String>();
        
        filters = new ArrayList();
        try
        {
            FileInputStream fis = new FileInputStream(propFileName);
            rb= new PropertyResourceBundle(fis);
        }
        catch(Exception ex)
        {
            throw new UnsupportedOperationException(ex.getMessage());
        }
        
        String[] filter_names=rb.getString("proxy.filters").split(",");
        for(int i=0;i<filter_names.length;i++)
        {
            HttpFilter hf=new HttpFilter();
            hf.name=filter_names[i];
            hf.request=hf.response=false;
            for(String ks: rb.keySet())
            {
                if(ks.contains("proxy."+filter_names[i]+".request"))
                {
                    hf.request=true;
                    if(ks.contains("proxy."+filter_names[i]+".request.property"))
                    {
                        if(hf.request_properties==null)
                        {
                            hf.request_properties = new ArrayList();
                        }
                        HttpPropertyFilter hpf = new HttpPropertyFilter();
                        hpf.property_name=ks.replace("proxy."+filter_names[i]+".request.property.", "");
                        hpf.property_value=rb.getString(ks);
                        hf.request_properties.add(hpf);
                    }
                    else if (ks.contains("proxy."+filter_names[i]+".request.param"))
                    {
                        if(hf.request_params==null)
                        {
                            hf.request_params = new ArrayList();
                        }
                        HttpPropertyFilter hpf = new HttpPropertyFilter();
                        hpf.property_name=ks.replace("proxy."+filter_names[i]+".request.param.", rb.getString(ks));
                        hf.request_params.add(hpf);
                    }
                    else if (ks.contains("proxy."+filter_names[i]+".request.body"))
                    {
                        hf.request_body=rb.getString(ks);
                    }
                }
                else if(ks.contains("proxy."+filter_names[i]+".response"))
                {
                    hf.response=true;
                    if (ks.contains("proxy."+filter_names[i]+".response.body"))
                    {
                        hf.response_body=rb.getString(ks);
                    }
                }
            }
            
            if(hf.request)
            {
                hf.method = rb.getString("proxy."+filter_names[i]+".request.method");
                hf.host = rb.getString("proxy."+filter_names[i]+".request.host");
            }
            
            if(hf.response)
            {
                hf.response_code = rb.getString("proxy."+filter_names[i]+".response.code");
            }
            
            
            filters.add(hf);
        }
        
        port=rb.getString("proxy.port");
        
        if(!type.equalsIgnoreCase("proxy")) 
        {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        
        prepareFilter();
    }
    
    public String getUserParam(String param_name)
    {
        for(Connectorparam p: user_defined_params)
        {
            if(p.name.equalsIgnoreCase(param_name))
                return p.value;
        }
        return "";
    }
    
    public String getScriptParam(String param_name)
    {
        for(Connectorparam p: script_defined_params)
        {
            if(p.name.equalsIgnoreCase(param_name))
                return p.value;
        }
        return "";
    }
    
    public final void prepareFilter()
    {
        if(port.contains("$PARAM$"))
        {
            Connectorparam p= new Connectorparam();
            p.name = "proxy.port";
            p.desc = "Порт, на котором будет ожидать proxy";
            script_defined_params.add(p);
        }
        else if(port.contains("$USER_INPUT$"))
        {
            Connectorparam p= new Connectorparam();
            p.name = "proxy.port";
            p.desc = "Порт, на котором будет ожидать proxy";
            script_defined_params.add(p);
        }
        
        for(HttpFilter f: filters)
        {
            if(f.host.contains("$PARAM$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = "proxy."+f.name+"request.host";
                p.desc = "Параметр";
                script_defined_params.add(p);
            }
            else if(f.host.contains("$USER_INPUT$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = "proxy."+f.name+"request.host";
                p.desc = "Параметр";
                user_defined_params.add(p);
            }
            
            if(f.method.contains("$PARAM$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = "proxy."+f.name+"request.method";
                p.desc = "Параметр";
                script_defined_params.add(p);
            }
            else if(f.method.contains("$USER_INPUT$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = "proxy."+f.name+"request.method";
                p.desc = "Параметр";
                user_defined_params.add(p);
            }
            
            if(f.response_code.contains("$PARAM$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = "proxy."+f.name+"response.code";
                p.desc = "Параметр";
                script_defined_params.add(p);
            }
            else if(f.response_code.contains("$USER_INPUT$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = "proxy."+f.name+"response.code";
                p.desc = "Параметр";
                user_defined_params.add(p);
            }
            
            if(f.request_body.contains("$PARAM$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = "proxy."+f.name+"request.body";
                p.desc = "Параметр";
                script_defined_params.add(p);
            }
            else if(f.request_body.contains("$USER_INPUT$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = "proxy."+f.name+"request.body";
                p.desc = "Параметр";
                user_defined_params.add(p);
            }
            
            if(f.response_body.contains("$PARAM$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = "proxy."+f.name+"response.body";
                p.desc = "Параметр";
                script_defined_params.add(p);
            }
            else if(f.response_body.contains("$USER_INPUT$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = "proxy."+f.name+"response.body";
                p.desc = "Параметр";
                user_defined_params.add(p);
            }
            
            if(f.request_properties !=null)
            {
                for(HttpPropertyFilter hf: f.request_properties)
                {
                    if(hf.property_value.contains("$PARAM$"))
                    {
                        Connectorparam p= new Connectorparam();
                        p.name = "proxy."+f.name+"request.property."+hf.property_name;
                        p.desc = "Параметр";
                        script_defined_params.add(p);
                    }
                    else if(hf.property_value.contains("$USER_INPUT$"))
                    {
                        Connectorparam p= new Connectorparam();
                        p.name = "proxy."+f.name+"request.property."+hf.property_name;
                        p.desc = "Параметр";
                        user_defined_params.add(p);
                    }
                }
            }
            
            if(f.request_params!=null)
            {
                for(HttpPropertyFilter hf: f.request_params)
                {
                    if(hf.property_value.contains("$PARAM$"))
                    {
                        Connectorparam p= new Connectorparam();
                        p.name = "proxy."+f.name+"request.param."+hf.property_name;
                        p.desc = "Параметр";
                        script_defined_params.add(p);
                    }
                    else if(hf.property_value.contains("$USER_INPUT$"))
                    {
                        Connectorparam p= new Connectorparam();
                        p.name = "proxy."+f.name+"request.param."+hf.property_name;
                        p.desc = "Параметр";
                        user_defined_params.add(p);
                    }
                }
            }
        }
    }
    
    public void fillFilter()
    {
        if(port.contains("$PARAM$"))
        {
            port=getScriptParam("proxy.port");
        }
        else if(port.contains("$USER_INPUT$"))
        {
            port=getUserParam("proxy.port");
        }
        
        for(HttpFilter f: filters)
        {
            if(f.host.contains("$PARAM$"))
            {
                f.host=getScriptParam("proxy."+f.name+"request.host");
            }
            else if(f.host.contains("$USER_INPUT$"))
            {
                f.host=getUserParam("proxy."+f.name+"request.host");
            }
            
            if(f.method.contains("$PARAM$"))
            {
                f.method=getScriptParam("proxy."+f.name+"request.method");
            }
            else if(f.method.contains("$USER_INPUT$"))
            {
                f.method=getUserParam("proxy."+f.name+"request.method");
            }
            
            if(f.response_code.contains("$PARAM$"))
            {
                f.response_code = getScriptParam("proxy."+f.name+"response.code");
            }
            else if(f.response_code.contains("$USER_INPUT$"))
            {
                f.response_code = getUserParam("proxy."+f.name+"response.code");
            }
            
            if(f.request_body.contains("$PARAM$"))
            {
                f.request_body = getScriptParam("proxy."+f.name+"request.body");
            }
            else if(f.request_body.contains("$USER_INPUT$"))
            {
                f.request_body = getUserParam("proxy."+f.name+"request.body");
            }
            
            if(f.response_body.contains("$PARAM$"))
            {
                f.response_body = getScriptParam("proxy."+f.name+"response.body");
            }
            else if(f.response_body.contains("$USER_INPUT$"))
            {
                f.response_body = getUserParam("proxy."+f.name+"response.body");
            }
            
            if(f.request_properties != null)
            {
                for(HttpPropertyFilter hf: f.request_properties)
                {
                    if(hf.property_value.contains("$PARAM$"))
                    {
                        hf.property_value = getScriptParam("proxy."+f.name+"request.property."+hf.property_name);
                    }
                    else if(hf.property_value.contains("$USER_INPUT$"))
                    {
                        hf.property_value = getUserParam("proxy."+f.name+"request.property."+hf.property_name);
                    }
                }
            }
            
            if(f.request_params != null)
            {
                for(HttpPropertyFilter hf: f.request_params)
                {
                    if(hf.property_value.contains("$PARAM$"))
                    {
                        hf.property_value = getScriptParam("proxy."+f.name+"request.param."+hf.property_name);
                    }
                    else if(hf.property_value.contains("$USER_INPUT$"))
                    {
                        hf.property_value = getUserParam("proxy."+f.name+"request.param."+hf.property_name);
                    }
                }
            }
        }
    }
    
    @Override
    public String getInvokeWrap()
    {
        String invoke_builder="";
        String inner_invoke_builder="";
        int i=0;
        for(Connectorparam p: script_defined_params)
        {
            i++;
            invoke_builder+=p.name.replace("-", "_").replace(".", "_") +(i==script_defined_params.size()?"":", ");
            inner_invoke_builder+=p.name.replace("-", "_").replace(".", "_")+(i==script_defined_params.size()?"":", ");

        }

        return "function invoke"+prefix+"("+invoke_builder+"){return eval('('+"+prefix+".invoke("+inner_invoke_builder+")+')');}";
    }
    
    @Override
    public String getGenerateWrap()
    {
        return null;
    }
    
    @Override
    public void registerMethods(CommonEngine jsEngine) 
    {
        try 
        {
            String source=getInvokeWrap();
            
            BasicScript bs=new BasicScript(source, prefix+" Proxy interface", true, false, "interfaces");
                   
            methods.add(bs);
            
            if(jsEngine!=null)
            {
                jsEngine.put(prefix, this);
                jsEngine.eval(source);
            }
            
            //additional web functions
        } 
        catch (ScriptException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void registerEvents()
    {
        events.add(new Event(prefix+"BeforeWebConnectorUserParamsRequested","Event of "+prefix+" proxy interface. Event triggers before user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"AfterWebConnectorUserParamsRequested","Event of "+prefix+" proxy interface. Event triggers after user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"ProxyEventPassesFilterThrough","Event of "+prefix+" proxy interface. Event triggers after HttpObject passed through filter. Attached object is a HttpObject"));
        events.add(new Event(prefix+"ProxyEventDoesNotPassesFilterThrough","Event of "+prefix+" proxy interface. Event triggers after HttpObject is blocked by filter. Attached object is a blocked HttpObject"));
    }
    
    private void setScriptDefinedParams(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10)
    {
        if(script_defined_params.size()>0){script_defined_params.get(0).value=param1;}
        if(script_defined_params.size()>1){script_defined_params.get(1).value=param2;}
        if(script_defined_params.size()>2){script_defined_params.get(2).value=param3;}
        if(script_defined_params.size()>3){script_defined_params.get(3).value=param4;}
        if(script_defined_params.size()>4){script_defined_params.get(4).value=param5;}
        if(script_defined_params.size()>5){script_defined_params.get(5).value=param6;}
        if(script_defined_params.size()>6){script_defined_params.get(6).value=param7;}
        if(script_defined_params.size()>7){script_defined_params.get(7).value=param8;}
        if(script_defined_params.size()>8){script_defined_params.get(8).value=param9;}
        if(script_defined_params.size()>9){script_defined_params.get(9).value=param10;}
    }
    
    public void invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9)
    {
        invoke(param1, param2, param3, param4, param5, param6, param7, param8, param9, "");
    }
    
    public void invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8)
    {
        invoke(param1, param2, param3, param4, param5, param6, param7, param8, "","");
    }
    
    public void invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7)
    {
        invoke(param1, param2, param3, param4, param5, param6, param7, "", "","");
    }
    
    public void invoke(String param1, String param2, String param3, String param4, String param5, String param6)
    {
        invoke(param1, param2, param3, param4, param5, param6, "", "", "","");
    }
    
    public void invoke(String param1, String param2, String param3, String param4, String param5)
    {
        invoke(param1, param2, param3, param4, param5, "", "", "", "","");
    }
    
    public void invoke(String param1, String param2, String param3, String param4)
    {
        invoke(param1, param2, param3, param4, "", "", "", "", "","");
    }
    
    public void invoke(String param1, String param2, String param3)
    {
        invoke(param1, param2, param3, "", "", "", "", "", "","");
    }
    
    public void invoke(String param1, String param2)
    {
        invoke(param1, param2, "", "", "", "", "", "", "","");
    }
    
    public void invoke(String param1)
    {
        invoke(param1, "", "", "", "", "", "", "", "","");
    }
    
    public void invoke()
    {
        invoke("", "", "", "", "", "", "", "", "","");
    }
    
    public void invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10)
    {
        //prepareFilter();
        // PUT ON ALL OF SCRIPT DEFINED PARAMS
        if(user_defined_params.size()>0)
        {
            Gson gson = new GsonBuilder().create();
            event_queue.add(new Event(prefix+"BeforeWebConnectorUserParamsRequested",null,gson.toJson(user_defined_params)));
            ask_users_params();
            event_queue.add(new Event(prefix+"AfterWebConnectorUserParamsRequested",null,gson.toJson(user_defined_params)));
        }
        setScriptDefinedParams(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10);
        fillFilter();
        
        final AtomicInteger shouldFilterCalls = new AtomicInteger(0);
        final AtomicInteger filterResponseCalls = new AtomicInteger(0);
        final AtomicInteger fullHttpRequestsReceived = new AtomicInteger(0);
        final AtomicInteger fullHttpResponsesReceived = new AtomicInteger(0);
        
        HttpProxyServer srv = DefaultHttpProxyServer.bootstrap().withPort(Integer.parseInt(port))
        .withFiltersSource(new HttpFiltersSourceAdapter() 
        {
            @Override
            public HttpFilters filterRequest(HttpRequest originalRequest, ChannelHandlerContext ctx) 
            {
               return new HttpFiltersAdapter(originalRequest) 
               {
                  @Override
                  public HttpResponse clientToProxyRequest(HttpObject httpObject) 
                  {
                      // TODO: implement your filtering here
                      fullHttpRequestsReceived.incrementAndGet();

                      return null;
                  }

                  @Override
                  public HttpResponse proxyToServerRequest(HttpObject httpObject) 
                  {
                      // TODO: implement your filtering here
                      return null;
                  }

                  @Override
                  public HttpObject serverToProxyResponse(HttpObject httpObject) 
                  {
                      // TODO: implement your filtering here
                      for(HttpFilter f: filters)
                      {
                          if(f.ApplyFilter(httpObject))
                          {
                              Gson gson = new GsonBuilder().create();
                              StringWriter sw=new StringWriter();
                              JsonWriter jw=new JsonWriter(sw);
                              try
                              {
                                if (httpObject instanceof HttpRequest)
                                {
                                    HttpRequest hr=(HttpRequest)httpObject;
                                    event_queue.add(new Event(prefix+"ProxyEventPassesFilterThrough",null,gson.toJson(hr.headers())));
                                }
                                else if (httpObject instanceof HttpResponse)
                                {
                                    jw.beginObject();
                                    jw.name("headers").beginArray();
                                    HttpResponse hr=(HttpResponse)httpObject;
                                    HashMap<String, Object> m=new HashMap();
                                    Set<String> headers= hr.headers().names();
                                    for(String h: headers)
                                    {
                                        jw.beginObject();
                                        Object o=hr.headers().get(h);
                                        if(o!=null)
                                        {
                                            if(o instanceof String)
                                            {
                                                gson.toJson((String)o, String.class, jw.name(h));
                                            }
                                            else if(o instanceof Integer)
                                            {
                                                gson.toJson((String)o, String.class, jw.name(h));
                                            }
                                            else if(o instanceof Float)
                                            {
                                                gson.toJson((String)o, String.class, jw.name(h));
                                            }
                                            else if(o instanceof Double)
                                            {
                                                gson.toJson((String)o, String.class, jw.name(h));
                                            }
                                            else if(o instanceof Boolean)
                                            {
                                                gson.toJson((String)o, String.class, jw.name(h));
                                            }
                                            else
                                            {
                                                gson.toJson(o.toString(), String.class, jw.name(h));
                                            }
                                        }
                                        jw.endObject();
                                    }
                                    jw.endArray();
                                    jw.name("status").beginObject();
                                    gson.toJson(Integer.valueOf(hr.getStatus().code()), Integer.class, jw.name("code"));
                                    gson.toJson(hr.getStatus().reasonPhrase(), String.class, jw.name("desc"));
                                    jw.endObject();
                                    jw.endObject();
                                                                    
                                    event_queue.add(new Event(prefix+"ProxyEventPassesFilterThrough",null,sw.toString()));
                                    jw.flush();
                                    jw.close();
                                    sw.flush();
                                    sw.close();
                                    
                                }

                              }
                              catch(IOException ie)
                              {
                                  LOG.error(ie.getMessage());
                              }
                                  
                              
                          }
                      }
                      
                      return httpObject;
                  }

                  @Override
                  public HttpObject proxyToClientResponse(HttpObject httpObject) 
                  {
                      // TODO: implement your filtering here
                      for(HttpFilter f: filters)
                      {
                          if(f.ApplyFilter(httpObject))
                          {
                              Gson gson = new GsonBuilder().create();
                              StringWriter sw=new StringWriter();
                              JsonWriter jw=new JsonWriter(sw);
                              try
                              {
                                    if (httpObject instanceof HttpRequest)
                                    {
                                        HttpRequest hr=(HttpRequest)httpObject;
                                        event_queue.add(new Event(prefix+"ProxyEventPassesFilterThrough",null,gson.toJson(hr.headers())));
                                    }
                                    else if (httpObject instanceof HttpResponse)
                                    {
                                        /*jw.beginObject();
                                        jw.name("headers").beginArray();
                                        HttpResponse hr=(HttpResponse)httpObject;
                                        //HashMap<String, Object> m=new HashMap();
                                        //m.put("headers", hr.headers());
                                        //m.put("status", hr.getStatus());
                                        //event_queue.add(new Event(prefix+"ProxyEventPassesFilterThrough",null,gson.toJson(m)));
                                          HashMap<String, Object> m=new HashMap();
                                          Set<String> headers= hr.headers().names();
                                          for(String h: headers)
                                          {
                                              jw.beginObject();
                                              Object o=m.get(h);

                                              if(o instanceof String)
                                              {
                                                  gson.toJson((String)o, String.class, jw.name(h));
                                              }
                                              else if(o instanceof Integer)
                                              {
                                                  gson.toJson((String)o, String.class, jw.name(h));
                                              }
                                              else if(o instanceof Float)
                                              {
                                                  gson.toJson((String)o, String.class, jw.name(h));
                                              }
                                              else if(o instanceof Double)
                                              {
                                                  gson.toJson((String)o, String.class, jw.name(h));
                                              }
                                              else if(o instanceof Boolean)
                                              {
                                                  gson.toJson((String)o, String.class, jw.name(h));
                                              }
                                              else
                                              {
                                                  gson.toJson(o.toString(), String.class, jw.name(h));
                                              }
                                              jw.endObject();
                                          }
                                          jw.endArray();
                                          jw.name("status").beginObject();
                                          gson.toJson(Integer.valueOf(hr.getStatus().code()), Integer.class, jw.name("code"));
                                          gson.toJson(hr.getStatus().reasonPhrase(), String.class, jw.name("desc"));
                                          jw.endObject();
                                          jw.endObject();

                                          event_queue.add(new Event(prefix+"ProxyEventPassesFilterThrough",null,sw.toString()));
                                          jw.close();
                                          jw.flush();
                                          sw.close();
                                          sw.flush();*/
                                        jw.beginObject();
                                    jw.name("headers").beginArray();
                                    HttpResponse hr=(HttpResponse)httpObject;
                                    HashMap<String, Object> m=new HashMap();
                                    Set<String> headers= hr.headers().names();
                                    for(String h: headers)
                                    {
                                        jw.beginObject();
                                        Object o=hr.headers().get(h);
                                        if(o!=null)
                                        {
                                            if(o instanceof String)
                                            {
                                                gson.toJson((String)o, String.class, jw.name(h));
                                            }
                                            else if(o instanceof Integer)
                                            {
                                                gson.toJson((String)o, String.class, jw.name(h));
                                            }
                                            else if(o instanceof Float)
                                            {
                                                gson.toJson((String)o, String.class, jw.name(h));
                                            }
                                            else if(o instanceof Double)
                                            {
                                                gson.toJson((String)o, String.class, jw.name(h));
                                            }
                                            else if(o instanceof Boolean)
                                            {
                                                gson.toJson((String)o, String.class, jw.name(h));
                                            }
                                            else
                                            {
                                                gson.toJson(o.toString(), String.class, jw.name(h));
                                            }
                                        }
                                        jw.endObject();
                                    }
                                    jw.endArray();
                                    jw.name("status").beginObject();
                                    gson.toJson(Integer.valueOf(hr.getStatus().code()), Integer.class, jw.name("code"));
                                    gson.toJson(hr.getStatus().reasonPhrase(), String.class, jw.name("desc"));
                                    jw.endObject();
                                    jw.endObject();
                                                                    
                                    event_queue.add(new Event(prefix+"ProxyEventPassesFilterThrough",null,sw.toString()));
                                    jw.flush();
                                    jw.close();
                                    sw.flush();
                                    sw.close();
                                    }
                              }
                              catch(IOException ie)
                              {
                                  LOG.error(ie.getMessage());
                              }
                          }
                      }
                      
                      return httpObject;
                  }   
               };
            }
        })
        .start();
    }
    
}
