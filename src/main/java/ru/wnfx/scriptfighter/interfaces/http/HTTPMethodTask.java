/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces.http;

import java.util.ArrayList;

/**
 *
 * @author sergk
 */
public class HTTPMethodTask 
{
    String function;
    String method;
    String request_body;
    ArrayList<HTTPHeader> headers;
    
    public HTTPMethodTask(String method, String task, String function){this.function=function; request_body=task; this.method=method;}
    public ArrayList<HTTPHeader> getHeaders(){return headers;}
    public void addHeader(HTTPHeader h){headers.add(h);}
}
