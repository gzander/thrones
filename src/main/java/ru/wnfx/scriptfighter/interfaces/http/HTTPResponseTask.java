/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces.http;

import java.util.ArrayList;

/**
 *
 * @author sergk
 */
public class HTTPResponseTask 
{
    String function;
    String response_body;
    String method;
    ArrayList<HTTPHeader> headers;
    
    public HTTPResponseTask(){headers=new ArrayList();}
    public HTTPResponseTask(String method, String task, String function){headers=new ArrayList(); this.function=function; response_body=task; this.method=method;}
    
    public ArrayList<HTTPHeader> getHeaders(){return headers;}
    public void addHeader(HTTPHeader h){headers.add(h);}
}
