/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces.http;

import thrones.*;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
/**
 *
 * @author sergk
 */
public class RestfulFilter extends Filter {

    private ArrayBlockingQueue requests_queue;
    private String function_name;
    private String method;
    
    public RestfulFilter(ArrayBlockingQueue requests_queue, String function_name, String method)
    {
        this.requests_queue = requests_queue;
        this.function_name=function_name;
        this.method=method;
    }
    
    @Override
    public String description() {
        return "Parses the requested URI for parameters";
    }

    @Override
    public void doFilter(HttpExchange exchange, Chain chain)
        throws IOException 
    {
        parseGetParameters(exchange);
        parsePostParameters(exchange);
        chain.doFilter(exchange);
    }    

    private void parseGetParameters(HttpExchange exchange)
        throws UnsupportedEncodingException 
    {
        if ("get".equalsIgnoreCase(exchange.getRequestMethod())) 
        {
            Map<String, Object> parameters = new HashMap<String, Object>();
            URI requestedUri = exchange.getRequestURI();
            String query = requestedUri.getRawQuery();
            //need for check for valid JSON
            
            if(query!=null) 
            {
                if(URLDecoder.decode(query,"UTF-8").equalsIgnoreCase("help"))
                {
                    exchange.setAttribute("help", "true");
                    exchange.setAttribute("function", function_name);
                }
                else if(URLDecoder.decode(query,"UTF-8").equalsIgnoreCase("clear"))
                {
                    exchange.setAttribute("clear", "true");
                    exchange.setAttribute("function", function_name);
                }
                else
                {
                    //need for check for valid JSON
                    requests_queue.offer(new HTTPMethodTask(method, URLDecoder.decode(query,"UTF-8"), function_name));
                }
                
            }
            else
            {
                requests_queue.offer(new HTTPMethodTask(method,"",function_name));
            }
        }
        //parseQuery(query, parameters);
        //exchange.setAttribute("parameters", parameters);
    }

    private void parsePostParameters(HttpExchange exchange)
        throws IOException 
    {

        if ("post".equalsIgnoreCase(exchange.getRequestMethod())) 
        {
            @SuppressWarnings("unchecked")
            Map<String, Object> parameters =
                (Map<String, Object>)exchange.getAttribute("parameters");
            InputStreamReader isr =
                new InputStreamReader(exchange.getRequestBody(),"utf-8");
            BufferedReader br = new BufferedReader(isr);
            String result="";
            
            for(String tmp_result=br.readLine();tmp_result!=null;tmp_result=br.readLine())
            {
                result+=tmp_result;
            }
            if(result.equalsIgnoreCase("help"))
            {
                exchange.setAttribute("help", "true");
                exchange.setAttribute("function", function_name);
            }
            else
            {
                //need for check for valid JSON
                requests_queue.offer(new HTTPMethodTask(method, result, function_name));
            }
        }
    }

     @SuppressWarnings("unchecked")
     private void parseQuery(String query, Map<String, Object> parameters)
         throws UnsupportedEncodingException {

         if (query != null) {
             String pairs[] = query.split("[&]");

             for (String pair : pairs) {
                 String param[] = pair.split("[=]");

                 String key = null;
                 String value = null;
                 if (param.length > 0) {
                     key = URLDecoder.decode(param[0],
                         System.getProperty("file.encoding"));
                 }

                 if (param.length > 1) {
                     value = URLDecoder.decode(param[1],
                         System.getProperty("file.encoding"));
                 }

                 if (parameters.containsKey(key)) {
                     Object obj = parameters.get(key);
                     if(obj instanceof List<?>) {
                         List<String> values = (List<String>)obj;
                         values.add(value);
                     } else if(obj instanceof String) {
                         List<String> values = new ArrayList<String>();
                         values.add((String)obj);
                         values.add(value);
                         parameters.put(key, values);
                     }
                 } else {
                     parameters.put(key, value);
                 }
             }
         }
    }
}
