/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces.http;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import ru.wnfx.scriptfighter.interfaces.BasicInterface;
import ru.wnfx.scriptfighter.interfaces.Connectorparam;
import ru.wnfx.scriptfighter.interfaces.Listener;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
//import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import ru.wnfx.scriptfighter.engine.messaging.DebugCommand;
import ru.wnfx.scriptfighter.engine.messaging.EngineMessage;
import ru.wnfx.scriptfighter.engine.core.ScriptThread;
import ru.wnfx.scriptfighter.engine.messaging.Event;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.engine.messaging.RequestMessage;
import ru.wnfx.scriptfighter.utils.Base64;
import ru.wnfx.scriptfighter.utils.MyBase64;
import ru.wnfx.scriptfighter.engine.core.BasicFunctions;
import ru.wnfx.scriptfighter.engine.core.BasicScript;
import ru.wnfx.scriptfighter.engine.core.LibraryHolder;
import ru.wnfx.scriptfighter.engine.core.Middleware;
import ru.wnfx.scriptfighter.engine.core.ScriptCategory;
import ru.wnfx.scriptfighter.engine.core.ThreadsHolder;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.core.manager.EnginePool;
import ru.wnfx.scriptfighter.engine.core.manager.SunRhinoEngine;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import sun.org.mozilla.javascript.internal.Undefined;
import thrones.MasterPage;
import thrones.ParameterFilter;
import thrones.Thrones;

/**
 *
 * @author sergk
 */
public class HTTPListener extends Listener implements HttpHandler
{
    private static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(HTTPListener.class);
    private int port;
    private String name;
    private ArrayList<HttpContext> contexts;
    private volatile ArrayBlockingQueue<HTTPMethodTask> requests_queue;
    private volatile ArrayBlockingQueue<HTTPResponseTask> response_queue;
    private HttpContext main_context;
    HttpServer server;
    
    public HTTPListener(Map<String,Object> mappings, ArrayBlockingQueue events_queue, LimitedMessageQueue incoming_events_queue, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, int port, Scriptable parent)
    {
        requests_queue=new ArrayBlockingQueue(1000);
        response_queue=new ArrayBlockingQueue(1000);
        contexts = new ArrayList();
        //this.event_name = event_name;
        //this.function_name = function_name;
        this.events_queue = events_queue;
        this.incoming_events_queue = incoming_events_queue;
        this.message_queue = message_queue;
        this.commands_queue = commands_queue;
        this.parent = parent;
        this.port=port;
        
        server=null;
        try 
        {
            server = HttpServer.create(new InetSocketAddress(port), 10);
        } 
        catch (IOException ex) 
        {
            LOG.error("Error starting listener with name: "+name+"... "+ex.getMessage());
        }
        
        HttpHandler mainhandler=new HttpHandler(){
                                                @Override public void handle(    HttpExchange exchange) throws IOException 
                                                {
                                                    Gson gson=new Gson();
                                                    StringWriter sw=new StringWriter();
                                                    JsonWriter jw=new JsonWriter(sw);
                                                    jw.beginObject();
                                                    gson.toJson(Integer.valueOf(100/*get load factor*/), Integer.class, jw.name("load_factor"));
                                                    jw.name("methods").beginArray();
                                                    for(HttpContext c:contexts)
                                                    {
                                                        jw.beginObject();
                                                        gson.toJson(c.getPath().replace("/", ""), String.class, jw.name("name"));
                                                        gson.toJson(c.getAttributes().get("cacheable"), Boolean.class, jw.name("cacheable"));
                                                        gson.toJson(c.getAttributes().get("clear_cache"), Boolean.class, jw.name("clear_cache"));
                                                        Object cache_id=c.getAttributes().get("cache_id");
                                                        if(cache_id instanceof String)
                                                        {
                                                            gson.toJson(c.getAttributes().get("cache_id"), String.class, jw.name("cache_id"));
                                                        }
                                                        else if(cache_id instanceof List)
                                                        {
                                                            jw.name("cache_id").beginArray();
                                                            for(String s: (List<String>)cache_id)
                                                            {
                                                                jw.value(s);
                                                            }
                                                            jw.endArray();
                                                        }
                                                        gson.toJson(c.getAttributes().get("impacts"), Boolean.class, jw.name("impacts"));
                                                        jw.endObject();
                                                    }
                                                    jw.endArray();
                                                    jw.endObject();
                                                    String response=sw.toString();
                                                    jw.flush();
                                                    sw.flush();
                                                    jw.close();
                                                    sw.close();
                                                    exchange.sendResponseHeaders(200,response.length());
                                                    exchange.getResponseBody().write(response.getBytes("UTF-8"));
                                                    exchange.close();
                                                }
                                              };
        
        main_context = server.createContext("/", mainhandler);
        //main_context.getFilters().add(new RestfulFilter(requests_queue, ""));
        
        for(String webmethod: mappings.keySet())
        {
            HttpContext context = server.createContext("/"+webmethod, this);
            Object mappin= ObjectUtils.convert(mappings.get(webmethod));
            if(mappin instanceof String)
            {
                context.getAttributes().put("cacheable", Boolean.FALSE);
                context.getAttributes().put("clear_cache", Boolean.FALSE);
                context.getFilters().add(new RestfulFilter(requests_queue, (String)mappin, webmethod));
            }
            else if(mappin instanceof Map)
            {
                Object handler=((Map)mappin).get("handler");
                if(handler instanceof Undefined)
                {
                    LOG.error("Adding listener for webmethod "+webmethod+"... No handler specified!");
                }
                else if(handler instanceof String)
                {
                    Object cach=((Map)mappin).get("cacheable");
                    if(cach instanceof Undefined)
                    {
                        LOG.error("Adding listener for webmethod "+webmethod+"... No caching policy specified!");
                    }
                    else if(cach instanceof Boolean)
                    {
                        boolean cache_politics=(Boolean)cach;
                        Object cach_id=ObjectUtils.convert(((Map)mappin).get("cache_id"));
                        if(cach_id instanceof Undefined)
                        {
                            LOG.error("Adding listener for webmethod "+webmethod+"... No cache_id specified!");
                        }
                        else if(cach_id instanceof String || cach_id instanceof List)
                        {
                            Object cache_id= cach_id;//(cach_id instanceof String)?cach_id:ObjectUtils.getListFromNative((NativeArray)cach_id);
                            Object cach_impacts=((Map)mappin).get("impacts");
                            if(cach_impacts instanceof Undefined)
                            {
                                LOG.error("Adding listener for webmethod "+webmethod+"... No cache impact specified!");
                            }
                            else if(cach_impacts instanceof Boolean)
                            {
                                Boolean cache_impacts=(Boolean)cach_impacts;
                                context.getAttributes().put("clear_cache", Boolean.FALSE);
                                context.getAttributes().put("cacheable", cache_politics);
                                context.getAttributes().put("impacts", cache_impacts);
                                context.getAttributes().put("cache_id", cache_id);
                                context.getFilters().add(new RestfulFilter(requests_queue, (String)handler, webmethod));
                            }
                            else
                            {
                                LOG.error("Adding listener for webmethod "+webmethod+"... Wrong impacts datatype");
                            }
                        }
                        else
                        {
                            LOG.error("Adding listener for webmethod "+webmethod+"... Wrong cache_id datatype");
                        }
                    }
                    else
                    {
                        LOG.error("Adding listener for webmethod "+webmethod+"... Wrong cacheable datatype");
                    }
                }
                else
                {
                    LOG.error("Adding listener for webmethod "+webmethod+"... Wrong handler datatype");
                }
            }
            
            contexts.add(context);
        }
        server.start();
    }
    
    public HTTPListener(String event_name, String function_name, ArrayBlockingQueue events_queue, LimitedMessageQueue incoming_events_queue, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, int port, Scriptable parent)
    {
        requests_queue=new ArrayBlockingQueue(1000);
        response_queue=new ArrayBlockingQueue(1000);
        contexts = new ArrayList();
        this.event_name = event_name;
        this.function_name = function_name;
        this.events_queue = events_queue;
        this.incoming_events_queue = incoming_events_queue;
        this.message_queue = message_queue;
        this.commands_queue = commands_queue;
        this.parent = parent;
        this.port=port;
        
        server=null;
        try 
        {
            server = HttpServer.create(new InetSocketAddress(port), 10);
        } 
        catch (IOException ex) 
        {
            LOG.error("Error starting listener with name: "+name+"... "+ex.getMessage());
        }
        
        HttpHandler mainhandler=new HttpHandler(){
                                                @Override public void handle(    HttpExchange exchange) throws IOException 
                                                {
                                                    Gson gson=new Gson();
                                                    StringWriter sw=new StringWriter();
                                                    JsonWriter jw=new JsonWriter(sw);
                                                    jw.beginObject();
                                                    gson.toJson(Integer.valueOf(100/*get load factor*/), Integer.class, jw.name("load_factor"));
                                                    jw.name("methods").beginArray();
                                                    for(HttpContext c:contexts)
                                                    {
                                                        jw.value(c.getPath().replace("/", ""));
                                                    }
                                                    jw.endArray();
                                                    jw.endObject();
                                                    String response=sw.toString();
                                                    jw.flush();
                                                    sw.flush();
                                                    jw.close();
                                                    sw.close();
                                                    exchange.sendResponseHeaders(200,response.length());
                                                    exchange.getResponseBody().write(response.getBytes("UTF-8"));
                                                    exchange.close();
                                                }
                                              };
        
        main_context = server.createContext("/", mainhandler);
        //main_context.getFilters().add(new RestfulFilter(requests_queue, ""));
        
        HttpContext context = server.createContext("/"+event_name, this);
        context.getFilters().add(new RestfulFilter(requests_queue, function_name, event_name));
        contexts.add(context);
        server.start();
    }
    
    @Override
    public void registerMethods(CommonEngine jsEngine) 
    {

    }
    
    @Override
    public void registerEvents()
    {
        /*events.add(new Event(prefix+"BeforeWebConnectorUserParamsRequested","Event of "+prefix+" proxy interface. Event triggers before user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"AfterWebConnectorUserParamsRequested","Event of "+prefix+" proxy interface. Event triggers after user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"ProxyEventPassesFilterThrough","Event of "+prefix+" proxy interface. Event triggers after HttpObject passed through filter. Attached object is a HttpObject"));
        events.add(new Event(prefix+"ProxyEventDoesNotPassesFilterThrough","Event of "+prefix+" proxy interface. Event triggers after HttpObject is blocked by filter. Attached object is a blocked HttpObject"));*/
    }
    
    
    @Override
    public void handle(HttpExchange exc) throws IOException 
    {
      String path=exc.getHttpContext().getPath();
      LOG.info("Context called: "+path);
      String is_help=(String)exc.getAttribute("help");
      String is_clear=(String)exc.getAttribute("clear");
      exc.getResponseHeaders().set("Content-Type", "text/html; charset=UTF-8");
      exc.sendResponseHeaders(200, 0);
      
      PrintWriter out = new PrintWriter(exc.getResponseBody());
      if(is_help!=null)
      {
          String func=(String)exc.getAttribute("function");
          BasicScript bs=LibraryHolder.getInstance().getScript(func);
          String script_body=bs==null?"":bs.getSource();
          out.println(script_body);
          exc.setAttribute("help", null);
      }
      else if(is_clear!=null)
      {
          //String cache_id=null;//=(String)exc.getAttribute("cache_id");
          //String func=(String)exc.getAttribute(path.replace("/", ""));
          /*for(HttpContext c:contexts)
          {
              if(c.getPath().replace("/", "").equalsIgnoreCase(func))
              {
                  cache_id=(String)c.getAttributes().get("cache_id");
                  break;
              }
          }
          if(cache_id!=null)
          {
              for(HttpContext c:contexts)
              {
                  if(((String)c.getAttributes().get("cache_id")).equalsIgnoreCase(cache_id))
                  {
                      c.getAttributes().put("clear_cache", true);
                  }
              }
              
          }
          else
          {
              
          }*/
          if(applyCachePolitics(path.replace("/", ""), false))
          {
              out.println("ok");
          }
          else
          {
              out.println("no linked cache");
          }
          exc.setAttribute("clear", null);
      }
      else
      {
        HTTPResponseTask e=null;
        try 
        {
            e = (HTTPResponseTask)response_queue.take();
            applyCachePolitics(e.method, true);
        } 
        catch (InterruptedException ex) 
        {
            LOG.error(ex.getMessage());
        }

        out.println(e.response_body);
      }
      out.close();
      exc.close();
    }
    
    private boolean applyCachePolitics(String func, boolean take_impact_attention)
    {
        Object cache_id=null;//=(String)exc.getAttribute("cache_id");
        //List<String> cache_id_list=null;//=(String)exc.getAttribute("cache_id");
        Boolean impacts=false;
        Boolean clear_cache=false;
        for(HttpContext c:contexts)
        {
            if(c.getPath().replace("/", "").equalsIgnoreCase(func))
            {
                if(take_impact_attention)
                {
                    impacts = (Boolean)c.getAttributes().get("impacts");
                    if(impacts!=null)
                    {
                        if(impacts)
                        {
                            cache_id = c.getAttributes().get("cache_id");
                        }
                    }
                }
                else
                {
                    cache_id = c.getAttributes().get("cache_id");
                }
                
                Object o = c.getAttributes().get("clear_cache");
                
                if(o!=null)
                {
                    if(o instanceof Boolean)
                    {
                        if(((Boolean)o))
                        {
                            c.getAttributes().put("clear_cache", false);
                        }
                    }
                }
                break;
            }
        }
        if(cache_id!=null)
        {
            for(HttpContext c:contexts)
            {
                Object c_cache_id=c.getAttributes().get("cache_id");
                if(c_cache_id!=null)
                {
                    if(c_cache_id instanceof String)
                    {
                        if(cache_id instanceof String)
                        {
                            if(((String)c_cache_id).equalsIgnoreCase((String)cache_id))
                            {
                                c.getAttributes().put("clear_cache", true);
                            }
                        }
                        else if(cache_id instanceof List)
                        {
                            for(String c_c_cache_id:(List<String>)cache_id)
                            {
                                if(c_c_cache_id.equalsIgnoreCase((String)c_cache_id))
                                {
                                    c.getAttributes().put("clear_cache", true);
                                }
                            }
                        }
                    }
                    else if(c_cache_id instanceof List)
                    {
                        if(cache_id instanceof String)
                        {
                            for(String c_c_cache_id:(List<String>)c_cache_id)
                            {
                                if(c_c_cache_id.equalsIgnoreCase((String)cache_id))
                                {
                                    c.getAttributes().put("clear_cache", true);
                                }
                            }
                        }
                        else if(cache_id instanceof List)
                        {
                            for(String c_c_cache_id:(List<String>)c_cache_id)
                            {
                                for(String c_c_c_cache_id:(List<String>)cache_id)
                                {
                                    if(c_c_cache_id.equalsIgnoreCase(c_c_c_cache_id))
                                    {
                                        c.getAttributes().put("clear_cache", true);
                                    }
                                }
                                /*if(c_c_cache_id.equalsIgnoreCase((String)cache_id))
                                {
                                    c.getAttributes().put("clear_cache", true);
                                }*/
                            }
                        }
                    }
                    
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    
    @Override
    public void listen()
    {
        Gson gson=new Gson();
        ThreadsHolder.getInstance().addThread(this);
        LOG.info("1");
        for(;!stop;)
        {
            LOG.info("2");
            try 
            {
                HTTPMethodTask e = (HTTPMethodTask)requests_queue.take();
                LOG.info("3");
                if(e!=null)
                {
                    LOG.info("4");
                    if(e.function!=null)
                    {
                        LOG.info("5");
                        jsEngine=EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
                        LOG.info("6");
                        this.status="processing_event";
                        String script_body=/*Middleware.getScript(e.function)+*/";\n $("+e.function+"("+e.request_body+"));\n";
                        LOG.info("7");
                        Object o=jsEngine.runScript(script_body, 0, java.util.UUID.randomUUID().toString().replace("-", ""));
                        LOG.info("8");
                        if(o instanceof String) 
                        {
                            response_queue.add(new HTTPResponseTask(e.method,(String)o, e.function));
                        }
                        else 
                        {
                            response_queue.add(new HTTPResponseTask(e.method,gson.toJson(o),e.function));
                        }
                        LOG.info("9");
                        
                        this.status="listening";
                    }
                }
                else if(e.function==null&&e.method==null&&e.request_body==null)
                {
                    break;
                }
            } 
            catch (Exception ex) 
            {
                LOG.error(ex.getMessage());
                break;
            }
        }
        server.stop(0);
        //server.
        ThreadsHolder.getInstance().removeThread(this);
    }
    
    @Override
    public void stop() 
    {
        LOG.info("Command STOP received for HTTP listener ID: "+id);
        stop=true;
        //server.stop(0);
        HTTPMethodTask e = new HTTPMethodTask(null, null, null);
        requests_queue.add(e);
        
    }
    
}
