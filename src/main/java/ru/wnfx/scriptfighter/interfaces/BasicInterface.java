/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces;

import java.util.ArrayList;
import javax.script.ScriptEngine;
import ru.wnfx.scriptfighter.engine.messaging.Event;
import ru.wnfx.scriptfighter.engine.core.BasicScript;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;

/**
 *
 * @author sergk
 */
public abstract class BasicInterface 
{
    public ArrayList<BasicScript> methods;
    public ArrayList<Event> events;
    public abstract void registerMethods(CommonEngine jsEngine);
    public abstract void registerEvents();
}
