/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces;

import ru.wnfx.scriptfighter.engine.messaging.DebugCommand;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.script.ScriptEngine;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.wnfx.scriptfighter.engine.messaging.RequestMessage;
//import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 *
 * @author sergk
 */
public abstract class Connector extends BasicInterface
{
    public String prefix;
    public String desc;
    public String type;
    public String auth;
    public String file;
    
    public ArrayList<Connectorparam> user_defined_params;
    public ArrayList<Connectorparam> script_defined_params;
    public ArrayList<Connectorparam> config_defined_params;
    public ArrayList<Connectorparam> all_of_params;
    //public ArrayList<BasicScript> methods;
    //public ArrayList<Event> events;
    public LimitedMessageQueue message_queue;
    public ArrayBlockingQueue commands_queue;
    public ArrayBlockingQueue event_queue;
    protected PropertyResourceBundle rb;
    
    public Connector()
    {
        user_defined_params=new ArrayList();
        script_defined_params=new ArrayList();
        config_defined_params=new ArrayList();
        all_of_params=new ArrayList();
        this.methods=new ArrayList();
        this.events=new ArrayList();
    }
    
    public Connector(String propFileName, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, ArrayBlockingQueue event_queue)
    {
        this();
        file=propFileName;
        try
        {
            FileInputStream fis = new FileInputStream(propFileName);
            rb= new PropertyResourceBundle(fis);
        }
        catch(Exception ex)
        {
            throw new UnsupportedOperationException(ex.getMessage());
        }
        
        for(String key: rb.keySet())
        {
            Connectorparam p=new Connectorparam();
            p.name=key;
            p.value=rb.getString(key);
            
            all_of_params.add(p);
            
            if(!rb.getString(key).equalsIgnoreCase("$PARAM$")&&!rb.getString(key).equalsIgnoreCase("$USER_INPUT$"))
            {
                
                config_defined_params.add(p);
            }
        }
        
        prefix=rb.getString("interface.prefix");
        desc=rb.getString("interface.desc");
        type=rb.getString("interface.type");
        auth=rb.getString("interface.auth");
                 
        this.message_queue=message_queue;
        this.commands_queue=commands_queue;
        this.event_queue=event_queue;
    }
    
    public abstract String getInvokeWrap();
    public abstract String getGenerateWrap();
    
    public abstract void autoGenerate(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10);
    
    
    public void ask_users_params()
    {
        Gson gson=new Gson();
        StringWriter sw=new StringWriter();
        JsonWriter jw=new JsonWriter(sw);
        String result="";
        String filled_user_params="";
        
        boolean need_to_ask=false;
        
        try 
        {
            jw.beginObject();
            jw.name("params").beginArray();
            for(Connectorparam p: user_defined_params)
            {
                    jw.beginObject();
                    gson.toJson(p.name, String.class, jw.name("name"));
                    gson.toJson(p.desc, String.class, jw.name("desc"));
                    gson.toJson(p.type, Integer.class, jw.name("type"));
                    gson.toJson(p.value, String.class, jw.name("value"));
                    gson.toJson(p.remember, Boolean.class, jw.name("remember"));
                    jw.endObject();
                    if(!p.remember)
                    {
                        need_to_ask=true;
                    }
            }
            jw.endArray();
            jw.endObject();
            result=sw.toString();
            jw.flush();
            sw.flush();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        // если не все параметры стоят "запомнить"
        if(need_to_ask)
        {
            //
            message_queue.offer(new RequestMessage(result)); // need to send params
            //
            try 
            {
                DebugCommand dc=(DebugCommand)commands_queue.take();
                if(dc.command.equalsIgnoreCase("SETPARAMS"))
                {
                    filled_user_params=dc.params;
                    Map<String, Object> RootMapObject = new Gson().fromJson(filled_user_params, Map.class);
                    List<Map> params_array=(List)RootMapObject.get("params");

                    for (Map<String, Object> par: params_array)
                    {
                        //System.out.println(entry.getKey() + "/" + entry.getValue());
                        for(Connectorparam p: user_defined_params)
                        {
                            if(p.name.equalsIgnoreCase((String)par.get("name")))
                            {
                                p.value = (String)par.get("value");
                                p.remember=(Boolean)par.get("remember");
                            }
                        }
                    }
                }
            } 
            catch (InterruptedException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    /*public String getUserParam(String name)
    {        
        for(Connectorparam p: user_defined_params)
        {
            if(p.name.equalsIgnoreCase(name))
            {
                return p.value;
            }
        }
        
        for(Connectorparam p: script_defined_params)
        {
            if(p.name.equalsIgnoreCase(name))
            {
                return p.value;
            }
        }
        
        return "";
    }*/
    
    public void requestUserParam(String name1, String name2, String name3, String name4, String name5, String name6, String name7, String name8, String name9, String name10, String name11)
    {   
        String complex_params=name1+";"+name2+";"+name3+";"+name4+";"+name5+";"+name6+";"+name7+";"+name8+";"+name9+";"+name10+";"+name11;
        String[] list_params=complex_params.split(";");
        
        for(String s:list_params)
        {
            boolean already_exists = false;
            if(s.length()>0)
            {
                for(Connectorparam p: user_defined_params)
                {
                    if(p.name.equalsIgnoreCase(s))
                    {
                        already_exists = true;
                        break;
                    }
                }
                
                if(!already_exists)
                {
                    Connectorparam new_param=new Connectorparam();
                    new_param.name = s;
                    user_defined_params.add(new_param);
                }
            }
        }
        
        Gson gson=new Gson();
        StringWriter sw=new StringWriter();
        JsonWriter jw=new JsonWriter(sw);
        String result="";
        String filled_user_params="";
        
        
        boolean need_to_ask=false;
        
        try 
        {
            jw.beginObject();
            jw.name("params").beginArray();
            for(Connectorparam p: user_defined_params)
            {
                    jw.beginObject();
                    gson.toJson(p.name, String.class, jw.name("name"));
                    gson.toJson(p.desc, String.class, jw.name("desc"));
                    gson.toJson(p.type, Integer.class, jw.name("type"));
                    gson.toJson(p.value, String.class, jw.name("value"));
                    gson.toJson(p.remember, Boolean.class, jw.name("remember"));
                    jw.endObject();
                    if(!p.remember)
                    {
                        need_to_ask=true;
                    }
            }
            jw.endArray();
            jw.endObject();
            result=sw.toString();
            jw.flush();
            sw.flush();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        //
        message_queue.offer(new RequestMessage(result)); // need to send params
        //
        try 
        {
            DebugCommand dc=(DebugCommand)commands_queue.take();
            if(dc.command.equalsIgnoreCase("SETPARAMS"))
            {
                filled_user_params=dc.params;
                Map<String, Object> RootMapObject = new Gson().fromJson(filled_user_params, Map.class);
                List<Map> params_array=(List)RootMapObject.get("params");

                for (Map<String, Object> par: params_array)
                {
                    //System.out.println(entry.getKey() + "/" + entry.getValue());
                    for(Connectorparam p: user_defined_params)
                    {
                        if(p.name.equalsIgnoreCase((String)par.get("name")))
                        {
                            p.value = (String)par.get("value");
                            p.remember=(Boolean)par.get("remember");
                        }
                    }
                }
            }
        } 
        catch (InterruptedException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    public void requestUserParam(String name1, String name2, String name3, String name4, String name5, String name6, String name7, String name8, String name9, String name10)
    {
        requestUserParam(name1, name2, name3, name4, name5, name6, name7, name8, name9, name10,"");
    }
    
    public void requestUserParam(String name1, String name2, String name3, String name4, String name5, String name6, String name7, String name8, String name9)
    {
        requestUserParam(name1, name2, name3, name4, name5, name6, name7, name8, name9, "","");
    }
    
    public void requestUserParam(String name1, String name2, String name3, String name4, String name5, String name6, String name7, String name8)
    {
        requestUserParam(name1, name2, name3, name4, name5, name6, name7, name8, "", "","");
    }
    
    public void requestUserParam(String name1, String name2, String name3, String name4, String name5, String name6, String name7)
    {
        requestUserParam(name1, name2, name3, name4, name5, name6, name7, "", "", "","");
    }
    
    public void requestUserParam(String name1, String name2, String name3, String name4, String name5, String name6)
    {
        requestUserParam(name1, name2, name3, name4, name5, name6, "", "", "", "","");
    }
    
    public void requestUserParam(String name1, String name2, String name3, String name4, String name5)
    {
        requestUserParam(name1, name2, name3, name4, name5, "", "", "", "", "","");
    }
    
    public void requestUserParam(String name1, String name2, String name3, String name4)
    {
        requestUserParam(name1, name2, name3, name4, "", "", "", "", "", "","");
    }
    
    public void requestUserParam(String name1, String name2, String name3)
    {
        requestUserParam(name1, name2, name3, "", "", "", "", "", "", "","");
    }
    
    public void requestUserParam(String name1, String name2)
    {
        requestUserParam(name1, name2, "", "", "", "", "", "", "", "","");
    }
        
    public void requestUserParam(String name1)
    {
        requestUserParam(name1, "", "", "", "", "", "", "", "", "","");
    }
    
    /*private void registerBasicMethods()
    {
        BasicScript bs=new BasicScript();
        bs.source = "function invoke"+prefix+"("+invoke_builder+"){return eval('('+"+prefix+".invoke("+inner_invoke_builder+")+')');}";
        bs.is_common = true;
        bs.desc = prefix+" Web interface";
        LOG.info(bs.source);

        methods.add(bs);

        if(jsEngine!=null)
        {
            jsEngine.put(prefix, this);
            jsEngine.eval("function invoke"+prefix+"("+invoke_builder+"){return eval('('+"+prefix+".invoke("+inner_invoke_builder+")+')');}");
        }
    }*/
    
}
