/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces;

import ru.wnfx.scriptfighter.utils.db_helpers.SPHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import ru.wnfx.scriptfighter.engine.messaging.ConsoleMessage;
//import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.zip.GZIPInputStream;
import ru.wnfx.scriptfighter.engine.messaging.RequestMessage;
import java.io.FileInputStream;
import java.util.PropertyResourceBundle;
import java.util.ArrayList;
import ru.wnfx.scriptfighter.engine.core.BasicScript;
import com.google.gson.JsonSyntaxException;
import ru.wnfx.scriptfighter.engine.messaging.Event;
import ru.wnfx.scriptfighter.engine.core.LibraryHolder;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.engine.messaging.OpenScriptMessage;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import ru.wnfx.scriptfighter.utils.SourceUtils;
import ru.wnfx.scriptfighter.utils.StringUtils;
import sun.org.mozilla.javascript.internal.Undefined;
/**
 *
 * @author sergk
 */
public class DatabaseConnector extends Connector 
{
    private String connection_mask;
    private String connection_schema;
    private String connection_user;
    private String connection_password;
    public String CONNECTION;
    private String driver;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(DatabaseConnector.class);
    
    public DatabaseConnector(String propFileName, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, ArrayBlockingQueue event_queue)
    {
        super(propFileName, message_queue, commands_queue, event_queue);
        
        if(!type.equalsIgnoreCase("Database")) 
        {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        Init();
    }
    
    public void Init()
    {
        connection_mask = rb.getString("connection.mask");
        driver = rb.getString("connection.driver"); 
        connection_schema = rb.getString("connection.schema"); 
        connection_user = rb.getString("connection.user"); 
        connection_password = rb.getString("connection.password"); 
        
        prepareConnection();
    }
    
    @Override
    public String getInvokeWrap()
    {
        String invoke_builder="";
        String inner_invoke_builder="";
        for(Connectorparam p: script_defined_params)
        {
            invoke_builder+=p.name.replace("-", "_") +", ";
            inner_invoke_builder+=p.name.replace("-", "_")+", ";
        }
        invoke_builder+="stored_name, params";
        inner_invoke_builder+="stored_name, params";


        // register invoke
        return "function invoke"+prefix+"("+invoke_builder+"){return eval('('+"+prefix+".invoke("+inner_invoke_builder+")+')');}";
    }
    
    @Override
    public String getGenerateWrap()
    {
        String invoke_builder="";
        String inner_invoke_builder="";
        for(Connectorparam p: script_defined_params)
        {
            invoke_builder+=p.name.replace("-", "_") +", ";
            inner_invoke_builder+=p.name.replace("-", "_")+", ";
        }
        if(invoke_builder.lastIndexOf(", ")==invoke_builder.length()-2)
        {
           invoke_builder=invoke_builder.substring(0, invoke_builder.length()-2);
        }
        
        if(inner_invoke_builder.lastIndexOf(", ")==inner_invoke_builder.length()-2)
        {
           inner_invoke_builder=inner_invoke_builder.substring(0, inner_invoke_builder.length()-2);
        }
        
        return "function autogenerate_"+prefix+"("+invoke_builder+"){eval('('+"+prefix+".autoGenerate("+inner_invoke_builder+")+')');}";
    }
    
    public void autoGenerate(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9)
    {
        autoGenerate(param1, param2, param3, param4, param5, param6, param7, param8, param9,"");
    }
    
    public void autoGenerate(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8)
    {
        autoGenerate(param1, param2, param3, param4, param5, param6, param7, param8, "","");
    }
    
    public void autoGenerate(String param1, String param2, String param3, String param4, String param5, String param6, String param7)
    {
        autoGenerate(param1, param2, param3, param4, param5, param6, param7, "", "","");
    }
    
    public void autoGenerate(String param1, String param2, String param3, String param4, String param5, String param6)
    {
        autoGenerate(param1, param2, param3, param4, param5, param6, "", "", "","");
    }
        
    public void autoGenerate(String param1, String param2, String param3, String param4, String param5)
    {
        autoGenerate(param1, param2, param3, param4, param5, "", "", "", "","");
    }
            
    public void autoGenerate(String param1, String param2, String param3, String param4)
    {
        autoGenerate(param1, param2, param3, param4, "", "", "", "", "","");
    }
            
    public void autoGenerate(String param1, String param2, String param3)
    {
        autoGenerate(param1, param2, param3, "", "", "", "", "", "","");
    }
            
    public void autoGenerate(String param1, String param2)
    {
        autoGenerate(param1, param2, "", "", "", "", "", "", "","");
    }
            
    public void autoGenerate(String param1)
    {
        autoGenerate(param1, "", "", "", "", "", "", "", "","");
    }
    
    public void autoGenerate()
    {
        autoGenerate("", "", "", "", "", "", "", "", "","");
    }
    
    @Override
    public void autoGenerate(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10)
    {
        String classicalCONNECTION=formatConnection(param1, param2, param3, param4, param5, param6, param7,  param8,  param9,  param10);
        for(String s:SPHelper.generateMethods(driver, classicalCONNECTION, connection_user, connection_password, "invoke"+prefix, connection_schema))
        {
            LibraryHolder.getInstance().openScriptTab(s);
        }
    }
    
    @Override
    public void registerMethods(CommonEngine jsEngine) 
    {
        try 
        {
            String source = getInvokeWrap();
            
            BasicScript bs=new BasicScript(source, prefix+" Database interface", true, false, "interfaces");
            
            methods.add(bs);

            // register autogenerate
            String auto_source = getGenerateWrap();
            BasicScript auto_bs=new BasicScript(auto_source, prefix+"  auto generate Database interface methods", true, false, "interfaces");
            
            methods.add(auto_bs);
            
            if(jsEngine!=null)
            {
                jsEngine.put(prefix, this);
                jsEngine.eval(source);
                jsEngine.eval(auto_source);
            }
            //additional web functions
        } 
        catch (Exception ex) 
        {
            LOG.error("Error register methods: "+ex.getMessage());
        }
    }
    
    @Override
    public void registerEvents()
    {
        events.add(new Event(prefix+"BeforeDatabaseConnectorUserParamsRequested","Event of "+prefix+" web interface. Event triggers before user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"AfterDatabaseConnectorUserParamsRequested","Event of "+prefix+" web interface. Event triggers after user params is requested. Attached object is full list of params"));
    }
    
    public final void prepareConnection()
    {          
        String[] connection_parts=StringUtils.getURLParts(connection_mask); //connection_mask.replace("%","").split(":");
        CONNECTION=connection_mask;
        for(int i=0; i<connection_parts.length;i++)
        {
            //String classical_part=connection_parts[i].replace("/", "");
            //String pref=connection_parts[i].replace(classical_part,"");
            String connection_component=rb.getString("connection."+connection_parts[i]);
            if(connection_component.equalsIgnoreCase("$PARAM$"))
            {
                Connectorparam p= new Connectorparam();
                /*if(classical_part.contains("host"))
                {
                    p.prefix = "@";
                }*/
                p.name = connection_parts[i];
                p.desc = "Параметр";
                script_defined_params.add(p);
            }
            else if(connection_component.equalsIgnoreCase("$USER_INPUT$"))
            {
                Connectorparam p= new Connectorparam();
                /*if(classical_part.contains("host"))
                {
                    p.prefix = "@";
                }*/
                p.name = connection_parts[i];
                p.desc = "Параметр";
                user_defined_params.add(p);
            }
            else
            {
                CONNECTION=CONNECTION.replace("%"+connection_parts[i]+"%", rb.getString("connection."+connection_parts[i]));
            
            }
            
        }
        
        if(connection_schema.equalsIgnoreCase("$PARAM$"))
        {
            Connectorparam p= new Connectorparam();
            p.name = "connection_schema";
            p.desc = "Database schema used for invoking stored or DML";
            script_defined_params.add(p);
        }
        else if(connection_schema.equalsIgnoreCase("$USER_INPUT$"))
        {
            Connectorparam p= new Connectorparam();
            p.name = "connection_schema";
            p.desc = "Database schema used for invoking stored or DML";
            user_defined_params.add(p);
        }
        
        if(driver.equalsIgnoreCase("$PARAM$"))
        {
            Connectorparam p= new Connectorparam();
            p.name = "connection_driver";
            p.desc = "Database driver used for invoking stored or DML";
            script_defined_params.add(p);
        }
        else if(driver.equalsIgnoreCase("$USER_INPUT$"))
        {
            Connectorparam p= new Connectorparam();
            p.name = "connection_driver";
            p.desc = "Database driver used for invoking stored or DML";
            user_defined_params.add(p);
        }
        
        if(connection_user.equalsIgnoreCase("$PARAM$"))
        {
            Connectorparam p= new Connectorparam();
            p.name = "connection.user";
            p.desc = "Database user used for invoking stored or DML";
            script_defined_params.add(p);
        }
        else if(connection_user.equalsIgnoreCase("$USER_INPUT$"))
        {
            Connectorparam p= new Connectorparam();
            p.name = "connection.user";
            p.desc = "Database user used for invoking stored or DML";
            user_defined_params.add(p);
        }
        
        if(connection_password.equalsIgnoreCase("$PARAM$"))
        {
            Connectorparam p= new Connectorparam();
            p.name = "connection.password";
            p.desc = "Database password used for invoking stored or DML";
            script_defined_params.add(p);
        }
        else if(connection_password.equalsIgnoreCase("$USER_INPUT$"))
        {
            Connectorparam p= new Connectorparam();
            p.name = "connection.password";
            p.desc = "Database password used for invoking stored or DML";
            user_defined_params.add(p);
        }
    }
    
    private void setScriptDefinedParams(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10)
    {
        if(script_defined_params.size()>0){script_defined_params.get(0).value=param1;}
        if(script_defined_params.size()>1){script_defined_params.get(1).value=param2;}
        if(script_defined_params.size()>2){script_defined_params.get(2).value=param3;}
        if(script_defined_params.size()>3){script_defined_params.get(3).value=param4;}
        if(script_defined_params.size()>4){script_defined_params.get(4).value=param5;}
        if(script_defined_params.size()>5){script_defined_params.get(5).value=param6;}
        if(script_defined_params.size()>6){script_defined_params.get(6).value=param7;}
        if(script_defined_params.size()>7){script_defined_params.get(7).value=param8;}
        if(script_defined_params.size()>8){script_defined_params.get(8).value=param9;}
        if(script_defined_params.size()>9){script_defined_params.get(9).value=param10;}
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String stored_name, String params)
    {
        return invoke(param1, param2, param3, param4, param5, param6, param7, param8, param9, "", stored_name, params);
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String stored_name, String params)
    {
        return invoke(param1, param2, param3, param4, param5, param6, param7, param8, "","", stored_name, params);
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String stored_name, Object params)
    {
        return invoke(param1, param2, param3, param4, param5, param6, param7, "", "","", stored_name, params);
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String param6, String stored_name, Object params)
    {
        return invoke(param1, param2, param3, param4, param5, param6, "", "", "","", stored_name, params);
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String stored_name, Object params)
    {
        return invoke(param1, param2, param3, param4, param5, "", "", "", "","", stored_name, params);
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String stored_name, Object params)
    {
        return invoke(param1, param2, param3, param4, "", "", "", "", "","", stored_name, params);
    }
    
    public String invoke(String param1, String param2, String param3, String stored_name, Object params)
    {
        return invoke(param1, param2, param3, "", "", "", "", "", "","", stored_name, params);
    }
    
    public String invoke(String param1, String param2, String stored_name, Object params)
    {
        return invoke(param1, param2, "", "", "", "", "", "", "","", stored_name, params);
    }
    
    public String invoke(String param1, String stored_name, Object params)
    {
        return invoke(param1, "", "", "", "", "", "", "", "","", stored_name, params);
    }
    
    public String invoke(String stored_name, Object params)
    {
        return invoke("", "", "", "", "", "", "", "", "","", stored_name, params);
    }
    
    public String formatConnection(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10)
    {
        // PUT ON ALL OF SCRIPT DEFINED PARAMS
        if(user_defined_params.size()>0)
        {
            Gson gson = new GsonBuilder().create();
            event_queue.add(new Event(prefix+"BeforeDatabaseConnectorUserParamsRequested",null,gson.toJson(user_defined_params)));
            ask_users_params();
            event_queue.add(new Event(prefix+"AfterDatabaseConnectorUserParamsRequested",null,gson.toJson(user_defined_params)));
        }
        setScriptDefinedParams(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10);

        String[] connection_parts=StringUtils.getURLParts(CONNECTION);
        String classicalCONNECTION=CONNECTION;

        int script_params_iterator=0;
        for(int i=0; i<connection_parts.length;i++)
        {
            /*if(connection_parts[i].startsWith("%")&&connection_parts[i].endsWith("%"))
            {*/
                String URL_component=rb.getString("connection."+connection_parts[i].replace("%", ""));
                if(URL_component.equalsIgnoreCase("$PARAM$"))
                {
                    classicalCONNECTION=classicalCONNECTION.replace("%"+connection_parts[i]+"%", script_defined_params.get(script_params_iterator).prefix+script_defined_params.get(script_params_iterator).value);
                    script_params_iterator++;
                }
            /*}*/
        }

        connection_parts=StringUtils.getURLParts(classicalCONNECTION);
        int user_params_iterator=0;
        for(int i=0; i<connection_parts.length;i++)
        {
            /*if(connection_parts[i].startsWith("%")&&connection_parts[i].endsWith("%"))
            {*/
                String connection_component=rb.getString("connection."+connection_parts[i].replace("%", ""));
                if(connection_component.equalsIgnoreCase("$USER_INPUT$"))
                {
                    classicalCONNECTION=classicalCONNECTION.replace("%"+connection_parts[i]+"%", user_defined_params.get(user_params_iterator).prefix+user_defined_params.get(user_params_iterator).value);
                    user_params_iterator++;
                }
            /*}*/
        }


        if(connection_schema.equalsIgnoreCase("$PARAM$"))
        {
            connection_schema=script_defined_params.get(script_params_iterator).value;
            script_params_iterator++;
        }
        else if(connection_schema.equalsIgnoreCase("$USER_INPUT$"))
        {
            connection_schema=user_defined_params.get(user_params_iterator).value;
            user_params_iterator++;
        }

        if(driver.equalsIgnoreCase("$PARAM$"))
        {
            driver=script_defined_params.get(script_params_iterator).value;
            script_params_iterator++;
        }
        else if(driver.equalsIgnoreCase("$USER_INPUT$"))
        {
            driver=user_defined_params.get(user_params_iterator).value;
            user_params_iterator++;
        }

        if(connection_user.equalsIgnoreCase("$PARAM$"))
        {
            connection_user=script_defined_params.get(script_params_iterator).value;
            script_params_iterator++;
        }
        else if(connection_user.equalsIgnoreCase("$USER_INPUT$"))
        {
            connection_user=user_defined_params.get(user_params_iterator).value;
            user_params_iterator++;
        }

        if(connection_password.equalsIgnoreCase("$PARAM$"))
        {
            connection_password=script_defined_params.get(script_params_iterator).value;
            script_params_iterator++;
        }
        else if(connection_password.equalsIgnoreCase("$USER_INPUT$"))
        {
            connection_password=user_defined_params.get(user_params_iterator).value;
            user_params_iterator++;
        }

        LOG.info("classicalCONNECTION: "+classicalCONNECTION);

        return classicalCONNECTION;
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10, String stored_name, Object native_params)
    {
        try
        {
            String classicalCONNECTION=formatConnection(param1, param2, param3, param4, param5, param6, param7,  param8,  param9,  param10);

            SPHelper getter = new SPHelper(stored_name, connection_schema);
            getter.Prepare(driver, classicalCONNECTION, connection_user, connection_password);
            getter.prepareStatementToCall();
            /*if(params instanceof NativeObject)
            {
                NativeObject nObj=(NativeObject)params;
                for (Object key: nObj.getAllIds()) 
                {
                    String kk="";

                    if(key instanceof String)
                    {
                        kk=(String)key;
                        Object o=nObj.get((String)key, nObj);
                        if(o instanceof Undefined)
                        {
                            o=null;
                        }
                        //NativeObject no = (NativeObject)o; 
                        getter.setParameterWithName(kk,o);
                    }
                }
            }
            else if(params instanceof NativeArray)
            {
                NativeArray nArr = (NativeArray)params;
                for (Object o : nArr.getIds()) 
                {
                   int index = (Integer) o;
                   getter.setParameterWithId(index, nArr.get(index, null));
                }
            }*/
            Object params=ObjectUtils.convert(native_params);
            
            if(params instanceof Map)
            {
                Set<String> keys=((Map<String, Object>)params).keySet();
                
                for(String key: keys)
                {
                    getter.setParameterWithName(key,((Map<String, Object>)params).get(key));
                }
            }
            else if(params instanceof Set)
            {
                int i=0;
                for(Object val: (Set<Object>)params)
                {
                    getter.setParameterWithId(i,val);
                    i++;
                }
            }
            else
            {
                getter.setParameterWithId(1,(String)params);
            }
            getter.ExecForce();
            Gson gson=new Gson();
            StringWriter sw=new StringWriter();
            JsonWriter jw=new JsonWriter(sw);
            getter.getJsonResult(jw, gson);
            //System.out.println("result is bytes:"+response.toString().getBytes());
            String result=sw.toString();
            LOG.debug("result is: "+result);
            sw.close();
            sw.flush();
            Init();
            return result;// new String(Gzip.decompress(response.toString().getBytes()));
        }
        catch(Exception Ex)
        {
            Gson gson=new Gson();
            Init();
            return gson.toJson(Ex.getMessage());
                    //"{exception:\""+Ex.getMessage()+"\"}";
        }
        
    }
    
    public String checkResponse(String jsonBody)
    {
        
        String result="";
        
        try 
        { 
            Integer.parseInt(jsonBody); 
            result = jsonBody;
            LOG.debug("result is int");
        } 
        catch(Exception e) 
        { 
            LOG.debug("Not int, "+e.getMessage());
            try
            {
                Map<String, Object> RootMapObject = new Gson().fromJson(jsonBody, Map.class);
                result = jsonBody;
                LOG.debug("result is object");
            }
            catch(Exception jse)
            {
                LOG.debug("Not object, "+jse.getMessage());
                try
                {
                    Map<String, Object> RootMapObject = new Gson().fromJson(jsonBody, Map.class);
                    result = jsonBody;
                    LOG.debug("result is complex object");
                }
                catch(Exception jse1)
                {
                    LOG.debug("Not complex, "+jse1.getMessage());
                    try
                    {
                        int last_ex=jsonBody.lastIndexOf("!");
                        if(last_ex>0)
                        {
                            LOG.debug("split parts by !");
                            jsonBody=jsonBody.substring(0, last_ex);
                            result=checkResponse(jsonBody);
                        }
                        else
                        {
                            LOG.error("Not splitted, "+e.getMessage());
                            if(jsonBody.indexOf("e{")>=0)
                            {
                                LOG.debug("result is exception!");
                                result=jsonBody.replace("e{", "{");
                            }
                        }
                    }
                    catch(Exception Ex)
                    {
                        LOG.error("Not splitted, "+Ex.getMessage());
                        if(jsonBody.indexOf("e{")>=0)
                        {
                            LOG.debug("result is exception!");
                            result=jsonBody.replace("e{", "{");
                        }
                    }
                }
            }
        }
        
        LOG.debug("result is: "+result);
        return result;
    }
    
}
