/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces;

/**
 *
 * @author sergk
 */
public class Connectorparam 
{
    public String name;
    public String desc;
    public String value;
    public String prefix;
    public String suffix;
    public int type;
    public boolean remember;
    
    public Connectorparam()
    {
        name="";
        desc="";
        value="";
        type=0;
        remember=false;
        prefix="";
        suffix="";
    }
}
