/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces;

import java.util.concurrent.ArrayBlockingQueue;
//import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import ru.wnfx.scriptfighter.engine.core.ScriptThread;
import ru.wnfx.scriptfighter.engine.messaging.Event;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.engine.core.Middleware;
import ru.wnfx.scriptfighter.engine.core.ThreadsHolder;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.core.manager.EnginePool;

/**
 *
 * @author sergk
 */
public class EventListener extends Listener
{
    private static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(EventListener.class);
    
    public EventListener(String event_name, String function_name, ArrayBlockingQueue events_queue, LimitedMessageQueue incoming_events_queue, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, Scriptable parent)
    {
        this.event_name = event_name;
        this.function_name = function_name;
        this.events_queue = events_queue;
        this.incoming_events_queue = new LimitedMessageQueue(640);//incoming_events_queue;
        this.message_queue = message_queue;
        this.commands_queue = commands_queue;
        this.parent = parent;
    }
    
    @Override
    public void listen()
    {
        ThreadsHolder.getInstance().addThread(this);
        for(;!stop;)
        {
            try 
            {
                Event e = (Event)incoming_events_queue.poll();
                if(e!=null)
                {
                    if(e.event_name.equalsIgnoreCase(event_name))
                    {
                        CommonEngine jsEngine=EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
                        this.status="processing_event";
                        String script_body=/*Middleware.getScript(function_name)+"\n"+*/"var obj="+e.event_object+";\n "+function_name+"(obj);\n";
                        jsEngine.runScript(script_body, 0, java.util.UUID.randomUUID().toString().replace("-", ""));
                        jsEngine.owner=this;
                        this.status="listening";
                    }
                }
                Thread.sleep(1);
            } 
            catch (InterruptedException ex) 
            {
                LOG.error(ex.getMessage());
                break;
            }
        }
        ThreadsHolder.getInstance().removeThread(this);
    }
    
    @Override
    public void registerMethods(CommonEngine jsEngine) 
    {
    
    }
    
    @Override
    public void registerEvents()
    {
    
    }

    @Override
    public void stop() 
    {
        LOG.info("Command STOP received for listener ID: "+id);
        stop=true;
    }
}
