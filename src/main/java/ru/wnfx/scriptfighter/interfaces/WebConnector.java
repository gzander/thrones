/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import ru.wnfx.scriptfighter.engine.messaging.ConsoleMessage;
//import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.zip.GZIPInputStream;
import ru.wnfx.scriptfighter.engine.messaging.RequestMessage;
import java.io.FileInputStream;
import java.util.PropertyResourceBundle;
import java.util.ArrayList;
import ru.wnfx.scriptfighter.engine.core.BasicScript;
import com.google.gson.JsonSyntaxException;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.messaging.AlertMessage;
import ru.wnfx.scriptfighter.engine.messaging.Event;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
/**
 *
 * @author sergk
 */
public class WebConnector extends Connector 
{

    private String auth_secret;
    private String url_mask;
    public String URL;
    private Map<String, String> request_properties;
    private String request_method;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(WebConnector.class);
    
    public WebConnector(String propFileName, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, ArrayBlockingQueue event_queue)
    {
        super(propFileName, message_queue, commands_queue, event_queue);
        request_properties=new HashMap<String, String>();
        
        auth_secret = rb.getString("auth.secret");
        url_mask = rb.getString("url.mask");
        
        this.message_queue=message_queue;
        this.commands_queue=commands_queue;
        this.event_queue=event_queue;
        
        if(!type.equalsIgnoreCase("Web")) 
        {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        
        prepareUrl();
        prepareRequest();
    }
    
    @Override
    public String getInvokeWrap()
    {
        String invoke_builder="";
        String inner_invoke_builder="";
        for(Connectorparam p: script_defined_params)
        {
            invoke_builder+=p.name.replace("-", "_") +", ";
            inner_invoke_builder+=p.name.replace("-", "_")+", ";
        }
        invoke_builder+="params";
        inner_invoke_builder+="$(params)";

        return "function invoke"+prefix+"("+invoke_builder+"){return eval('('+"+prefix+".invoke("+inner_invoke_builder+")+')');}";
    }
    
    @Override
    public String getGenerateWrap()
    {
        return null;
    }
    
    @Override
    public void autoGenerate(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10)
    {
        message_queue.add(new AlertMessage("Automatic method generaion for interface type: "+type+" not supported yet"));
    }
    
    
    @Override
    public void registerMethods(CommonEngine jsEngine) 
    {
        try 
        {
            String source = getInvokeWrap();
            BasicScript bs=new BasicScript(source, prefix+" Web interface", true, false, "interfaces");
                   
            methods.add(bs);
            
            if(jsEngine!=null)
            {
                jsEngine.put(prefix, this);
                jsEngine.eval(source);
                //jsEngine.eval("function invoke"+prefix+"("+invoke_builder+"){return eval('('+"+prefix+".invoke("+inner_invoke_builder+")+')');}");
            }
            
            //additional web functions
        } 
        catch (ScriptException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void registerEvents()
    {
        events.add(new Event(prefix+"BeforeWebConnectorUserParamsRequested","Event of "+prefix+" web interface. Event triggers before user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"AfterWebConnectorUserParamsRequested","Event of "+prefix+" web interface. Event triggers after user params is requested. Attached object is full list of params"));
    }
    
    public final void prepareUrl()
    {
        String[] url_parts=url_mask.replace("http://", "").replace("%","").split("/");
        URL=url_mask;
        for(int i=0; i<url_parts.length;i++)
        {
            String URL_component=rb.getString("url."+url_parts[i]);
            if(URL_component.equalsIgnoreCase("$PARAM$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = url_parts[i];
                p.desc = "Параметр";
                script_defined_params.add(p);
            }
            else if(URL_component.equalsIgnoreCase("$USER_INPUT$"))
            {
                Connectorparam p= new Connectorparam();
                p.name = url_parts[i];
                p.desc = "Параметр";
                user_defined_params.add(p);
            }
            else
            {
                URL=URL.replace("%"+url_parts[i]+"%", rb.getString("url."+url_parts[i]));
            }
            
        }
    }
    
    public final void prepareRequest()
    {
        
        request_method=rb.getString("request.method");
        
        Set<String> keys=rb.keySet();

        for(String key: keys)
        {
            
            if(key.startsWith("request.property"))
            {
                String component=rb.getString(key);
                if(component.equalsIgnoreCase("$PARAM$"))
                {
                    Connectorparam p= new Connectorparam();
                    p.name = key.replace("request.property.", "");
                    p.desc = "Параметр";
                    script_defined_params.add(p);
                }
                else if(component.equalsIgnoreCase("$USER_INPUT$"))
                {
                    Connectorparam p= new Connectorparam();
                    p.name = key.replace("request.property.", "");
                    p.desc = "Параметр";
                    user_defined_params.add(p);
                }
                else
                {
                    request_properties.put(key.replace("request.property.", ""), component);
                }
            }
        }
    }
    
    private void setScriptDefinedParams(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10)
    {
        if(script_defined_params.size()>0){script_defined_params.get(0).value=param1;}
        if(script_defined_params.size()>1){script_defined_params.get(1).value=param2;}
        if(script_defined_params.size()>2){script_defined_params.get(2).value=param3;}
        if(script_defined_params.size()>3){script_defined_params.get(3).value=param4;}
        if(script_defined_params.size()>4){script_defined_params.get(4).value=param5;}
        if(script_defined_params.size()>5){script_defined_params.get(5).value=param6;}
        if(script_defined_params.size()>6){script_defined_params.get(6).value=param7;}
        if(script_defined_params.size()>7){script_defined_params.get(7).value=param8;}
        if(script_defined_params.size()>8){script_defined_params.get(8).value=param9;}
        if(script_defined_params.size()>9){script_defined_params.get(9).value=param10;}
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9,String params)
    {
        return invoke(param1, param2, param3, param4, param5, param6, param7, param8, param9, "", params);
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String params)
    {
        return invoke(param1, param2, param3, param4, param5, param6, param7, param8, "","", params);
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String params)
    {
        return invoke(param1, param2, param3, param4, param5, param6, param7, "", "","", params);
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String param6, String params)
    {
        return invoke(param1, param2, param3, param4, param5, param6, "", "", "","", params);
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String params)
    {
        return invoke(param1, param2, param3, param4, param5, "", "", "", "","", params);
    }
    
    public String invoke(String param1, String param2, String param3, String param4,String params)
    {
        return invoke(param1, param2, param3, param4, "", "", "", "", "","", params);
    }
    
    public String invoke(String param1, String param2, String param3, String params)
    {
        return invoke(param1, param2, param3, "", "", "", "", "", "","", params);
    }
    
    public String invoke(String param1, String param2, String params)
    {
        return invoke(param1, param2, "", "", "", "", "", "", "","", params);
    }
    
    public String invoke(String param1, String params)
    {
        return invoke(param1, "", "", "", "", "", "", "", "","", params);
    }
    
    public String invoke(String params)
    {
        return invoke("", "", "", "", "", "", "", "", "","", params);
    }
    
    public String invoke(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10, String params)
    {
        try
        {
            // PUT ON ALL OF SCRIPT DEFINED PARAMS
            if(user_defined_params.size()>0)
            {
                Gson gson = new GsonBuilder().create();
                event_queue.add(new Event(prefix+"BeforeWebConnectorUserParamsRequested",null,gson.toJson(user_defined_params)));
                ask_users_params();
                event_queue.add(new Event(prefix+"AfterWebConnectorUserParamsRequested",null,gson.toJson(user_defined_params)));
            }
            setScriptDefinedParams(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10);

            String[] url_parts=URL.replace("http://", "").split("/");
            String classicalURL=URL;

            int script_params_iterator=0;
            for(int i=0; i<url_parts.length;i++)
            {
                if(url_parts[i].startsWith("%")&&url_parts[i].endsWith("%"))
                {
                    String URL_component=rb.getString("url."+url_parts[i].replace("%", ""));
                    if(URL_component.equalsIgnoreCase("$PARAM$"))
                    {
                        classicalURL=classicalURL.replace(url_parts[i], script_defined_params.get(script_params_iterator).value);
                        script_params_iterator++;
                    }
                }
            }

            Set<String> keys=rb.keySet();

            for(String key: keys)
            {

                if(key.startsWith("request.property"))
                {
                    String component=rb.getString(key);
                    if(component.equalsIgnoreCase("$PARAM$"))
                    {
                        request_properties.put(key.replace("request.property.", ""), script_defined_params.get(script_params_iterator).value);
                        script_params_iterator++;
                    }

                }
            }

            url_parts=classicalURL.replace("http://", "").split("/");
            int user_params_iterator=0;
            for(int i=0; i<url_parts.length;i++)
            {
                if(url_parts[i].startsWith("%")&&url_parts[i].endsWith("%"))
                {
                    String URL_component=rb.getString("url."+url_parts[i].replace("%", ""));
                    if(URL_component.equalsIgnoreCase("$USER_INPUT$"))
                    {
                        classicalURL=classicalURL.replace(url_parts[i], user_defined_params.get(user_params_iterator).value);
                        user_params_iterator++;
                    }
                }
            }

            for(String key: keys)
            {

                if(key.startsWith("request.property"))
                {
                    String component=rb.getString(key);
                    if(component.equalsIgnoreCase("$USER_INPUT$"))
                    {
                        request_properties.put(key.replace("request.property.", ""), user_defined_params.get(user_params_iterator).value);
                        user_params_iterator++;
                    }

                }
            }

            URL url=null;
            try 
            {
                url = new URL(classicalURL);
            } 
            catch (MalformedURLException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            HttpURLConnection connection = null;  
            try 
            {
                connection = (HttpURLConnection)url.openConnection();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            try 
            {
                connection.setRequestMethod(request_method);
            } 
            catch (ProtocolException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }

            for (Map.Entry<String, String> entry : request_properties.entrySet())
            {
                connection.setRequestProperty(entry.getKey(),entry.getValue());
                System.out.println(entry.getKey() + "/" + entry.getValue());
            }
            connection.setRequestProperty("Content-Length", "" + Integer.toString(params.getBytes().length));
            //connection.setRequestProperty("Content-Language", "en-US");  

            connection.setUseCaches (false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr;
            try 
            {
                wr = new DataOutputStream (connection.getOutputStream ());
                wr.writeBytes (params);
                wr.flush ();
                wr.close ();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }


            InputStream is = null;
            try 
            {
                is = connection.getInputStream();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Get Response	
            if ("gzip".equals(connection.getContentEncoding())) 
            {
                try 
                {
                    is = new GZIPInputStream(is);
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }


            //GZIPInputStream  gzip = new GZIPInputStream (new ByteArrayInputStream (tBytes));
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer(); 
            try 
            {
                //java.io.By
                while((line = rd.readLine()) != null) 
                {
                  response.append(line);
                  //response.append('\r');
                }
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            try 
            {
                rd.close();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }

            //System.out.println("result is bytes:"+response.toString().getBytes());
            LOG.debug("result is:"+response.toString());
            
            String gogno=response.toString();
            
            gogno=gogno.replace("\\r", "ЖОПА");
            gogno=gogno.replace("\u2028", "ЖОПА");
            gogno=gogno.replace("\\u2028", "ЖОПА");
            gogno=gogno.replace("\\n", "ЖОПА");
            gogno=gogno.replace("\n", "ЖОПА");
            gogno=gogno.replace("\r", "ЖОПА");
            gogno=gogno.replaceAll("\\r", "ЖОПА");
            gogno=gogno.replaceAll("\\n", "ЖОПА");
            gogno=gogno.replaceAll("\r", "ЖОПА");
            gogno=gogno.replaceAll("\n", "ЖОПА");
            gogno=gogno.replaceAll("\\u2028", "ЖОПА");
            gogno=gogno.replaceAll("\u2028", "ЖОПА");

            return checkResponse(gogno/*.replaceAll("\\p{C}", "?")*/);// new String(Gzip.decompress(response.toString().getBytes()));
        }
        catch(Exception Ex)
        {
            return "{exception:\""+Ex.getMessage()+"\"}";
        }
        
    }
    
    public String checkResponse(String jsonBody)
    {
        
        String result="";
        // LOG.error("Try again. Fuckin LS in: "+jsonBody.indexOf("\n")+" or "+jsonBody.indexOf("\r")+" or "+jsonBody.indexOf("\\n")+" or "+jsonBody.indexOf("\\r"));
        try 
        { 
            Integer.parseInt(jsonBody); 
            result = jsonBody;
            LOG.debug("result is int");
        } 
        catch(Exception e) 
        { 
            LOG.debug("Not int, "+e.getMessage());
            try
            {
                Map<String, Object> RootMapObject = new Gson().fromJson(jsonBody, Map.class);
                result = jsonBody;
                LOG.debug("result is object");
            }
            catch(Exception jse)
            {
                LOG.debug("Not object, "+jse.getMessage());
                try
                {
                    List<Object> RootMapObject = new Gson().fromJson(jsonBody, List.class);
                    result = jsonBody;
                    LOG.debug("result is array object");
                }
                catch(Exception jse1)
                {
                    LOG.debug("Not complex, "+jse1.getMessage());
                    try
                    {
                        int last_ex=jsonBody.lastIndexOf("!");
                        if(last_ex>0)
                        {
                            LOG.debug("split parts by !");
                            jsonBody=jsonBody.substring(0, last_ex);
                            result=checkResponse(jsonBody);
                        }
                        else
                        {
                            LOG.error("Not splitted, "+e.getMessage());
                            if(jsonBody.indexOf("e{")>=0)
                            {
                                LOG.debug("result is exception!");
                                result=jsonBody.replace("e{", "{");
                            }
                        }
                    }
                    catch(Exception Ex)
                    {
                        LOG.error("Not splitted, "+Ex.getMessage());
                        if(jsonBody.indexOf("e{")>=0)
                        {
                            LOG.debug("result is exception!");
                            result=jsonBody.replace("e{", "{");
                        }
                    }
                }
            }
        }
        
        LOG.debug("result is: "+result);
        return result;
    }
    
}
