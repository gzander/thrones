/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.docs.core;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.Item;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import ru.wnfx.scriptfighter.html.core.*;
import ru.wnfx.scriptfighter.http.core.*;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.*;
import java.util.zip.GZIPInputStream;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import sun.org.mozilla.javascript.internal.NativeObject;
import sun.org.mozilla.javascript.internal.NativeArray;
import org.jsoup.*;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;


/**
 *
 * @author sergk
 */
public class Pdf  extends Library
{
    Document doc;
    Elements elements;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(Html.class);
    private String doc_url;
    PdfReader reader;
    PdfStamper stamper;
    public Pdf(CommonEngine g)
    {
        doc=null;
        elements=null;
        prefix="Pdf";
        desc="HTML Parse API";
        doc_url=null;
        
        if(g!=null)
        {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String url", desc="open pdf doc")
    public Pdf openDoc(String src, String dest)
    {
        doc_url = src;
        try 
        {
            reader = new PdfReader(src);
            stamper = new PdfStamper(reader, new FileOutputStream(dest));
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (DocumentException ex) 
        {
            Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="Object form_data, Object params", desc="fill pdf form")
    public Pdf fillForm(Map<String, Object> form_data, Map<String, String> params)
    {
        AcroFields fields = stamper.getAcroFields();
        //-----------------------------------------
        BaseFont bf=null;
        String font = params.get("font");
        if(font!=null)
        {
            try 
            {
                bf = BaseFont.createFont(font, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, false, null, null, false);
            } 
            catch (DocumentException ex) 
            {
                Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //------------------------------------------
        Float fs=null;
        String font_size = params.get("font_size");
        if(font_size!=null)
        {
           fs = Float.parseFloat(font_size);
        }
        //------------------------------------------        
        int al=-1;
        String align = params.get("align");
        if(align!=null)
        {
            if(align.equalsIgnoreCase("left"))
                al=PdfFormField.Q_LEFT;
            else if(align.equalsIgnoreCase("center"))
                al=PdfFormField.Q_CENTER;
            else if(align.equalsIgnoreCase("right"))
                al=PdfFormField.Q_RIGHT;
            else
            {
                al=PdfFormField.Q_CENTER;
            }
        }
        //------------------------------------------        
        Map<String,Item> form_fields = fields.getFields();
        for(String data_key: form_data.keySet())
        {
            for(String field_key: form_fields.keySet())
            {
                if(data_key.equalsIgnoreCase(field_key))
                {
                    if(form_data.get(data_key) instanceof String)
                    {
                        try 
                        {
                            if(align!=null)
                            {                          
                                Item item = fields.getFieldItem(field_key);
                                item.getMerged(0).put(PdfName.Q, new PdfNumber(al));
                            }
                            
                            if(bf!=null)
                            {
                                fields.setFieldProperty(field_key, "textfont", bf, null);
                            }
                            
                            if(fs!=null)
                            {
                                fields.setFieldProperty(field_key, "textsize", fs, null);
                            }
                            fields.setField(field_key, (String)form_data.get(data_key));
                        } 
                        catch (IOException ex) 
                        {
                            Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                        } 
                        catch (DocumentException ex) 
                        {
                            Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    if(form_data.get(data_key) instanceof Integer)
                    {
                        try 
                        {
                            if(bf!=null)
                            {
                                fields.setFieldProperty(field_key, "textfont", bf, null);
                            }
                            fields.setField(field_key, ((Integer)form_data.get(data_key)).toString());

                        } 
                        catch (IOException ex) 
                        {
                            Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                        } 
                        catch (DocumentException ex) 
                        {
                            Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }                    
                }
            }
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="", desc="saves document")
    public Pdf save()
    {
        stamper.setFormFlattening(true);
        try 
        {        
            stamper.close();
        } 
        catch (DocumentException ex) 
        {
            Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String stamp_image, String field_name", desc="adds a stamp")
    public Pdf makeStamp(String stamp_image, String field_name)
    {
        Image img;
        try 
        {
            AcroFields fields = stamper.getAcroFields();
            List<AcroFields.FieldPosition> positions = fields.getFieldPositions(field_name);
            for(AcroFields.FieldPosition pos: positions)
            {
                img = Image.getInstance(stamp_image);
                LOG.info(pos.page);
                float x = pos.position.getLeft();
                float y = pos.position.getTop();
                float w = img.getScaledWidth();
                float h = img.getScaledHeight();
                img.setAbsolutePosition(x-w/2, y-h/2);
                stamper.getOverContent(pos.page).addImage(img);
            }
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        /*float x = 10;
        float y = 650;
        float w = img.getScaledWidth();
        float h = img.getScaledHeight();
        img.setAbsolutePosition(x, y);
        stamper.getOverContent(1).addImage(img);
        Rectangle linkLocation = new Rectangle(x, y, x + w, y + h);
        PdfDestination destination = new PdfDestination(PdfDestination.FIT);
        PdfAnnotation link = PdfAnnotation.createLink(stamper.getWriter(),
                linkLocation, PdfAnnotation.HIGHLIGHT_INVERT,
                reader.getNumberOfPages(), destination);
        link.setBorder(new PdfBorderArray(0, 0, 0));
        stamper.addAnnotation(link, 1);
        stamper.close();*/
        
        return this;
    }
  
}
