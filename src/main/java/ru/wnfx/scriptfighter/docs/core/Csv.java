/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.docs.core;


import ru.wnfx.scriptfighter.html.core.*;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;


/**
 *
 * @author sergk
 */
public class Csv  extends Library
{
    Document doc;
    Elements elements;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(Html.class);
    private String doc_url;
    File reader;
    public Csv(CommonEngine g)
    {
        doc=null;
        elements=null;
        prefix="Csv";
        desc="Csv API";
        doc_url=null;
        
        if(g!=null)
        {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String url", desc="open pdf doc")
    public Csv openDoc(String src)
    {
        doc_url = src;
        try 
        {
            reader = new File(src);
            
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(Csv.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="Object form_data, Object params", desc="fill pdf form")
    public Csv generateCSV(Map<String, Object> form_data)
    {
        String body = ru.wnfx.scriptfighter.utils.StringUtils.mapToCSV(form_data);
        
        try
        {
            FileOutputStream fos = new FileOutputStream(reader); 
            fos.write(body.getBytes()); 
            fos.close();
        }
        catch(Exception ex)
        {
            LOG.error("Error saving file: "+reader.getAbsolutePath()+", caused by: "+ex.getMessage());
        }
        
        return this;
    }
      
}
