package ru.wnfx.scriptfighter.config;

import java.io.*;
import java.util.*;


/**
 * Класс реализует чтение и запись в конфигурационный файл.
 * Все значения, найденные в конфигурационном файле становятся системными свойствами с теми же именами, что и в конфигурационном файле.
 * @author Smagin S
 */
public class Config {

	public final static String PREFIX = "tank_runner.";
	public final static String FILE_PROP_NAME = "config_slave.config";
	public final static String FILE_PROP_VALUE = "config_slave.properties";
        public final static String PORT = "thrones.port";


	/** Загружает свойства из файла, если таковой имеется и объединяет их со системными свойствами. */
	public static void load () {
		Properties sysProps = System.getProperties ();
		Properties appProps = new Properties ();
		try {
			FileInputStream fis = new FileInputStream (getConfigPath ());
			appProps.load (fis);
		} catch (Exception e) {
		}
		sysProps.putAll (appProps);
	}


	/** Сохраняет свойства в файл. */
	public static void store () throws IOException {
		Properties sysProps = System.getProperties ();
		Properties appProps = new Properties ();
		Enumeration en = sysProps.propertyNames ();
		while (en.hasMoreElements ()) {
			String name = (String) en.nextElement ();
			if (name.startsWith (PREFIX)) {
				appProps.put (name, sysProps.getProperty (name));
			}
		}
		FileOutputStream fos = new FileOutputStream (getConfigPath ());
		appProps.store (fos, null);
	}


	/** Возвращает путь к конфиг-файлу. */
	private static String getConfigPath () {
		return System.getProperty (FILE_PROP_NAME, FILE_PROP_VALUE);
	}
}
