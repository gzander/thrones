/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

import com.google.gson.Gson;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author sergk
 */
public class StringUtils 
{
    public static final Logger LOG=Logger.getLogger(StringUtils.class);
    private static final Gson gson = new Gson();
    
    public static String Capitalize(String input)
    {
        return input==null?null:(input.substring(0, 1).toUpperCase() + input.substring(1));
    }
    
    public static boolean isNumeric(String str)  
    {  
      try  
      {  
        double d = Double.parseDouble(str);  
      }  
      catch(NumberFormatException nfe)  
      {  
        return false;  
      }  
      return true;  
    }
    
    public static boolean isInteger(String str)  
    {  
      try  
      {  
        int d = Integer.parseInt(str);  
      }  
      catch(NumberFormatException nfe)  
      {  
        return false;  
      }  
      return true;  
    }
    
  public static int tryInteger(String val)
  {
      if(isInteger(val)) 
      {
          return Integer.parseInt(val);
      }
      if(isNumeric(val))
      {
          double d = Double.parseDouble(val);
          if(Math.ceil(d)==d)
          {
              return (int)d;
          }
          else 
          {
              throw new NumberFormatException();
          }  
      }  
      throw new NumberFormatException();
  }
  
  public static short tryShort(String val)
  {
      if(isInteger(val)) 
      {
          return Short.parseShort(val);
      }
      if(isNumeric(val))
      {
          double d = Double.parseDouble(val);
          if(Math.ceil(d)==d)
          {
              return (short)d;
          }
          else 
          {
              throw new NumberFormatException();
          }  
      }  
      throw new NumberFormatException();
  }

  public static boolean isJSONValid(String JSON_STRING) 
  {
      
      /*if(JSON_STRING==null)
          return false;
      if(JSON_STRING.length()==0)
          return false;*/
      
      try 
      {
          gson.fromJson(JSON_STRING, Map.class);
          return true;
      } 
      catch(com.google.gson.JsonSyntaxException ex) 
      { 
          try 
          {
                gson.fromJson(JSON_STRING, Set.class);
                return true;
          } 
          catch(com.google.gson.JsonSyntaxException ex1) 
          { 
                return false;
          }
      }
  }
  
  public static String[] getURLParts(String URL)
  {
      int start=0;
      int finish=0;
      int count_tags=(URL.length()-URL.replace("%", "").length())/2;
      String[] result=new String[count_tags];
      for(int i=0;i<count_tags;i++)
      {
        start = URL.indexOf("%", finish);
        finish = URL.indexOf("%",start+1);
        result[i]=URL.substring(start+1, finish);
        finish++;
      }
      return result;
  }
  
  public static String MD5(String md5)
  {
        try 
        {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.length; ++i) 
            {
               sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
         } 
         catch (java.security.NoSuchAlgorithmException e) 
         {
         }
         return null;
  }
  
   public static String MD5(String md5, String encoding)
  {
        try 
        {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array=null;
            try 
            {
                array = md.digest(md5.getBytes(encoding));
            } 
            catch (UnsupportedEncodingException ex) 
            {
                java.util.logging.Logger.getLogger(StringUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.length; ++i) 
            {
               sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
         } 
         catch (java.security.NoSuchAlgorithmException e) 
         {
         }
         return null;
  }
   
   static public String join(List<String> list, String conjunction)
    {
       StringBuilder sb = new StringBuilder();
       boolean first = true;
       for (String item : list)
       {
          if (first)
             first = false;
          else
             sb.append(conjunction);
          sb.append(item);
       }
       return sb.toString();
    }
   
    public static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }
    public static String urlEncodeUTF8(Map<?,?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?,?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                urlEncodeUTF8(entry.getKey().toString()),
                urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        return sb.toString();       
    }
    
    public static String listToCSV(List<Map<?,?>> list) {
        StringBuilder sb = new StringBuilder();
        if(list.size()>0)
        {
            Map<String,Object> first = (Map<String,Object>)list.get(0);
            for (String key : first.keySet()) {
                if (sb.length() > 0) {
                    sb.append(";");
                }
                sb.append(key);
            }
            sb.append(System.getProperty("line.separator"));
            
            for (Map<?,?> entry : list) {
                if (sb.length() > 0) {
                    sb.append(mapToCSV(entry));
                }
                sb.append(System.getProperty("line.separator"));
            }
        }
        return sb.toString();       
    }
    
    public static String mapToCSV(Map<?,?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?,?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append(";");
            }
            sb.append(entry.getValue()==null?"":entry.getValue().toString().replaceAll(";", ","));
        }
        return sb.toString();       
    }
}
