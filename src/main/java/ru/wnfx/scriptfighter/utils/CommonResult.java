/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author sergk
 */
public class CommonResult 
{
    
    static final Logger LOG=Logger.getLogger(CommonResult.class);
    
    public static String initOkResult(String title, String body)
    {
        LOG.debug("format OK result with title: "+title+ " ,and body: "+body);
        
        return initResult(true, title, body);

    }
    
    public static String initErrorResult(String title, String body)
    {
        
        LOG.debug("format error result with title: "+title+ " ,and body: "+body);
        
        return initResult(false, title, body);

    }    
    
    private static String initResult(boolean result, String title, String body)
    {
        Gson gson = new Gson();
        StringWriter sw=new StringWriter();
        JsonWriter jw=new JsonWriter(sw);
        try 
        {
            jw.beginObject();
            gson.toJson(result,Boolean.class,jw.name("result"));
            gson.toJson(title,String.class,jw.name("title"));
            gson.toJson(body,String.class,jw.name("body"));
            jw.endObject();
        } catch (IOException ex) 
        {
            java.util.logging.Logger.getLogger(CommonResult.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return sw.toString();
    }
}
