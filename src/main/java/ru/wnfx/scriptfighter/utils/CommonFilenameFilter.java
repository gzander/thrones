/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;
import java.io.File;
import java.io.FilenameFilter;
import org.apache.log4j.Logger;

/**
 *
 * @author sergk
 */
public class CommonFilenameFilter implements FilenameFilter
{
    public static final String[] ext_array={".jpg",".png",".gif",".bmp",".vdata",".swf",".txt"};
    public static final Logger LOG = Logger.getLogger(CommonFilenameFilter.class);

    public boolean is_video;
    
    public CommonFilenameFilter()
    {
        this.is_video=is_video;
    }
    
    @Override
    public boolean accept(File dir, String name) 
    {
        for(int i=0; i<ext_array.length; i++)
        {
            //LOG.info("check_file: "+name);
            if(name.endsWith(ext_array[i]))
            {
                
               return true;
            }
        }
        
        return false;
    }
}
