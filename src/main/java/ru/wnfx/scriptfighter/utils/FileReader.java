/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

/**
 *
 * @author sergk
 */
import java.io.*;
import org.apache.log4j.*;

public class FileReader {

    public static final Logger LOG=Logger.getLogger(FileReader.class);
    
    public static String readPage(String filename, String encoding) 
    {
        
        String str = "";
        String fileString="";
        try
            {
            try {
                FileInputStream fis = new FileInputStream(filename);
                InputStreamReader isr = new InputStreamReader(
                        new FileInputStream(filename), encoding);
                BufferedReader in = new BufferedReader(isr);
                while (((str = in.readLine()) != null)) {
                    fileString = fileString + "\n" + str/* + "<br>"*/;
                }
                in.close();
            } 
            catch (IOException e) 
            {
                LOG.error("Any IO problems: "+e.getMessage());
                return e.getMessage();
            }
           }
        catch (Exception Ex)
        {
          LOG.error("Any big problem: "+Ex.getMessage());  
          return Ex.getMessage();  
        }
        
        LOG.debug("File: "+filename+" succesfully readed using encoding "+encoding);
        
        return fileString;
    }
    
    public static String readFile(String filename, String encoding) 
    {
        
        String str = "";
        String fileString="";
        try
            {
            try {
                FileInputStream fis = new FileInputStream(filename);
                InputStreamReader isr = new InputStreamReader(
                        new FileInputStream(filename), encoding);
                BufferedReader in = new BufferedReader(isr);
                while (((str = in.readLine()) != null)) {
                    fileString = fileString + str/* + "<br>"*/;
                }
                in.close();
            } 
            catch (IOException e) 
            {
                LOG.error("Any IO problems: "+e.getMessage());
                return e.getMessage();
            }
           }
        catch (Exception Ex)
        {
          LOG.error("Any big problem: "+Ex.getMessage());  
          return Ex.getMessage();  
        }
        
        LOG.info("File: "+filename+" succesfully readed using encoding "+encoding);
        
        return fileString;
    }
    
    public static boolean matches(String filename, String encoding, String regex) 
    {
        
        String str = "";
        String fileString="";
        try
            {
            try {
                FileInputStream fis = new FileInputStream(filename);
                InputStreamReader isr = new InputStreamReader(
                        new FileInputStream(filename), encoding);
                BufferedReader in = new BufferedReader(isr);
                while (((str = in.readLine()) != null)) {
                    if(str.matches(regex))
                        return true;
                }
                in.close();
            } 
            catch (IOException e) 
            {
                LOG.error("Any IO problems: "+e.getMessage());
                return false;
            }
           }
        catch (Exception Ex)
        {
          LOG.error("Any big problem: "+Ex.getMessage());  
          return false; 
        }
        
        LOG.info("File: "+filename+" succesfully readed using encoding "+encoding);
        
        return fileString.matches(regex);
    }
    
    public static boolean matches(File file, String encoding, String regex) 
    {
        
        String str = "";
        String fileString="";
        try
            {
            try {
                FileInputStream fis = new FileInputStream(file);
                InputStreamReader isr = new InputStreamReader(
                        new FileInputStream(file), encoding);
                BufferedReader in = new BufferedReader(isr);
                while (((str = in.readLine()) != null)) {
                    if(str.matches(regex))
                        return true;
                }
                in.close();
            } 
            catch (IOException e) 
            {
                LOG.error("Any IO problems: "+e.getMessage());
                return false;
            }
           }
        catch (Exception Ex)
        {
          LOG.error("Any big problem: "+Ex.getMessage());  
          return false; 
        }
        
        LOG.info("File: "+file.getName()+" succesfully readed using encoding "+encoding);
        
        return fileString.matches(regex);
    }
}        