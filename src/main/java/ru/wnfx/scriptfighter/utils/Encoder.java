/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

/**
 *
 * @author sergk
 */
import java.security.MessageDigest;
import org.apache.log4j.*;

public class Encoder 
{

    String message;
    public static final Logger LOG=Logger.getLogger(Encoder.class);
   
    public Encoder(String msg)
    {
        message=msg;
    }

    public String EncodeMD5() 
    {
        LOG.debug("EncodeMD5 started");
        String text = message;
        String md5 = null;
        try {
            StringBuffer code = new StringBuffer();
            MessageDigest messageDigest =
                    MessageDigest.getInstance("MD5");
            byte bytes[] = text.getBytes();
            byte digest[] = messageDigest.digest(bytes);
            for (int i = 0; i < digest.length; ++i) {
                code.append(Integer.toHexString(0x0100 +
                        (digest[i] & 0x00FF)).substring(1));
            }
            md5 = code.toString();
        } 
        catch (Exception e) 
        {
            LOG.error(e.getMessage());
            return text;
        }
      LOG.debug("EncodeMD5 finished");
      return md5;
    }
}
