/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

/**
 *
 * @author sergk
 */
public class Const 
{
    public static String getCategoryImgNPath(String office, String category, int n)
    {
        return "uploads/"+office+"/img/categories/"+category+"/"+(category+"")+n+".jpg";
    }
    
    public static String getCategoryMainImgPath(String office, String category)
    {
        return "uploads/"+office+"/img/categories/"+category+"/"+(category+"")+".png";
    }
}
