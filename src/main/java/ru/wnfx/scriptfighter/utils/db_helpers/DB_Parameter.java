/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.wnfx.scriptfighter.utils.db_helpers;

import java.sql.*;
import java.util.ArrayList;
/**
 *
 * @author sergk
 */
public class DB_Parameter {
                            private String name;
                            private String value;
                            private String type;
                            private String subtype;
                            private String InOut;
                            private String common_name;
                            public ArrayList possible_values;
                            public ResultSet cursor;
                            public boolean is_return;
        
public DB_Parameter()
{
    possible_values=new ArrayList();
    is_return=false;
}
      
public void SetParameter(String iname, String ivalue, String itype, String iInOut)
{
    name=iname;
    value=ivalue;
    type=itype;
    InOut=iInOut;
    is_return=false;
}

public void SetParameterReturn(String iname, String ivalue, String itype, String iInOut)
{
    name=iname;
    value=ivalue;
    type=itype;
    InOut=iInOut;
    is_return=true;
}

public void SetParameter(String iname, String ivalue, String itype, String iInOut, String iSubType)
{
    name=iname;
    value=ivalue;
    type=itype;
    InOut=iInOut;
    subtype = iSubType;
    is_return=false;
}

public void SetParameter(String iname, String ivalue, String itype)
{
    name=iname;
    value=ivalue;
    type=itype;
    is_return=false;
}

public void SetName(String iname)
{
    name=iname;
}

public void SetCommonName(String iname)
{
    common_name=iname;
}

public void SetValue(String ivalue)
{

    value=ivalue;
}

public void SetType(String itype)
{
    type=itype;
}

public void SetInOut(String iInOut)
{
    InOut=iInOut;
}

public String _value()
{
    return value;
}

public String _subtype()
{
    return subtype;
}
public String _name()
{
    return name;
}

public String _common_name()
{
    return common_name;
}

public String _type()
{
    return type;
}

public String _InOut()
{
    return InOut;
}

public void RegisterPossibleValues(DB_Parameter a, String name_field, String code_field)
{
    try
    {
    ResultSetMetaData rsmd;
    rsmd = a.cursor.getMetaData();
    int n = rsmd.getColumnCount();
    
    while (a.cursor.next()) 
    {
        PossibleValue val=new PossibleValue();
        val.code=a.cursor.getString(code_field);
        val.value=a.cursor.getString(name_field);
        possible_values.add(val);
    }
    }
    catch(SQLException Se)
    {
    }
}

public boolean IsListed()
{
    if(possible_values.size()>0)
        return true;
    return false;
}
}
