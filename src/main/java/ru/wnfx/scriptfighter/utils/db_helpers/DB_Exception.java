package ru.wnfx.scriptfighter.utils.db_helpers;


/**
 * Инкапсулирует все ошибки модуля, могущие возникнуть в процессе его работы.
 * @author Смагин Сергей
 */
public class DB_Exception extends Exception {

	public DB_Exception (String message) {
		super (message);
	}
}

