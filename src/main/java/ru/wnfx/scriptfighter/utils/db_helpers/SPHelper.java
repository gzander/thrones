/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.wnfx.scriptfighter.utils.db_helpers;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import ru.wnfx.scriptfighter.db.core.Serializer;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.ArrayList;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import org.apache.log4j.*;
import oracle.jdbc.*;
import org.postgresql.jdbc4.Jdbc4Array;
import ru.wnfx.scriptfighter.utils.StringUtils;
import ru.wnfx.scriptfighter.engine.core.BasicScript;
import sun.org.mozilla.javascript.internal.NativeJavaObject;


/**
 *
 * @author sergk
 * реализует подключение к БД, создание хелпера к процедуре, её выполнения
 */
public class SPHelper {
        public static final Logger LOG=Logger.getLogger(SPHelper.class);
	private String driver = System.getProperty ("dispatcher.db.driver");
	private String cnString = System.getProperty ("dispatcher.db.cnstring");
	private String user = System.getProperty ("dispatcher.db.user");
	private String password = System.getProperty ("dispatcher.db.password");
        private String sqltext = System.getProperty ("dispatcher.db.sqltext");
        private String sp_name;
        private String schema_name;
        //private Collection<DB_Parameter> parameters;
        public ConnectionFactory db_connect=null;
        public ArrayList parameters1;
        public ResultSet rs;
        private CallableStatement toesUp = null;
        
        private static String[] supported_date_formats={"dd-MM-yyyy HH:mm:ss","dd/MM/yyyy HH:mm:ss","dd.MM.yyyy HH:mm:ss","yyyy.MM.dd HH:mm:ss","yyyy/MM/dd HH:mm:ss","yyyy/MM/dd HH:mm:ss","dd-MM-yyyy","dd/MM/yyyy","dd.MM.yyyy","yyyy.MM.dd","yyyy/MM/dd","yyyy/MM/dd"};

/** 
// новый экземпляр хелпера        
*/
public SPHelper(String stored_name)
{
  
  sp_name=stored_name;
  schema_name="public";
  parameters1=new ArrayList();
  //if (debug) log("MenuFilter:doFilter()");
  LOG.debug("Helper created for procedure: "+sp_name);
}

public SPHelper(String stored_name, String _schema_name)
{
  
  sp_name=stored_name;
  schema_name=_schema_name;
  parameters1=new ArrayList();
  //if (debug) log("MenuFilter:doFilter()");
  LOG.debug("Helper created for procedure: "+sp_name);
}

public static ArrayList<String> generateMethods(String _driver, String _cnString, String _user, String _password, String _invoke_name, String _connection_schema)
{
     ArrayList<SPHelper> heplers_list=new ArrayList();
     ArrayList<String> result=new ArrayList();
     if(_driver.contains("racle"))
     {
        String statement_text = "select distinct ua.package_name, ua.object_name from user_arguments ua";
        //String statement_text="select 'O_RESULT', 'REF_CURSOR', 'OUT', 1 from dual";
        //System.out.println(statement_text);
        Connection connectionPrepare = null;
        PreparedStatement statementPrepare = null;
        ResultSet ParamSet = null;
        try {
            ConnectionFactory dbst_connect=new ConnectionFactory(_driver,_cnString,_user,_password,"");
            try
            {
                dbst_connect.connect();
            }
            catch(DB_Exception dbe)
            {
               LOG.error(dbe.getMessage());
            }
            connectionPrepare = dbst_connect.connection;
            // для PostgreSQL сначала нужно создать транзакцию (AutoCommit == false)...
            //db_connect.connection.setAutoCommit(false);
            statementPrepare = connectionPrepare.prepareStatement(statement_text);
            ParamSet = statementPrepare.executeQuery();
            //int i=0;
            while (ParamSet.next()) 
            {
                DB_Parameter par = new DB_Parameter();

                //System.out.println("---- " + ParamSet.getObject(1).toString() + "---" + ParamSet.getObject(2).toString() + "--" + ParamSet.getObject(3).toString());

                String param_name=null;
                try
                {
                    if(ParamSet.getObject(1).toString()!=null)
                    {
                        param_name=ParamSet.getObject(1).toString();
                    }
                    SPHelper sh=new SPHelper(ParamSet.getString(2),ParamSet.getString(1));
                    heplers_list.add(sh);
                }
                catch(NullPointerException npe)
                {
                     System.out.println("May be function output");
                }
            }
        }
        catch (SQLException exc) 
        {
            LOG.error("Error during prepare statement: "+exc.getMessage());
            //
        }
        catch (Exception ex) 
        {
            LOG.error("Error during prepare statement: "+ex.getMessage());
            //
        }
        finally 
        {
            try
            {
                 statementPrepare.close();
                 connectionPrepare.close();
            }
            catch( SQLException SE)
            {
                LOG.error("Error during prepare statement: "+SE.getMessage());
            }
        }
     }
     else
     {
        String statement_text=_connection_schema==null?"select specific_schema, routine_name from information_schema.routines rout":"select specific_schema, routine_name from information_schema.routines rout where rout.specific_schema='"+_connection_schema+"'";
                //"SELECT pars.PARAMETER_NAME AS par_name, pars.data_type AS data_type, pars.PARAMETER_MODE AS in_out, pars.ordinal_position AS POSITION, ltrim(pars.udt_name,'_') as subtype FROM information_schema.routines rout inner join information_schema.PARAMETERS pars on (rout.specific_name=pars.specific_name) where pars.specific_schema='"+schema_name+"' and rout.specific_schema='"+schema_name+"' and rout.routine_name= '"+sp_name+"' order by 4";
        //String statement_text="select 'default' name, 'REF_CURSOR' type, 'OUT' in_out  from dual";
        PreparedStatement ParamStructure;
        ResultSet ParamSet;
        ConnectionFactory dbst_connect=new ConnectionFactory(_driver,_cnString,_user,_password,"");
        try
        {
          try
          {
          dbst_connect.connect();
          }
          catch(DB_Exception dbe)
          {
             LOG.error(dbe.getMessage());
             System.out.println((dbe.getMessage()));
          }
          // для PostgreSQL сначала нужно создать транзакцию (AutoCommit == false)...
          //db_connect.connection.setAutoCommit(false);
          //System.out.println(db_connect.connection.);
          ParamStructure=dbst_connect.connection.prepareStatement(statement_text);
          LOG.debug("param query prepared");
          //ParamStructure.setString (1, sp_name);
          ParamSet=ParamStructure.executeQuery();
          LOG.debug("param query executed");
          //int i=0;
          while(ParamSet.next())
          {   
              SPHelper sh=new SPHelper(ParamSet.getString(2),ParamSet.getString(1));
              heplers_list.add(sh);        
          }
      }
      catch (Exception exc)
      {
        // Мы должны защитить эти вызовы.
         LOG.error("Error during prepare statement: "+exc.getMessage());

      } 
      finally
      {
       try
       {
        dbst_connect.connection.close();
       }
       catch( SQLException SE)
       {
           LOG.error("Error during prepare statement: "+SE.getMessage());
       }
      }
    }
     
     for(SPHelper sh: heplers_list)
     {
         sh.Prepare(_driver, _cnString, _user, _password);
         result.add(sh.autoGenerate(_invoke_name));
     }
     
     return result;
}

public String autoGenerate(String interface_call_method)
{
    String comment="/*";
    for(Object par1: parameters1)
    {
        DB_Parameter par=(DB_Parameter) par1;
        if(par._InOut().equalsIgnoreCase("IN"))
        {
            comment+=par._name()+":"+
                    ((
                        par._type().equalsIgnoreCase("text")||
                        par._type().equalsIgnoreCase("character varying")||
                        par._type().equalsIgnoreCase("varchar")||
                        par._type().equalsIgnoreCase("varchar2")
                    )?"'text'":
                                (
                                    (
                                        par._type().equalsIgnoreCase("bool")||
                                        par._type().equalsIgnoreCase("boolean")
                                    )?"false":
                                               (
                                                    (
                                                        par._type().equalsIgnoreCase("date")||
                                                        par._type().equalsIgnoreCase("timestamp")||
                                                        par._type().equalsIgnoreCase("datetime")
                                                    )?"'2016-01-01'":"value"
                                                )
                                )
                    )+", ";
        }
    }
    if(comment.lastIndexOf(", ")==comment.length()-2)
    {
        comment=comment.substring(0, comment.length()-2);
    }
    comment+="*/";
    
    String function_name="";
    
    for(String s: sp_name.split("_"))
    {
        function_name+=(function_name.length()==0)?s:StringUtils.Capitalize(s);
    }
    
    String function_call="function "+function_name+"(obj)\n{return "+interface_call_method+"('"+schema_name+"','"+sp_name+"',{";
    for(Object par1: parameters1)
    {
        DB_Parameter par=(DB_Parameter) par1;
        if(par._InOut().equalsIgnoreCase("IN"))
        {
            function_call+=par._name()+": obj."+par._name()+", ";
        }
    }
    if(function_call.lastIndexOf(", ")==function_call.length()-2)
    {
        function_call=function_call.substring(0, function_call.length()-2);
    }
    function_call+="});\n}";
    
    return comment+"\n"+function_call;
}
/** 
// подготовка к выполнению, зачитываем параметры из базы 
*/
public void Prepare(String _driver, String _cnString, String _user, String _password)
{
    driver=_driver;
    cnString=_cnString;
    user=_user;
    password=_password;
    if(sp_name!=null)
    {
        if(driver.contains("racle"))
        {
            String statement_text = "select ua.argument_name/*decode(ua.argument_name,null,'default',ua.argument_name)*/ as par_name, ua.data_type, ua.in_out, ua.position from user_arguments ua where upper(ltrim(ua.package_name||'.'||ua.object_name,'.')) = upper(:1) order by 4";
            //String statement_text="select 'O_RESULT', 'REF_CURSOR', 'OUT', 1 from dual";
            //System.out.println(statement_text);
            Connection connectionPrepare = null;
            PreparedStatement statementPrepare = null;
            ResultSet ParamSet = null;
            try {
                ConnectionFactory dbst_connect=new ConnectionFactory(driver,cnString,user,password,sqltext);
                try
                {
                    dbst_connect.connect();
                }
                catch(DB_Exception dbe)
                {
                   LOG.error(dbe.getMessage());
                }
                connectionPrepare = dbst_connect.connection;
                // для PostgreSQL сначала нужно создать транзакцию (AutoCommit == false)...
                //db_connect.connection.setAutoCommit(false);
                statementPrepare = connectionPrepare.prepareStatement(statement_text);
                statementPrepare.setString(1, sp_name);
                ParamSet = statementPrepare.executeQuery();
                //int i=0;
                while (ParamSet.next()) 
                {
                    DB_Parameter par = new DB_Parameter();

                    //System.out.println("---- " + ParamSet.getObject(1).toString() + "---" + ParamSet.getObject(2).toString() + "--" + ParamSet.getObject(3).toString());

                    String param_name=null;
                    try
                    {
                        if(ParamSet.getObject(1).toString()!=null)
                        {
                            param_name=ParamSet.getObject(1).toString();
                        }
                    }
                    catch(NullPointerException npe)
                    {
                         System.out.println("May be function output");
                    }

                    if(ParamSet.wasNull())
                    {
                        par.SetParameterReturn(param_name, null, ParamSet.getObject(2).toString(), ParamSet.getObject(3).toString());
                    }
                    else
                    {
                        par.SetParameter(param_name, null, ParamSet.getObject(2).toString(), ParamSet.getObject(3).toString());
                    }
                    //parameters.add(par);
                    try 
                    {
                        parameters1.add(par);
                    } 
                    catch (Exception exc) 
                    {
                        System.out.println(exc.getMessage() + "????");
                    }
                }
            }
            catch (SQLException exc) 
            {
                LOG.error("Error during prepare statement: "+exc.getMessage());
                //
            }
            catch (Exception ex) 
            {
                LOG.error("Error during prepare statement: "+ex.getMessage());
                //
            }
            finally 
            {
                try
                {
                     statementPrepare.close();
                     connectionPrepare.close();
                }
                catch( SQLException SE)
                {
                    LOG.error("Error during prepare statement: "+SE.getMessage());
                }
            }
        }
        else
        {
            String statement_text="SELECT pars.PARAMETER_NAME AS par_name, pars.data_type AS data_type, pars.PARAMETER_MODE AS in_out, pars.ordinal_position AS POSITION, ltrim(pars.udt_name,'_') as subtype FROM information_schema.routines rout inner join information_schema.PARAMETERS pars on (rout.specific_name=pars.specific_name) where pars.specific_schema='"+schema_name+"' and rout.specific_schema='"+schema_name+"' and rout.routine_name= '"+sp_name+"' order by 4";
            //String statement_text="select 'default' name, 'REF_CURSOR' type, 'OUT' in_out  from dual";
            PreparedStatement ParamStructure;
            ResultSet ParamSet;
            ConnectionFactory dbst_connect=new ConnectionFactory(driver,cnString,user,password,sqltext);
            try
            {
              try
              {
              dbst_connect.connect();
              }
              catch(DB_Exception dbe)
              {
                 LOG.error(dbe.getMessage());
                 System.out.println((dbe.getMessage()));
              }
              // для PostgreSQL сначала нужно создать транзакцию (AutoCommit == false)...
              //db_connect.connection.setAutoCommit(false);
              //System.out.println(db_connect.connection.);
              ParamStructure=dbst_connect.connection.prepareStatement(statement_text);
              LOG.debug("param query prepared");
              //ParamStructure.setString (1, sp_name);
              ParamSet=ParamStructure.executeQuery();
              LOG.debug("param query executed");
              //int i=0;
              while(ParamSet.next())
              {   
                  DB_Parameter par=new DB_Parameter();
                  LOG.info(ParamSet.getObject(1).toString()+", "+ParamSet.getObject(2).toString()+", "+ParamSet.getObject(3).toString()+", "+ParamSet.getObject(5).toString());
                  par.SetParameter(ParamSet.getObject(1).toString(),null, ParamSet.getObject(2).toString(),ParamSet.getObject(3).toString(), ParamSet.getObject(5).toString());
                  //parameters.add(par);
                  try
                  {
                      parameters1.add(par);
                  }
                  catch(Exception exc)
                  {
                      LOG.error(exc.getMessage()+". Cant't parse params");   
                  }            
              }
          }
          catch (Exception exc)
          {
            // Мы должны защитить эти вызовы.
             LOG.error("Error during prepare statement: "+exc.getMessage());

          } 
          finally
          {
           LOG.info("Prepare of "+sp_name+" finished");
           try
           {
            dbst_connect.connection.close();
           }
           catch( SQLException SE)
           {
               LOG.error("Error during prepare statement: "+SE.getMessage());
           }
          }
        }
    }
}

//задание параметров
public void SetParams(String param1)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    LOG.debug("Set params (1) passed");
    
}

public void _SetParams(Object param1)
{
    if(param1.getClass() == String.class)
    {
        DB_Parameter par=new DB_Parameter();
        par=(DB_Parameter)parameters1.get(0);
        par.SetValue(param1.toString());
        parameters1.set(0, par);
        LOG.debug("Set params (1) passed");
    }
}

//задание параметров
public void SetParams(String param1, String param2)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    LOG.debug("Set params (2) passed");
    
}


//задание параметров
public void SetParams(String param1, String param2, String param3)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par);    
    
    LOG.debug("Set params (3) passed");
    
}
 
public void SetParams(String param1, String param2, String param3, String param4)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par); 
    
    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);     
    
    LOG.debug("Set params (4) passed");
    
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par); 
    
    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);     
    
    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);     
    
    LOG.debug("Set params (5) passed");
    
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par); 
    
    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);     
    
    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);     
    
    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);    
    
    LOG.debug("Set params (6) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par); 
    
    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);     
    
    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);     
    
    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);   
    
    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);   
    
    LOG.debug("Set params (7) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par); 
    
    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);     
    
    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);     
    
    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);   
    
    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);
    
    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);
    
    LOG.debug("Set params (8) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8, String param9)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par); 
    
    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);     
    
    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);     
    
    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);   
    
    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);
    
    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);
    
    par=(DB_Parameter)parameters1.get(8);
    par.SetValue(param9);
    parameters1.set(8, par);
    
    LOG.debug("Set params (9) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8, String param9, String param10)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par); 
    
    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);     
    
    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);     
    
    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);   
    
    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);
    
    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);
    
    par=(DB_Parameter)parameters1.get(8);
    par.SetValue(param9);
    parameters1.set(8, par);
    
    par=(DB_Parameter)parameters1.get(9);
    par.SetValue(param10);
    parameters1.set(9, par);    
    
    LOG.debug("Set params (10) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8, String param9, String param10, String param11)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par); 
    
    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);     
    
    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);     
    
    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);   
    
    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);
    
    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);
    
    par=(DB_Parameter)parameters1.get(8);
    par.SetValue(param9);
    parameters1.set(8, par);
    
    par=(DB_Parameter)parameters1.get(9);
    par.SetValue(param10);
    parameters1.set(9, par);    
    
    par=(DB_Parameter)parameters1.get(10);
    par.SetValue(param11);
    parameters1.set(10, par);     
    
    LOG.debug("Set params (11) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8, String param9, String param10, String param11, String param12)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par); 
    
    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);     
    
    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);     
    
    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);   
    
    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);
    
    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);
    
    par=(DB_Parameter)parameters1.get(8);
    par.SetValue(param9);
    parameters1.set(8, par);
    
    par=(DB_Parameter)parameters1.get(9);
    par.SetValue(param10);
    parameters1.set(9, par);    
    
    par=(DB_Parameter)parameters1.get(10);
    par.SetValue(param11);
    parameters1.set(10, par);    
    
    par=(DB_Parameter)parameters1.get(11);
    par.SetValue(param12);
    parameters1.set(11, par);    
    
    LOG.debug("Set params (12) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8, String param9, String param10, String param11, String param12, String param13)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par); 
    
    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);     
    
    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);     
    
    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);   
    
    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);
    
    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);
    
    par=(DB_Parameter)parameters1.get(8);
    par.SetValue(param9);
    parameters1.set(8, par);
    
    par=(DB_Parameter)parameters1.get(9);
    par.SetValue(param10);
    parameters1.set(9, par);    
    
    par=(DB_Parameter)parameters1.get(10);
    par.SetValue(param11);
    parameters1.set(10, par);    
    
    par=(DB_Parameter)parameters1.get(11);
    par.SetValue(param12);
    parameters1.set(11, par);    
    
    par=(DB_Parameter)parameters1.get(12);
    par.SetValue(param13);
    parameters1.set(12, par);        
    
    LOG.debug("Set params (13) passed");
}


public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8, String param9, String param10, String param11, String param12, String param13, String param14)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);
    
    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);
    
    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par); 
    
    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);     
    
    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);     
    
    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);   
    
    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);
    
    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);
    
    par=(DB_Parameter)parameters1.get(8);
    par.SetValue(param9);
    parameters1.set(8, par);
    
    par=(DB_Parameter)parameters1.get(9);
    par.SetValue(param10);
    parameters1.set(9, par);    
    
    par=(DB_Parameter)parameters1.get(10);
    par.SetValue(param11);
    parameters1.set(10, par);    
    
    par=(DB_Parameter)parameters1.get(11);
    par.SetValue(param12);
    parameters1.set(11, par);    
    
    par=(DB_Parameter)parameters1.get(12);
    par.SetValue(param13);
    parameters1.set(12, par);     
    
    par=(DB_Parameter)parameters1.get(13);
    par.SetValue(param14);
    parameters1.set(13, par);       
    
    LOG.debug("Set params (13) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8, String param9, String param10, String param11, String param12, String param13, String param14,
                        String param15, String param16, String param17, String param18, String param19)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);

    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);

    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par);

    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);

    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);

    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);

    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);

    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);

    par=(DB_Parameter)parameters1.get(8);
    par.SetValue(param9);
    parameters1.set(8, par);

    par=(DB_Parameter)parameters1.get(9);
    par.SetValue(param10);
    parameters1.set(9, par);

    par=(DB_Parameter)parameters1.get(10);
    par.SetValue(param11);
    parameters1.set(10, par);

    par=(DB_Parameter)parameters1.get(11);
    par.SetValue(param12);
    parameters1.set(11, par);

    par=(DB_Parameter)parameters1.get(12);
    par.SetValue(param13);
    parameters1.set(12, par);

    par=(DB_Parameter)parameters1.get(13);
    par.SetValue(param14);
    parameters1.set(13, par);

    par=(DB_Parameter)parameters1.get(14);
    par.SetValue(param15);
    parameters1.set(14, par);

    par=(DB_Parameter)parameters1.get(15);
    par.SetValue(param16);
    parameters1.set(15, par);

    par=(DB_Parameter)parameters1.get(16);
    par.SetValue(param17);
    parameters1.set(16, par);

    par=(DB_Parameter)parameters1.get(17);
    par.SetValue(param18);
    parameters1.set(17, par);

    par=(DB_Parameter)parameters1.get(18);
    par.SetValue(param19);
    parameters1.set(18, par);

    LOG.debug("Set params (19) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8, String param9, String param10, String param11, String param12, String param13, String param14,
                        String param15, String param16, String param17, String param18)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);

    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);

    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par);

    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);

    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);

    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);

    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);

    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);

    par=(DB_Parameter)parameters1.get(8);
    par.SetValue(param9);
    parameters1.set(8, par);

    par=(DB_Parameter)parameters1.get(9);
    par.SetValue(param10);
    parameters1.set(9, par);

    par=(DB_Parameter)parameters1.get(10);
    par.SetValue(param11);
    parameters1.set(10, par);

    par=(DB_Parameter)parameters1.get(11);
    par.SetValue(param12);
    parameters1.set(11, par);

    par=(DB_Parameter)parameters1.get(12);
    par.SetValue(param13);
    parameters1.set(12, par);

    par=(DB_Parameter)parameters1.get(13);
    par.SetValue(param14);
    parameters1.set(13, par);

    par=(DB_Parameter)parameters1.get(14);
    par.SetValue(param15);
    parameters1.set(14, par);

    par=(DB_Parameter)parameters1.get(15);
    par.SetValue(param16);
    parameters1.set(15, par);

    par=(DB_Parameter)parameters1.get(16);
    par.SetValue(param17);
    parameters1.set(16, par);

    par=(DB_Parameter)parameters1.get(17);
    par.SetValue(param18);
    parameters1.set(17, par);


    LOG.debug("Set params (18) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8, String param9, String param10, String param11, String param12, String param13, String param14,
                        String param15, String param16, String param17, String param18, String param19, String param20)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);

    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);

    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par);

    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);

    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);

    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);

    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);

    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);

    par=(DB_Parameter)parameters1.get(8);
    par.SetValue(param9);
    parameters1.set(8, par);

    par=(DB_Parameter)parameters1.get(9);
    par.SetValue(param10);
    parameters1.set(9, par);

    par=(DB_Parameter)parameters1.get(10);
    par.SetValue(param11);
    parameters1.set(10, par);

    par=(DB_Parameter)parameters1.get(11);
    par.SetValue(param12);
    parameters1.set(11, par);

    par=(DB_Parameter)parameters1.get(12);
    par.SetValue(param13);
    parameters1.set(12, par);

    par=(DB_Parameter)parameters1.get(13);
    par.SetValue(param14);
    parameters1.set(13, par);

    par=(DB_Parameter)parameters1.get(14);
    par.SetValue(param15);
    parameters1.set(14, par);

    par=(DB_Parameter)parameters1.get(15);
    par.SetValue(param16);
    parameters1.set(15, par);

    par=(DB_Parameter)parameters1.get(16);
    par.SetValue(param17);
    parameters1.set(16, par);

    par=(DB_Parameter)parameters1.get(17);
    par.SetValue(param18);
    parameters1.set(17, par);

    par=(DB_Parameter)parameters1.get(18);
    par.SetValue(param19);
    parameters1.set(18, par);

    par=(DB_Parameter)parameters1.get(19);
    par.SetValue(param20);
    parameters1.set(19, par);

    LOG.debug("Set params (20) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8, String param9, String param10, String param11, String param12, String param13, String param14,
                        String param15, String param16, String param17, String param18, String param19, String param20, String param21)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);

    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);

    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par);

    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);

    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);

    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);

    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);

    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);

    par=(DB_Parameter)parameters1.get(8);
    par.SetValue(param9);
    parameters1.set(8, par);

    par=(DB_Parameter)parameters1.get(9);
    par.SetValue(param10);
    parameters1.set(9, par);

    par=(DB_Parameter)parameters1.get(10);
    par.SetValue(param11);
    parameters1.set(10, par);

    par=(DB_Parameter)parameters1.get(11);
    par.SetValue(param12);
    parameters1.set(11, par);

    par=(DB_Parameter)parameters1.get(12);
    par.SetValue(param13);
    parameters1.set(12, par);

    par=(DB_Parameter)parameters1.get(13);
    par.SetValue(param14);
    parameters1.set(13, par);

    par=(DB_Parameter)parameters1.get(14);
    par.SetValue(param15);
    parameters1.set(14, par);

    par=(DB_Parameter)parameters1.get(15);
    par.SetValue(param16);
    parameters1.set(15, par);

    par=(DB_Parameter)parameters1.get(16);
    par.SetValue(param17);
    parameters1.set(16, par);

    par=(DB_Parameter)parameters1.get(17);
    par.SetValue(param18);
    parameters1.set(17, par);

    par=(DB_Parameter)parameters1.get(18);
    par.SetValue(param19);
    parameters1.set(18, par);

    par=(DB_Parameter)parameters1.get(19);
    par.SetValue(param20);
    parameters1.set(19, par);

    par=(DB_Parameter)parameters1.get(20);
    par.SetValue(param21);
    parameters1.set(20, par);

    LOG.debug("Set params (20) passed");
}

public void SetParams(String param1, String param2, String param3, String param4,  String param5, String param6, String param7, String param8, String param9, String param10, String param11, String param12, String param13, String param14,
                        String param15, String param16, String param17, String param18, String param19, String param20, String param21, String param22)
{

    DB_Parameter par=new DB_Parameter();
    par=(DB_Parameter)parameters1.get(0);
    par.SetValue(param1);
    parameters1.set(0, par);

    par=(DB_Parameter)parameters1.get(1);
    par.SetValue(param2);
    parameters1.set(1, par);

    par=(DB_Parameter)parameters1.get(2);
    par.SetValue(param3);
    parameters1.set(2, par);

    par=(DB_Parameter)parameters1.get(3);
    par.SetValue(param4);
    parameters1.set(3, par);

    par=(DB_Parameter)parameters1.get(4);
    par.SetValue(param5);
    parameters1.set(4, par);

    par=(DB_Parameter)parameters1.get(5);
    par.SetValue(param6);
    parameters1.set(5, par);

    par=(DB_Parameter)parameters1.get(6);
    par.SetValue(param7);
    parameters1.set(6, par);

    par=(DB_Parameter)parameters1.get(7);
    par.SetValue(param8);
    parameters1.set(7, par);

    par=(DB_Parameter)parameters1.get(8);
    par.SetValue(param9);
    parameters1.set(8, par);

    par=(DB_Parameter)parameters1.get(9);
    par.SetValue(param10);
    parameters1.set(9, par);

    par=(DB_Parameter)parameters1.get(10);
    par.SetValue(param11);
    parameters1.set(10, par);

    par=(DB_Parameter)parameters1.get(11);
    par.SetValue(param12);
    parameters1.set(11, par);

    par=(DB_Parameter)parameters1.get(12);
    par.SetValue(param13);
    parameters1.set(12, par);

    par=(DB_Parameter)parameters1.get(13);
    par.SetValue(param14);
    parameters1.set(13, par);

    par=(DB_Parameter)parameters1.get(14);
    par.SetValue(param15);
    parameters1.set(14, par);

    par=(DB_Parameter)parameters1.get(15);
    par.SetValue(param16);
    parameters1.set(15, par);

    par=(DB_Parameter)parameters1.get(16);
    par.SetValue(param17);
    parameters1.set(16, par);

    par=(DB_Parameter)parameters1.get(17);
    par.SetValue(param18);
    parameters1.set(17, par);

    par=(DB_Parameter)parameters1.get(18);
    par.SetValue(param19);
    parameters1.set(18, par);

    par=(DB_Parameter)parameters1.get(19);
    par.SetValue(param20);
    parameters1.set(19, par);

    par=(DB_Parameter)parameters1.get(20);
    par.SetValue(param21);
    parameters1.set(20, par);

    par=(DB_Parameter)parameters1.get(21);
    par.SetValue(param22);
    parameters1.set(21, par);

    LOG.debug("Set params (20) passed");
}

public int[] toIntArray(String[] numberStrs)
{
    int[] numbers = new int[numberStrs.length];
    for(int i = 0;i < numberStrs.length;i++)
    {
       // Note that this is assuming valid input
       // If you want to check then add a try/catch 
       // and another index for the numbers if to continue adding the others
       numbers[i] = StringUtils.tryInteger(numberStrs[i]);//Integer.parseInt(numberStrs[i]);
    }
    
    return numbers;
}

public void prepareStatementToCall()  throws SQLException, ParseException
{ 
    String CallStatement;
    if(driver.contains("racle"))
    {
        boolean is_function=false;
        int i_par=-1;
        CallStatement="{call "+sp_name+"(";
        for (int i = 0; i < parameters1.size(); i++) 
        {
            //CallStatement += ":" + (i + 1) + ",";
            CallStatement += "?" + ",";
            if(((DB_Parameter)parameters1.get(i)).is_return)
            {
                is_function=true;
                i_par=i;
                break;
            }
        }
        
        
        if(is_function)
        {
           //CallStatement="{:"+(i_par+1)+" = call "+sp_name+"(";
            CallStatement="{? = call "+sp_name+"(";
            for (int i = 0; i < parameters1.size(); i++) 
            {
                if(!((DB_Parameter)parameters1.get(i)).is_return)
                {
                    //CallStatement += ":" + (i + 1) + ",";
                    CallStatement += "?" + ",";
                }
            }
        }
        
        if(!CallStatement.endsWith("("))
        {
            CallStatement = CallStatement.substring(0, CallStatement.length() - 1);
        }
        CallStatement += ")}";
    }
    else
    {
        CallStatement="{call "+schema_name+"."+sp_name+"(";
        for(int i=0;i<parameters1.size();i++)
        {
            //CallStatement+=":"+(i+1)+",";
            CallStatement+="?,";
        }
        CallStatement=CallStatement.substring(0, CallStatement.length()-1);
        CallStatement+=")}";
    }

    toesUp = null;
    LOG.info(CallStatement);
    db_connect=new ConnectionFactory(driver,cnString,user,password,sqltext);

    // для PostgreSQL сначала нужно создать транзакцию (AutoCommit == false)...
        try
        {
        db_connect.connect();
        //db_connect.connection.setAutoCommit(false);
        }
        catch(DB_Exception dbe)
        {
            LOG.error("Connect failed: "+dbe.getMessage());
        }

        // Настраиваем вызов.
        toesUp  = db_connect.connection.prepareCall(CallStatement);
}

public void setParameterWithId(int id, Object value)  throws SQLException, ParseException
{
    String name=((DB_Parameter)parameters1.get(id))._name();
    setParameterWithName(name, value);
}

public static Float adjustFloat(Object value)
{
    Float tmp_value;
    if(value instanceof Double)
        tmp_value=((Double)value).floatValue();
    else if(value instanceof Integer)
        tmp_value=((Integer)value).floatValue();
    else if(value instanceof String)
        tmp_value=Float.parseFloat((String)value);
    else if(value instanceof Float)
        tmp_value=(Float)value;
    else
        tmp_value=null;
    
    return tmp_value;
}

public static Integer adjustInt(Object value)
{
    Integer tmp_value=null;
    if(value instanceof Double)
        tmp_value=((Double)value).intValue();
    else if(value instanceof Integer)
        tmp_value=((Integer)value).intValue();
    else if(value instanceof String)
        tmp_value=Integer.parseInt((String)value);
    else if(value instanceof Float)
        tmp_value=(Integer)value;
    else
        tmp_value=null;
    
    return tmp_value;
}

public static java.sql.Date adjustDate(Object value)
{
    java.sql.Date tmp_value=null;
    if(value instanceof Double)
    {
        tmp_value=new java.sql.Date(((Double)value).intValue());
    }
    else if(value instanceof Integer)
    {
        tmp_value=new java.sql.Date(((Integer)value).intValue());
    }
    else if(value instanceof String)
    {
        for(String format: supported_date_formats)
        {
            DateFormat df = new SimpleDateFormat(format);
            LOG.debug("Trying to get date");
            try
            {
                tmp_value=new java.sql.Date((df.parse((String)value)).getTime());   
                break;
            } 
            catch (ParseException e)
            {
                LOG.error("No correct data format");
            }
        }
    }
    else if(value instanceof Float)
    {
        tmp_value=new java.sql.Date(((Float)value).intValue());
    }
    else if(value instanceof Date)
    {
        tmp_value=new java.sql.Date(((Date)value).getTime());
    }
    else
        tmp_value=null;
    
    return tmp_value;
}

public void setParameterWithName(String name, Object value)  throws SQLException, ParseException
{
    if(driver.contains("racle"))
        {
            for (int i = 0; i < parameters1.size(); i++) 
            {
                if(((DB_Parameter)parameters1.get(i))._name().equalsIgnoreCase(name))
                {
                    DB_Parameter par = null;//new DB_Parameter();
                    par = (DB_Parameter) parameters1.get(i);
                    System.out.println("PARAM: " + par._name() + ", type: " + par._type() + ", inout: " + par._InOut());
                    
                    if(value!=null)
                        par.SetValue(value.toString());

                    if (par._type().contains("VARCHAR")) 
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            if(value!=null)
                            {
                                toesUp.setString(i + 1, value.toString());
                                System.out.println("bind parameters:" + par._name() + " = " + par._value());
                            }
                            else
                                toesUp.setNull(i+1, Types.VARCHAR);
                        }
                    }
                    if (par._type().contains("NUMBER")) 
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            if(value!=null)
                            {
                                toesUp.setFloat(i + 1, adjustFloat(value));
                                System.out.println("bind parameters:" + par._name() + " = " + par._value());
                            }
                            else
                                toesUp.setNull(i+1, Types.FLOAT);
                        }
                    }
                    if (par._type().contains("DATE")) 
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            //DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                            //long t = df.parse(par._value()).getTime();
                            //java.sql.Date dt = new java.sql.Date(t);
                            if(value!=null)
                            {
                                toesUp.setDate(i + 1, adjustDate(value));
                                System.out.println("bind parameters:" + par._name() + " = " + par._value());
                            }
                            else
                                toesUp.setNull(i+1, Types.DATE);
                        }
                    }
                }
            }
        }
        else
        {
            for(int i=0;i<parameters1.size();i++)
            {
                if(((DB_Parameter)parameters1.get(i))._name().equalsIgnoreCase(name))
                {
                    DB_Parameter par = null;//new DB_Parameter();
                    par = (DB_Parameter) parameters1.get(i);
                    System.out.println("PARAM: " + par._name() + ", type: " + par._type() + ", inout: " + par._InOut());
                    
                    if(value!=null)
                    {
                        if(value instanceof NativeJavaObject)
                        {
                            NativeJavaObject n=(NativeJavaObject)value;
                            Object object = n.unwrap();
                            par.SetValue((String)object);
                        }
                        else
                        {
                            par.SetValue(value.toString());
                        }
                    }
                    
                    if(par._type().equalsIgnoreCase("character varying")||par._type().equalsIgnoreCase("text"))
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            try
                            {
                                if(par._value()!=null)
                                {
                                    toesUp.setString(i+1, par._value());
                                    LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                                }
                                else
                                toesUp.setNull(i+1, Types.VARCHAR);
                            }
                            catch(Exception Ex)
                            {
                                LOG.debug("Bind exception: "+Ex.getMessage());
                                toesUp.setNull(i+1, Types.VARCHAR);
                            }
                        }
                    }
                    if(par._type().equalsIgnoreCase("numeric"))
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            try
                            {
                                if(par._value()!=null)
                                {
                                    toesUp.setBigDecimal(i+1, BigDecimal.valueOf(Float.parseFloat(par._value())));
                                    LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                                }
                                else
                                toesUp.setNull(i+1, Types.NUMERIC);
                            }
                            catch(Exception Ex)
                            {
                                LOG.debug("Bind exception: "+Ex.getMessage());
                                toesUp.setNull(i+1, Types.NUMERIC);
                            }

                        }
                    }
                    if(par._type().equalsIgnoreCase("double precision"))
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            try
                            {
                                if(par._value()!=null)
                                {
                                    toesUp.setDouble(i+1, Float.parseFloat(par._value()));
                                    LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                                }
                                else
                                toesUp.setNull(i+1, Types.DOUBLE);
                            }
                            catch(Exception Ex)
                            {
                                LOG.debug("Bind exception: "+Ex.getMessage());
                                toesUp.setNull(i+1, Types.DOUBLE);
                            }

                        }
                    }
                    if(par._type().equalsIgnoreCase("date"))
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            try
                            {
                                if(par._value()!=null)
                                {
                                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                                    long t = df.parse(par._value()).getTime();
                                    java.sql.Date dt = new java.sql.Date(t);
                                    toesUp.setDate(i+1, dt);
                                    LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                                }
                                else
                                toesUp.setNull(i+1, Types.DATE);
                            }
                            catch(Exception Ex)
                            {
                                LOG.debug("Bind exception: "+Ex.getMessage());
                                toesUp.setNull(i+1, Types.DATE);
                            }
                        }
                    }
                    if(par._type().equalsIgnoreCase("boolean"))
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            try
                            {
                                if(par._value()!=null)
                                {
                                    boolean des;
                                    if(par._value().equalsIgnoreCase("true")||par._value().equalsIgnoreCase("Yes")||par._value().equalsIgnoreCase("on"))
                                        des=true;
                                    else
                                        des=false;
                                    toesUp.setBoolean(i+1, des);
                                    LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                                }
                                else
                                toesUp.setNull(i+1, Types.BOOLEAN);
                            }
                            catch(Exception Ex)
                            {
                                LOG.debug("Bind exception: "+Ex.getMessage());
                                toesUp.setNull(i+1, Types.BOOLEAN);
                            }
                        }
                    }
                    if(par._type().equalsIgnoreCase("bigint"))
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            try
                            {
                                if(par._value()!=null)
                                {
                                    toesUp.setLong(i+1, StringUtils.tryInteger(par._value()));
                                    LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                                }
                                else
                                toesUp.setNull(i+1, Types.BIGINT);
                            }
                            catch(Exception Ex)
                            {
                                LOG.debug("Bind exception: "+Ex.getMessage());
                                toesUp.setNull(i+1, Types.BIGINT);
                            }

                        }
                    }
                    if(par._type().equalsIgnoreCase("integer"))
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            try
                            {
                                if(par._value()!=null)
                                {
                                    toesUp.setInt(i+1, StringUtils.tryInteger(par._value()));
                                    LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                                }
                                else
                                toesUp.setNull(i+1, Types.INTEGER);
                            }
                            catch(Exception Ex)
                            {
                                LOG.debug("Bind exception: "+Ex.getMessage());
                                toesUp.setNull(i+1, Types.INTEGER);
                            }
                        }
                    }
                    if(par._type().equalsIgnoreCase("smallint"))
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            try
                            {
                                if(par._value()!=null)
                                {
                                    toesUp.setShort(i+1, StringUtils.tryShort(par._value()));
                                    LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                                }
                                else
                                toesUp.setNull(i+1, Types.SMALLINT);
                            }
                            catch(Exception Ex)
                            {
                                LOG.debug("Bind exception: "+Ex.getMessage());
                                toesUp.setNull(i+1, Types.SMALLINT);
                            }

                        }
                    }
                    if(par._type().equalsIgnoreCase("ARRAY"))
                    {
                        if (!par._InOut().contains("OUT")) 
                        {
                            try
                            {
                                if(par._value()!=null)
                                {
                                    //toesUp.setShort(i+1, Short.parseShort(par._value()));
                                    if(par._subtype().contains("char"))
                                        toesUp.setObject(i+1,  new PostgreSQLTextArray(par._value().split(";")), Types.ARRAY);
                                    else if(par._subtype().contains("int"))
                                        toesUp.setObject(i+1,  new PostgreSQLInt4Array(toIntArray(par._value().split(";"))), Types.ARRAY);
                                    LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                                }
                                else
                                toesUp.setNull(i+1, Types.ARRAY);
                            }
                            catch(Exception Ex)
                            {
                                LOG.debug("Bind exception: "+Ex.getMessage());
                                toesUp.setNull(i+1, Types.ARRAY);
                            }

                        }
                    }  
                }              
            }
        }
}

public void ExecForce()   throws SQLException, ParseException
{
    //LOG.debug(autoGenerate("invokestaytonight"));
    SimpleDateFormat timestampFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    // доразбор out-параметров
    if(driver.contains("racle"))
        {
            for (int i = 0; i < parameters1.size(); i++) 
            {
                DB_Parameter par = null;//new DB_Parameter();
                par = (DB_Parameter) parameters1.get(i);
                System.out.println("PARAM: " + par._name() + ", type: " + par._type() + ", inout: " + par._InOut());

                if (par._type().contains("REF CURSOR")) {
                    System.out.println("REFCURSOR FOUND");
                    if (par._InOut().contains("OUT")) 
                    {
                        System.out.println("OUT_REFCURSOR FOUND");
                        toesUp.registerOutParameter(i + 1, oracle.jdbc.driver.OracleTypes.CURSOR);
                    }
                }
                if (par._type().contains("VARCHAR")) 
                {
                    if (par._InOut().contains("OUT")) 
                    {
                        toesUp.registerOutParameter(i + 1, Types.VARCHAR);
                    } 
                }
                if (par._type().contains("NUMBER")) 
                {
                    if (par._InOut().contains("OUT")) 
                    {
                        toesUp.registerOutParameter(i + 1, Types.NUMERIC);
                    } 
                }
                if (par._type().contains("BINARY_INTEGER")) 
                {
                    if (par._InOut().contains("OUT")) 
                    {
                        toesUp.registerOutParameter(i + 1, Types.INTEGER);
                    } 
                }
                if (par._type().contains("DATE")) 
                {
                    if (par._InOut().contains("OUT")) 
                    {
                        toesUp.registerOutParameter(i + 1, Types.DATE);
                    } 
                }
            }
        }
        else
        {
            for(int i=0;i<parameters1.size();i++)
            {
                DB_Parameter par=new DB_Parameter();
                par=(DB_Parameter)parameters1.get(i);
                LOG.debug("PARAM: "+par._name()+", type: "+par._type()+", inout: "+par._InOut());

                if(par._type().equalsIgnoreCase("refcursor"))
                {
                    LOG.debug("REFCURSOR FOUND");
                     if(par._InOut().equalsIgnoreCase("OUT"))
                     {
                        LOG.debug("OUT_REFCURSOR FOUND");
                        db_connect.connection.setAutoCommit(false);
                        toesUp.registerOutParameter(i+1, Types.OTHER);
                     }
                }
                if(par._type().equalsIgnoreCase("REF CURSOR"))
                {
                    LOG.debug("ORACLE REFCURSOR FOUND");
                     if(par._InOut().equalsIgnoreCase("OUT"))
                     {
                        LOG.debug("OUT_REFCURSOR FOUND");
                        //db_connect.connection.setAutoCommit(false);
                        toesUp.registerOutParameter(i+1, Types.OTHER);
                     }
                }            
                if(par._type().equalsIgnoreCase("character varying"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.VARCHAR);
                    }
                }
                if(par._type().equalsIgnoreCase("numeric"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.NUMERIC);
                    }
                }
                if(par._type().equalsIgnoreCase("double precision"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.DOUBLE);
                    }
                }
                if(par._type().equalsIgnoreCase("date"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.DATE);
                    }
                }
                if(par._type().equalsIgnoreCase("boolean"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.BOOLEAN);
                    }
                }
                if(par._type().equalsIgnoreCase("bigint"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.BIGINT);
                    }
                }
                if(par._type().equalsIgnoreCase("integer"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.INTEGER);
                    }
                }
                if(par._type().equalsIgnoreCase("smallint"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.SMALLINT);
                    }
                }
                if(par._type().equalsIgnoreCase("ARRAY"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.ARRAY);
                    }
                }                
            }
        }
        //выполняем запрос
        try
        {
            toesUp.execute();
        }
        finally
        {
            java.sql.SQLWarning warning = toesUp.getWarnings();

            if (warning != null) {
                while (warning != null) {
                    System.out.println("DB Message: " + warning.getMessage());
                    warning = warning.getNextWarning();
                }
            }
        }
 
        LOG.debug("Trying to parse params");
        //разбор OUT-параметров
        if(driver.contains("racle"))
        {
            for (int i = 0; i < parameters1.size(); i++) 
            {
                    DB_Parameter par = new DB_Parameter();
                    par = (DB_Parameter) parameters1.get(i);
                    try
                    {
                        if (par._InOut().contains("OUT")) 
                        {
                            System.out.println("OUT param was found");
                            if (par._type().contains("REF CURSOR")) 
                            {
                                System.out.println("REF CURSOR was found");
                                par.cursor = (ResultSet) toesUp.getObject(i + 1); //par._name()
                                System.out.println("GettingObject");
                            }
                            if (par._type().contains("VARCHAR")) 
                            {
                                par.SetValue(toesUp.getString(i + 1));
                            }
                            if (par._type().contains("NUMBER")) 
                            {
                                par.SetValue(Float.toString(toesUp.getFloat(i + 1)));
                                if (par._value().endsWith(".0"))// это значит, что используется тип NUMBER для кодировки чисел INTEGER
                                {
                                    par.SetValue(par._value().replace(".0", ""));
                                }
                            }
                            if (par._type().contains("BINARY_INTEGER")) 
                            {
                                par.SetValue(Integer.toString(toesUp.getInt(i + 1)));
                                if (par._value().endsWith(".0"))// это значит, что используется тип NUMBER для кодировки чисел INTEGER
                                {
                                    par.SetValue(par._value().replace(".0", ""));
                                }
                            }
                            if (par._type().contains("DATE")) 
                            {
                                par.SetValue(toesUp.getDate(i + 1).toString());
                            }
                        }
                    }
                    catch(NullPointerException Ex)
                    {
                        par.SetValue(""); // если вдруг случился null
                    }
                }
        }
        else
        {
            for(int i=0;i<parameters1.size();i++)
            {
                DB_Parameter par=new DB_Parameter();
                par=(DB_Parameter)parameters1.get(i);
                try
                    {
                        if(par._InOut().equalsIgnoreCase("OUT"))
                        {
                            LOG.debug("OUT param was found");
                            if(par._type().equalsIgnoreCase("refcursor"))
                            {
                                LOG.debug("REF CURSOR was found");
                                par.cursor = (ResultSet) toesUp.getObject(i+1); //par._name()
                            }
                            if(par._type().equalsIgnoreCase("character varying"))
                            {
                                par.SetValue(toesUp.getString(i+1));
                                if(toesUp.wasNull())
                                {
                                    par.SetValue("");
                                }
                            }
                            if(par._type().equalsIgnoreCase("numeric"))
                            {
                                par.SetValue(Float.toString(toesUp.getFloat(i+1)));
                                if(par._value().endsWith(".0"))// это значит, что используется тип NUMBER для кодировки чисел INTEGER
                                {
                                    par.SetValue(par._value().replace(".0", ""));
                                }
                            }
                            if(par._type().equalsIgnoreCase("double precision"))
                            {
                                par.SetValue(Double.toString(toesUp.getDouble(i+1)));
                                if(par._value().endsWith(".0"))// это значит, что используется тип NUMBER для кодировки чисел INTEGER
                                {
                                    par.SetValue(par._value().replace(".0", ""));
                                }
                            }
                            if(par._type().equalsIgnoreCase("bigint"))
                            {
                                par.SetValue(Long.toString(toesUp.getLong(i+1)));
                            }
                            if(par._type().equalsIgnoreCase("integer"))
                            {
                                par.SetValue(Integer.toString(toesUp.getInt(i+1)));
                            }
                            if(par._type().equalsIgnoreCase("smallint"))
                            {
                                par.SetValue(Integer.toString(toesUp.getShort(i+1)));
                            }
                            if(par._type().equalsIgnoreCase("boolean"))
                            {
                                par.SetValue(Boolean.toString(toesUp.getBoolean(i+1)));
                            }
                            if(par._type().equalsIgnoreCase("date"))
                            {
                                par.SetValue(dateFormat.format(toesUp.getDate(i+1)));
                            }
                            if(par._type().equalsIgnoreCase("timestamp")||par._type().equalsIgnoreCase("timestamptz"))
                            {
                                par.SetValue(timestampFormat.format(toesUp.getTimestamp(i+1)));
                            }
                        }
                    }
                    catch(NullPointerException Ex)
                    {
                        par.SetValue(""); // если вдруг случился null
                    }
            }
        }
}

public void getJsonResult(JsonWriter jw, Gson gson)   throws SQLException, ParseException, IOException
{
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    SimpleDateFormat timestampFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
    
    jw.beginObject();
    for(int i=0;i<parameters1.size();i++)
    {
        DB_Parameter par=new DB_Parameter();
        par=(DB_Parameter)parameters1.get(i);
        if(driver.contains("racle"))
        {
            try
            {
                if (par._InOut().contains("OUT")) 
                {
                    System.out.println("OUT param was found");
                    if(par._name() == null)
                    {
                        par.SetName("return");
                    }
                    if (par._type().contains("REF CURSOR")) 
                    {
                        ResultSetMetaData mtd=par.cursor.getMetaData();
                        int numberOfColumns = mtd.getColumnCount();
                        try 
                        {
                            jw.name(par._name()).beginArray();
                            for (; par.cursor.next();)
                            {
                                jw.beginObject();
                                for (int j = 1; j < numberOfColumns + 1; j++) 
                                {
                                    String fieldName = mtd.getColumnName(j);
                                    int fieldType = mtd.getColumnType(j);
                                    switch(fieldType)
                                    {
                                        case Types.VARCHAR:
                                            gson.toJson(par.cursor.getString(j), String.class, jw.name(fieldName));
                                            break;
                                        case Types.NUMERIC:
                                            gson.toJson(par.cursor.getFloat(j), Float.class, jw.name(fieldName));
                                            break;      
                                        case Types.BIGINT:
                                            gson.toJson(par.cursor.getLong(j), Long.class, jw.name(fieldName));
                                            break;
                                        case Types.INTEGER:
                                            gson.toJson(par.cursor.getInt(j), Integer.class, jw.name(fieldName));
                                            break;
                                        case Types.SMALLINT:
                                            gson.toJson(par.cursor.getShort(j), Integer.class, jw.name(fieldName));
                                            break;
                                        case Types.DATE:
                                            gson.toJson(par.cursor.getDate(j), Integer.class, jw.name(fieldName));
                                            break;
                                        case Types.BOOLEAN:
                                            gson.toJson(par.cursor.getBoolean(j), Boolean.class, jw.name(fieldName));
                                            break;
                                        case Types.DOUBLE:
                                            gson.toJson(par.cursor.getDouble(j), Double.class, jw.name(fieldName));
                                            break;
                                        case Types.ARRAY:
                                            gson.toJson(par.cursor.getArray(j), Array.class, jw.name(fieldName));
                                            break;
                                        default:
                                            gson.toJson("unparsable", String.class, jw.name(par._name()));
                                            break;
                                    }
                                }
                                jw.endObject();
                            }
                            par.cursor.close();
                            jw.endArray();  
                        } 
                        catch (SQLException ex) 
                        {
                              LOG.error("OUT REFCUR ERROR: "+ex.getMessage());
                        }
                        
                    }
                    if (par._type().contains("VARCHAR")) 
                    {
                        gson.toJson(par._value(), String.class, jw.name(par._name()));
                    }
                    if (par._type().contains("NUMBER")) 
                    {
                        gson.toJson(Float.parseFloat(par._value()), Float.class, jw.name(par._name()));
                    }
                    if (par._type().contains("BINARY_INTEGER")) 
                    {
                        // сделать проверку, если этот параметр is_return то нашаманить простой возвращаемый тип
                        gson.toJson(Integer.parseInt(par._value()), Integer.class, jw.name(par._name()));
                    }
                    if (par._type().contains("DATE")) 
                    {
                        //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        gson.toJson(dateFormat.parse(par._value()), Date.class, jw.name(par._name()));
                    }
                }
            }
            catch(NullPointerException Ex)
            {
                par.SetValue(""); // если вдруг случился null
            }
        }
        else
        {
        try
            {                
                if(par._InOut().equalsIgnoreCase("OUT"))
                {
                    LOG.debug("OUT param was found");
                    if(par._type().equalsIgnoreCase("refcursor"))
                    {
                        LOG.debug("REF CURSOR was found");
                        ResultSetMetaData mtd=par.cursor.getMetaData();
                        int numberOfColumns = mtd.getColumnCount();
                        try 
                        {
                            jw.name(par._name()).beginArray();
                            for (; par.cursor.next();)
                            {
                                jw.beginObject();
                                for (int j = 1; j < numberOfColumns + 1; j++) 
                                {
                                    String fieldName = mtd.getColumnName(j);
                                    int fieldType = mtd.getColumnType(j);
                                    try
                                    {
                                        switch(fieldType)
                                        {
                                            case Types.VARCHAR:
                                                gson.toJson(par.cursor.getString(j), String.class, jw.name(fieldName));
                                                break;
                                            case Types.NUMERIC:
                                                gson.toJson(par.cursor.getFloat(j), Float.class, jw.name(fieldName));
                                                break;      
                                            case Types.BIGINT:
                                                gson.toJson(par.cursor.getLong(j), Long.class, jw.name(fieldName));
                                                break;
                                            case Types.INTEGER:
                                                gson.toJson(par.cursor.getInt(j), Integer.class, jw.name(fieldName));
                                                break;
                                            case Types.SMALLINT:
                                                gson.toJson(par.cursor.getShort(j), Integer.class, jw.name(fieldName));
                                                break;
                                            case Types.DATE:
                                                if(par.cursor.getDate(j)!=null)
                                                {
                                                    gson.toJson(dateFormat.format(par.cursor.getDate(j)), String.class, jw.name(fieldName));
                                                }
                                                else
                                                {
                                                    LOG.debug(fieldName+" with type "+fieldType+" is null");
                                                }
                                                break;
                                            case Types.TIME:
                                                if(par.cursor.getDate(j)!=null)
                                                {
                                                    gson.toJson(dateTimeFormat.format(par.cursor.getDate(j)), String.class, jw.name(fieldName));
                                                }
                                                else
                                                {
                                                    LOG.debug(fieldName+" with type "+fieldType+" is null");
                                                }
                                                break;                                            
                                            case Types.TIMESTAMP:
                                                if(par.cursor.getDate(j)!=null)
                                                {
                                                    gson.toJson(timestampFormat.format(par.cursor.getDate(j)), String.class, jw.name(fieldName));
                                                }
                                                else
                                                {
                                                    LOG.debug(fieldName+" with type "+fieldType+" is null");
                                                }
                                                break;
                                            case Types.BOOLEAN:
                                                gson.toJson(par.cursor.getBoolean(j), Boolean.class, jw.name(fieldName));
                                                break;
                                            case Types.BIT: // same than boolean
                                                gson.toJson(par.cursor.getBoolean(j), Boolean.class, jw.name(fieldName));
                                                break;
                                            case Types.DOUBLE:
                                                gson.toJson(par.cursor.getDouble(j), Double.class, jw.name(fieldName));
                                                break;
                                            case Types.ARRAY:
                                                Jdbc4Array a=(Jdbc4Array)par.cursor.getArray(j);
                                                Object[] obj1=(Object[])a.getArray();
                                                gson.toJson(obj1, Object[].class, jw.name(fieldName));
                                                
                                                break;
                                            default:
                                                gson.toJson("unparsable", String.class, jw.name(par._name()));
                                                break;
                                        }
                                    }
                                    catch(Exception ex)
                                    {
                                    }
                                }
                                jw.endObject();
                            }
                            jw.endArray();  
                            par.cursor.close();
                        } 
                        catch (SQLException ex) 
                        {
                              LOG.error("OUT REFCUR ERROR: "+ex.getMessage());
                        }
                    }
                    if(par._type().equalsIgnoreCase("character varying"))
                    {
                        gson.toJson(par._value(), String.class, jw.name(par._name()));
                    }
                    if(par._type().equalsIgnoreCase("numeric"))
                    {
                        gson.toJson(Float.parseFloat(par._value()), Float.class, jw.name(par._name()));
                    }
                    if(par._type().equalsIgnoreCase("double precision"))
                    {
                        gson.toJson(Double.parseDouble(par._value()), Double.class, jw.name(par._name()));
                    }
                    if(par._type().equalsIgnoreCase("bigint"))
                    {
                        gson.toJson(Long.parseLong(par._value()), Long.class, jw.name(par._name()));
                    }
                    if(par._type().equalsIgnoreCase("integer"))
                    {
                        gson.toJson(Integer.parseInt(par._value()), Integer.class, jw.name(par._name()));
                    }
                    if(par._type().equalsIgnoreCase("smallint"))
                    {
                        gson.toJson(Integer.parseInt(par._value()), Integer.class, jw.name(par._name()));
                    }
                    if(par._type().equalsIgnoreCase("boolean"))
                    {
                        gson.toJson(par._value().equalsIgnoreCase("true"), Boolean.class, jw.name(par._name()));
                    }
                    if(par._type().equalsIgnoreCase("date"))
                    {
                        //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        if(par._value()!=null)
                        {
                            if(par._value().length()>0)
                            {
                                gson.toJson(par._value(), String.class, jw.name(par._name()));
                            }
                        }
                    }
                }
            }
            catch(NullPointerException Ex)
            {
                par.SetValue(""); // если вдруг случился null
            }
        }
    }
    jw.endObject();
}

public void Exec() throws SQLException, ParseException
{
    String CallStatement;
    if(driver.contains("racle"))
    {
        CallStatement="{call "+sp_name+"(";
        for (int i = 0; i < parameters1.size(); i++) {
            CallStatement += ":" + (i + 1) + ",";
        }
        CallStatement = CallStatement.substring(0, CallStatement.length() - 1);
        CallStatement += ")}";
    }
    else
    {
        CallStatement="{call "+schema_name+"."+sp_name+"(";
        for(int i=0;i<parameters1.size();i++)
        {
            //CallStatement+=":"+(i+1)+",";
            CallStatement+="?,";
        }
        CallStatement=CallStatement.substring(0, CallStatement.length()-1);
        CallStatement+=")}";
    }

    CallableStatement toesUp = null;
    LOG.info(CallStatement);
    db_connect=new ConnectionFactory(driver,cnString,user,password,sqltext);

    // для PostgreSQL сначала нужно создать транзакцию (AutoCommit == false)...
    try
    {
        try
        {
        db_connect.connect();
        //db_connect.connection.setAutoCommit(false);
        }
        catch(DB_Exception dbe)
        {
            LOG.error("Connect failed: "+dbe.getMessage());
        }

        // Настраиваем вызов.
        toesUp  = db_connect.connection.prepareCall(CallStatement);
        //System.out.println(toesUp.+"ыыыыыыыыы");
        if(driver.contains("racle"))
        {
            for (int i = 0; i < parameters1.size(); i++) 
            {
                DB_Parameter par = null;//new DB_Parameter();
                par = (DB_Parameter) parameters1.get(i);
                System.out.println("PARAM: " + par._name() + ", type: " + par._type() + ", inout: " + par._InOut());

                if (par._type().contains("REF CURSOR")) {
                    System.out.println("REFCURSOR FOUND");
                    if (par._InOut().contains("OUT")) {
                        System.out.println("OUT_REFCURSOR FOUND");
                        toesUp.registerOutParameter(i + 1, oracle.jdbc.driver.OracleTypes.CURSOR);
                    }
                }
                if (par._type().contains("VARCHAR")) {
                    if (par._InOut().contains("OUT")) {
                        toesUp.registerOutParameter(i + 1, Types.VARCHAR);
                    } else {
                        toesUp.setString(i + 1, par._value());
                        System.out.println("bind parameters:" + par._name() + " = " + par._value());
                    }
                }
                if (par._type().contains("NUMBER")) {
                    if (par._InOut().contains("OUT")) {
                        toesUp.registerOutParameter(i + 1, Types.NUMERIC);
                    } else {
                        toesUp.setFloat(i + 1, Float.parseFloat(par._value()));
                        System.out.println("bind parameters:" + par._name() + " = " + par._value());
                    }
                }
                if (par._type().contains("DATE")) {
                    if (par._InOut().contains("OUT")) {
                        toesUp.registerOutParameter(i + 1, Types.DATE);
                    } else {
                        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                        long t = df.parse(par._value()).getTime();
                        java.sql.Date dt = new java.sql.Date(t);
                        toesUp.setDate(i + 1, dt);
                        System.out.println("bind parameters:" + par._name() + " = " + par._value());
                    }
                }
            }
        }
        else
        {
            for(int i=0;i<parameters1.size();i++)
            {
                DB_Parameter par=new DB_Parameter();
                par=(DB_Parameter)parameters1.get(i);
                LOG.debug("PARAM: "+par._name()+", type: "+par._type()+", inout: "+par._InOut());

                if(par._type().equalsIgnoreCase("refcursor"))
                {
                    LOG.debug("REFCURSOR FOUND");
                     if(par._InOut().equalsIgnoreCase("OUT"))
                     {
                        LOG.debug("OUT_REFCURSOR FOUND");
                        db_connect.connection.setAutoCommit(false);
                        toesUp.registerOutParameter(i+1, Types.OTHER);
                     }
                }
                if(par._type().equalsIgnoreCase("REF CURSOR"))
                {
                    LOG.debug("ORACLE REFCURSOR FOUND");
                     if(par._InOut().equalsIgnoreCase("OUT"))
                     {
                        LOG.debug("OUT_REFCURSOR FOUND");
                        //db_connect.connection.setAutoCommit(false);
                        toesUp.registerOutParameter(i+1, Types.OTHER);
                     }
                }            
                if(par._type().equalsIgnoreCase("character varying"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.VARCHAR);
                    }
                    else
                    {
                        try
                        {
                            if(par._value()!=null)
                            {
                                toesUp.setString(i+1, par._value());
                                LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                            }
                            else
                            toesUp.setNull(i+1, Types.VARCHAR);
                        }
                        catch(Exception Ex)
                        {
                            LOG.debug("Bind exception: "+Ex.getMessage());
                            toesUp.setNull(i+1, Types.VARCHAR);
                        }
                    }
                }
                if(par._type().equalsIgnoreCase("numeric"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.NUMERIC);
                    }
                    else
                    {
                        try
                        {
                            if(par._value()!=null)
                            {
                                toesUp.setFloat(i+1, Float.parseFloat(par._value()));
                                LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                            }
                            else
                            toesUp.setNull(i+1, Types.NUMERIC);
                        }
                        catch(Exception Ex)
                        {
                            LOG.debug("Bind exception: "+Ex.getMessage());
                            toesUp.setNull(i+1, Types.NUMERIC);
                        }

                    }
                }
                if(par._type().equalsIgnoreCase("double precision"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.DOUBLE);
                    }
                    else
                    {
                        try
                        {
                            if(par._value()!=null)
                            {
                                toesUp.setFloat(i+1, Float.parseFloat(par._value()));
                                LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                            }
                            else
                            toesUp.setNull(i+1, Types.DOUBLE);
                        }
                        catch(Exception Ex)
                        {
                            LOG.debug("Bind exception: "+Ex.getMessage());
                            toesUp.setNull(i+1, Types.DOUBLE);
                        }

                    }
                }
                if(par._type().equalsIgnoreCase("date"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.DATE);
                    }
                    else
                    {
                        try
                        {
                            if(par._value()!=null)
                            {
                                DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                                long t = df.parse(par._value()).getTime();
                                java.sql.Date dt = new java.sql.Date(t);
                                toesUp.setDate(i+1, dt);
                                LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                            }
                            else
                            toesUp.setNull(i+1, Types.DATE);
                        }
                        catch(Exception Ex)
                        {
                            LOG.debug("Bind exception: "+Ex.getMessage());
                            toesUp.setNull(i+1, Types.DATE);
                        }
                    }
                }
                if(par._type().equalsIgnoreCase("boolean"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.BOOLEAN);
                    }
                    else
                    {
                        try
                        {
                            if(par._value()!=null)
                            {
                                boolean des;
                                if(par._value().equalsIgnoreCase("true")||par._value().equalsIgnoreCase("Yes")||par._value().equalsIgnoreCase("on"))
                                    des=true;
                                else
                                    des=false;
                                toesUp.setBoolean(i+1, des);
                                LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                            }
                            else
                            toesUp.setNull(i+1, Types.BOOLEAN);
                        }
                        catch(Exception Ex)
                        {
                            LOG.debug("Bind exception: "+Ex.getMessage());
                            toesUp.setNull(i+1, Types.BOOLEAN);
                        }
                    }
                }
                if(par._type().equalsIgnoreCase("bigint"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.BIGINT);
                    }
                    else
                    {
                        try
                        {
                            if(par._value()!=null)
                            {
                                toesUp.setLong(i+1, Integer.parseInt(par._value()));
                                LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                            }
                            else
                            toesUp.setNull(i+1, Types.BIGINT);
                        }
                        catch(Exception Ex)
                        {
                            LOG.debug("Bind exception: "+Ex.getMessage());
                            toesUp.setNull(i+1, Types.BIGINT);
                        }

                    }
                }
                if(par._type().equalsIgnoreCase("integer"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.INTEGER);
                    }
                    else
                    {
                        try
                        {
                            if(par._value()!=null)
                            {
                                toesUp.setInt(i+1, Integer.parseInt(par._value()));
                                LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                            }
                            else
                            toesUp.setNull(i+1, Types.INTEGER);
                        }
                        catch(Exception Ex)
                        {
                            LOG.debug("Bind exception: "+Ex.getMessage());
                            toesUp.setNull(i+1, Types.INTEGER);
                        }
                    }
                }
                if(par._type().equalsIgnoreCase("smallint"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.SMALLINT);
                    }
                    else
                    {
                        try
                        {
                            if(par._value()!=null)
                            {
                                toesUp.setShort(i+1, Short.parseShort(par._value()));
                                LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                            }
                            else
                            toesUp.setNull(i+1, Types.SMALLINT);
                        }
                        catch(Exception Ex)
                        {
                            LOG.debug("Bind exception: "+Ex.getMessage());
                            toesUp.setNull(i+1, Types.SMALLINT);
                        }

                    }
                }
                if(par._type().equalsIgnoreCase("ARRAY"))
                {
                    if(par._InOut().equalsIgnoreCase("OUT"))
                    {
                        toesUp.registerOutParameter(i+1, Types.ARRAY);
                    }
                    else
                    {
                        try
                        {
                            if(par._value()!=null)
                            {
                                //toesUp.setShort(i+1, Short.parseShort(par._value()));
                                if(par._subtype().contains("char"))
                                    toesUp.setObject(i+1,  new PostgreSQLTextArray(par._value().split(";")), Types.ARRAY);
                                else if(par._subtype().contains("int"))
                                    toesUp.setObject(i+1,  new PostgreSQLInt4Array(toIntArray(par._value().split(";"))), Types.ARRAY);
                                LOG.debug("bind parameters:"+ par._name() + " = "+par._value());
                            }
                            else
                            toesUp.setNull(i+1, Types.ARRAY);
                        }
                        catch(Exception Ex)
                        {
                            LOG.debug("Bind exception: "+Ex.getMessage());
                            toesUp.setNull(i+1, Types.ARRAY);
                        }

                    }
                }                
            }
        }
        //выполняем запрос
        toesUp.execute();

        
        LOG.debug("Trying to parse params");
        //разбор OUT-параметров
        if(driver.contains("racle"))
        {
            for (int i = 0; i < parameters1.size(); i++) 
            {
                    DB_Parameter par = new DB_Parameter();
                    par = (DB_Parameter) parameters1.get(i);
                    try
                    {
                        if (par._InOut().contains("OUT")) 
                        {
                            System.out.println("OUT param was found");
                            if (par._type().contains("REF CURSOR")) 
                            {
                                System.out.println("REF CURSOR was found");
                                par.cursor = (ResultSet) toesUp.getObject(i + 1); //par._name()
                                System.out.println("GettingObject");
                            }
                            if (par._type().contains("VARCHAR")) 
                            {
                                par.SetValue(toesUp.getString(i + 1));
                            }
                            if (par._type().contains("NUMBER")) 
                            {
                                par.SetValue(Float.toString(toesUp.getFloat(i + 1)));
                                if (par._value().endsWith(".0"))// это значит, что используется тип NUMBER для кодировки чисел INTEGER
                                {
                                    par.SetValue(par._value().replace(".0", ""));
                                }
                            }
                            if (par._type().contains("DATE")) 
                            {
                                par.SetValue(toesUp.getDate(i + 1).toString());
                            }
                        }
                    }
                    catch(NullPointerException Ex)
                    {
                        par.SetValue(""); // если вдруг случился null
                    }
                }
        }
        else
        {
            for(int i=0;i<parameters1.size();i++)
            {
                DB_Parameter par=new DB_Parameter();
                par=(DB_Parameter)parameters1.get(i);
                try
                    {
                        if(par._InOut().equalsIgnoreCase("OUT"))
                        {
                            LOG.debug("OUT param was found");
                            if(par._type().equalsIgnoreCase("refcursor"))
                            {
                                LOG.debug("REF CURSOR was found");
                                par.cursor = (ResultSet) toesUp.getObject(i+1); //par._name()
                            }
                            if(par._type().equalsIgnoreCase("character varying"))
                            {
                                par.SetValue(toesUp.getString(i+1));
                                if(toesUp.wasNull())
                                {
                                    par.SetValue("");
                                }
                            }
                            if(par._type().equalsIgnoreCase("numeric"))
                            {
                                par.SetValue(Float.toString(toesUp.getFloat(i+1)));
                                if(par._value().endsWith(".0"))// это значит, что используется тип NUMBER для кодировки чисел INTEGER
                                {
                                    par.SetValue(par._value().replace(".0", ""));
                                }
                            }
                            if(par._type().equalsIgnoreCase("double precision"))
                            {
                                par.SetValue(Double.toString(toesUp.getDouble(i+1)));
                                if(par._value().endsWith(".0"))// это значит, что используется тип NUMBER для кодировки чисел INTEGER
                                {
                                    par.SetValue(par._value().replace(".0", ""));
                                }
                            }
                            if(par._type().equalsIgnoreCase("bigint"))
                            {
                                par.SetValue(Long.toString(toesUp.getLong(i+1)));
                            }
                            if(par._type().equalsIgnoreCase("integer"))
                            {
                                par.SetValue(Integer.toString(toesUp.getInt(i+1)));
                            }
                            if(par._type().equalsIgnoreCase("smallint"))
                            {
                                par.SetValue(Integer.toString(toesUp.getShort(i+1)));
                            }
                            if(par._type().equalsIgnoreCase("boolean"))
                            {
                                par.SetValue(Boolean.toString(toesUp.getBoolean(i+1)));
                            }
                            if(par._type().equalsIgnoreCase("date"))
                            {
                                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                                par.SetValue(dateFormat.format(toesUp.getDate(i+1)));
                            }
                        }
                    }
                    catch(NullPointerException Ex)
                    {
                        par.SetValue(""); // если вдруг случился null
                    }
            }
        }
    }
    finally
    {
       LOG.info("Execution of "+sp_name+" finished");
       if(driver.contains("racle"))
       {
       }
       else
       {
            try
            {
                java.sql.SQLWarning warning = toesUp.getWarnings();

                if (warning != null) {
                    System.out.println("\n---Warning---\n");
                    while (warning != null) {
                        System.out.println("Message: " + warning.getMessage());
                        System.out.println("SQLState: " + warning.getSQLState());
                        System.out.print("Vendor error code: ");
                        System.out.println(warning.getErrorCode());
                        System.out.println("");
                        warning = warning.getNextWarning();
                    }
                }
                db_connect.connection.commit();
                db_connect.connection.close();
            }
            catch( SQLException SE)
            {
                LOG.error("Error during prepare statement: "+SE.getMessage());
            }
        }
    }
}

public DB_Parameter GetParam(String param_name)
{
    for(int i=0;i<parameters1.size();i++)
    {
      DB_Parameter par=new DB_Parameter();
      par=(DB_Parameter)parameters1.get(i);
      if(par._name().equalsIgnoreCase(param_name))
      {
       LOG.debug("Trying to get cursor");
       return par;
      }
    } 
    return null;
}

public ResultSet GetParamRefcursor(String param_name)
{
    for(int i=0;i<parameters1.size();i++)
    {
      DB_Parameter par=new DB_Parameter();
      par=(DB_Parameter)parameters1.get(i);
      if(par._name().equalsIgnoreCase(param_name))
      {
       LOG.debug("Trying to get cursor");
       return par.cursor;
      }
    } 
    return null;
}

public String GetParamString(String param_name)
{
    for(int i=0;i<parameters1.size();i++)
    {
      DB_Parameter par=new DB_Parameter();
      par=(DB_Parameter)parameters1.get(i);
      if(par._name().equalsIgnoreCase(param_name))
      {
       LOG.debug("Trying to get string: "+par._value());
       return par._value();
      }
    } 
    return "";
}

public boolean GetParamBoolean(String param_name)
{
    for(int i=0;i<parameters1.size();i++)
    {
      DB_Parameter par=new DB_Parameter();
      par=(DB_Parameter)parameters1.get(i);
      if(par._name().equalsIgnoreCase(param_name))
      {
       LOG.debug("Trying to get boolean");
       return Boolean.parseBoolean(par._value());
      }
    } 
    return false;
}

public int GetParamInt(String param_name)
{
    for(int i=0;i<parameters1.size();i++)
    {
      DB_Parameter par=new DB_Parameter();
      par=(DB_Parameter)parameters1.get(i);
      if(par._name().equalsIgnoreCase(param_name))
      {
       LOG.debug("Trying to get Int");
       return Integer.parseInt(par._value());
      }
    } 
    return -99;
}

public float GetParamFloat(String param_name)
{
    try
    {
    for(int i=0;i<parameters1.size();i++)
    {
      DB_Parameter par=new DB_Parameter();
      par=(DB_Parameter)parameters1.get(i);
      if(par._name().equalsIgnoreCase(param_name))
      {
       LOG.debug("Trying to get float");
       return java.lang.Float.parseFloat(par._value());
      }
    } 
    }
    catch(Exception Ex)
    {
       return Float.parseFloat("-99"); 
    }
    return Float.parseFloat("-99");
}

public double GetParamDouble(String param_name)
{
    try
    {
    for(int i=0;i<parameters1.size();i++)
    {
      DB_Parameter par=new DB_Parameter();
      par=(DB_Parameter)parameters1.get(i);
      if(par._name().equalsIgnoreCase(param_name))
      {
       LOG.debug("Trying to get double");
       return java.lang.Double.parseDouble(par._value());
      }
    }
    }
    catch(Exception Ex)
    {
       return Double.parseDouble("-99");
    }
    return Double.parseDouble("-99");
}

public Date GetParamDate(String param_name)
{
    for(int i=0;i<parameters1.size();i++)
    {
      DB_Parameter par=new DB_Parameter();
      par=(DB_Parameter)parameters1.get(i);
      if(par._name().equalsIgnoreCase(param_name))
      {
       DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
       LOG.debug("Trying to get date");
       try
       {
           return df.parse(par._value());            
       } catch (ParseException e)
       {
           LOG.error("No correct data format");
           return null;
       }
      }
    } 
    return null;
}

public void SetCommonName(String name, String common_name)
{ 
    for(int i=0;i<parameters1.size();i++)
    {
      DB_Parameter par=new DB_Parameter();
      par=(DB_Parameter)parameters1.get(i);
      if(par._name().equalsIgnoreCase(name))
      {
          par.SetCommonName(common_name);
          parameters1.set(i, par);
          return;
      }
    }
    
}

public void SetValueList(String param_name, String ProcName, String refcurname, String code_field, String value_field)
{
    SPHelper test = new SPHelper(ProcName);
    //test.Prepare();
    test.SetParams("8");
    
    try
    {
    test.Exec();
    }
    catch(Exception Ex)
    {
    }
    
    for(int i=0;i<parameters1.size();i++)
    {
      DB_Parameter par=new DB_Parameter();
      par=(DB_Parameter)parameters1.get(i);
      if(par._name().equalsIgnoreCase(param_name))
      {
          par.RegisterPossibleValues(par, code_field, value_field);
          parameters1.set(i, par);
          return;
      }
    }    
    //ResultSet rs = test.GetParamRefcursor(refcurname);
}

public void SetValueList(String param_name, DB_Parameter param,  String value_field, String code_field)
{
       
    for(int i=0;i<parameters1.size();i++)
    {
      DB_Parameter par=new DB_Parameter();
      par=(DB_Parameter)parameters1.get(i);
      if(par._name().equalsIgnoreCase(param_name))
      {
          par.RegisterPossibleValues(param,  value_field, code_field);
          parameters1.set(i, par);
          return;
      }
    }    
    //ResultSet rs = test.GetParamRefcursor(refcurname);
}

public ArrayList GetParameters()
{
    return parameters1;
}

public void Close()
{
    try
       {
            db_connect.connection.commit();
            db_connect.connection.close();
       }
       catch( SQLException SE)
       {
           LOG.error("Error closing connection: "+SE.getMessage());
       }
}


}
