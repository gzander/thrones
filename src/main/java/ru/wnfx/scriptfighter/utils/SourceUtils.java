/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sergk
 */
public class SourceUtils 
{
    public static int greatest(int a, int b)
    {
        if(a>b) return a; else return b;
    }
    
    public static String clearComments(String source)
    {
        String tmp_source="";
        String result="";
        HashMap<Integer, Integer> final_comments=new HashMap();
        HashMap<Integer, Integer> literals_double=new HashMap();
        for(int i=source.indexOf("\""), j=0;i!=-1;i=source.indexOf("\"",j+1))
        {
            j=(source.indexOf("\"",i+1)==-1?source.length():source.indexOf("\"",i+1));
            literals_double.put(i, j);
        }
        
        HashMap<Integer, Integer> literals_single=new HashMap();
        for(int i=source.indexOf("'"), j=0;i!=-1;i=source.indexOf("'",j+1))
        {
            j=(source.indexOf("'",i+1)==-1?source.length():source.indexOf("'",i+1));
            literals_single.put(i, j);
        }
        
        HashMap<Integer, Integer> singlelines=new HashMap();
        for(int i=source.indexOf("//"), j=0;i!=-1;i=source.indexOf("//",j+1))
        {
            j=(source.indexOf("\n",i+1)==-1?source.length():source.indexOf("\n",i+1));
            boolean not_in_literal=true;
            for(Entry<Integer, Integer> e:literals_double.entrySet())
            {
               /*if(
                   i>e.getKey()&&j>e.getValue()
                       ||
                   i<e.getKey()&&j<e.getValue()
                       ||
                   i<e.getKey()&&j>e.getValue()
                 ) */
                if(i>e.getKey()&&i<e.getValue())
                {
                    not_in_literal=false;
                    break;
                }
            }
            for(Entry<Integer, Integer> e:literals_single.entrySet())
            {
                if(i>e.getKey()&&i<e.getValue())
                {
                    not_in_literal=false;
                    break;
                }
            }
            if(not_in_literal)
            {
                singlelines.put(i, j);
            }
        }
        
        HashMap<Integer, Integer> multilines=new HashMap();
        for(int i=source.indexOf("/*"), j=0;i!=-1;i=source.indexOf("/*",j+1))
        {
            j=(source.indexOf("*/",i+1)==-1?source.length():source.indexOf("*/",i+1))+2/*count length of comment symbol*/;
            boolean not_in_literal=true;
            for(Entry<Integer, Integer> e:literals_double.entrySet())
            {
                if(i>e.getKey()&&i<e.getValue())
                {
                    not_in_literal=false;
                    break;
                }
                else if(j>e.getKey()&&j<e.getValue())
                {   
                    j=source.indexOf("*/",e.getValue())==-1?source.length():source.indexOf("*/",e.getValue());
                }
            }
            for(Entry<Integer, Integer> e:literals_single.entrySet())
            {
                if(i>e.getKey()&&i<e.getValue())
                {
                    not_in_literal=false;
                    break;
                }
                else if(j>e.getKey()&&j<e.getValue())
                {   
                    j=source.indexOf("*/",e.getValue())==-1?source.length():source.indexOf("*/",e.getValue());
                }
            }
            if(not_in_literal)
            {
                multilines.put(i, j);
            }
            
        }
        
        for(Entry<Integer, Integer> mult:multilines.entrySet())
        {
            boolean skip=false;
            /*for(Entry<Integer, Integer> singl:singlelines.entrySet())
            {
                if(mult.getKey()>mult.getKey()&&singl.getKey()<singl.getValue())
                {
                    skip=true;
                    break;
                }
            }*/
            if(!skip)
            {
                final_comments.put(mult.getKey(), mult.getValue());
            }
        }
        
        for(Entry<Integer, Integer> singl:singlelines.entrySet())
        {
            final_comments.put(singl.getKey(), singl.getValue());
        }
        
        ArrayList<Integer> positions = new ArrayList<Integer>(final_comments.keySet());

        LinkedHashMap<Integer, Integer> final_sorted_comments=new LinkedHashMap();
        
        Collections.sort(positions, new Comparator<Integer>() 
        {

            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });
        
        for(Integer i: positions)
        {
            final_sorted_comments.put(i, final_comments.get(i));
        }
        
        
        Map.Entry<Integer, Integer> previous_entry=null;
        for(Iterator<Map.Entry<Integer, Integer>> it = final_sorted_comments.entrySet().iterator(); it.hasNext(); ) 
        {
            Map.Entry<Integer, Integer> entry = it.next();
            if(previous_entry!=null) 
            {
                Integer end_previous=previous_entry.getValue();
                Integer start_current=entry.getKey();
                if(end_previous>start_current)
                {
                    previous_entry.setValue(greatest(entry.getValue(),previous_entry.getValue()));
                    it.remove();
                }
                else
                {
                    previous_entry=entry;
                }
            }
            else
            {
                previous_entry=entry;
            }
        }
        
        int i=0;
        try
        {
            for(Entry<Integer, Integer> e:final_sorted_comments.entrySet())
            {
                if(i<e.getKey())
                {
                    tmp_source+=source.substring(i, e.getKey());
                }
                i=e.getValue();
            }
        }
        catch(Exception ex)
        {
            return source;
        }
        
        tmp_source+=source.substring(i, source.length());
        tmp_source=tmp_source.trim();
        
        if(final_comments.size()>0)
            return tmp_source;
        else
            return tmp_source;
    }
    
    public static int lineAComment(String line)
    {
        return line.indexOf("//");
    }

    
    public static String formatScriptCall(String script_name)
    {
       return script_name+"()";
    }
    
    public static String formatScriptCall(String script_name, String param)
    {
       if(param!=null)
       {
           if(param.length()>0)
           {
                return script_name+"("+param+")";
           }
       }
       
       return script_name+"()";
    }
    
    public static String getFunctionHead(String source)
    {
       String function_result=clearComments(source);
       if(function_result.startsWith("function "));
       {
          function_result=function_result.replace("function ", "");
          function_result=function_result.substring(0, ((function_result.indexOf("{")>0)?function_result.indexOf("{"):function_result.length()));
       }
       
       return function_result.replace("\n","").replace("\r", "").replace(" ", "");
    }
    
    public static String getFunctionName(String source)
    {
       String function_result=clearComments(source);
       if(function_result.startsWith("function "));
       {
          function_result=function_result.replace("function ", "");
          function_result=function_result.substring(0, ((function_result.indexOf("(")>0)?function_result.indexOf("("):function_result.length()));
       }
       
       return function_result.replace("\n","").replace("\r", "").replace(" ", "");
    }
    
    public static String getFunctionBody(String source)
    {
       String function_result=clearComments(source);
       if(function_result.startsWith("function "));
       {
          function_result=function_result.replace("function ", "");
          function_result=function_result.substring(function_result.indexOf("{"),function_result.length());
       }
       
       return function_result;//.replace("\n","").replace("\r", "").replace(" ", "");
    }
    
    public static String getFunctionParams(String source)
    {
       String function_result=clearComments(source);
       if(function_result.startsWith("function "));
       {
          function_result=function_result.replace("function ", "");
          function_result=function_result.substring(function_result.indexOf("(")+1, function_result.indexOf(")"));
       }
       
       return function_result.replace("\n","").replace("\r", "").replace(" ", "");
    }
    
    public static String GenerateHash(String source)
    {
       return StringUtils.MD5(source.replace("\n","").replace("\r", "").replace(" ", ""));
    }
    
    public static boolean checkDependent(String source, String function_name)
    {
        String[] chars_for_check=" #\n#\r#\t#-#+#||#&&#/#*#>#=#<#!#(#,#{#}".split("#");
        String tmp_source=source;
        boolean found=false;
        int begin_index=-1;
        if(tmp_source.indexOf(function_name)>0) //fast check
        {
            for(String s:chars_for_check)
            {
                begin_index=tmp_source.indexOf(s+function_name);
                if(begin_index>=0)
                {
                    tmp_source=tmp_source.substring(begin_index+s.length());
                    found=true;
                    break;
                }
            }
            
            if(found)
            {
                for(int i=function_name.length();i<tmp_source.length();i++)
                {
                    if(tmp_source.substring(i, i+1).equalsIgnoreCase("("))//function call
                    {
                        return true;
                    }    
                    else if(tmp_source.substring(i, i+1).equalsIgnoreCase(" ")||tmp_source.substring(i, i+1).equalsIgnoreCase("\r")||tmp_source.substring(i, i+1).equalsIgnoreCase("\n")||tmp_source.substring(i, i+1).equalsIgnoreCase("\t"))
                    {
                        //continue
                    }
                    else
                    {
                        checkDependent(tmp_source, function_name);
                    }
                }
            }
            else
            {
                return false;
            }
        }
        else //if no such keyword
        {
            return false;
        }
        
        return false;
    }
    

}
