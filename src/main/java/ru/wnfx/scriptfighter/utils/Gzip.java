/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;
import java.util.zip.*;
import org.apache.commons.io.IOUtils;
import java.io.*;
/**
 *
 * @author sergk
 */
public class Gzip 
{
   public static byte[] decompress(byte[] contentBytes)
   {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try{
            IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(contentBytes)), out);
        } catch(IOException e){
            throw new RuntimeException(e);
        }
        return out.toByteArray();
    }
}
