/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author sergk
 */
public class DateUtils 
{
    public static String getUnversalDate(String dateRepresentation)
    {
        Date result=new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        f.setLenient(false);
        try
        {
            result= f.parse(dateRepresentation);
        }
        catch(java.text.ParseException pe)
        {
            f = new SimpleDateFormat("dd.MM.yyyy");
            f.setLenient(false);
            try
            {
                result= f.parse(dateRepresentation);
            }
            catch(java.text.ParseException pe1)
            {
                f = new SimpleDateFormat("dd/MM/yyyy");
                f.setLenient(false);
                try
                {
                    result= f.parse(dateRepresentation);
                }
                catch(java.text.ParseException pe2)
                {
                    f = new SimpleDateFormat("yyyy/MM/dd");
                    f.setLenient(false);
                    try
                    {
                        result= f.parse(dateRepresentation);
                    }
                    catch(java.text.ParseException pe3)
                    {
                        f = new SimpleDateFormat("yyyy.MM.dd");
                        f.setLenient(false);
                        try
                        {
                            result= f.parse(dateRepresentation);
                        }
                        catch(java.text.ParseException pe4)
                        {
                            f = new SimpleDateFormat("yyyyMMdd");
                            f.setLenient(false);
                            try
                            {
                                result= f.parse(dateRepresentation);
                            }
                            catch(java.text.ParseException pe5)
                            {
                                f = new SimpleDateFormat("dd-MM-yyyy");
                                f.setLenient(false);
                                try
                                {
                                    result= f.parse(dateRepresentation);
                                }
                                catch(java.text.ParseException pe6)
                                {
                                }
                            }
                        }
                    }
                }
            }
        }
        
        f = new SimpleDateFormat("yyyy-MM-dd");
        return f.format(result);
    }
    
    public static Date getDate(String dateRepresentation)
    {
        Date result=new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            result= f.parse(getUnversalDate(dateRepresentation));
        }
        catch(java.text.ParseException pe)
        {
            return null;
        }
        
        return result;
    }
    
    public static String getString(Date dateRepresentation)
    {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        String result=f.format(dateRepresentation);

        result= getUnversalDate(result);
        
        return result;
    }
    
    public static Date addDaysToDate(final Date date, int noOfDays) 
    {
        Date newDate = new Date(date.getTime());

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(newDate);
        calendar.add(Calendar.DATE, noOfDays);
        newDate.setTime(calendar.getTime().getTime());

        return newDate;
    }
    
}
