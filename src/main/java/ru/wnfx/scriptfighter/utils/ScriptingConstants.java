/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

/**
 *
 * @author sergk
 */
public class ScriptingConstants 
{
    public static final String SCRIPT_FILENAME="scripts.db3";
    
    public static final int COMPARISON_SAME=0;
    public static final int COMPARISON_SAME_HEADER_DIFFERENT_BODY=-1;
    public static final int COMPARISON_DIFFERENT_ALL=1;
}
