/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

import org.apache.log4j.Logger;

/**
 *
 * @author sergk
 */
public class ConfigUtils 
{
    public static final Logger LOG=Logger.getLogger(ConfigUtils.class);
    
    public static int getConfigInt(String setting_name, int default_value)
    {
        int result;
        try
        {
            result=Integer.parseInt(System.getProperty(setting_name));
        }
        catch(Exception Ex)
        {
            LOG.debug("Error getting setting: "+setting_name+". Reason: "+Ex.getMessage());
            result=default_value;
        }
        return result;
    }
    
    public static boolean getConfigBoolean(String setting_name, boolean default_value)
    {
        boolean result;
        try
        {
            result=Boolean.parseBoolean(System.getProperty(setting_name).equalsIgnoreCase("yes")?"true":System.getProperty(setting_name));
        }
        catch(Exception Ex)
        {
            LOG.debug("Error getting setting: "+setting_name+". Reason: "+Ex.getMessage());
            result=default_value;
        }
        return result;
    }
    
    public static String getConfigString(String setting_name, String default_value)
    {
        String result;
        try
        {
            result=System.getProperty(setting_name);
        }
        catch(Exception Ex)
        {
            LOG.debug("Error getting setting: "+setting_name+". Reason: "+Ex.getMessage());
            result=default_value;
        }
        return result;
    }
}
