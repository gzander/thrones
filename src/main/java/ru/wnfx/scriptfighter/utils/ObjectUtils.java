/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.Bindings;
//import jdk.nashorn.api.scripting.ScriptObjectMirror;
//import ru.wnfx.scriptfighter.engine.core.ThreadsHolder;
import sun.org.mozilla.javascript.internal.NativeArray;
import sun.org.mozilla.javascript.internal.NativeObject;

/**
 *
 * @author sergk
 */
public class ObjectUtils 
{
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(ObjectUtils.class);
    public static final String engine_type = ConfigUtils.getConfigString("thrones.engine.type", "rhino");
    /*public static Map<String, Object> getMapFromNative(Object object) 
    {
        if(object instanceof NativeObject) // Rhino
        {
            NativeObject obj=(NativeObject) object;
            
            HashMap<String, Object> mapParams = new HashMap<String, Object>();

            if(obj != null) 
            {
                Object[] propIds = NativeObject.getPropertyIds(obj);
                for(Object propId: propIds) {
                    String key = propId.toString();
                    Object value = NativeObject.getProperty(obj, key);
                    mapParams.put(key, value);
                }
            }

            return mapParams;
        }
        else //Nashorn
        {
            ScriptObjectMirror obj=(ScriptObjectMirror) object;
            
            return obj.to(Map.class);
        }
        //work with mapParams next..
    }*/
    
    public static Map<String, Object> getMapFromNative(NativeObject obj) 
    {
        HashMap<String, Object> mapParams = new HashMap<String, Object>();

        if(obj != null) 
        {
            Object[] propIds = NativeObject.getPropertyIds(obj);
            for(Object propId: propIds) {
                String key = propId.toString();
                Object value = NativeObject.getProperty(obj, key);
                mapParams.put(key, value);
            }
        }

        return mapParams;
        //work with mapParams next..
    }
    
    public static Object convert(final Object obj) {
    LOG.debug("JAVASCRIPT OBJECT: {}"+ obj.getClass());
    if (obj instanceof Bindings) {
        try {
            final Class<?> cls = Class.forName("jdk.nashorn.api.scripting.ScriptObjectMirror");
            LOG.debug("Nashorn detected");
            if (cls.isAssignableFrom(obj.getClass())) {
                final Method isArray = cls.getMethod("isArray");
                final Object result = isArray.invoke(obj);
                if (result != null && result.equals(true)) {
                    final Method values = cls.getMethod("values");
                    final Object vals = values.invoke(obj);
                    if (vals instanceof Collection<?>) 
                    {
                        final Collection<?> coll = (Collection<?>) vals;
                        //return coll.toArray(new Object[0]);
                        List list;
                        if (coll instanceof List)
                          list = (List)coll;
                        else
                          list = new ArrayList(coll);
                        
                        return list;
                    }
                }
            }
        } catch (Exception e) {}
    }
    if (obj instanceof NativeObject) 
    {
        return getMapFromNative((NativeObject)obj);
    }
    else if (obj instanceof NativeArray)
    {
        return getListFromNative((NativeArray)obj);
    }
    return obj;
}
    
    public static List<Object> getListFromNative(NativeArray obj) 
    {
        List<Object> result = new ArrayList();

        if(obj != null) 
        {
            Object[] propIds = NativeObject.getPropertyIds(obj);
            for(Object o : obj.getIds()) 
            {
                int index = (Integer) o;
                Object value = obj.get(index, null);
                result.add(value);
            }

        }

        return result;
        //work with mapParams next..
    }
    
    public static Map<String, Object> getMap(Object o) 
    {
        Map<String, Object> result = new HashMap<String, Object>();
        Field[] declaredFields = o.getClass().getDeclaredFields();
        for (Field field : declaredFields) 
        {
                try {
                    result.put(field.getName(), field.get(o));
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        return result;
    }
    
    private static Map<String, Object> _introspect(Object obj) 
    {
        Map<String, Object> result = new HashMap<String, Object>();
        BeanInfo info=null;
        try 
        {
            info = Introspector.getBeanInfo(obj.getClass());
        } 
        catch (IntrospectionException ex) 
        {
            Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for (PropertyDescriptor pd : info.getPropertyDescriptors()) 
        {
            Method reader = pd.getReadMethod();
            if (reader != null) {
                try 
                {
                    result.put(pd.getName(), reader.invoke(obj));
                } 
                catch (IllegalAccessException ex) 
                {
                    Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
                } 
                catch (IllegalArgumentException ex) 
                {
                    Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
                } 
                catch (InvocationTargetException ex) 
                {
                    Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return result;
    }
    
    public static Map<String, Object> introspect(Object obj)
    {
        Map<String, Object> result = getMap(obj);
        result.putAll(_introspect(obj));
        return result;
    }
    
    public static Object listToNativeArray(List l)
    {
        if(l!=null)
        {
            if(engine_type.equalsIgnoreCase("rhino"))
            {
                NativeArray nobj = new NativeArray(l.size());
                int i=0;
                for (Object entry : l) 
                {
                    nobj.put(i, nobj, entry);
                    i++;
                }
                return nobj;
            }
            else if(engine_type.equalsIgnoreCase("nashorn"))
            {
                return l;
            }
                
        }
        return null;
        
    }
    
    public static Object mapToNativeObect(Map<String, Object> l)
    {
        if(l!=null)
        {
            if(engine_type.equalsIgnoreCase("rhino"))
            {
                NativeObject nobj = new NativeObject();
                for (Map.Entry<String, Object> entry : l.entrySet()) 
                {
                    nobj.defineProperty(entry.getKey(), entry.getValue(), NativeObject.READONLY);
                }
                return nobj;
            }
            else if(engine_type.equalsIgnoreCase("nashorn"))
            {
                return l;
            }
                
        }
        return null;
        
    }
    
    public static Object convertNative(Object obj)
    {
        if(obj instanceof Map)
        {
            return mapToNativeObect((Map)obj);
        }
        else if (obj instanceof List)
        {
            return listToNativeArray((List) obj);
        }
        else
        {
            return obj;
        }
    }
}
