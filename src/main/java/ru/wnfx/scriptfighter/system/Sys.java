/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.system;

import ru.wnfx.scriptfighter.html.core.*;
import ru.wnfx.scriptfighter.http.core.*;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
//import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.*;
import java.util.zip.GZIPInputStream;
import javax.management.MBeanServerConnection;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import sun.org.mozilla.javascript.internal.NativeObject;
import sun.org.mozilla.javascript.internal.NativeArray;
import org.jsoup.*;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import com.sun.management.OperatingSystemMXBean;


/**
 *
 * @author sergk
 */
public class Sys  extends Library
{
    Document doc;
    Elements elements;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(Sys.class);
    private String last_url;
    public Sys(CommonEngine g)
    {
        doc=null;
        elements=null;
        prefix="System";
        desc="System info API";
        last_url=null;
        
        if(g!=null)
        {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="", desc="returns current cpu usage")
    public Double getSelfCPUUsage()
    {
        
        MBeanServerConnection mbsc = ManagementFactory.getPlatformMBeanServer();

        OperatingSystemMXBean osMBean=null;
        try 
        {
            osMBean = ManagementFactory.newPlatformMXBeanProxy(mbsc, ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME, OperatingSystemMXBean.class);
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(Sys.class.getName()).log(Level.SEVERE, null, ex);
        }   
        
        Method methodToFind = null;
        try 
        {
            methodToFind = OperatingSystemMXBean.class.getMethod("getProcessCpuLoad");//.getMethod("getProcessCpuLoad", (Class<?>[]) null);
        } 
        catch (Exception ex) 
        {
             return osMBean.getSystemLoadAverage();
        } 
        
        if(methodToFind != null) 
        {
           // Method not found.long nanoBefore = System.nanoTime();
            Double cpu_percentage;
            try 
            {
                cpu_percentage = (Double)methodToFind.invoke(osMBean);
                        //(new Object(), (Object[]) null);
            } 
            catch (Exception ex) 
            {
                 return osMBean.getProcessCpuLoad();//.getSystemLoadAverage();
            }

            return cpu_percentage;
        } 
        else 
        {
           // Method found. You can invoke the method like
           return osMBean.getSystemLoadAverage(); 
        }
        
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="", desc="returns current cpu usage")
    public Double getCPUUsage()
    {
        
        MBeanServerConnection mbsc = ManagementFactory.getPlatformMBeanServer();

        OperatingSystemMXBean osMBean=null;
        try 
        {
            osMBean = ManagementFactory.newPlatformMXBeanProxy(mbsc, ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME, OperatingSystemMXBean.class);
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(Sys.class.getName()).log(Level.SEVERE, null, ex);
        }   
        
        Method methodToFind = null;
        try 
        {
            methodToFind = OperatingSystemMXBean.class.getMethod("getSystemCpuLoad");//.getMethod("getProcessCpuLoad", (Class<?>[]) null);
        } 
        catch (Exception ex) 
        {
             return osMBean.getSystemLoadAverage();
        } 
        
        if(methodToFind != null) 
        {
           // Method not found.long nanoBefore = System.nanoTime();
            Double cpu_percentage;
            try 
            {
                cpu_percentage = (Double)methodToFind.invoke(osMBean);
                        //(new Object(), (Object[]) null);
            } 
            catch (Exception ex) 
            {
                 return osMBean.getProcessCpuLoad();//.getSystemLoadAverage();
            }

            return cpu_percentage;
        } 
        else 
        {
           // Method found. You can invoke the method like
           return osMBean.getSystemLoadAverage(); 
        }
        
    }
    
    
    
  
}
