/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.ide;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import ru.wnfx.scriptfighter.dispatcher.DispatcherThread;
import ru.wnfx.scriptfighter.interfaces.Connector;
import ru.wnfx.scriptfighter.interfaces.Connectorparam;
import ru.wnfx.scriptfighter.interfaces.DatabaseConnector;
import ru.wnfx.scriptfighter.interfaces.ProxyConnector;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.Logger;
import ru.wnfx.scriptfighter.engine.core.BasicLib;
import ru.wnfx.scriptfighter.engine.core.BasicMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.engine.core.LibraryHolder;
import ru.wnfx.scriptfighter.engine.messaging.DebugCommand;
import ru.wnfx.scriptfighter.engine.messaging.EngineMessage;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.engine.messaging.OpenScriptMessage;
import ru.wnfx.scriptfighter.engine.messaging.RequestMessage;
import ru.wnfx.scriptfighter.utils.Base64;
import ru.wnfx.scriptfighter.utils.MyBase64;
import ru.wnfx.scriptfighter.utils.SourceUtils;
import ru.wnfx.scriptfighter.engine.core.BasicFunctions;
import ru.wnfx.scriptfighter.engine.core.BasicScript;
import ru.wnfx.scriptfighter.engine.core.Middleware;
import ru.wnfx.scriptfighter.engine.core.ScriptCategory;
import ru.wnfx.scriptfighter.engine.core.ScriptThread;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import ru.wnfx.scriptfighter.engine.core.ThreadsHolder;
import thrones.MasterPage;
import thrones.PathHolder;


/**
 *
 * @author sergk
 */
public class IdeHandler implements HttpHandler
{
    LimitedMessageQueue queue;
    DispatcherThread dispatcher;
    public static final Logger LOG=Logger.getLogger(IdeHandler.class);
    
    public IdeHandler(DispatcherThread dispatcher)
    {
        this.dispatcher=dispatcher;
    }
    
    @Override
  public void handle(HttpExchange exc) throws IOException 
  {
    
    Map<String, Object> params =
           (Map<String, Object>)exc.getAttribute("parameters");

    String action="";
    try
    {
        action=params.get("action").toString();
    }
    catch(NullPointerException NPE)
    {

    }

    exc.sendResponseHeaders(200, 0);
    PrintWriter out = new PrintWriter(exc.getResponseBody());

    if(action.equalsIgnoreCase("getMessages"))
    {
        
        try
        {
            Gson gson = new Gson();
            JsonWriter jw=new JsonWriter(out);
            jw.beginObject();
            jw.name("processes").beginArray();

            for(Scriptable sc: ThreadsHolder.getInstance().threads)//fucking concurrent bug
            {
                jw.beginObject();
                gson.toJson(sc.id, String.class, jw.name("id"));
                jw.name("messages").beginArray();
                try
                {
                    EngineMessage e=(EngineMessage)sc.jsEngine.message_queue.poll();
                    for(;e!=null;)
                    {
                      System.out.println(e+": "+e.message);

                      jw.beginObject();
                      gson.toJson(e.line, Integer.class, jw.name("line"));
                      gson.toJson(e.type, Integer.class, jw.name("type"));
                      gson.toJson(e.message, String.class, jw.name("text"));
                      gson.toJson(e.level, Integer.class, jw.name("level"));
                      if(e instanceof RequestMessage)
                      {
                          //need to request user params
                          Object object = gson.fromJson(((RequestMessage)e).json_param_request, Object.class);
                          gson.toJson(object, Object.class, jw.name("params"));
                      }
                      jw.endObject();            
                      //out.println(start_tag+e.message+finish_tag+"<br>");
                      e=(EngineMessage)sc.message_queue.poll();
                    }
                }
                catch(NullPointerException Ex)
                {
                    //LOG.error("Something going wrong! Here's null");
                }
                jw.endArray();
                jw.endObject();
            }
            jw.endArray();
            jw.name("core_messages").beginArray();
            EngineMessage e=(EngineMessage)LibraryHolder.getInstance().message_queue.poll();
            for(;e!=null;)
            {
              System.out.println(e);

              jw.beginObject();
              gson.toJson(e.line, Integer.class, jw.name("line"));
              gson.toJson(e.type, Integer.class, jw.name("type"));
              gson.toJson(e.message, String.class, jw.name("text"));
              gson.toJson(e.level, Integer.class, jw.name("level"));
              if(e instanceof RequestMessage)
              {
                  //need to request user params
                  Object object = gson.fromJson(((RequestMessage)e).json_param_request, Object.class);
                  gson.toJson(object, Object.class, jw.name("params"));
              }
              else if (e instanceof OpenScriptMessage)
              {
                  gson.toJson(((OpenScriptMessage)e).title, String.class, jw.name("title"));
              }
              jw.endObject();            
              //out.println(start_tag+e.message+finish_tag+"<br>");
              e=(EngineMessage)LibraryHolder.getInstance().message_queue.poll();
            }
            jw.endArray();
            jw.endObject();
            jw.flush();
        }
        catch(Exception ie)
        {
            LOG.error("Exception! "+ie.getMessage());
            ie.printStackTrace();
        }
        finally
        {
            ThreadsHolder.getInstance().notifyMessagesTaken();
        }
        //out.println(jw.toString());
        
    }
    else if (action.equalsIgnoreCase("putCommand"))
    {
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        jw.beginObject();
        try
        {
            DebugCommand dm=new DebugCommand();
            dm.id=params.get("id")==null?"":(String)params.get("id");
            dm.command=params.get("command")==null?"":(String)params.get("command");
            dm.line=Integer.parseInt(params.get("line")==null?"0":(String)params.get("line"));
            
            for(Scriptable sc: ThreadsHolder.getInstance().threads)
            {
                if(sc.mode>0)
                {
                    if(dm.id.length()==0)
                    {
                        sc.commands_queue.put(dm);
                    }
                    else if(sc.id.equalsIgnoreCase(dm.id))
                    {
                        sc.commands_queue.put(dm);
                    }
                }
            }
            gson.toJson("0", String.class, jw.name("state"));
            gson.toJson("Ok", String.class, jw.name("desc"));
        }
        catch(Exception Ex)
        {
            EngineMessage e=new EngineMessage();
            e.level=2;
            e.message="Ошибка передачи команды: "+Ex.getMessage();
            queue.add(e);
            gson.toJson("-1", String.class, jw.name("state"));
            gson.toJson(Ex.getMessage(), String.class, jw.name("desc"));
        }
        jw.endObject();
        jw.flush();
    }
    else if (action.equalsIgnoreCase("setParams"))
    {
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        jw.beginObject();
        try
        {
            DebugCommand dm=new DebugCommand();
            dm.id=params.get("id")==null?"":(String)params.get("id");
            dm.command="SETPARAMS";
            dm.params=params.get("params")==null?"":(String)params.get("params");
            
            for(Scriptable sc: ThreadsHolder.getInstance().threads)
            {
                if(sc.id.equalsIgnoreCase(dm.id))
                {
                    sc.commands_queue.put(dm);
                    break;
                }
            }
            gson.toJson("0", String.class, jw.name("state"));
            gson.toJson("Ok", String.class, jw.name("desc"));
        }
        catch(Exception Ex)
        {
            EngineMessage e=new EngineMessage();
            e.level=2;
            e.message="Ошибка передачи параметров: "+Ex.getMessage();
            queue.add(e);
            gson.toJson("-1", String.class, jw.name("state"));
            gson.toJson(Ex.getMessage(), String.class, jw.name("desc"));
        }
        jw.endObject();
        jw.flush();
    }
    else if (action.equalsIgnoreCase("getKeywords"))
    {
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        jw.beginObject();
        try
        {
            jw.name("keywords").beginArray();
            for(BasicScript bs: LibraryHolder.getInstance().basicScripts)
            {
                String function_result=bs.getSource();
                if(function_result.startsWith("function "));
                {
                    function_result=function_result.replace("function ", "");
                    function_result=function_result.substring(0, ((function_result.indexOf("{")>0)?function_result.indexOf("{"):function_result.length()));
                }
                gson.toJson(function_result, String.class, jw);
            }
            for(BasicLib bs: LibraryHolder.getInstance().getLibs())
            {
                jw.value(bs.name); 
            }
            jw.endArray();
            
            jw.name("libraries").beginArray();
            for(BasicLib bs: LibraryHolder.getInstance().getLibs())
            {
                jw.value(bs.name); 
            }
            jw.endArray();
            
            jw.name("libraries_metadata").beginArray();
            for(BasicLib bs: LibraryHolder.getInstance().getLibs())
            {
                jw.beginObject();
                gson.toJson(bs.name, String.class, jw.name("keyword"));
                gson.toJson(bs.type, String.class, jw.name("category_name"));
                gson.toJson(bs.desc, String.class, jw.name("category_desc"));
                gson.toJson(bs.image, String.class, jw.name("category_image"));
                gson.toJson(true, Boolean.class, jw.name("is_library"));
                jw.name("methods").beginArray();
                for(BasicMethod bm: bs.methods)
                {
                    jw.beginObject();
                    gson.toJson(bm.name, String.class, jw.name("keyword"));
                    gson.toJson(bm.type, String.class, jw.name("category_name"));
                    gson.toJson(bm.desc, String.class, jw.name("category_desc"));
                    gson.toJson("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACs0lEQVR42pWWu2tTURzHv0PMu02btCFpUy22xUdTi7XRqKhptdpGqYoILkJHRwc3FRxcRAQRqoP/hlZ0Ef8BR50UFwfBwUFBRYjfk3xPe3q5J4/Ah3Bf53u+v9e9aDQa6AR/EbKDxEg8hJiuR0Kf73Jxs1CSpEk/GXDo1/mk7osGxToJxPRwHxkkQ6RAimREmONhkpVgUs9F2oo4Aimz24/A6nfgTvN28RW49xa4xuuTZJyUSF7u0nIVaSdiLCefc4G/wEt38SC/gVf3gRXev1diRblKW0c+F/FHwOg/4EU7AZerwCKfK5PdEhpU6KJeF9+ANbvAH2DjMXClApzjtSWycgO4/gVYt/e8B+7y/GEJjSt0/c2whYg0c+G6uAmc4bmjZIEskwvkErnsuuHxKTJP9pDRTTceERPPnJI5RWZJVYsYJ3WyOs3EW4GfwBu0NlOVm12qxrQvXCntoqgY7ydzclPTYvXPwFMrsgHcRiucx8kBhcwrYpovQTKq/5LK1AgdNDtdAy7+Al5bgU/AM+OMnJXITCeRbT2iBBqhCbJvnblw82DcSOA8OU2OaENjCnnK1yd2lKQVNiM09oRl6gp8AB5o8bpcnFBYpxTqgWZUOoyVuEaKaa7iD+ChFXgH3NLCy/qvqYSnnVD1hZawpwjMjgqui/lWly+JmopiVvnbcmGi0sUUtkVQCPSErbJFJXtO/VGSi5R3rHhCltG0ndBubSkvKA8VJ0x53d9+QPpEAk5OiqoTphEVSqrjqO9S5JgcHTKlrQ4ftsnu9c0Y16DLB0QqmlMzIS6ivYrYWTbkzLKyRkdZyd65bep2+/r1zLKCwjKpxe1bsRB8UfUq4jZlTguWtPuSjnObjce+6OlrxRGJOh8UWYUur/+szifDvlS6EgnkJuF8FmWcz6FEWJgs/wHoNvy1hXthmgAAAABJRU5ErkJggg==", String.class, jw.name("category_image"));
                    gson.toJson(false, Boolean.class, jw.name("is_library"));
                    jw.endObject();
                }
                jw.endArray();
                jw.endObject();
            }
            jw.endArray();
            
            jw.name("metadata").beginArray();
            
            ArrayList<ScriptCategory> cats=LibraryHolder.getInstance().getScriptCategories(null);
                        
            for(BasicScript bs: LibraryHolder.getInstance().basicScripts)
            {
                jw.beginObject();
                for(ScriptCategory c: cats)
                {
                    if(c.category_id.equalsIgnoreCase(bs.getCategory()))
                    {
                        gson.toJson(c.category_name, String.class, jw.name("category_name"));
                        gson.toJson(c.category_image, String.class, jw.name("category_image"));
                        break;
                    }
                }
                gson.toJson(bs.getCategory(), String.class, jw.name("category"));
                gson.toJson(bs.getDesc(), String.class, jw.name("desc"));
                String function_result=bs.getSource();
                if(function_result.startsWith("function "));
                {
                    function_result=function_result.replace("function ", "");
                    function_result=function_result.substring(0, ((function_result.indexOf("{")>0)?function_result.indexOf("{"):function_result.length()));
                }
                gson.toJson(function_result, String.class, jw.name("keyword"));
                gson.toJson(false, Boolean.class, jw.name("is_library"));
                jw.endObject();
            }
            
            for(BasicLib bs: LibraryHolder.getInstance().getLibs())
            {
                jw.beginObject();
                gson.toJson(bs.name, String.class, jw.name("keyword"));
                gson.toJson(bs.type, String.class, jw.name("category_name"));
                gson.toJson(bs.desc, String.class, jw.name("category_desc"));
                gson.toJson(bs.type, String.class, jw.name("category"));
                gson.toJson(bs.desc, String.class, jw.name("desc"));
                gson.toJson(bs.image, String.class, jw.name("category_image"));
                gson.toJson(true, Boolean.class, jw.name("is_library"));
                jw.name("methods").beginArray();
                for(BasicMethod bm: bs.methods)
                {
                    jw.beginObject();
                    gson.toJson(bm.name, String.class, jw.name("keyword"));
                    gson.toJson(bm.type, String.class, jw.name("category_name"));
                    gson.toJson(bm.desc, String.class, jw.name("category_desc"));
                    gson.toJson("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACs0lEQVR42pWWu2tTURzHv0PMu02btCFpUy22xUdTi7XRqKhptdpGqYoILkJHRwc3FRxcRAQRqoP/hlZ0Ef8BR50UFwfBwUFBRYjfk3xPe3q5J4/Ah3Bf53u+v9e9aDQa6AR/EbKDxEg8hJiuR0Kf73Jxs1CSpEk/GXDo1/mk7osGxToJxPRwHxkkQ6RAimREmONhkpVgUs9F2oo4Aimz24/A6nfgTvN28RW49xa4xuuTZJyUSF7u0nIVaSdiLCefc4G/wEt38SC/gVf3gRXev1diRblKW0c+F/FHwOg/4EU7AZerwCKfK5PdEhpU6KJeF9+ANbvAH2DjMXClApzjtSWycgO4/gVYt/e8B+7y/GEJjSt0/c2whYg0c+G6uAmc4bmjZIEskwvkErnsuuHxKTJP9pDRTTceERPPnJI5RWZJVYsYJ3WyOs3EW4GfwBu0NlOVm12qxrQvXCntoqgY7ydzclPTYvXPwFMrsgHcRiucx8kBhcwrYpovQTKq/5LK1AgdNDtdAy7+Al5bgU/AM+OMnJXITCeRbT2iBBqhCbJvnblw82DcSOA8OU2OaENjCnnK1yd2lKQVNiM09oRl6gp8AB5o8bpcnFBYpxTqgWZUOoyVuEaKaa7iD+ChFXgH3NLCy/qvqYSnnVD1hZawpwjMjgqui/lWly+JmopiVvnbcmGi0sUUtkVQCPSErbJFJXtO/VGSi5R3rHhCltG0ndBubSkvKA8VJ0x53d9+QPpEAk5OiqoTphEVSqrjqO9S5JgcHTKlrQ4ftsnu9c0Y16DLB0QqmlMzIS6ivYrYWTbkzLKyRkdZyd65bep2+/r1zLKCwjKpxe1bsRB8UfUq4jZlTguWtPuSjnObjce+6OlrxRGJOh8UWYUur/+szifDvlS6EgnkJuF8FmWcz6FEWJgs/wHoNvy1hXthmgAAAABJRU5ErkJggg==", String.class, jw.name("category_image"));
                    gson.toJson(false, Boolean.class, jw.name("is_library"));
                    gson.toJson(bm.params, String.class, jw.name("params"));
                    jw.endObject();
                }
                jw.endArray();
                jw.endObject();
            }
            
            jw.endArray();
            
            gson.toJson("0", String.class, jw.name("state"));
            gson.toJson("Ok", String.class, jw.name("desc"));
        }
        catch(Exception Ex)
        {
            EngineMessage e=new EngineMessage();
            e.level=2;
            e.message="Ошибка передачи параметров: "+Ex.getMessage();
            queue.add(e);
            gson.toJson("-1", String.class, jw.name("state"));
            gson.toJson(Ex.getMessage(), String.class, jw.name("desc"));
        }
        jw.endObject();
        jw.flush();
    }
    else if (action.equalsIgnoreCase("getCategories"))
    {
        ArrayList<BasicScript> uncategorized_scripts=new ArrayList();
        String library_name=(String)params.get("library_name")==null?"":(String)params.get("library_name");
        
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        jw.beginObject();
        try
        {
            jw.name("categories").beginArray();
            
            for(ScriptCategory bs: LibraryHolder.getInstance().getScriptCategories(library_name))
            {
                jw.beginObject();
                gson.toJson(bs.category_id, String.class, jw.name("id"));
                gson.toJson(bs.category_name, String.class, jw.name("name"));
                gson.toJson(bs.category_desc, String.class, jw.name("desc"));
                gson.toJson(bs.category_image, String.class, jw.name("image"));
                jw.name("functions").beginArray();
                for(BasicScript bss: LibraryHolder.getInstance().getScriptByCategory(library_name, bs.category_id))
                {
                    jw.beginObject();
                    if(bss.getSource()!=null)
                    {
                        String function_result=SourceUtils.getFunctionHead(bss.getSource());
                        gson.toJson(function_result, String.class, jw.name("spec"));
                        gson.toJson(bss.getSource(), String.class, jw.name("body"));
                        gson.toJson(Base64.encode(bss.getSource()), String.class, jw.name("body_base64"));
                    }
                    if(bss.getDesc()!=null) {
                        gson.toJson(bss.getDesc(), String.class, jw.name("desc"));
                    }
                    //if(bss.is_common!=null)
                    gson.toJson(bss.is_common, Boolean.class, jw.name("is_common"));
                    jw.endObject();
                }
                jw.endArray();
                jw.endObject();
            }
            jw.beginObject();
            gson.toJson("nocategory", String.class, jw.name("id"));
            gson.toJson("Unknown", String.class, jw.name("name"));
            gson.toJson("Uncategorized", String.class, jw.name("desc"));
            gson.toJson("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACs0lEQVR42pWWu2tTURzHv0PMu02btCFpUy22xUdTi7XRqKhptdpGqYoILkJHRwc3FRxcRAQRqoP/hlZ0Ef8BR50UFwfBwUFBRYjfk3xPe3q5J4/Ah3Bf53u+v9e9aDQa6AR/EbKDxEg8hJiuR0Kf73Jxs1CSpEk/GXDo1/mk7osGxToJxPRwHxkkQ6RAimREmONhkpVgUs9F2oo4Aimz24/A6nfgTvN28RW49xa4xuuTZJyUSF7u0nIVaSdiLCefc4G/wEt38SC/gVf3gRXev1diRblKW0c+F/FHwOg/4EU7AZerwCKfK5PdEhpU6KJeF9+ANbvAH2DjMXClApzjtSWycgO4/gVYt/e8B+7y/GEJjSt0/c2whYg0c+G6uAmc4bmjZIEskwvkErnsuuHxKTJP9pDRTTceERPPnJI5RWZJVYsYJ3WyOs3EW4GfwBu0NlOVm12qxrQvXCntoqgY7ydzclPTYvXPwFMrsgHcRiucx8kBhcwrYpovQTKq/5LK1AgdNDtdAy7+Al5bgU/AM+OMnJXITCeRbT2iBBqhCbJvnblw82DcSOA8OU2OaENjCnnK1yd2lKQVNiM09oRl6gp8AB5o8bpcnFBYpxTqgWZUOoyVuEaKaa7iD+ChFXgH3NLCy/qvqYSnnVD1hZawpwjMjgqui/lWly+JmopiVvnbcmGi0sUUtkVQCPSErbJFJXtO/VGSi5R3rHhCltG0ndBubSkvKA8VJ0x53d9+QPpEAk5OiqoTphEVSqrjqO9S5JgcHTKlrQ4ftsnu9c0Y16DLB0QqmlMzIS6ivYrYWTbkzLKyRkdZyd65bep2+/r1zLKCwjKpxe1bsRB8UfUq4jZlTguWtPuSjnObjce+6OlrxRGJOh8UWYUur/+szifDvlS6EgnkJuF8FmWcz6FEWJgs/wHoNvy1hXthmgAAAABJRU5ErkJggg==", String.class, jw.name("image"));
            jw.name("functions").beginArray();
            for(BasicScript bss: uncategorized_scripts)
            {
                jw.beginObject();
                if(bss.getSource()!=null)
                {
                    String function_result=bss.getSource();
                    if(function_result.startsWith("function "));
                    {
                        function_result=function_result.replace("function ", "");
                        function_result=function_result.substring(0, ((function_result.indexOf("{")>0)?function_result.indexOf("{"):function_result.length()));
                    }
                    gson.toJson(function_result, String.class, jw.name("spec"));
                    gson.toJson(bss.getSource(), String.class, jw.name("body"));
                    gson.toJson(Base64.encode(bss.getSource()), String.class, jw.name("body_base64"));
                }
                if(bss.getDesc()!=null)
                    gson.toJson(bss.getDesc(), String.class, jw.name("desc"));
                //if(bss.is_common!=null)
                gson.toJson(bss.is_common, Boolean.class, jw.name("is_common"));
                jw.endObject();
            }
            jw.endArray();
            jw.endObject();

            jw.endArray();
            gson.toJson("0", String.class, jw.name("state"));
            gson.toJson("Ok", String.class, jw.name("desc"));
            
        }
        catch(Exception Ex)
        {
            EngineMessage e=new EngineMessage();
            e.level=2;
            e.message="Ошибка передачи параметров: "+Ex.getMessage();
            queue.add(e);
            gson.toJson("-1", String.class, jw.name("state"));
            gson.toJson(Ex.getMessage(), String.class, jw.name("desc"));
        }
        jw.endObject();
        jw.flush();
    }
    else if (action.equalsIgnoreCase("getFunctions"))
    {
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        jw.beginObject();
        try
        {
            jw.name("categories").beginArray();
            for(ScriptCategory bs: LibraryHolder.getInstance().getScriptCategories(null))
            {
                jw.beginObject();
                gson.toJson(bs.category_id, String.class, jw.name("id"));
                gson.toJson(bs.category_name, String.class, jw.name("name"));
                gson.toJson(bs.category_desc, String.class, jw.name("desc"));
                gson.toJson(bs.category_image, String.class, jw.name("image"));
                jw.endObject();
            }
            jw.endArray();
            
            
            
            gson.toJson("0", String.class, jw.name("state"));
            gson.toJson("Ok", String.class, jw.name("desc"));
        }
        catch(Exception Ex)
        {
            EngineMessage e=new EngineMessage();
            e.level=2;
            e.message="Ошибка передачи параметров: "+Ex.getMessage();
            queue.add(e);
            gson.toJson("-1", String.class, jw.name("state"));
            gson.toJson(Ex.getMessage(), String.class, jw.name("desc"));
        }
        jw.endObject();
        jw.flush();
    }  
    else if (action.equalsIgnoreCase("getInterfaces"))
    {
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        jw.beginObject();
        try
        {
            jw.name("interfaces").beginArray();
            for(Connector l: LibraryHolder.getInstance().getInterfacesList())
            {
                jw.beginObject();
                gson.toJson(l.type, String.class, jw.name("type"));
                gson.toJson(l.prefix, String.class, jw.name("prefix"));
                gson.toJson(l.desc, String.class, jw.name("desc"));
                jw.name("params").beginArray();
                for(Connectorparam p: l.all_of_params)
                {
                    jw.beginObject();
                    gson.toJson(p.name, String.class, jw.name("name"));
                    gson.toJson(p.value, String.class, jw.name("value"));
                    jw.endObject();
                }
                jw.endArray();
                jw.endObject();
                //gson.toJson(l., String.class, jw.name("image"));
            }
            jw.endArray();
         
            gson.toJson("0", String.class, jw.name("state"));
            gson.toJson("Ok", String.class, jw.name("desc"));
        }
        catch(Exception Ex)
        {
            EngineMessage e=new EngineMessage();
            e.level=2;
            e.message="Ошибка передачи параметров: "+Ex.getMessage();
            queue.add(e);
            gson.toJson("-1", String.class, jw.name("state"));
            gson.toJson(Ex.getMessage(), String.class, jw.name("desc"));
        }
        jw.endObject();
        jw.flush();
    }    
    else if (action.equalsIgnoreCase("saveLibrary"))
    {
        String library_name=(String)params.get("library_name");
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        jw.beginObject();
        try
        {
            boolean found_same=false;
            for(String l: LibraryHolder.getInstance().getExternalLibrariesList())
            {
                if(l.equalsIgnoreCase(library_name))
                {
                    LibraryHolder.getInstance().alert("Library with name \""+l+"\" already exists");
                    found_same=true;
                    break;
                }
            }
            
            if(!found_same)
            {
                LibraryHolder.getInstance().addLibrary(library_name);
            }
         
            gson.toJson("0", String.class, jw.name("state"));
            gson.toJson("Ok", String.class, jw.name("desc"));
        }
        catch(Exception Ex)
        {
            EngineMessage e=new EngineMessage();
            e.level=2;
            e.message="Ошибка передачи параметров: "+Ex.getMessage();
            queue.add(e);
            gson.toJson("-1", String.class, jw.name("state"));
            gson.toJson(Ex.getMessage(), String.class, jw.name("desc"));
        }
        jw.endObject();
        jw.flush();
    }  
    else if (action.equalsIgnoreCase("getLibraries"))
    {
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        jw.beginObject();
        try
        {
            jw.name("libraries").beginArray();
            for(String l: LibraryHolder.getInstance().getExternalLibrariesList())
            {
                jw.value(l);
            }
            jw.endArray();
         
            gson.toJson("0", String.class, jw.name("state"));
            gson.toJson("Ok", String.class, jw.name("desc"));
        }
        catch(Exception Ex)
        {
            EngineMessage e=new EngineMessage();
            e.level=2;
            e.message="Ошибка передачи параметров: "+Ex.getMessage();
            queue.add(e);
            gson.toJson("-1", String.class, jw.name("state"));
            gson.toJson(Ex.getMessage(), String.class, jw.name("desc"));
        }
        jw.endObject();
        jw.flush();
    }    
    else if(action.equalsIgnoreCase("getThreads"))
    {
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        try
        {
            ThreadsHolder.getInstance().buildHierarchy(jw, gson, null,new ConcurrentLinkedQueue(),false);
        }
        catch(Exception Ex)
        {
            LOG.error(Ex.getMessage());
            Ex.printStackTrace();
        }
        jw.flush();
    }
    else if(action.equalsIgnoreCase("getJustThreads"))
    {
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        try
        {
            ThreadsHolder.getInstance().buildHierarchy(jw, gson, null,new ConcurrentLinkedQueue(), true);
        }
        catch(Exception Ex)
        {
            LOG.error(Ex.getMessage());
            Ex.printStackTrace();
        }
        jw.flush();
    }
    else if(action.equalsIgnoreCase("saveScript"))
    {
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        jw.beginObject();
        
        try
        {
            String desc=params.get("desc")==null?"":(String)params.get("desc");
            String category=params.get("category")==null?"":(String)params.get("category");
            String library=params.get("library")==null?"":(String)params.get("library");
            if(library.length()==0)
            {
                library=null;
            }   
            String script_body=new String(MyBase64.decode((String)params.get("scriptBody")));
            boolean is_public= ((String)(params.get("textcomplete")==null?"":(String)params.get("textcomplete"))).equalsIgnoreCase("yes");
            
            LibraryHolder.getInstance().addScript(script_body, desc, category, library, is_public);
            
            gson.toJson("0", String.class, jw.name("state"));
            gson.toJson("Ok", String.class, jw.name("desc"));
        }
        catch(Exception Ex)
        {
            EngineMessage e=new EngineMessage();
            e.level=2;
            e.message="Ошибка сохранения скрипта: "+Ex.getMessage();
            queue.add(e);
            gson.toJson("-1", String.class, jw.name("state"));
            gson.toJson(Ex.getMessage(), String.class, jw.name("desc"));
        }
        jw.endObject();
        jw.flush();
        //String scriptParams=new String(MyBase64.decode((String)params.get("urlParams")));
        //Middleware.invokeWebMethod(scriptBody, scriptParams);
    }    
    else if(action.equalsIgnoreCase("autoGenerate"))
    {
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        jw.beginObject();
        
        
        for(Connector l: LibraryHolder.getInstance().getInterfacesList())
        {
            if(l.prefix.equalsIgnoreCase((String)params.get("scriptBody")))
            {
                Connector w=null;
                if(l.type.equalsIgnoreCase("Web"))
                {
                    w = new WebConnector(l.file, null, null, dispatcher.events_queue);
                }
                else if(l.type.equalsIgnoreCase("Proxy"))
                {
                    w = new ProxyConnector(l.file, null, null, dispatcher.events_queue);
                }
                else if(l.type.equalsIgnoreCase("Database"))
                {
                    w = new DatabaseConnector(l.file, null, null, dispatcher.events_queue);
                }
                LibraryHolder.getInstance().openScriptTab("autogeneration", SourceUtils.getFunctionHead(w.getGenerateWrap()));
                //ScriptThread st=new ScriptThread(w.getGenerateWrap(), 0, dispatcher.events_queue);
            }
            //gson.toJson(l., String.class, jw.name("image"));
        }
            
            
        try
        {
            gson.toJson("0", String.class, jw.name("state"));
            gson.toJson("Ok", String.class, jw.name("desc"));
        }
        catch(Exception Ex)
        {
            EngineMessage e=new EngineMessage();
            e.level=2;
            e.message="Ошибка сохранения скрипта: "+Ex.getMessage();
            queue.add(e);
            gson.toJson("-1", String.class, jw.name("state"));
            gson.toJson(Ex.getMessage(), String.class, jw.name("desc"));
        }
        jw.endObject();
        jw.flush();
        //String scriptParams=new String(MyBase64.decode((String)params.get("urlParams")));
        //Middleware.invokeWebMethod(scriptBody, scriptParams);
    }  
    else if(action.equalsIgnoreCase("runScript"))
    {
        /*String scriptBody=new String(MyBase64.decode((String)params.get("scriptBody")));
        Engine g=new Engine(queue);
        g.runScript(scriptBody);*/
        Gson gson = new Gson();
        JsonWriter jw=new JsonWriter(out);
        jw.beginObject();
        
        try
        {
            String script_body=(String)params.get("scriptBody");
            int mode=Integer.parseInt((String)params.get("mode"));
            ScriptThread st=new ScriptThread(script_body, mode, dispatcher.events_queue);
                    //new String(MyBase64.decode((String)params.get("scriptBody"))), mode, dispatcher.events_queue);
            //ThreadsHolder.getInstance().threads.add(st);
            gson.toJson("0", String.class, jw.name("state"));
            gson.toJson("Ok", String.class, jw.name("desc"));
            gson.toJson(st.id, String.class, jw.name("id"));
        }
        catch(Exception Ex)
        {
            EngineMessage e=new EngineMessage();
            e.level=2;
            e.message="Ошибка старта скрипта: "+Ex.getMessage();
            queue.add(e);
            gson.toJson("-1", String.class, jw.name("state"));
            gson.toJson(Ex.getMessage(), String.class, jw.name("desc"));
        }
        jw.endObject();
        jw.flush();
        //String scriptParams=new String(MyBase64.decode((String)params.get("urlParams")));
        //Middleware.invokeWebMethod(scriptBody, scriptParams);
    }
    else
    {
        out.println(MasterPage.getMasterPage(PathHolder.getInstance().path));
    }
    out.close();
    exc.close();
  }
}
