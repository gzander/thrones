/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core.manager;

import ru.wnfx.scriptfighter.engine.core.*;
import ru.wnfx.scriptfighter.interfaces.EventListener;
import ru.wnfx.scriptfighter.interfaces.Listener;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.PrintWriter;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptException;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.utils.ConfigUtils;
import sun.org.mozilla.javascript.internal.Context;
//import sun.sun.org.mozilla.javascript.internal.internal.IdScriptableObject;
//import sun.sun.org.mozilla.javascript.internal.internal.NativeDate;

/**
 *
 * @author sergk
 */
public class EnginePool extends Scriptable
{
    private static volatile EnginePool instance;
    public static final Logger LOG=Logger.getLogger(EnginePool.class);
    public CommonEngine commonEngine;
    //private ArrayList<Scriptable> format_threads;

    public static EnginePool initInstance() 
    {
        EnginePool localInstance = instance;
        if (localInstance == null) {
            synchronized (EnginePool.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new EnginePool();
                }
            }
        }
        return localInstance;
    }
    
    public static EnginePool getInstance() 
    {
        EnginePool localInstance = instance;
        if (localInstance == null) {
            synchronized (EnginePool.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new EnginePool();
                }
            }
        }
        return localInstance;
    }
    
    private EnginePool()
    {
        LOG.info("First init script engine pool");
        this.message_queue = new LimitedMessageQueue(640);
        this.commands_queue = new ArrayBlockingQueue(1000);
        this.events_queue = new ArrayBlockingQueue(10000); 
        this.incoming_events_queue = new LimitedMessageQueue(640); 
        //this.scriptBody=scriptBody;
        //this.mode = mode;
        id=java.util.UUID.randomUUID().toString().replace("-", "");
        //overriding_params=null;
        //
        String engine_name=ConfigUtils.getConfigString("thrones.engine.type", "rhino");
        if(engine_name.equalsIgnoreCase("rhino"))
        {
            commonEngine=new SunRhinoEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
        }
        else if(engine_name.equalsIgnoreCase("nashorn"))
        {
            commonEngine=new NashornEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
        }
        else
        {
            LOG.error("Unknown engine type: "+engine_name);
        }
        
        try
        {
            //commonEngine.put("_", commonEngine);
            
            for(BasicScript s: BasicFunctions.getBasicScripts())
            {
                if(!s.do_not_eval) {
                    Object eval = commonEngine.eval(s.getSource());
                }
            }
            //commonEngine.put("_", cs);
            //commonEngine.put("__", this);
            //jsEngine.put("DB",new NOSQL());

            commonEngine.eval("String.prototype.object = function(){return eval('('+this+')');}");

            commonEngine.registerInterfaces(message_queue, commands_queue, events_queue);
            
            LibraryHolder.getInstance().registerLibrary(commonEngine, "");
        }
        catch(ScriptException se)
        {
            LOG.error("Error init engine pool: "+se.getMessage());
        }
                //new ArrayList();
    }
    
    public CommonEngine getSharedEngine()
    {
        return commonEngine;
    }
    
    public CommonEngine getEngine(LimitedMessageQueue q, ArrayBlockingQueue bq, ArrayBlockingQueue eq, LimitedMessageQueue incoming_events_queue, Scriptable owner)
    {
        //CommonEngine e=new SunRhinoEngine( q,  bq,  eq, incoming_events_queue, owner);
        CommonEngine e=null;//=new NashornEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
        
        String engine_name=ConfigUtils.getConfigString("thrones.engine.type", "rhino");
        if(engine_name.equalsIgnoreCase("rhino"))
        {
            e=new SunRhinoEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
        }
        else if(engine_name.equalsIgnoreCase("nashorn"))
        {
            e=new NashornEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
        }
        else
        {
            LOG.error("Unknown engine type: "+engine_name);
        }
        
        try
        {
            
            /*Bindings b=commonEngine.getBindings();
            b.put("_", e);
            e.setBindings(b);*/
            e.put("_", e);
            
            for(BasicScript s: BasicFunctions.getBasicScripts())
            {
                if(!s.do_not_eval) {
                    Object eval = e.eval(s.getSource());
                }
            }
            
            e.eval("String.prototype.object = function(){return eval('('+this+')');}");

            e.registerInterfaces(q, bq, eq);
            
            LibraryHolder.getInstance().registerLibrary(e, "");
        }
        catch(/*Script*/Exception se)
        {
        }
        return e;
    }

    @Override
    public void registerMethods(CommonEngine jsEngine) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void registerEvents() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
}
