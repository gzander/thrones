/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

import ru.wnfx.scriptfighter.interfaces.BasicInterface;
import java.util.concurrent.ArrayBlockingQueue;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
//import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.Logger;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;

/**
 *
 * @author sergk
 */
public abstract class Scriptable extends BasicInterface
{
    public static final Logger LOG=Logger.getLogger(ScriptThread.class);

     private volatile boolean stopped;
     private volatile boolean paused;
     public LimitedMessageQueue message_queue = null;
     public ArrayBlockingQueue commands_queue = null;
     public ArrayBlockingQueue events_queue = null;
     public LimitedMessageQueue incoming_events_queue = null;
     public String id;
     public int mode;
     public CommonEngine jsEngine;
     public String status;
     
     public Scriptable parent;
     
     public Scriptable()
     {
         message_queue = null;
         commands_queue = null;
         events_queue = null;
         incoming_events_queue = null;
     }
}
