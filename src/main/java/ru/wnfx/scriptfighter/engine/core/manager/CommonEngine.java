/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core.manager;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptException;
import ru.wnfx.scriptfighter.db.core.Param;
import ru.wnfx.scriptfighter.db.core.Serializer;
import ru.wnfx.scriptfighter.engine.core.ScriptThread;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import ru.wnfx.scriptfighter.engine.core.ThreadsHolder;
import ru.wnfx.scriptfighter.engine.messaging.ConsoleMessage;
import ru.wnfx.scriptfighter.engine.messaging.DebugCommand;
import ru.wnfx.scriptfighter.engine.messaging.Event;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.engine.messaging.RequestMessage;
import ru.wnfx.scriptfighter.interfaces.Connector;
import ru.wnfx.scriptfighter.interfaces.Connectorparam;
import ru.wnfx.scriptfighter.interfaces.DatabaseConnector;
import ru.wnfx.scriptfighter.interfaces.EventListener;
import ru.wnfx.scriptfighter.interfaces.Listener;
import ru.wnfx.scriptfighter.interfaces.ProxyConnector;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import ru.wnfx.scriptfighter.interfaces.http.HTTPListener;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import ru.wnfx.scriptfighter.utils.SourceUtils;
import ru.wnfx.scriptfighter.utils.StringUtils;
import sun.org.mozilla.javascript.internal.Context;
import sun.org.mozilla.javascript.internal.NativeArray;
import sun.org.mozilla.javascript.internal.NativeObject;
import thrones.PathHolder;
import org.json.simple.JSONObject;
import ru.wnfx.scriptfighter.utils.ConfigUtils;

/**
 *
 * @author sergk
 */
public abstract class CommonEngine 
{
    
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(CommonEngine.class);
    public String engine_type;
    
    private Gson gson;
    private StringWriter sw;
    private JsonWriter jw;
    public LimitedMessageQueue message_queue;
    public ArrayBlockingQueue commands_queue;
    public ArrayBlockingQueue events_queue;
    public LimitedMessageQueue incoming_events_queue;
    private String database_name;

    public int mode;
    private boolean stop;
    private ArrayList<Integer> breakpoints;
    public ArrayList<Connectorparam> commonParams =null;
    public ArrayList<Connector> interfaces = null;
    public ArrayList<ScriptThread> scripts;
    public ArrayList<Listener> listeners = null;
    public Scriptable owner = null;
    
    public CommonEngine(LimitedMessageQueue q, ArrayBlockingQueue bq, ArrayBlockingQueue eq, LimitedMessageQueue incoming_events_queue, Scriptable owner)
    {
        message_queue=q;
        this.commands_queue=bq;
        this.events_queue=eq;
        stop=false;
        breakpoints=new ArrayList();
        commonParams=new ArrayList();
        scripts=new ArrayList();
        listeners=new ArrayList();
        this.incoming_events_queue=incoming_events_queue;
        this.owner=owner;
        database_name=null;
    }
    
    public abstract Object eval(String scriptBody)  throws ScriptException;
    
    public abstract void put(String name, Object obj);
    
    public abstract Object get(String name);
    
    public abstract ScriptContext getContext();
    
    public abstract Bindings getBindings();
    public abstract void setBindings(Bindings b);
        
    public ArrayList<Connector> registerInterfaces(LimitedMessageQueue mq, ArrayBlockingQueue commands_queue, ArrayBlockingQueue eq)
    {
        ArrayList<Connector> result=new ArrayList();
        File dir = new File(PathHolder.getInstance().path);
          File[] files = dir.listFiles(new FilenameFilter() 
          {
              @Override
              public boolean accept(File dir, String name) 
              {
                  //LOG.debug("check_file: "+name);
                  return name.endsWith(".interface");
              }
          });

          if(files!=null)
          {
              for (File file : files) 
              {
                  if (file.isFile()) 
                  {
                      Connector w=null;
                      try 
                      {
                          try
                          {
                              FileInputStream fis = new FileInputStream(file);
                              PropertyResourceBundle rb= new PropertyResourceBundle(fis);
                              if(rb.getString("interface.type").equalsIgnoreCase("Web"))
                              {
                                  w = new WebConnector(file.getCanonicalPath(), mq, commands_queue, eq);
                              }
                              else if(rb.getString("interface.type").equalsIgnoreCase("Proxy"))
                              {
                                  w = new ProxyConnector(file.getCanonicalPath(), mq, commands_queue, eq);
                              }
                              else if(rb.getString("interface.type").equalsIgnoreCase("Database"))
                              {
                                  w = new DatabaseConnector(file.getCanonicalPath(), mq, commands_queue, eq);
                              }
                          }
                          catch(Exception ex)
                          {
                              throw new UnsupportedOperationException(ex.getMessage());
                          }

                      } 
                      catch (Exception ex) 
                      {
                         LOG.error(ex.getMessage());
                      }
                      if(w!=null)
                      {
                          w.registerMethods(this);
                      }
                      result.add(w);
                  }
              }
          }
          this.interfaces = result;
          return result;
    }
    
    public Object runScript(String scriptBody, int mode, String id)// throws ScriptException
    {
        LOG.debug("Calling: "+scriptBody);
        this.mode=mode;
        gson=new Gson();
        sw=new StringWriter();
        jw=new JsonWriter(sw);
        Object scriptReturnValue = new Object();
        //jsEngine=EnginePool.getInstance().getEngine(this);

        owner.status="initializing";   
        
        
        try
        {                        
            owner.status="preparing";

            LOG.debug("Calling 'eval' on the script file");
            
            try 
            {
                owner.status="running";
                scriptReturnValue = eval(scriptBody);
            } 
            catch (ScriptException ex) 
            {
                ex.printStackTrace();
                error(ex.getMessage());
                for(StackTraceElement ste: ex.getStackTrace()) {
                    if (ste.getClassName().startsWith("jdk.nashorn.internal.scripts.")) {
                        {
                             LOG.error("Error invoking script: "+ex.getMessage()+" in "+ste.getMethodName()+" at "+ste.getLineNumber());
                        }
                    }
                }

                
            }
            
            owner.status="waiting_childrens";
            Thread.sleep(10);
            for(;!ThreadsHolder.getInstance().checkThreadsFinished(owner);)
            {
                Thread.sleep(10);
            }

            if (scriptReturnValue == null) 
            {
                LOG.debug("Script returned null");
            } else 
            {
                LOG.debug(
                    "Script returned type " + scriptReturnValue.getClass().getName() +
                    ", with string value '" + scriptReturnValue + "'"
                );
            }
           
        }
        catch (Exception ex) 
        {
            error(ex.getMessage());
        }        
        finally
        {
            try
            {
                jw.flush();
                sw.flush();
            }
            catch(IOException ie)
            {
                error(ie.getMessage());
            }
            
        }
        
        return scriptReturnValue;
    }
     
    public void breakpoint()
    {
        try 
        {
            String command=(String)commands_queue.take();
            if(command.equalsIgnoreCase("stepforward"))
            {
                return;
            }
        } 
        catch (InterruptedException ex) 
        {
            LOG.error(ex.getMessage());
        }
    }
    
    
    public boolean canstop(int line)
    {
        if(mode==1)
        {
            if(breakpoints.size()>0)
            {
                for(Integer i: breakpoints)
                {
                    if(line==i)
                    {
                        DebugCommand current_command=null;
                        try 
                        {
                            message_queue.offer(new ConsoleMessage("Breakpoint reached at line "+line,2,line));
                            current_command = (DebugCommand)commands_queue.take();
                        } 
                        catch (InterruptedException ex) 
                        {
                            LOG.error("Intterupt queue ineraction: "+ex.getMessage());
                        }

                        if(current_command.command.equalsIgnoreCase("stop"))
                        {
                            message_queue.offer(new ConsoleMessage("Stopped at line "+line,2,line));
                            return true;
                        }
                        else if(current_command.command.equalsIgnoreCase("step_forward"))
                        {
                            mode=2;
                            return false;
                        }
                        else if(current_command.command.equalsIgnoreCase("run"))
                        {
                            mode=1;
                            return false;
                        }
                    }
                }
            }
            return stop;
        }
        else if(mode==2)
        {
            DebugCommand current_command=null;
            try 
            {
                current_command = (DebugCommand)commands_queue.take();
            } 
            catch (InterruptedException ex) 
            {
                LOG.error("Intterupt queue ineraction: "+ex.getMessage());
            }
            
            if(current_command.command.equalsIgnoreCase("stop"))
            {
                message_queue.offer(new ConsoleMessage("Stopped at line "+line,2,line));
                return true;
            }
            else if(current_command.command.equalsIgnoreCase("step_forward"))
            {
                return false;
            }
            else if(current_command.command.equalsIgnoreCase("run"))
            {
                message_queue.offer(new ConsoleMessage("Resumed at line "+line,2,line));
                mode=1;
                return false;
            }
        }
        else
        {
            return false;
        }
        
        return false;
    }
    
    public void setDB(String databaseName)
    {
        if(databaseName!=null)
        {
            if(databaseName.equalsIgnoreCase("undefined"))
            {
                database_name=null;
            }
            else
            {
                database_name=databaseName+".db3";
            }
        }
        else
        {
            database_name=null;
        }
    }
    
    
    public void putDB(String result, String table, String key)
    {
        try
        {
        Map<String, Object> RootMapObject = new Gson().fromJson(result, Map.class);
        ArrayList<String> keys=new ArrayList();
        keys.addAll(RootMapObject.keySet());
        ArrayList<Param> params=new ArrayList();
        for(String keyname: keys)
        {
            Object val=RootMapObject.get(keyname);
            if(val instanceof String)
            {
                params.add(new Param(keyname, ((String)val).replace("'", ""), "TEXT"));
            }
            if(val instanceof Integer)
            {
                params.add(new Param(keyname, (Integer)val, "INT"));
            }
            if(val instanceof Float)
            {
                params.add(new Param(keyname, (Float)val, "FLOAT"));
            }
            if(val instanceof Double)
            {
                params.add(new Param(keyname, (Double)val, "DOUBLE"));
            }          
            if((val instanceof Map))
            {
                params.add(new Param(keyname, new Gson().toJson(val), "COMPLEX"));
            }
            if((val instanceof List))
            {
                params.add(new Param(keyname, new Gson().toJson(val), "LIST"));
            }
        }

        Serializer.Serialize(database_name, table, params, key);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            LOG.error("Error data:" + result);
        }
    }

    public String getDB(String table, String key, String value)
    {   
        return Serializer.Deserialize(database_name, table, key, value);
    }

    public void killDB(String table, String key, String value)
    {   
        Serializer.Kill(database_name, table, key, value);
    }
  
    public void sleep(int seconds)
    {
        try
        {
          Thread.sleep(seconds);
        }
        catch(InterruptedException ex)
        {
            LOG.error("INTERRUPTED!");
        }
    }
    
    public void debug(String message)
    {
        LOG.debug(message);
        message_queue.offer(new ConsoleMessage(message,0));
    }
    
    public void log(String message)
    {
        LOG.debug(message);
        message_queue.offer(new ConsoleMessage(message,1));
    }
    
    public String MD5(Object md5)
    {
        if(md5 instanceof String)
            return StringUtils.MD5((String)md5);
        else
            return null;
    }
    
    public String MD5UseEncoding(Object md5, Object encoding)
    {
        if(md5 instanceof String&&encoding instanceof String)
            return StringUtils.MD5((String)md5, (String)encoding);
        else
            return null;
    }
    
    public Object versionObjects(Object arr1, Object arr2)
    {
       Map<String, Object> obj = (Map) arr2;
       List<String> objects = new ArrayList(((Map)obj.get("data")).keySet());
       LOG.info((new Date())+" Try to sync of user: "+obj.get("userid").toString());
       Map<String, Object> old_data = (Map) arr1; //getDB('sync', 'userid', obj.userid); //
       if(objects.isEmpty())
       {
           objects = new ArrayList(((Map)old_data.get("data")).keySet());
       }
       LOG.info((new Date())+" Edited: "+ obj.get("edited").toString());

        //console.log("old_data" + $(old_data));

        Map<String, Object> result_data = new HashMap();
        result_data.put("data", new HashMap());
        result_data.put("userid",old_data.get("userid"));
        for(int i=0; i<objects.size(); i++)
        {
           List<Map<String, Object>> storage = new ArrayList();
           LOG.info((new Date())+" check object " + objects.get(i));
           //Object objz = ((Map<String, Object>)(Map<String, Object>)old_data.get("data")).get(objects.get(i));
           List curr_obj_array = (List)((Map<String, Object>)((Map<String, Object>)old_data.get("data"))).get(objects.get(i));
           List new_obj_array = (List)((Map<String, Object>)((Map<String, Object>)obj.get("data"))).get(objects.get(i));

           for(int j=0; j<curr_obj_array.size();j++)
           {
              boolean is_found = false;

              for(int k=0; k<new_obj_array.size();k++)
              {
                //console.log("check " + $(curr_obj_array[j])+" against "+$(new_obj_array[k]));
                if(((Map<String, Object>)(curr_obj_array.get(j))).get("uuid").toString().equalsIgnoreCase(((Map<String, Object>)(new_obj_array.get(k))).get("uuid").toString()))
                {
                  is_found=true;
                  if((Long)((Map<String, Object>)(curr_obj_array.get(j))).get("edited")  > (Long)((Map<String, Object>)(new_obj_array.get(k))).get("edited"))
                  {
                    storage.add((Map<String, Object>)curr_obj_array.get(j));
                    break;
                  }
                  else
                  {
                    storage.add((Map<String, Object>)new_obj_array.get(k));
                    break;
                  }
                }
              }

              if(!is_found/*&&curr_obj_array[j].edited>=obj.edited*/)
              {
                //console.log("Date last edit: "+obj.edited+", date of object: "+curr_obj_array[j].edited);
                storage.add((Map<String, Object>)curr_obj_array.get(j));
              }
           }

           for(int l=0; l<new_obj_array.size();l++)
           {
              boolean is_found = false;
 
              for(int m=0; m<curr_obj_array.size();m++)
              {
                 //console.log("check " + $(new_obj_array[l])+" against "+$(curr_obj_array[m]));
                 if(((Map<String, Object>)(curr_obj_array.get(m))).get("uuid").toString().equalsIgnoreCase(((Map<String, Object>)(new_obj_array.get(l))).get("uuid").toString()))
                 {
                   is_found = true;
                   break;
                 }
              }
              if(!is_found/* &&new_obj_array[l].edited>=old_data.edited*/)
              {
                storage.add((Map<String, Object>)new_obj_array.get(l));
              }
           }
           ((Map<String, Object>)result_data.get("data")).put(objects.get(i), storage);
        }
        result_data.put("edited",(new Date()).getTime());

        putDB((new Gson()).toJson(result_data),"sync","userid");
        //console.log("result_data" + $(result_data));
        return ObjectUtils.mapToNativeObect(result_data);
    }
    
    public void warning(String message)
    {
        LOG.debug(message);
        message_queue.offer(new ConsoleMessage(message,2));
    }
    
    public void error(String message)
    {
        LOG.error(message);

        message_queue.offer(new ConsoleMessage(message,3));
    }
    
    
    
    public void setUserParam(String name, String value)
    {

        boolean already_exists = false;
        if(name.length()>0)
        {
            for(Connectorparam p: commonParams)
            {
                if(p.name.equalsIgnoreCase(name))
                {
                    p.value = value;
                    p.remember=true;
                }
            }

            if(!already_exists)
            {
                Connectorparam new_param=new Connectorparam();
                new_param.name = name;
                new_param.value = value;
                new_param.remember = true;
                commonParams.add(new_param);
            }
        }
        
        for(Connector c: interfaces)
        {
            for(Connectorparam ps: c.user_defined_params)
            {
                if(ps.name.equalsIgnoreCase(name))
                {
                    ps.value=value;
                    ps.remember=true;
                }
            }
        }
        
        
    }
    
    public void requestUserParam(String name1, String name2, String name3, String name4, String name5, String name6, String name7, String name8, String name9, String name10, String name11)
    {   
        String complex_params=name1+";"+name2+";"+name3+";"+name4+";"+name5+";"+name6+";"+name7+";"+name8+";"+name9+";"+name10+";"+name11;
        String[] list_params=complex_params.split(";");
        
        for(String s:list_params)
        {
            boolean already_exists = false;
            if(s.length()>0)
            {
                for(Connectorparam p: commonParams)
                {
                    if(p.name.equalsIgnoreCase(s))
                    {
                        already_exists = true;
                        break;
                    }
                }
                
                if(!already_exists)
                {
                    Connectorparam new_param=new Connectorparam();
                    new_param.name = s;
                    commonParams.add(new_param);
                }
            }
        }
        
        Gson gson=new Gson();
        StringWriter sw=new StringWriter();
        JsonWriter jw=new JsonWriter(sw);
        String result="";
        String filled_user_params="";
        
        events_queue.add(new Event("BeforeEngineUserParamsRequested",null,gson.toJson(commonParams)));
        
        
        boolean need_to_ask=false;
        
        try 
        {
            jw.beginObject();
            jw.name("params").beginArray();
            for(Connectorparam p: commonParams)
            {
                    jw.beginObject();
                    gson.toJson(p.name, String.class, jw.name("name"));
                    gson.toJson(p.desc, String.class, jw.name("desc"));
                    gson.toJson(p.type, Integer.class, jw.name("type"));
                    gson.toJson(p.value, String.class, jw.name("value"));
                    gson.toJson(p.remember, Boolean.class, jw.name("remember"));
                    jw.endObject();
                    if(!p.remember)
                    {
                        need_to_ask=true;
                    }
            }
            jw.endArray();
            jw.endObject();
            result=sw.toString();
            jw.flush();
            sw.flush();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        //
        if(need_to_ask)
        {
            message_queue.offer(new RequestMessage(result)); // need to send params
            //
            try 
            {
                DebugCommand dc=(DebugCommand)commands_queue.take();
                if(dc.command.equalsIgnoreCase("SETPARAMS"))
                {
                    filled_user_params=dc.params;
                    Map<String, Object> RootMapObject = new Gson().fromJson(filled_user_params, Map.class);
                    List<Map> params_array=(List)RootMapObject.get("params");

                    for (Map<String, Object> par: params_array)
                    {
                        //System.out.println(entry.getKey() + "/" + entry.getValue());
                        for(Connectorparam p: commonParams)
                        {
                            if(p.name.equalsIgnoreCase((String)par.get("name")))
                            {
                                p.value = (String)par.get("value");
                                p.remember=(Boolean)par.get("remember");
                            }

                            for(Connector c: interfaces)
                            {
                                for(Connectorparam ps: c.user_defined_params)
                                {
                                    if(ps.name.equalsIgnoreCase(p.name))
                                    {
                                        ps.value=p.value;
                                        ps.remember=p.remember;
                                    }
                                }
                            }
                        }
                    }
                }
            } 
            catch (InterruptedException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        events_queue.add(new Event("AfterEngineUserParamsRequested",null,gson.toJson(commonParams)));
    } 
    
    public String getUserParam(String param)
    {
        for(Connectorparam p: commonParams)
        {
            if(p.name.equalsIgnoreCase(param))
            {
                return p.value;
            }
        }
        
        return "";
    }
    
   
    
    public String objectToString(Object o)
    {
        String result=null;
        
        if(o instanceof NativeObject || o instanceof NativeArray)
        {
            return null;
        }
        if(o instanceof String)
        {
            if(ConfigUtils.getConfigString("thrones.stringify.quote", "no").equalsIgnoreCase("yes")) {
                return JSONObject.escape((String)o);
            }
            else {
                return (String)o;
            }
        }
        if(o instanceof Integer || o instanceof Double)
        {
            return null; //String.valueOf((Integer)o);
        }
        
        try
        {
            result = (new Gson()).toJson(o);
        }
        catch(StackOverflowError soe)
        {
            return null;
        }
        
        return result;
    }
    
    // clear thread's queues
    public void clearQueues()
    {
        message_queue.clear();
        events_queue.clear();
        incoming_events_queue.clear();
    }
    
    // clear thread's queues
    public void clearMessageQueue()
    {
        message_queue.clear();
    }
    
    // clear thread's queues
    public void clearEventsQueue()
    {
        events_queue.clear();
    }
    
    // clear thread's queues
    public void clearIncomingEventQueue()
    {
        incoming_events_queue.clear();
    }
    
    // clear thread's queues
    public void clearAllQueues()
    {
        ThreadsHolder.getInstance().clearAllQueues();
    }
    
    // clear thread's queues
    public void clearMessageQueues()
    {
        ThreadsHolder.getInstance().clearMessageQueues();
    }
    
    // clear thread's queues
    public void clearEventQueues()
    {
        ThreadsHolder.getInstance().clearEventQueues();
    }
    
    // clear thread's queues
    public void clearIncomingQueues()
    {
        ThreadsHolder.getInstance().clearIncomingQueues();
    }
    
    public void runSeparateThread(String function_name, String object)
    {
        String script_body=/*SourceUtils.getFunctionName(Middleware.getScript(function_name))+"\n"*/"var obj="+object+";\n "+SourceUtils.getFunctionName(function_name)+"(obj);\n";
        ScriptThread st=new ScriptThread(script_body, owner);
    }
    
    public void runIndependentThread(String function_name, String object)
    {
        String script_body=/*SourceUtils.getFunctionName(Middleware.getScript(function_name))+"\n"*/"var obj="+object+";\n "+SourceUtils.getFunctionName(function_name)+"(obj);\n";
        ScriptThread st=new ScriptThread(script_body, mode, events_queue, message_queue);
    }
    
    public String addEventListener(String event_name, String function_name)
    {
        Listener lr=new EventListener(event_name, function_name, events_queue, incoming_events_queue, message_queue, commands_queue, owner);
        listeners.add(lr);
        return lr.id;
    }
    
    public String addHTTPListener(String event_name, String function_name, int port)
    {
        HTTPListener lr=new HTTPListener(event_name, function_name, events_queue, incoming_events_queue, message_queue, commands_queue, port, owner);
        listeners.add(lr);
        return lr.id;
                //new EventListener(event_name, function_name, events_queue, incoming_events_queue, message_queue, commands_queue, owner);
    }
    
    public String addHTTPListenerBunch(Object mappings, int port)
    {
        Map<String,Object> mappingsMap=(Map<String,Object>) ObjectUtils.convert(mappings);
        HTTPListener lr=new HTTPListener(mappingsMap, events_queue, incoming_events_queue, message_queue, commands_queue, port, owner);
        listeners.add(lr);
        return lr.id;
                //new EventListener(event_name, function_name, events_queue, incoming_events_queue, message_queue, commands_queue, owner);
    }
    
    public void stopListener(String id)
    {
        ThreadsHolder.getInstance().removeListener(id);
    }
    
    public void postEvent(String event_name, String object, String desc)
    {
       events_queue.add(new Event(event_name, desc, object));
    }
}
