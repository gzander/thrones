/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;
import ru.wnfx.scriptfighter.interfaces.Connector;
import ru.wnfx.scriptfighter.interfaces.DatabaseConnector;
import ru.wnfx.scriptfighter.interfaces.ProxyConnector;
import ru.wnfx.scriptfighter.db.core.DBHelper;
import ru.wnfx.scriptfighter.db.core.Serializer;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.zip.GZIPInputStream;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.File;
import java.io.FilenameFilter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
//import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import thrones.PathHolder;
import java.io.FileInputStream;
import java.util.PropertyResourceBundle;
import java.net.HttpURLConnection;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.utils.StringUtils;
/**
 *
 * @author sergk
 */
public class Middleware 
{
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(Middleware.class);

    public static ArrayList<BasicScript> getStoredScripts()
    {
        ArrayList<BasicScript> result= new ArrayList();
        DBHelper h=new DBHelper();
        ResultSet rs=h.runSelect("select body, name, ifnull(is_public,1), ifnull(do_not_eval,0), category from stored;");

        try 
        {
              for (; rs.next();)
              {
                  result.add(new BasicScript(rs.getString(1),rs.getString(2),rs.getInt(3)==1,rs.getInt(4)==1,rs.getString(5)));
              }
              rs.close();
              h.closeDatabase();
        } 
        catch (SQLException ex) 
        {
              Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    


    public static String getScript(String function_name)
    {
        ArrayList<BasicScript> result = getStoredScripts();
        result.addAll(BasicFunctions.getBasicScripts());

        for(BasicScript sc: result)
        {
              String function_result=sc.source;
              if(function_result.startsWith("function "));
              {
                  function_result=function_result.replace("function ", "");
                  function_result=function_result.substring(0, ((function_result.indexOf("(")>0)?function_result.indexOf("("):function_result.length()));
              }

              if(function_result.replace(" ", "").equalsIgnoreCase(function_name))
              {
                  return sc.source;
              }
        }

        return "";
    }
    
}
