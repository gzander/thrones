/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

/**
 *
 * @author sergk
 */
public class Protector 
{
    int level;
    
    public  Protector()
    {
        level=0;
    }
    
    public void increase() throws Exception
    {
        level++;
        
        if(level>=100)
        {
            throw new Exception();
        }
    }
    
    public void decrease()
    {
        level--;
        if(level<=0) level=0;
    }
}
