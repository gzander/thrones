/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

import ru.wnfx.scriptfighter.interfaces.EventListener;
import ru.wnfx.scriptfighter.interfaces.Listener;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.PrintWriter;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.script.Bindings;
import javax.script.ScriptContext;
import ru.wnfx.scriptfighter.interfaces.http.HTTPListener;
import ru.wnfx.scriptfighter.utils.ConfigUtils;
import sun.org.mozilla.javascript.internal.Context;
//import sun.sun.org.mozilla.javascript.internal.internal.IdScriptableObject;
import sun.org.mozilla.javascript.internal.NativeArray;
//import sun.sun.org.mozilla.javascript.internal.internal.NativeDate;
import sun.org.mozilla.javascript.internal.NativeObject;
//import sun.sun.org.mozilla.javascript.internal.internal.NativeDate;

/**
 *
 * @author sergk
 */
public class ThreadsHolder 
{
    private static volatile ThreadsHolder instance;
    public static final Logger LOG=Logger.getLogger(ThreadsHolder.class);
    public CopyOnWriteArrayList<Scriptable> threads;
    public final String notifyer;
    //private ArrayList<Scriptable> format_threads;

    public static ThreadsHolder initInstance() 
    {
        ThreadsHolder localInstance = instance;
        if (localInstance == null) {
            synchronized (ThreadsHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ThreadsHolder();
                }
            }
        }
        return localInstance;
    }
    
    public static ThreadsHolder getInstance() 
    {
        ThreadsHolder localInstance = instance;
        if (localInstance == null) {
            synchronized (ThreadsHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ThreadsHolder();
                }
            }
        }
        return localInstance;
    }
    
    private ThreadsHolder()
    {
        LOG.info("First init menu");
        threads=new CopyOnWriteArrayList();
        notifyer="notifyme";
                //new ArrayList();
    }
    
    public void addThread(Scriptable t)
    {
        //LOG.info("Thread added!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        //Thread.dumpStack();
        threads.add(t);
    }
    
    public void notifyMessagesTaken()
    {
        synchronized (notifyer) 
        {
           notifyer.notifyAll();
        }
    }
    
    public void removeThread(Scriptable t)
    {
        if(!ConfigUtils.getConfigBoolean("thrones.silent", true))
        {
            for(;t.jsEngine.message_queue.size()>0;) 
            {
                try 
                {
                    //Thread.sleep(100);
                    synchronized (notifyer) 
                    {
                       notifyer.wait();
                    }
                } 
                catch (InterruptedException ex) 
                {
                    LOG.debug(ex.getMessage());
                }
            }
        }
        threads.remove(t);
    }
    
    public void removeListener(String ID)
    {
        for(Scriptable s:threads)
        {
            /*if(s.jsEngine!=null)
            {
                for(Listener l:s.jsEngine.listeners)
                {
                    if(l.id.equalsIgnoreCase(ID))
                    {
                        l.stop();
                        for(;l.status.equalsIgnoreCase("listening");)
                        {
                            try 
                            {
                                Thread.sleep(10);
                            } 
                            catch (InterruptedException ex) 
                            {
                                LOG.error("Interrupted waiting listener finish: "+ex.getMessage());
                            }
                        }
                    }
                }
            }*/
            if(s instanceof HTTPListener)
            {
                if(s.id.equalsIgnoreCase(ID))
                {
                    ((HTTPListener)s).stop();
                    LOG.info("Command STOP sent to listener "+ID);
                    for(;s.status.equalsIgnoreCase("listening");)
                    {
                        try 
                        {
                            Thread.sleep(10);
                        } 
                        catch (InterruptedException ex) 
                        {
                            LOG.error("Interrupted waiting listener finish: "+ex.getMessage());
                        }
                    }
                }
            }
        }
    }
    
    public boolean checkThreadsFinished(Scriptable t)
    {
        boolean result=true;
        
        /*for(Scriptable s: threads)
        {
            if(s.parent != null)
            {
                if(s.parent.id.equalsIgnoreCase(t.id))
                {
                    result=false;
                    break;
                }
            }
        }*/
        
        return result;
    }
    
    public void clearAllQueues()
    {
        for(Scriptable s: threads)
        {
            s.events_queue.clear();
            s.incoming_events_queue.clear();
            s.message_queue.clear();
        }
    }
    
    public void clearEventQueues()
    {
        for(Scriptable s: threads)
        {
            s.events_queue.clear();
        }
    }
    
    public CopyOnWriteArrayList<Scriptable> getThreads()
    {
        return threads;
    }
    
    public void clearMessageQueues()
    {
        for(Scriptable s: threads)
        {
            s.message_queue.clear();
        }
    }
    
    public void clearIncomingQueues()
    {
        for(Scriptable s: threads)
        {
            s.incoming_events_queue.clear();
        }
    }
    
    public void buildHierarchy(JsonWriter jw, Gson gson, Scriptable sc, ConcurrentLinkedQueue<Scriptable> format_threads, boolean just_threads) throws IOException
    {
        jw.beginObject();
        if(sc==null)
        {
            format_threads=new ConcurrentLinkedQueue(threads);
        }
        else
        {
            gson.toJson(sc.id, String.class, jw.name("id"));
            if(sc instanceof ScriptThread)
            {
                gson.toJson("script", String.class, jw.name("type"));
                ScriptContext context=((ScriptThread)sc).jsEngine.getContext();
                if(!just_threads)
                {
                    Bindings b = ((ScriptThread)sc).jsEngine.getBindings();
                    gson.toJson(b.size(), Integer.class, jw.name("objects_allocated"));
                    // collect variables
                    jw.name("variables").beginArray();
                    for(String s: b.keySet())
                    {
                        if(
                                ((ScriptThread)sc).jsEngine.get(s) instanceof java.lang.Double||
                                ((ScriptThread)sc).jsEngine.get(s) instanceof java.lang.Boolean||
                                ((ScriptThread)sc).jsEngine.get(s) instanceof java.lang.String||
                                ((ScriptThread)sc).jsEngine.get(s).getClass().toString().endsWith("NativeDate")||
                                ((ScriptThread)sc).jsEngine.get(s).getClass().toString().endsWith("NativeObject")||
                                ((ScriptThread)sc).jsEngine.get(s).getClass().toString().endsWith("NativeArray")
                           )
                        {
                            jw.beginObject();
                            gson.toJson(s, String.class, jw.name("name"));
                            gson.toJson(((ScriptThread)sc).jsEngine.get(s).getClass().toString(), String.class, jw.name("class"));
                            describeJavascriptObject(jw,gson,((ScriptThread)sc).jsEngine.get(s),"value", context);
                            jw.endObject();
                        }
                    }
                    jw.endArray();
                }
            }
            else if(sc instanceof EventListener)
            {
                gson.toJson("listener", String.class, jw.name("type"));
                if(!just_threads)
                {
                    if(sc.status.equalsIgnoreCase("processing_event"))
                    {
                        Bindings b = ((ScriptThread)sc).jsEngine.getBindings();
                        gson.toJson(b.size(), Integer.class, jw.name("objects_allocated"));
                        jw.name("variables").beginArray();
                        for(String s: b.keySet())
                        {
                            jw.beginObject();

                            gson.toJson(s, String.class, jw.name("name"));
                            gson.toJson(((ScriptThread)sc).jsEngine.get(s),String.class, jw.name("value"));
                            jw.endObject();
                        }
                        jw.endArray();
                    }
                }
            }
            else
            {
                gson.toJson("unknown", String.class, jw.name("type"));
            }

            gson.toJson(sc.status, String.class, jw.name("status"));
            gson.toJson(sc.commands_queue.size(), Integer.class, jw.name("commands_queue_length"));
            gson.toJson(sc.events_queue.size(), Integer.class, jw.name("events_queue_length"));
            gson.toJson(sc.incoming_events_queue.size(), Integer.class, jw.name("incoming_events_queue_length"));
            gson.toJson(sc.message_queue.size(), Integer.class, jw.name("message_queue_length"));
        }
        jw.name("threads").beginArray();

        for (Iterator<Scriptable> it = format_threads.iterator(); it.hasNext();) 
        {
            Scriptable s = it.next();
            if((s.parent==null?"":(s.parent.id==null?"":s.parent.id)).equalsIgnoreCase((sc==null?"":sc.id)))
            {
                it.remove();
                buildHierarchy(jw,gson,s, format_threads,just_threads);
            }
        }
        jw.endArray();
        if(sc==null)
        {
            if(format_threads.size()>0)
            {
                jw.name("zombies").beginArray();
                for(Scriptable zomb: format_threads)
                {
                    jw.beginObject();
                    gson.toJson(zomb.id, String.class, jw.name("id"));
                    if(zomb instanceof ScriptThread)
                    {
                        gson.toJson("script", String.class, jw.name("type"));
                    }
                    else if(zomb instanceof EventListener)
                    {
                        gson.toJson("listener", String.class, jw.name("type"));
                    }
                    else
                    {
                        gson.toJson("unknown", String.class, jw.name("type"));
                    }

                    gson.toJson(zomb.status, String.class, jw.name("status"));
                    gson.toJson(zomb.commands_queue.size(), Integer.class, jw.name("commands_queue_length"));
                    gson.toJson(zomb.events_queue.size(), Integer.class, jw.name("events_queue_length"));
                    gson.toJson(zomb.incoming_events_queue.size(), Integer.class, jw.name("incoming_events_queue_length"));
                    gson.toJson(zomb.message_queue.size(), Integer.class, jw.name("message_queue_length"));
                    jw.endObject();
                }
                jw.endArray();
            }
            format_threads=null;
        }

        jw.endObject();
    }
    
    public void describeJavascriptObject(JsonWriter jw, Gson gson, Object obj, String prop, ScriptContext context) throws IOException
    {
        try
        {
            if(obj instanceof NativeObject)
            {
                if(prop!=null)
                {
                    jw.name(prop).beginObject();
                }
                else
                {
                    jw.beginObject();
                }
                NativeObject nObj=(NativeObject)obj;
                String oldkey="";
                int i=0;
                for (Object key: nObj.getAllIds()) 
                {
                    String kk="";
                    
                    if(key instanceof String)
                    {
                        kk=(String)key;
                        describeJavascriptObject(jw, gson,nObj.get((String)key, nObj), kk, context);
                    }
                    else if(key instanceof Integer)
                    {
                        kk=((Integer)key).toString();
                        describeJavascriptObject(jw, gson,nObj.get((Integer)key, nObj), kk, context);
                    }
                    else if(key instanceof Double)
                    {
                        kk=((Double)key).toString();
                        describeJavascriptObject(jw, gson,nObj.get((Integer)key, nObj), kk, context);
                    }
                    else
                    {
                        kk=key.toString();
                        describeJavascriptObject(jw, gson,nObj.get((String)key, nObj), kk, context);
                    }
                    /////
                    /*if(oldkey.equalsIgnoreCase(kk))
                    {
                        gson.toJson("json corrupted, key '"+kk+"' used twice" ,String.class, jw.name((kk+i)));
                        //describeJavascriptObject(jw, gson,nObj.get((String)key, nObj), (String)key, context);
                    }
                    else
                    {
                        describeJavascriptObject(jw, gson,nObj.get(key, nObj), kk, context);
                        oldkey=kk;
                    }*/
                    i++;
                }
                jw.endObject();
            }
            else if(obj instanceof NativeArray)
            {
                if(prop!=null)
                {
                    jw.name(prop).beginArray();
                }
                else
                {
                    jw.beginArray();
                }

                NativeArray nArr = (NativeArray)obj;
                for (Object o : nArr.getIds()) 
                {
                   int index = (Integer) o;
                   describeJavascriptObject(jw, gson, nArr.get(index, null),null, context);
                }
                jw.endArray();
            }
            else if(obj instanceof Double)
            {
                if(prop!=null)
                {
                    gson.toJson(obj ,Double.class, jw.name(prop));
                }
                else
                {
                    gson.toJson(obj ,Double.class,jw);
                }

            }
            else if(obj instanceof Float)
            {
                if(prop!=null)
                {
                    gson.toJson(obj ,Float.class, jw.name(prop));
                }
                else
                {
                    gson.toJson(obj ,Float.class,jw);
                }

            }
            else if(obj instanceof Integer)
            {
                if(prop!=null)
                {
                    gson.toJson(obj ,Integer.class, jw.name(prop));
                }
                else
                {
                    gson.toJson(obj ,Integer.class,jw);
                }

            }
            else if(obj instanceof Boolean)
            {
                if(prop!=null)
                {
                    gson.toJson(obj ,Boolean.class, jw.name(prop));
                }
                else
                {
                    gson.toJson(obj ,Boolean.class,jw);
                }
            }
            else if(obj instanceof String)
            {
                if(prop!=null)
                {
                    gson.toJson(obj ,String.class, jw.name(prop));
                }
                else
                {
                    gson.toJson(obj ,String.class,jw);
                }
            }
            else
            {
                Date d=(Date)Context.jsToJava(obj, Date.class);
                if(prop!=null)
                {
                    gson.toJson(d.toString() ,String.class, jw.name(prop));
                }
                else
                {
                    gson.toJson(d.toString() ,String.class,jw);
                }
            }
        }
        catch(Throwable thr)
        {
            gson.toJson(thr.getMessage() ,String.class, jw.name(prop));
        }
    }
    
    

}
