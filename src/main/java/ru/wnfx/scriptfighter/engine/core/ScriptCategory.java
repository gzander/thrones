/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

import ru.wnfx.scriptfighter.db.core.DBHelper;

/**
 *
 * @author sergk
 */
public class ScriptCategory 
{
    public String category_id;
    public String category_name;
    public String category_desc;
    public String category_image;
    
    public ScriptCategory()
    {
        category_id="";
        category_name="";
        category_desc="";
        category_image="";
    }
    
    public ScriptCategory(String category_id, String category_name, String category_desc, String category_image)
    {
        this.category_id=category_id;
        this.category_name=category_name;
        this.category_desc=category_desc;
        this.category_image=category_image;
    }
    
    public void putDb()
    {
        DBHelper h=new DBHelper();
        
        
        h.runInsertUpdate("insert into stored_category(category_id, category_name, category_desc, category_image) values('"+category_id+"','"+category_name +"','"+category_desc+"','"+category_image+"');");
        h.closeDatabase();

    }
}
