/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import ru.wnfx.scriptfighter.engine.messaging.Console;

/**
 *
 * @author sergk
 */
public class AnnotParser 
{
    public static ArrayList<BasicMethod> dumpMethods(Object o)
    {
        ArrayList<BasicMethod> Result=new ArrayList();
        Class clazz=o.getClass();
        try 
        {
            //List<Method> result = new ArrayList<Method>();
            while (clazz != null) 
            {
                for (Method method : clazz.getDeclaredMethods()) 
                {
                    int modifiers = method.getModifiers();
                    if (Modifier.isPublic(modifiers)) 
                    {
                        if (method.isAnnotationPresent(JavaScriptMethod.class))
                        {
                            JavaScriptMethod produce = method.getAnnotation(JavaScriptMethod.class);
                            if (produce != null)
                            {
                                Result.add(new BasicMethod(method.getName(),produce.type(), produce.autocomplete(),produce.params(),produce.desc()));
                            }
                            //result.add(method);
                        }
                    }
                }
                clazz = clazz.getSuperclass();
            }
        } 
        catch (Throwable e) 
        {
            System.err.println(e);
        }
        
        return Result;
    }
}
