/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.messaging.Console;
import ru.wnfx.scriptfighter.engine.messaging.Event;
/**
 *
 * @author sergk
 */
//@Retention(RetentionPolicy.RUNTIME)
//@Target(ElementType.METHOD)


public class Library 
{
    public String prefix;
    public String type;
    public boolean autocomplete;
    String image;
    public ArrayList<BasicMethod> methods;
    public ArrayList<Event> events;
    public String desc;
    public CommonEngine g;
    public boolean is_external;
    String id;
    
    public Library()
    {
        autocomplete=true;
        is_external=false;
        prefix="";
        type="";
        desc="";
        image="data:image/gif;base64,R0lGODlhGQAZAP8AAK6xska5xQBjStXp2drw48jn1sR1OaGfpLPJ42aFuGuXkdl4OL7BvYqnpIKJppekqKamqr1zOWyWtIabu7dmN6jD4nqhnPekVfy8ZRmFeJOitJm5eOyoZLeyaAp2ZkRyt+aHOLJnQpeInZiKpnd+owBZQ1+hraOasrKpssyUT+aUPIScm7XF3Le3x73k0WiX07fiyVl/wZikxzuzxMRqNcXjzafOmuCiRIKepnOZyuODQVHHzKRIKSaancV6Q/zGa593doSv4J244QJyXcvh87jS7Jey2qW32SuKiWuhmHdmiKS+gcaYUoe+hbm5utiHPKSDrneGq0SKu6eqwMHCvVaDxWaIwcLDwT2ytT13qkRmqtuFQMFpLbNdNmirqkZqtXaVu5aYtplKM7N3U7mYUwNqVfCxWo9dWsx4GKedXVc7XZqcVsZTM+ipWNalWfuXNqpjPZ9XP+9gJ8+WOcJrRLtnL+x0LLNXLK15QtxYKsJ7IMuETLOfS7dyL8B0Uu2aULl0SeicSN6XHrJXS6KHSs+SYEmmtzaus5LZsajct1uku6etutGdujtgrFebqGtxqKbbrimpsVdxsLCsr16U0lrM0XKe0bO708vf8XCh2YWr13Ktv9ykvp/hvE6mwtWqymmUyqrkyVRxp9qnfX1leq6IdT58gFOLhJt/hKvDgB44Y7inYvCuYYlHaNOWdKp9apS4aWp1mZN9kXZtkVSfmp7MoWVtoI/RnjVLihmRiDqNh4tujE93jmmSjZ/LjAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAL8ALAAAAAAZABkAAAj/AH8JHEiwoMGDCH+NKuQnoUOBqyDZ6JBiD52HBfF0SESAAAwbqTowSRECIxlfLggMGECggAsXiGCZWcCDwkEmGzqprFGAZ40BMJa4eRKBQpeCrjhwSLPhVqgBBQqsrMVHxQIKPiLwGHinDYcLPzBcSLGmCYyWG+aoMBDBAJcQagaWAgLoCYhAGMRuIdRkyQ0VIQz4oBHHVkEhMka8WgDiD6uwOm4s6NKWyxkrLxIQRICggpAwQAxsAXFBB408CwzUIVXlBSgtA5WMQNGCBYIjYVCNoUGBzRs7cGK9QJDjA66BDqKQECHixJHORnbpESRnkKRMmIrEyKJqoAYAGsBI6HAg68QlBDL6oGlVJQgRIx+yYCH4gAoD8GASLD8xQowSShUQYUkjjwQwn0C8SDDFFVfcB8EECUQxCwkVaKKdFlAwYkguBHmxyQQytMCAE1QAsEgCQbCQwxdSKMLJJ0gIQBAOFnhhggQTACAiAw7EwIIVUlQSABSmCFACfQcc8EASjpgwwRROOPBFAp7ssMMMHshYkAIQTILCAStYQMuNonwQQAAzHOLBkQY1oIAFDQBwAAoQ4JBED2cG0EMZbCLUwAq9KHAAAHIigWYGWmL0ywpvPgCBLpFkqWhBbp6SwRCJTkrQpZkeFBAAOw==";
    }

}
