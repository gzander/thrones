/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import ru.wnfx.scriptfighter.db.core.DBHelper;
import ru.wnfx.scriptfighter.db.core.Param;
import ru.wnfx.scriptfighter.db.core.Serializer;
import ru.wnfx.scriptfighter.utils.ConfigUtils;
import ru.wnfx.scriptfighter.utils.ScriptingConstants;
import ru.wnfx.scriptfighter.utils.SourceUtils;
import ru.wnfx.scriptfighter.utils.StringUtils;
import thrones.PathHolder;

/**
 *
 * @author sergk
 */
public class BasicScript 
{
    String id;
    String source;
    String desc;
    String category;
    String header;
    ArrayList<String> requires;
    String library;
    
    public boolean is_common;
    public boolean do_not_eval;
    
    public BasicScript()
    {
        source="";
        desc="";
        category="";
        header="";
        is_common=false;  
        do_not_eval=false;
        requires=new ArrayList();
    }
    
    public String getId()
    {
        return id;
    }
    
    public String getLibrary()
    {
        return library;
    }
    
    public String getSource()
    {
        return source;
    }
    
    public String getDesc()
    {
        return desc;
    }
    
    public String getCategory()
    {
        return category;
    }
    
    public boolean getis_Common()
    {
        return is_common;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public void setSource(String source)
    {
        this.source=source;
    }
    
    public void setDesc(String desc)
    {
        this.desc=desc;
    }
    
    public BasicScript(String source, String desc, boolean is_common, boolean do_not_eval, String category)
    {
        this.id=SourceUtils.GenerateHash(source);
        this.source=source;
        this.desc=desc;
        this.is_common=is_common; 
        this.do_not_eval=do_not_eval;
        this.category=category;
        this.requires=new ArrayList();
        this.header=SourceUtils.getFunctionHead(source);
    }
    
    public BasicScript(String source, String desc, boolean is_common, boolean do_not_eval, String category, String library_name)
    {
        this.id=SourceUtils.GenerateHash(source);
        this.source=source;
        this.desc=desc;
        this.is_common=is_common; 
        this.do_not_eval=do_not_eval;
        this.category=category;
        this.requires=new ArrayList();
        this.header=SourceUtils.getFunctionHead(source);
        this.library=library_name;
    }
    
    public int compareWith(BasicScript s)
    {
        if(this.header.equalsIgnoreCase(s.header)&&!this.id.equalsIgnoreCase(s.id)) {
            return ScriptingConstants.COMPARISON_SAME_HEADER_DIFFERENT_BODY;
        }
        if(this.header.equalsIgnoreCase(s.header)&&this.id.equalsIgnoreCase(s.id)) {
            return ScriptingConstants.COMPARISON_SAME;
        }
        
        return ScriptingConstants.COMPARISON_DIFFERENT_ALL;
    }
    
    public boolean dependsOn(BasicScript s)
    {
        return SourceUtils.checkDependent(source, SourceUtils.getFunctionName(s.source));
    }
    
    
    public boolean putDb(String library_name)
    {
        String app_path=PathHolder.getInstance().path;
        if(library_name==null)
        {
            app_path+=System.getProperty("file.separator")+ConfigUtils.getConfigString("thrones.lib.mainfile", "scripts.scriptlib");
        }
        else
        {
            app_path+=System.getProperty("file.separator")+library_name+".scriptlib";
        }
        DBHelper h=new DBHelper(app_path);
        /*ResultSet rs=h.runSelect("select body, name, ifnull(is_public,1), ifnull(do_not_eval,0), category, header from stored;");
        boolean same_found=false;
        try 
        {
              for (; rs.next();)
              {
                  if(rs.getString(6).equalsIgnoreCase(header))
                  {
                      same_found=true;
                  }
                  //result.add(new BasicScript(rs.getString(1),rs.getString(2),rs.getInt(3)==1,rs.getInt(4)==1,rs.getString(5)));
              }
              rs.close();
              h.closeDatabase();
        } 
        catch (SQLException ex) 
        {
              java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        String function_name=SourceUtils.getFunctionName(source);
        h.runInsertUpdate("insert into stored_source_safe(name, desc, body, is_public, id ,category) select name, desc, body, is_public, id ,category from stored s where s.name='"+function_name+"'");
        h.closeDatabase();
        h.runInsertUpdate("delete from stored where name='"+function_name+"'");
        h.closeDatabase();
        h.runInsertUpdate("update version set minor_version=(select ifnull(max(revision),0) from stored_source_safe);");
        h.closeDatabase();
        String src="insert into stored(name, desc, body, is_public, id ,category) values('"+function_name+"','"+desc.replace("'", "''")+"','"+source.replace("'", "''") +"',"+(is_common?1:0)+",'"+StringUtils.MD5(source)+"','"+category+"')";
        h.closeDatabase();
        
        h.runInsertUpdate(src);
        h.closeDatabase();
        
        return true;
    }
}
