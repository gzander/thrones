/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

import ru.wnfx.scriptfighter.interfaces.Connector;
import ru.wnfx.scriptfighter.interfaces.DatabaseConnector;
import ru.wnfx.scriptfighter.interfaces.ProxyConnector;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PropertyResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import thrones.*;
import org.apache.log4j.Logger;
import ru.wnfx.scriptfighter.db.core.DBHelper;
import ru.wnfx.scriptfighter.db.core.Serializer;
import ru.wnfx.scriptfighter.db.nosql.NOSQL;
import ru.wnfx.scriptfighter.docs.core.Pdf;
import ru.wnfx.scriptfighter.email.core.Email;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.messaging.AlertMessage;
import ru.wnfx.scriptfighter.engine.messaging.Console;
import ru.wnfx.scriptfighter.engine.messaging.ConsoleMessage;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.engine.messaging.OpenScriptMessage;
import ru.wnfx.scriptfighter.file.core.FileInterface;
import ru.wnfx.scriptfighter.utils.MyBase64;
import ru.wnfx.scriptfighter.utils.ScriptingConstants;
import ru.wnfx.scriptfighter.http.core.Http;
import ru.wnfx.scriptfighter.utils.ConfigUtils;
import ru.wnfx.scriptfighter.utils.SourceUtils;
import ru.wnfx.scriptfighter.html.core.Html;
import ru.wnfx.scriptfighter.push.core.Push;
import ru.wnfx.scriptfighter.system.Sys;

/**
 *
 * @author sergk
 */
public class LibraryHolder 
{
    //protected static final String scriptFilename="scripts.db3";
    private static volatile LibraryHolder instance;
    public static final Logger LOG=Logger.getLogger(LibraryHolder.class);
    public ArrayList<Library> libs;
    public DBHelper scriptDatabaseHelper;
    public ArrayList<BasicScript> basicScripts;
    public LimitedMessageQueue message_queue;
    public ConcurrentHashMap<String, ArrayList<BasicScript>> library_cache; 
    public int cache_misses_factor;
    //public ArrayList<BasicScript> external_scripts;

    public static LibraryHolder initInstance() 
    {
        LibraryHolder localInstance = instance;
        if (localInstance == null) {
            synchronized (PathHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new LibraryHolder();
                }
            }
        }
        return localInstance;
    }
    
    public static LibraryHolder getInstance() 
    {
        LibraryHolder localInstance = instance;
        if (localInstance == null) {
            synchronized (PathHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new LibraryHolder();
                }
            }
        }
        return localInstance;
    }
    
    public ArrayList<String> getExternalLibrariesList()
    {
        ArrayList<String> result=new ArrayList();
        for(Library l: libs)
        {
            if(l.is_external)
            {
                result.add(l.prefix);
            }
        }
        
        return result;
    }
    
    public BasicScript getScript(String function_name)
    {
        for(BasicScript bs: basicScripts)
        {
            if(SourceUtils.getFunctionName(
                bs.getSource()).equalsIgnoreCase(function_name))
                return bs;
        }
        return null;
    }
    
    public ArrayList<BasicScript>  getAllStoredScripts()
    {
        ArrayList<BasicScript> all_scripts=new ArrayList();
        String app_path=PathHolder.getInstance().path;

        LOG.info("get main script file::with app_path:"+app_path);
        String main_list[] = new File(app_path).list();
        boolean is_found=false;
        for(int i = 0; i < main_list.length; i++)
        {
            if(main_list[i].equalsIgnoreCase(ConfigUtils.getConfigString("thrones.lib.mainfile", "scripts.scriptlib")))
            {
                all_scripts.addAll(getStoredScripts(main_list[i],true));
                is_found=true;
            }
        }
        if(!is_found)
        {
            LOG.error("load scripts::No main script library found");
        }
        is_found=false;
        String list[] = main_list;//new File(app_path).list();
        for(int i = 0; i < list.length; i++)
        {
            if(list[i].contains(".scriptlib")&&!main_list[i].equalsIgnoreCase(ConfigUtils.getConfigString("thrones.lib.mainfile", "scripts.scriptlib")))
            {
               all_scripts.addAll(getStoredScripts(list[i], false));
               is_found=true;
            }
        }
        if(!is_found)
        {
            LOG.info("no additional scripts was found");
        }
        
        return all_scripts;
    }
    
    public String extractLibName(String filename)
    {
        if(filename==null)
        {
            return null;
        }
        String result="";
        String[] url_parts=filename.split("\\.");
        if(url_parts.length>1)
        {
            result=url_parts[url_parts.length-2];
        }
        else
        {
            result=url_parts[url_parts.length-1];
        }
        
        return result;
    }
    
    public ArrayList<BasicScript> getStoredScripts(String filename, boolean is_main)
    {
        ArrayList<BasicScript> result;//= new ArrayList();
        if(library_cache.get(filename)==null||cache_misses_factor>10)
        {
            result = new ArrayList();
            DBHelper h=new DBHelper(filename);
            ResultSet rs=h.runSelect("select body, name, ifnull(is_public,1), ifnull(do_not_eval,0), category from stored;");

            try 
            {
                  for (; rs.next();)
                  {
                      result.add(new BasicScript(rs.getString(1),rs.getString(2),rs.getInt(3)==1,rs.getInt(4)==1,rs.getString(5), extractLibName(is_main?null:filename)));
                  }
                  rs.close();
                  h.closeDatabase();
                  
                  library_cache.put(filename, result);
            } 
            catch (SQLException ex) 
            {
                  LOG.error("Error getting stored scripts: "+ex.getMessage());
            }
            
            cache_misses_factor=0;
        }
        else
        {
            result = library_cache.get(filename);
            cache_misses_factor++;
        }

        return result;
    }
    
    private LibraryHolder()
    {
        LOG.info("First init library holder");
        libs=new ArrayList();
        library_cache = new ConcurrentHashMap();
        scriptDatabaseHelper=new DBHelper(ScriptingConstants.SCRIPT_FILENAME);
        basicScripts=new ArrayList();
        registerLibrary(null);
        message_queue = new LimitedMessageQueue(640);
        cache_misses_factor = 0;
        
        //libs.add(new Console(e));
    }  
    
    public final void registerLibrary(CommonEngine g)
    {
        registerLibrary(g, null);
    }
    
    public void log(String message)
    {
        LOG.debug(message);
        message_queue.offer(new ConsoleMessage(message,1));
    }
    
    public void warning(String message)
    {
        LOG.debug(message);
        message_queue.offer(new ConsoleMessage(message,2));
    }
    
    public void alert(String message)
    {
        LOG.debug(message);
        message_queue.offer(new AlertMessage(message));
    }
    
    public void error(String message)
    {
        LOG.error(message);
        message_queue.offer(new ConsoleMessage(message,3));
    }
    
    public void openScriptTab(String script_body)
    {
        LOG.info("need to open new script tab with source: "+script_body);
        message_queue.offer(new OpenScriptMessage(script_body));
    }
    
    public void openScriptTab(String script_title, String script_body)
    {
        LOG.info("need to open new script tab with source: "+script_body);
        message_queue.offer(new OpenScriptMessage(script_title, script_body));
    }
    
    public ArrayList<ScriptCategory> getScriptCategories(String library)
    {
        String app_path=PathHolder.getInstance().path;
        String library_name=library==null?"":library;
        app_path+=System.getProperty("file.separator")+(library_name.length()==0?ConfigUtils.getConfigString("thrones.lib.mainfile", "scripts.scriptlib"):(library_name+".scriptlib"));

        //ConfigUtils.getConfigString("thrones.lib.mainfile", "scripts.scriptlib")
        ArrayList<ScriptCategory> result = new ArrayList();
        DBHelper h=new DBHelper(app_path);
        ResultSet rs=h.runSelect("select category_id, category_name, category_desc, category_image from stored_category;");

        try 
        {
              for (; rs.next();)
              {
                  result.add(new ScriptCategory(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4)));
              }
              rs.close();
              h.closeDatabase();
        } 
        catch (SQLException ex) 
        {
              LOG.error("Error getting script categories: "+ex.getMessage());
        }
        return result;
    }
    
    public ArrayList<BasicScript> getScriptByCategory(String library, String category)
    {
        String app_path=PathHolder.getInstance().path;
        ArrayList<BasicScript> result=new ArrayList();
        String library_name=library==null?"":library;
        String category_name=category==null?"":category;
        app_path+=System.getProperty("file.separator")+(library_name.length()==0?ConfigUtils.getConfigString("thrones.lib.mainfile", "scripts.scriptlib"):(library_name+".scriptlib"));
        DBHelper h=new DBHelper(app_path);
        ResultSet rs=h.runSelect(category_name.length()==0?"select body, name, ifnull(is_public,1), ifnull(do_not_eval,0), category from stored where category is null":"select body, name, ifnull(is_public,1), ifnull(do_not_eval,0), category from stored where category ='"+category+"'");

        try 
        {
              for (; rs.next();)
              {
                  result.add(new BasicScript(rs.getString(1),rs.getString(2),rs.getInt(3)==1,rs.getInt(4)==1,rs.getString(5), (library.length()==0?null:library)));
              }
              rs.close();
              h.closeDatabase();
        } 
        catch (SQLException ex) 
        {
              LOG.error("Error getting scripts: "+ex);
        }
        return result;
    }
    
    public void addLibrary(String library_name)
    {
        String app_path=PathHolder.getInstance().path;
        app_path+=System.getProperty("file.separator")+library_name+".scriptlib";
        DBHelper dh=new DBHelper(app_path);
        dh.runInsertUpdate("CREATE TABLE stored_category ( category_id TEXT, category_name TEXT, category_desc  TEXT, category_image TEXT);");
        dh.runInsertUpdate("CREATE TABLE version (major_version integer, minor_version integer);");
        dh.runInsertUpdate("insert into version(major_version, minor_version) values(0,0)");
        dh.runInsertUpdate("CREATE TABLE stored_source_safe(name TEXT,body TEXT,is_public INTEGER,do_not_eval INTEGER,id TEXT, category TEXT, header TEXT,[desc] TEXT,revision INTEGER PRIMARY KEY AUTOINCREMENT);");
        dh.runInsertUpdate("CREATE TABLE stored (name TEXT PRIMARY KEY, desc TEXT, body TEXT, is_public INTEGER DEFAULT(0), do_not_eval INTEGER, id TEXT, category TEXT, header TEXT);");
        dh.runInsertUpdate("INSERT INTO [stored_category] ([category_id], [category_name], [category_desc], [category_image]) VALUES ('common', 'Common functions', 'Common functions', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACbElEQVR42pWW7W9LURzH+6Jb263tZliLzqZaimEoOuZpNrEGEZONxBDPEckQQeyFJSIR8cJ/fH1P8znLydm5t1eTT7aee3/n+3s8p5koijL9+J9P0D7FxlkYEIMOA5D12CaWJGA3zouCGBZFh2HW8w45x4FsrIgjYIyGREmMil0eY6wbRkSZd4vY5RHMhgSs9yU2qpjoHQ6IKTEh9ok9ogoVx4Ey0eZCAgU82y1qnoDluDgqWoFnU9iNE2UxTqASs/m8uCouidnA89M40EKs2hNCxNagjAchgXviPn9viyXv+QJOzIm2OIZQxUaRo2Amn5MxIoYX4plYEw9jHLH/nyWimo2iQP6iFLwX71K8d1mcpFF6bVZMKWD52uf5MukzaWvYVJViim3S8yGl8DfxUbwUK2IRkWbGKXgVVWtkivxcrKfw/Kf4Lj6JV9hep9sO+pHUxYy4Iu6IR+INHm7ECPwVv8UmIsaxu+xxgg7bqonprP0MWUfcILdPxNs+kfxBxKT2qbglLrBXzR3CUY6IBtGYfu+S3zQ12aTz1rDr0MJ73UG00UzEHBe+56H1dVK85IvYaIaIpprC418JQo9D6XKjKRGNNQidTxsI/UhwZFvh7WeQo3kH6ibUcwzVsrOB6bTP4kuf1jYtfMa2sH+PjDgzM8Px4Bq/pq1dkia+6d/luUA7R96APoBVOm+V7/adFU7pa1si3o8Gt52rDOe0OM89Yo6Km7CItwt871LwLmsXOSDrobvdRjNG+9W5G05Row6ibXLeZn2W2ZrjHZPqw736JvyIMEI7iWiSGrUcmqwZDokjRD1NmhukfPwfNq9+eG48axAAAAAASUVORK5CYII=');");
        dh.runInsertUpdate("INSERT INTO [stored_category] ([category_id], [category_name], [category_desc], [category_image]) VALUES ('math', 'Mathematical functions', 'Everything that you need to calculate', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAADVElEQVR42q1WO0wUURQ9uDO7C8v+ZNlV2LiASABjYRZ/hcZoTIyVRBvsUSlMqCAIDbFQY2UsJZYWxoL4gaix0cpGjZqoiYmVjcvPD4omup735s7wZmcXE+MjJ49h5t1z77mfBwBYhH0dyK0AI2+BXXxuJGJEPRFR7/GPq1wu6916Cmwpq2fiIPCDf5szUPJg42NNIADfChMNk5OTI4rkAIH/A9+Kijx7KkiWdSS2RknDicjvvT+SNUniRJOQ/JaPfiGPAe4ZIiH5CeamKJAU1CS5D/S4OfFFY2GR+24ipSQVaauTOEQ1c+JFQmyg4QL9vmd49JrIi6RRqcZqy6oBvSJiQHmbJVqJbuKNQXSXSBqSWVUIwuKECa/8w3I4IdHkiI3EXmLRILpgRGNXEETERqMgLnuDfO99FBNvm6bZkF+Aiyo/54isWwjACfku6snxCp186//5jDG+S4vjMdcb2yjlFAuh7zawcwrY7iuEEMitJ0KD4b17Li2VmNO5daRvEiJUJRoGOt4BA9+AqU1JzBqyjRqyxPAEXYFI5jAhkjdLrgPJU14maKntBXD0J5Pen8C0QXJeZE2I9kkxlBHDeUGrRJWurHNFVH8DaB8HS5lhvwxjZrBOGtTCJ+59YjQpJAn5PSMytQqMSIIk0ffAfrcxZ9fhe9aNogXDonXaqKL433NSDHRtVCpCebsqUwq3uLcYJHEjElc6dWa9wKiuIImbl1MeQRgfeGyrSJAVI8mKaJJiOBXskyCJzcrvpnBLmqBOD8rjIoErQ8YwmDCSnzZy1bja8dUjeexFkcVlZfQMsHkBGH/AfCnd2f499OKswmdg7BGwT5NEiDxz14EjOopi5RR1SCaMcn3OI82stm1uITwD+lX1XGGVqWe1q6ZVTuhEF7RxN493giQ92IE67z4py+wquVexMnoIuoznCzaW1PNoCMtDIXylKPP6gmM1eudzuORUrElSwOm1rlRzxFD4cpeAZVcerHamU/eUFZRLXVQhjjjnWi0J9DWsI7H0uwU2w1LKiXThKrAyBEbDCDX4Hm04vHo1FFEbzjyrnwF6zZvzIWfaTeCY+8ypee2kMzg7ZKQ0V9w/ay7buDkzMi7aiS6dQeje6ZVLrpNok4b1/V/wB4rSOkpAaUaeAAAAAElFTkSuQmCC');");
        dh.runInsertUpdate("INSERT INTO [stored_category] ([category_id], [category_name], [category_desc], [category_image]) VALUES ('crypto', 'Crypto functions', 'Crypto algorythms', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAAB6klEQVR42rWWSU8CQRCF5wAjKAIuuLK5YNyioqCEgNG4QNBETYzxgtGbBw8e/P/4Cl+bmkkTBgKTfGG6p7peVXdXN06n03H6MchjHT8Kx/3ExiLgFxqbgBYKIhAaXUpep2HgggmFy/7QcOl4nUfAJIiBaRDnb4z9EYoawVBQkRAjjdLhDEiBRbBEpD0LkhZBVxH2BfAfhMtBCf9mQ98WKIANkAUrSjChshSmFDqIrpI04srxNjhS7TNQAgf8tgbSzHABzPuY42yYrLtZyMtcv+KHzQWoSABglxmKYN5HjkEsU/QvC4kqwAkzEAxCBLuLLWmtjkHkGOw4XCBZxEyAQZ+gDZ7BHbgBV4LFtgHOwYlVBO+P4EO1f8A3Rd4ock9HItSwiDzye9UzXbKYFuMvIgLv4IUC4vySXFvGSSAtUPcsPNhkejLwAbxyetp8fwJN7rIqt7bstlqPTMS25tnCMmVcqDIdNTn3EtGt2sJFsA/2+FtkvxnTUsGUPcXIwsqZYqRBxVKM67TLsi4KFCsxwzozOGW/91jhkZFmkW32OFZ0lS+wL09bcXrIgHYYkDPMAanPq2n2pSiWU5Wf4Vhn2KPe3DfG3ojNUzDFdU7absRBLy0dXFRlaE7laJC7PehNaAvuL7Bx/okwzy+bFL/hKn9AeAAAAABJRU5ErkJggg==');");
        dh.runInsertUpdate("INSERT INTO [stored_category] ([category_id], [category_name], [category_desc], [category_image]) VALUES ('interfaces', 'Interface functions', 'Interface functions', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAAB4klEQVR42rWWa1OCQBRA+wCIJqiJaGmKZqWFvWys///H7DKdnbkRu0FTzpzRCO7Z+4Dl5Hg8nvxEk0/l9X8V3CX7F0FZVCe4Bz54iloil8AEbgmh0FaEHPdrCR2CgIBdoSf0FTHHO7WEDkERIDaJyu+JcM73SBgijBCaDAMltEp8To51u+TvK2EtrIS5MBXGQmKR+a4yFis5rXH7fEPJOpTQs01SyIlJKcAr7IUHYUtmmTArZWUyCqpERlI0NhUWSvImvPP9IuyEjRJN6VfCoFiz0ZKiuQsCPQkHJAeVzZ1wS79Mn1KVTWiTtDghoQzXQi48q5I9IblnETfCEtGEyYvUAFQ3XhhwQcZqC9EjFIJclfIO0YqFjShZ2ybx+WePk2dcfEOw3FAajJzFZCxuwGIDmyT85QjnlHdGubuU3zrGXRqY0tA5Db4lo4eS4ECvNgzMiAEKbZIWI9hjRRNEGSNbBNopgRntZ+6fBYtzSvTDsY9oTBmMaMsA7NXUPZLpJddENkk5mwjRGau7oHQrAt5zY+ZkuORB6my83ktClVFUymqqZGv6lbGIRJXKd201RhRYZEOaq7eAlOMxVfh8dtXYz72KXbKDMC5tZr3Kh2PDFwev4bZc8erS7OM5XjK+xP8AlE0+3Iawqa8AAAAASUVORK5CYII=');");
        dh.runInsertUpdate("INSERT INTO [stored_category] ([category_id], [category_name], [category_desc], [category_image]) VALUES ('user', 'User functions', 'User defined functions', null);");
        dh.runInsertUpdate("INSERT INTO [stored_category] ([category_id], [category_name], [category_desc], [category_image]) VALUES ('security', 'Security functions', 'Security functions, accounts, passwords', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACO0lEQVR42q2W32sTQRSF8xBNTVZq2600NC2pWmt/YtTYPlRQtKIgQakPLZVSaJUqLRRBRBBRFHxR8cEHwYf+qfHc5Zt4u02UmF34yGZ27jk7c2fmbq7ZbObStLnyXZBcR/Q6GISAk6Ig+uDUXwh9CsQlGsdMnEEQLonTol+cEQNiEIbc/QDP++lfcob5tMEJHkYExWJEjIqKGBfVNozzfJT+MfFRoudM8s5gqPlMzUL3F8WsWAhttF8TV8UV1zZLfzMuM9rIGxQwiL1Ytyh+SVzGrMKoWtNUtLntxcAZ3WKkU0xhchVI2NmMTB6LO0zneZ8LS1QlI5Md8UjcIE+JSZEkVTMyeSmeiLuiFkxKJOhcRiZvGc0DUQ9Jt1U1LC5kZPJBPBcPWW3JSLI2+ShepE1a0yWu92jwk5HsiobpHUu838H/afKDnGyL+6YX9klrCYv5Hk2+ilesrtuml96MdrhdEss9TNUnsSdWTQe91rES8jLBTr3XpcGh+CJeiy2LR2fC15AwZXbWzNhu7dLku3jH0l1lt8/4sytHNYvcaGqcP+v/EP+FwXuxb/2Jq6ETp+t4H9WtzFG9yLA3Ohh8E59ZTZaHDfovEl9GL5ceTZGSaittms1kgWviqThg3t/we0D7Gv2WiKugU0x/nRwpXmKM1VFnOTYQ2yS5m/xv8LxO/zHio0SvzWeQL8MxbzRJtbO3vClWeOsV/odqOOmqYYROvtO3ljcaZP9Umec5BGv8zrm6PkL/Pwa6fgPUSLKrrvsJsAAAAABJRU5ErkJggg==');");
        dh.runInsertUpdate("INSERT INTO [stored_category] ([category_id], [category_name], [category_desc], [category_image]) VALUES ('logging', 'Logging functions', 'Any kind of logging: console, file, database', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACFUlEQVR42rWW60sUYRSH58OurrrqKuOl8orKrtJKRbBqBooYecHLCkIUFKSYipAgEVhB+iUQpBCkPvb/+D+tv7M8u8yOexllXHg+7Mx7zvNezztOLpdzanGbX9n4MBLXkgURRCAKdT4KzwvtbogqCQpJ60VMNIgmEa9AE21ixERLhD5BhN7FCGwWCdEuXNEpunx08q6dts3ExsgV8QqivIjT2AK7RY8YEENiRCRFCpI8G6JNDzEuOeLkLPbehttCg4cW9O/KyQVBbTPimUgjHiCHS868oFG02vCDJq4ifCGeMtJ+pjQ/glAEHtErMcnIBh3mrSMsgUe0LF6KcZMkwhb4RFMmce9R8lbMO2GuRRnJllhx2NfJexD8FHsi67DF7EBlQhRcihOvxGWb2d6eD0HwX/wR38WOWHU4/lYORtkJtiPei893lPwVv8QX8UG8Lp4TSsE4e3uZnfFJHIlv4pTgM3EufouLCiJrvy82xWzJiWfa0pxWO7XryD6KXXEgDhF/FcfiR41zMnmjdlFvUqyR1aE5scDcbtC7N+Id07HFiPfoyDbv1sQMeSpXYSpqmgqbYc2mCZ5jtEt0IEsnLPkibZ7T4eIv6H1iQWPisXhCoglGPQ0TJZXYd6cHuRlN/AB5H+s4TMJRSNGp3vwSVPl4qHbHtyBv88ltmh/Ria7iDRngM6jc14pX3ugRtyJP8N+eN1wDltmteWTG4UwAAAAASUVORK5CYII=');");
        dh.runInsertUpdate("INSERT INTO [stored_category] ([category_id], [category_name], [category_desc], [category_image]) VALUES ('debug', 'Debugging functions', 'Debug functions', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACl0lEQVR42qWW22sTQRjF89BcmsY2zTa6ahpjXJPWSrGklpp6QYoYezG1KlKtlyjeQEEQ8UVRn/qgr6KC+qL/ZzyznCnH6cZuMPAjXyY7c+ab7zKb6Ha7CRd8BkASpEAGDIIsGOpBls+lOW/gr/V6CNjFc2AY5MEoKACPjIk9ymdyFEyrUJRAmrsb4QL7wscA7HFQlt9ljpWAT2ErlrFCUR5kZZGS2JNgChyXMbWPggrFCiqkIkme/YhMrIs9DxbAaRk7I/asEQVHwAEKDYUbF5E01T2Z2BD7ElgBl0Hbjsv/i+AUmKaQzyM33iTsUWUYZL/HTm+K3RH7rtgXKTTFeHn0ZltkkEE7KJNa7o4jPHgh9lV61ACBSRqwx4okqVhwsqcdQ+Sd2Ld5pMabCcYm74qYFKzIpOsxRD6J/RCsg3NMgnHW0LZIjiKHZdJGDJEvYj8DN3hkJ8AhxmWHSHW3hf8VH3ALXAAzrJsxK5KiSJHpZ3K+Be7EWPg3+Aq2wEvGpcXgV3uJBP/hyStwjzU1yw0XrUiaqaZ9aqWPxX9oDYFlMMc03qsibiGu9iHyU+wHTGPThmqs/PATVe3tPkS+OyKrFKm7IqbP7DdF1Gccfjkx6ezqidN5OzFEvjkpvMnAz7H9+zsCr9llcl7sLbE/i/1R7EemS7AYZ5jCxagU1opfp/vPwWvwBrwHH/j91hHYYGYtsK2U3Yq3DdJcpZNOZ91kQB+Dp+QJF77P/6+BJXCWXgQ8qrxevTb4Pt2cZjdd5GRzWa2BK/xe49gy75HzoMmeVeNmvfCEIm7GAlt0QJcbzJQmj6FJ8XlyktVtFj/GeSW2k+Fw886LRIYJ4DGdK5xU5/0wQbvGzAnYOqp81r61eBQwF2HqD03nMY1ZAoRoAAAAAElFTkSuQmCC');");
        dh.runInsertUpdate("INSERT INTO [stored] ([name], [body], [is_public], [do_not_eval], [id], [category], [header], [desc]) VALUES ('version', 'function version(){return DB.get(''version'').apply();};', 1, null, 'f44cc6bf1d197905db7fd0147012935f', 'user', null, 'returns current version of library');");
        dh.closeDatabase();
        
        registerLibrary(null);
    }
    
    public void addScript(String script_body, String desc, String category, String library_name, boolean is_public)
    {
        BasicScript st= new BasicScript(script_body, desc, is_public, false, category);

        /*for(BasicScript toCheck:basicScripts)
        {
            if(toCheck.compareWith(st)==ScriptingConstants.COMPARISON_SAME)
            {
                //skip
            }
            else if(toCheck.compareWith(st)==ScriptingConstants.COMPARISON_SAME_HEADER_DIFFERENT_BODY)
            {
                ArrayList<String> newDependencies=new ArrayList();
                
                for(BasicScript toCompile:basicScripts)
                {
                    if(toCompile.dependsOn(st))
                    {
                        newDependencies.add(toCompile.getId());
                    }
                }
                //update all compile dependencies
            }
            else
            {
                st.putDb();
                //just insert new script
            }
        }*/
        st.putDb(library_name);
    }
    
    public ArrayList<Library> getExternalLibs(ArrayList<BasicScript> stored_scripts, CommonEngine g)
    {
        ArrayList<Library> result=new ArrayList();
        for(BasicScript bs: stored_scripts)
        {
            if(bs.getLibrary()!=null)
            {
                boolean is_found=false;
                for (Iterator<Library> it = result.iterator(); it.hasNext();) 
                {
                    Library l = it.next();
                    if(l.prefix.equalsIgnoreCase(bs.getLibrary()))
                    {
                        is_found=true;
                        break;
                    }
                }
                //-------------
                if(!is_found)
                {
                    Library ls=new Library();
                    ls.type="user";
                    ls.prefix=bs.getLibrary();
                    ls.methods = new ArrayList();
                    ls.is_external=true;
                    result.add(ls);
                }
            }
        }
        for(Library l: result)
        {
            for(BasicScript bs: stored_scripts)
            {
                if(l.prefix.equalsIgnoreCase(bs.getLibrary()))
                {
                    try
                    {
                        BasicMethod b=new BasicMethod();
                        b.autocomplete=true;
                        b.desc=bs.getDesc();
                        b.name=SourceUtils.getFunctionName(bs.getSource());
                        b.params=SourceUtils.getFunctionParams(bs.getSource());
                        b.desc = bs.getDesc();
                        b.source=bs.getSource();
                        l.methods.add(b);
                    }
                    catch(Exception ex)
                    {
                        LOG.error(ex.getMessage());
                    }
                }
            }
        }
        
        if(g!=null)
        {
            for(Library l: result)
            {
                String object_hash="lib"+java.util.UUID.randomUUID().toString().replace("-", "");

                String libtext="function "+object_hash+"(){};";
                
                libtext+="\n";
                libtext+="var "+l.prefix + " = new "+object_hash+"();";
                LOG.debug("libtext: "+libtext);
                try
                {
                    g.eval(libtext);
                } 
                catch (ScriptException ex) 
                {
                    LOG.error("Error preparing lib "+l.prefix+": "+ex);
                }
                
                for(BasicMethod bm: l.methods)
                {
                    if(bm.name.equalsIgnoreCase("Grab"))
                    {
                        LOG.debug("Debug "+l.prefix+": "+bm.name);
                    }
                    String method_body=object_hash+".prototype."+SourceUtils.getFunctionName(bm.source)+" = function ("
                            +SourceUtils.getFunctionParams(bm.source)+") "+ SourceUtils.getFunctionBody(bm.source);
                    try
                    {
                        g.eval(method_body);
                    } 
                    catch (ScriptException ex) 
                    {
                        LOG.error("Error preparing lib "+l.prefix+": "+ex);
                    }
                    /*libtext+=object_hash+".prototype."+SourceUtils.getFunctionName(bm.source)+" = function ("
                            +SourceUtils.getFunctionParams(bm.source)+") "+ SourceUtils.getFunctionBody(bm.source);
                    libtext+=";\n";*/
                }
            }
        }
        
        return result;
    }
    
    public ArrayList<BasicScript> getExternalScripts(ArrayList<BasicScript> stored_scripts)
    {
        ArrayList<BasicScript> result=new ArrayList();
        for(BasicScript bs: stored_scripts)
        {
            if(bs.getLibrary()==null)
            {
                result.add(bs);
            }
        }
        
        return result;
    }
    
    public final void registerLibrary(CommonEngine jsEngine, String script_body)
    {
        libs.clear();
        basicScripts.clear();
        if(jsEngine!=null)
        {
            LOG.info("Init libraries for thread: "+jsEngine.owner.id);
        }
        else
        {
            LOG.info("FirstLibraryUpload");
        }
        libs.add(new Console(jsEngine));
        libs.add(new NOSQL(jsEngine));
        libs.add(new Http(jsEngine));
        libs.add(new Html(jsEngine));
        libs.add(new Sys(jsEngine));
        libs.add(new Pdf(jsEngine));
        libs.add(new Email(jsEngine));
        libs.add(new Push(jsEngine));
        File dir = new File(PathHolder.getInstance().path);
        File[] files = dir.listFiles(new FilenameFilter() 
        {
            @Override
            public boolean accept(File dir, String name) 
            {
                //LOG.debug("check_file: "+name);
                return name.endsWith(".interface");
            }
        });

        if(files!=null)
        {
            for (File file : files) 
            {
                if (file.isFile()) 
                {
                    try 
                    {
                        try
                        {
                            FileInputStream fis = new FileInputStream(file);
                            PropertyResourceBundle rb= new PropertyResourceBundle(fis);
                            if(rb.getString("interface.type").equalsIgnoreCase("File"))
                            {
                                libs.add(new FileInterface(jsEngine,rb));
                            }
                        }
                        catch(Exception ex)
                        {
                            throw new UnsupportedOperationException(ex.getMessage());
                        }

                    } 
                    catch (Exception ex) 
                    {
                       LOG.error(ex.getMessage());
                    }
                }
            }
        }
        
        
        ArrayList<BasicScript> stored_scripts = getAllStoredScripts();
        libs.addAll(getExternalLibs(stored_scripts, jsEngine));

        basicScripts=getExternalScripts(stored_scripts);
        basicScripts.addAll(BasicFunctions.getBasicScripts());

        if(jsEngine!=null)
        {
            for(Connector c:jsEngine.interfaces)
            {
                if(c!=null) 
                {
                    basicScripts.addAll(c.methods);
                }
            }
            
            for(BasicScript script: basicScripts)
            {
                if(!script.do_not_eval)
                {
                    try 
                    {
                        jsEngine.eval(script.getSource());
                    } 
                    catch (ScriptException ex) 
                    {
                        LOG.error("Error evaluating script: "+script);
                    }
                    catch (NullPointerException ex1) 
                    {
                        LOG.error("Error evaluating script: "+script);
                    }
                }
            }
        }
        /*if(g==null));
        if(g!=null)
        {
            g.
        }
        return 
        {
            ResultSet rs=scriptDatabaseHelper.runSelect("select body, name, ifnull(is_public,1), ifnull(do_not_eval,0), category from stored;");
            try 
            {
                  for (;rs.next();)
                  {
                      basicScripts.add(new BasicScript(
                                                        rs.getString(1)!=null?rs.getString(1):"",
                                                        rs.getString(2)!=null?rs.getString(2):"",
                                                        rs.getInt(3)==1,
                                                        rs.getInt(4)==1,
                                                        rs.getString(5)!=null?rs.getString(5):"")
                                      );
                  }
                  rs.close();
                  scriptDatabaseHelper.closeDatabase();
            } 
            catch (SQLException ex) 
            {
                  java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else 
        {
            for(BasicScript bs: basicScripts)
            {
                try 
                {
                    g.jsEngine.eval(bs.getSource());
                } 
                catch (ScriptException ex) 
                {
                    java.util.logging.Logger.getLogger(LibraryHolder.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }*/
        
    }
    
    public ArrayList<BasicLib> getLibs()
    {
        ArrayList<BasicLib> result=new ArrayList();
        for(Library lib: libs)
        {
            if(lib.g==null)
            {
                result.add(new BasicLib(lib.prefix, lib.type, lib.autocomplete, lib.desc, lib.image, lib.methods));
            }
        }
        return result;
    }
    
    /*public void unregisterEngine(Engine e)
    {
        ArrayList<Library> tmp_libs=new ArrayList();
        for(Library lib: libs)
        {
            if(lib.g!=null)
            {
                if(lib.g.owner.id.equalsIgnoreCase(e.owner.id))
                {
                    tmp_libs.add(lib);
                }
            }
        }
        libs=tmp_libs;
    }*/
    
    public ArrayList<Connector> getInterfacesList()
    {
        ArrayList<Connector> result=new ArrayList();
        File dir = new File(PathHolder.getInstance().path);
        File[] files = dir.listFiles(new FilenameFilter() 
        {
            @Override
            public boolean accept(File dir, String name) 
            {
                //LOG.debug("check_file: "+name);
                return name.endsWith(".interface");
            }
        });
        
        if(files!=null)
        {
            for (File file : files) 
            {
                if (file.isFile()) 
                {
                    Connector w=null;
                    try 
                    {
                        try
                        {
                                w = new Connector(file.getCanonicalPath(), null, null, null) 
                                {

                                @Override
                                public void registerMethods(CommonEngine jsEngine) {
                                    throw new UnsupportedOperationException("Not supported yet.");
                                }

                                @Override
                                public void registerEvents() {
                                    throw new UnsupportedOperationException("Not supported yet.");
                                }

                                @Override
                                public void autoGenerate(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10) {
                                    throw new UnsupportedOperationException("Not supported yet.");
                                }

                                @Override
                                public String getInvokeWrap() {
                                    throw new UnsupportedOperationException("Not supported yet.");
                                }

                                @Override
                                public String getGenerateWrap() {
                                    throw new UnsupportedOperationException("Not supported yet.");
                                }
                                
                                
                            };
                        }
                        catch(Exception ex)
                        {
                            throw new UnsupportedOperationException(ex.getMessage());
                        }
                        
                    } 
                    catch (Exception ex) 
                    {
                       LOG.error(ex.getMessage());
                    }

                    result.add(w);
                }
            }
        }
        
        return result;
    }
}
