/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

/**
 *
 * @author sergk
 */
public class BasicMethod 
{
    public String name;
    public boolean autocomplete;
    public String params;
    public String desc;
    public String type;
    public String source;
    
    public BasicMethod() 
    {
        name="";
        autocomplete=false;
        params="";
        desc="";
        type="";
        source="";
    }
    
    public BasicMethod(String name, String type, boolean autocomplete, String params, String desc)
    {
        this.name=name;
        this.type=type;
        this.autocomplete=autocomplete;
        this.params=params;
        this.desc=desc;
        this.source="";
    }
    
}
