/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core.manager;

/**
 *
 * @author sergk
 */
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.ArrayBlockingQueue;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;


public class NashornEngine extends CommonEngine
{
    private ScriptEngine jsEngine;
    ScriptContext scriptCtxt;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(NashornEngine.class);
    
    public NashornEngine(LimitedMessageQueue q, ArrayBlockingQueue bq, ArrayBlockingQueue eq, LimitedMessageQueue incoming_events_queue, Scriptable owner)
    {
        super(q,  bq,  eq,  incoming_events_queue, owner);
        //this.g =g;
        //ScriptEngine engine = factory.getEngineByName("nashorn");
        ScriptEngineManager scriptEngineMgr = new ScriptEngineManager();
        jsEngine = scriptEngineMgr.getEngineByName("nashorn");

        if (jsEngine != null) {
            scriptCtxt = jsEngine.getContext();
        }


        if (jsEngine == null) {
            LOG.error("No script engine found for JavaScript");
            System.exit(1);
        }
        
        StringWriter scriptOutput = new StringWriter();
        scriptCtxt.setWriter(new PrintWriter(scriptOutput));
    }
    
    @Override
    public Object eval(String scriptBody) throws ScriptException
    {
        return jsEngine.eval(scriptBody);
    }
    
    @Override
    public void put(String name, Object obj)
    {
        jsEngine.put(name, obj);
    }
    
    @Override
    public Object get(String name)
    {
        return jsEngine.get(name);
    }
    
    @Override
    public ScriptContext getContext()
    {
        return jsEngine.getContext();
    }
    
    @Override
    public Bindings getBindings()
    {
        return jsEngine.getBindings(ScriptContext.ENGINE_SCOPE);
    }

    @Override
    public void setBindings(Bindings b) {
        jsEngine.setBindings(b, ScriptContext.GLOBAL_SCOPE);
    }
}

