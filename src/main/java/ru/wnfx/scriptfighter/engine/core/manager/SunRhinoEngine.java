/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core.manager;

/**
 *
 * @author sergk
 */
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.PropertyResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import ru.wnfx.scriptfighter.engine.core.ScriptThread;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.interfaces.Connector;
import ru.wnfx.scriptfighter.interfaces.DatabaseConnector;
import ru.wnfx.scriptfighter.interfaces.EventListener;
import ru.wnfx.scriptfighter.interfaces.ProxyConnector;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import thrones.PathHolder;


public class SunRhinoEngine extends CommonEngine
{
    private ScriptEngine jsEngine;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(SunRhinoEngine.class);
    
    public SunRhinoEngine(LimitedMessageQueue q, ArrayBlockingQueue bq, ArrayBlockingQueue eq, LimitedMessageQueue incoming_events_queue, Scriptable owner)
    {
        super(q,  bq,  eq,  incoming_events_queue, owner);
        //this.g =g;
        ScriptEngineManager scriptEngineMgr = new ScriptEngineManager();
        jsEngine = scriptEngineMgr.getEngineByName("rhino");

        if (jsEngine == null) {
            LOG.error("No script engine found for JavaScript");
            System.exit(1);
        }
        
        StringWriter scriptOutput = new StringWriter();
            jsEngine.getContext().setWriter(new PrintWriter(scriptOutput));
    }
    
    @Override
    public Object eval(String scriptBody) throws ScriptException
    {
        return jsEngine.eval(scriptBody);
    }
    
    @Override
    public void put(String name, Object obj)
    {
        jsEngine.put(name, obj);
    }
    
    @Override
    public Object get(String name)
    {
        return jsEngine.get(name);
    }
    
    @Override
    public ScriptContext getContext()
    {
        return jsEngine.getContext();
    }
    
    @Override
    public Bindings getBindings()
    {
        return jsEngine.getBindings(ScriptContext.ENGINE_SCOPE);
    }

    @Override
    public void setBindings(Bindings b) {
        jsEngine.setBindings(b, ScriptContext.GLOBAL_SCOPE);
    }
}

