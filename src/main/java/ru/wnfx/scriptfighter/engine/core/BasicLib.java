/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author sergk
 */
public class BasicLib 
{
    public String name;
    public boolean autocomplete;
    public String desc;
    public String type;
    public String image;
    public ArrayList<BasicMethod> methods;
    
    public BasicLib() 
    {
        name="";
        autocomplete=false;
        desc="";
        type="";
        image="";
    }
    
    public BasicLib(String name, String type, boolean autocomplete, String desc, String image, ArrayList<BasicMethod> methods)
    {
        this.name=name;
        this.type=type;
        this.autocomplete=autocomplete;
        this.desc=desc;
        this.image=image;
        this.methods=new ArrayList(methods);
        //Collections.copy(this.methods, methods);
    }
}
