/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author sergk
 */
@Retention(RetentionPolicy.RUNTIME)
//@Target({ElementType.METHOD})

public @interface JavaScriptMethod 
{
    public String type();
    public boolean autocomplete();
    public String params();
    public String desc();
}
