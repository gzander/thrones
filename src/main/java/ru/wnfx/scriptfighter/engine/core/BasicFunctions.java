/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;
import ru.wnfx.scriptfighter.interfaces.Connector;
import ru.wnfx.scriptfighter.db.core.DBHelper;
import ru.wnfx.scriptfighter.db.core.Serializer;
import ru.wnfx.scriptfighter.interfaces.Connectorparam;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.PropertyResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import thrones.PathHolder;
import java.sql.ResultSet;
import java.sql.SQLException;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;

/**
 *
 * @author sergk
 */
public class BasicFunctions 
{
    public static ArrayList<BasicScript> getBasicScripts()
    {
        ArrayList<BasicScript> scripts = new ArrayList();
        
        scripts.add(new BasicScript("function  $(obj) {var res = null; res=_.objectToString(obj); if(res!=null) return res; \n var t = typeof (obj);\n if (t != \"object\" || obj === null)\n {  // simple data type\n if (t == \"string\") obj = obj;\n return String(obj);\n  }\n else\n {\n  // recurse array or object\n var n, v, json = [], arr = (obj && obj.constructor == Array);\n  for (n in obj) { v = obj[n]; t = typeof(v);\n  if (t == \"string\")\n v = '\"'+_.objectToString(v)+'\"';\n else if (t == \"object\" && v !== null)\n v = $(v); \n  json.push((arr ? \"\" : '\"' + n + '\":') + String(v));\n }\n  return (arr ? \"[\" : \"{\") + String(json) + (arr ? \"]\" : \"}\");\n }\n };","Serializes objects to string",true,false,"debug"));
        scripts.add(new BasicScript("function sleep(millisec){_.sleep(millisec);}","Sleep in seconds",true,false,"common"));
        scripts.add(new BasicScript("function log(mess){_.log(mess);}","Log info to console",true,false,"logging"));
        scripts.add(new BasicScript("function setDB(db_name){_.setDB(db_name);}","Set personal database",true,false,"common"));
        scripts.add(new BasicScript("function putDB(obj, table, key){_.putDB($(obj),table,key);}","Put obect to common database",true,false,"common"));
        scripts.add(new BasicScript("function getDB(table, key, key_value){return eval('('+_.getDB(table, key, key_value)+')');}","Get object from common database",true,false,"common"));
        scripts.add(new BasicScript("function killDB(table, key, key_value){_.killDB(table, key, key_value);}","Kills objects in database",true,false,"common"));
        scripts.add(new BasicScript("function MD5(str){return _.MD5(str);}","MD5 hash of string",true,false,"crypto"));
        scripts.add(new BasicScript("function MD5UseEncoding(str, encoding){return _.MD5UseEncoding(str,encoding);}","MD5 hash of string using encoding specified",true,false,"crypto"));
        scripts.add(new BasicScript("function canstop1231(){if(_.canstop()) return;}","Serializes objects to string",false,false,"debug"));
        scripts.add(new BasicScript("function requestUserParam(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11){_.requestUserParam(param1, param2===undefined?'':param2, param3===undefined?'':param3, param4===undefined?'':param4, param5===undefined?'':param5, param6===undefined?'':param6, param7===undefined?'':param7, param8===undefined?'':param8, param9===undefined?'':param9, param10===undefined?'':param10, param11===undefined?'':param11);}","Requests given user params",true,false,"interfaces"));
        scripts.add(new BasicScript("function getUserParam(str){return _.getUserParam(str);}","returns given user param",true,false,"interfaces"));
        scripts.add(new BasicScript("function setUserParam(name, value){_.setUserParam(name, value);}","sets given user param",true,false,"interfaces"));
        scripts.add(new BasicScript("function addEventListener(event_name, function_name){return _.addEventListener(event_name, function_name);}","Starts new Event Listener in separate thread",true,false,"common"));
        scripts.add(new BasicScript("function postEvent(event_name, event_object, event_desc){_.postEvent(event_name, $(event_object), event_desc);}","Posts new Event with name and object",true,false,"common"));
        scripts.add(new BasicScript("function makeHttpGet(url){return _.makeHttpGet(url);}","Makes HTTP GET request",true,false,"interfaces"));
        scripts.add(new BasicScript("function clearCookies(){_.clearCookies();}","Clears all of cookies",true,false,"interfaces"));
        scripts.add(new BasicScript("function versionObjects(arr1, arr2){return _.versionObjects(arr1, arr2); }","checks_versions_of_objects",true,false,"interfaces"));
        ////////////////////////////////////////////////////////////////////////
        // CLEAR FUNCTIONS
        ////////////////////////////////////////////////////////////////////////
        scripts.add(new BasicScript("function clearQueues(){_.clearQueues();}","Clears all thread's queues",true,false,"debug"));
        scripts.add(new BasicScript("function clearMessageQueue(){_.clearMessageQueue();}","Clears thread's message queue",true,false,"debug"));
        scripts.add(new BasicScript("function clearEventsQueue(){_.clearEventsQueue();}","Clears thread's event queue",true,false,"debug"));
        scripts.add(new BasicScript("function clearIncomingEventQueue(){_.clearIncomingEventQueue();}","Clears thread's incoming events queue",true,false,"debug"));
        scripts.add(new BasicScript("function clearAllQueues(){_.clearAllQueues();}","Clears all threads queues",true,false,"debug"));
        scripts.add(new BasicScript("function clearMessageQueues(){_.clearMessageQueues();}","Clears all threads messages queues",true,false,"debug"));
        scripts.add(new BasicScript("function clearEventQueues(){_.clearEventQueues();}","Clears all threads events queues",true,false,"debug"));
        scripts.add(new BasicScript("function clearIncomingQueues(){_.clearIncomingQueues();}","Clears all threads incoming events queues",true,false,"debug"));
        ////////////////////////////////////////////////////////////////////////
        // HTTP LISTENER FUNCTIONS
        ////////////////////////////////////////////////////////////////////////
        scripts.add(new BasicScript("function addHTTPListener(event_name, function_name, port){return _.addHTTPListener(event_name, function_name, port);}","Starts new HTTP Listener with single method in separate thread",true,false,"interfaces"));
        scripts.add(new BasicScript("function stopListener(id){_.stopListener(id);}","Stops any listener",true,false,"interfaces"));
        scripts.add(new BasicScript("function addHTTPListenerBunch(mappings, port){return _.addHTTPListenerBunch(mappings, port);}","Starts new HTTP Listener with lots of methods in separate thread",true,false,"interfaces"));
        ////////////////////////////////////////////////////////////////////////
        //
        ////////////////////////////////////////////////////////////////////////
        scripts.add(new BasicScript("function runSeparateThread(function_name, object){t = typeof (object); if(t == 'string') object='\"'+object+'\"'; _.runSeparateThread(function_name, $(object));}","Starts new method in separate thread",true,false,"common"));
        scripts.add(new BasicScript("function runIndependentThread(function_name, object){t = typeof (object); if(t == 'string') object='\"'+object+'\"'; _.runIndependentThread(function_name, $(object));}","Starts new method in independent separate thread",true,false,"common"));
        
        //scripts.addAll(Middleware.getStoredScripts());
        //scripts.addAll(getInterfaceScripts());
        
        //basic_scripts[0]= "function invokeWeb(method_name, params){return eval('('+_.invokeWeb(method_name,$(params))+')');}");
        
        return scripts;
    }
    
    /*public static ArrayList<BasicScript> getInterfaceScripts()
    {
        ArrayList<BasicScript> scripts = new ArrayList();
        
        ArrayList<Connector> interfaces= Middleware.registerInterfaces(null, null, null, null);
        for(Connector c: interfaces)
        {
            scripts.addAll(c.methods);
        }
        
        
        return scripts;
    }
    */
}
