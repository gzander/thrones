/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.messaging;

/**
 *
 * @author sergk
 */
public class EngineMessage 
{
    public String message;
    public int level;
    public int type; //0- message, 1 - debug info, 2 - request params
    public int line;
    
    public EngineMessage()
    {
        type=0;
        line=0;
    }
}
