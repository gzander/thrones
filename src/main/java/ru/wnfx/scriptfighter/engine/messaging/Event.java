/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.messaging;

/**
 *
 * @author sergk
 */
public class Event 
{
    public String event_name;
    public String event_desc;
    public String event_object;
    
    public Event()
    {
        event_name="";
        event_object="";
        event_desc="";
    }
    
    public Event(String event_name, String event_desc)
    {
        this.event_name=event_name;
        this.event_desc=event_desc;
        this.event_object="";
    }
    
    public Event(String event_name, String event_desc, String event_object)
    {
        this.event_name=event_name;
        this.event_desc=event_desc;
        this.event_object=event_object;
    }
    
    @Override
    public String toString()
    {
        return "event {:event_name,:event_desc,:event_object}".replace(":event_name",event_name).replace(":event_desc",event_desc==null?"":event_desc).replace(":event_object",event_object==null?"":event_object);
    }
}
