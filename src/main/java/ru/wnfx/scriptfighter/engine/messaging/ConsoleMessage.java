/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.messaging;

/**
 *
 * @author sergk
 */
public class ConsoleMessage extends EngineMessage 
{
    public ConsoleMessage(String message){this.message=message;}
    public ConsoleMessage(String message, int level){this.message=message; this.level=level;}
    public ConsoleMessage(String message, int level, int line){this.message=message; this.level=level; this.line=line;}
}
