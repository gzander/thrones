/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.messaging;

/**
 *
 * @author sergk
 */
public class RequestMessage extends EngineMessage 
{
    public String json_param_request;
    public RequestMessage(String json_param_request)
    {
        type=2;
        this.json_param_request=json_param_request;
        this.message="Params requested";
    }
}
