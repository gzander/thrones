/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.messaging;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 *
 * @author sergk
 */
public class LimitedMessageQueue<E> extends ConcurrentLinkedQueue<E> 
{
    private int limit;

    public LimitedMessageQueue(int limit) 
    {
        this.limit = limit;
    }

    @Override
    public boolean add(E o) 
    {
        super.add(o);
        while (size() > limit) { super.remove(); }
        return true;
    }
    
    @Override
    public boolean offer(E o) 
    {
        super.offer(o);
        while (size() > limit) { super.remove(); }
        return true;
    }
}
