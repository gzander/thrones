/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.messaging;

import ru.wnfx.scriptfighter.utils.SourceUtils;

/**
 *
 * @author sergk
 */
public class OpenScriptMessage extends EngineMessage 
{
    public String title;
    public OpenScriptMessage(String script_body)
    {
        type=5;
        this.title=SourceUtils.getFunctionName(script_body);
        this.message=script_body;
    }
    public OpenScriptMessage(String script_title, String script_body)
    {
        type=5;
        this.title=script_title;
        this.message=script_body;
    }
}
