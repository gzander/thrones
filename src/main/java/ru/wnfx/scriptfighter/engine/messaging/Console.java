/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.messaging;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import ru.wnfx.scriptfighter.engine.core.*;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;

/**
 *
 * @author sergk
 */
public class Console extends Library
{
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(Console.class);
    
    //LimitedMessageQueue<EngineMessage> message_queue;
    public Console(CommonEngine g)
    {
        prefix="console";
        desc="logging API";
        if(g!=null)
        {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }
    
    
    @JavaScriptMethod(type="messaging", autocomplete=true, params="String message", desc="logs a message to console")
    public void log(String message)
    {
        LOG.debug(message);
        g.message_queue.offer(new ConsoleMessage(message,1));
    }
    
    @JavaScriptMethod(type="messaging", autocomplete=true, params="String message", desc="")
    public void debug(String message)
    {
        LOG.debug(message);
        g.message_queue.offer(new ConsoleMessage(message,0));
    }
     
    @JavaScriptMethod(type="messaging", autocomplete=true, params="String message", desc="")
    public void warning(String message)
    {
        LOG.debug(message);
        g.message_queue.offer(new ConsoleMessage(message,2));
    }
    
    @JavaScriptMethod(type="messaging", autocomplete=true, params="String message", desc="")
    public void error(String message)
    {
        LOG.error(message);
        g.message_queue.offer(new ConsoleMessage(message,3));
    }
}
