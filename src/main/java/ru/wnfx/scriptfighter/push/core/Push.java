/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.push.core;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javapns.*;
import javapns.communication.exceptions.*;
import javapns.notification.*;
import org.json.JSONException;
import org.jsoup.*;
import org.jsoup.nodes.Element;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
/**
 *
 * @author sergk
 */
public class Push extends Library {
    
    /*
     
     
     */
    private String google_server_key;
    private int device_type;
    private String cert_path;
    private String cert_password;
    private String feedback_file;
    List<PushedNotification> notifications=null;
    ArrayList<String> restrictedByFeedback;
    
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(Push.class);
    private String last_url;
    
    private void loadFeedback()
    {
        try
        {
            restrictedByFeedback = new ArrayList();
            if(feedback_file!=null)
            {
                
                String str="";
                InputStreamReader isr = new InputStreamReader(
                        new FileInputStream(feedback_file), "UTF8");
                BufferedReader in = new BufferedReader(isr);

                //OutputStreamWriter osr = new OutputStreamWriter(new FileOutputStream(processed_directory+File.separatorChar+current_file),System.getProperty("dispatcher.file.encoding","utf-8"));

                LOG.debug("IO created");
                while (((str = in.readLine()) != null))
                {
                    LOG.debug("read line: "+str);
                    restrictedByFeedback.add(str);
                    LOG.debug("Line ok");
                }
                LOG.debug("Close all");
                // догрузим оставшееся
                in.close();
            }
        }
        catch(Exception ex)
        {
            LOG.error("trouble to load feedback: "+feedback_file);
        }
    }
    
    private boolean checkToken(String token)
    {
        if(restrictedByFeedback!=null)
        {
            for(String s: restrictedByFeedback)
            {
                if(s.equalsIgnoreCase(token))
                {
                    return false;
                }
            }
        }
        else
        {
            return true;
        }
        return true;
    }
    
    public Push(CommonEngine g)
    {
       // doc=null;
        //elements=null;
        google_server_key = null;
        prefix="Push";
        desc="HTML Parse API";
        last_url=null;
        device_type=0;
        if(g!=null)
        {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);

    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String google_server_key", desc="sets up for google push")
    public Push google(String google_server_key)
    {
        this.google_server_key = google_server_key;
        device_type=1;
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String cert_path, String cert_password", desc="sets up for apple push")
    public Push apple(String cert_path, String cert_password, String feedback_file)
    {
        this.cert_path = cert_path;
        this.cert_password = cert_password;
        this.feedback_file=feedback_file;
        device_type=0;
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String msg, String token", desc="sends any push")
    public Push send(String msg, String token)
    {
        //check URL
        if(device_type==1)
        {
            if(checkToken(token))
            {
                try 
                {
                    Sender sender = new Sender(google_server_key);
                    Message message = new Message.Builder().timeToLive(86400).delayWhileIdle(true).addData("message", msg).build();
                    Result result = null;

                    result = sender.send(message, token, 1);
                    LOG.info("Push notification sent "+result+" to: " + token);
                } 
                catch (IOException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                }
            }
        }
        else if(device_type==0)
        {
            try 
            {  
                notifications = javapns.Push.alert(msg, cert_path, cert_password, true, token);
            } 
            catch (CommunicationException ex) 
            {
                LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
            } 
            catch (KeystoreException ex) 
            {
                LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
            }
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="", desc="analyze results of send")
    public Push analyze()
    {
        if(device_type==0)
        {
            if(notifications != null)
            {
                for (PushedNotification notification : notifications) 
                {
                    if (notification.isSuccessful()) 
                    {
                        /* Apple accepted the notification and should deliver it */  
                        LOG.debug("Push notification sent successfully to: " +
                                                        notification.getDevice().getToken());
                        /* Still need to query the Feedback Service regularly */  
                    } 
                    else 
                    {

                        //String invalidToken = notification.getDevice().getToken();
                        /* Add code here to remove invalidToken from your database */  

                        /* Find out more about what the problem was */  
                        Exception theProblem = notification.getException();
                        LOG.error("Notification exception:"+theProblem.getMessage());

                        /* If the problem was an error-response packet returned by Apple, get it */  
                        ResponsePacket theErrorResponse = notification.getResponse();
                        if (theErrorResponse != null) 
                        {
                                LOG.error("Error sending notification:"+theErrorResponse.getMessage());
                        }
                    }
                }
            }
        }
        else
        {
            notifications=null;
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String sound, String token", desc="sends apple sound only")
    public Push sound(String sound, String token)
    {
        if(device_type==0)
        {
            if(checkToken(token))
            {
                try 
                {  
                    notifications = javapns.Push.sound(sound, cert_path, cert_password, true, token);
                } 
                catch (CommunicationException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                } 
                catch (KeystoreException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                }
            }
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="int badge, String token", desc="sends apple badge only")
    public Push badge(int badge, String token)
    {
        if(device_type==0)
        {
            if(checkToken(token))
            {            
                try 
                {  
                    notifications = javapns.Push.badge(badge, cert_path, cert_password, true, token);
                } 
                catch (CommunicationException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                } 
                catch (KeystoreException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                }
            }
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String msg, String sound, int badge, String token", desc="sends apple complex only")
    public Push combined(String msg, String sound, int badge, String token)
    {
        if(device_type==0)
        {
            if(checkToken(token))
            {
                try 
                {  
                    notifications = javapns.Push.combined(msg, badge, sound, cert_path, cert_password, true, token);
                } 
                catch (CommunicationException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                } 
                catch (KeystoreException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                }
            }
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String action_loc_key, String loc_key, Array loc_args, String token", desc="sends apple localized alert only")
    public Push localized_alert(String action_loc_key, String loc_key, List<String> loc_args, String token) throws javapns.org.json.JSONException {
        if(device_type==0)
        {
            if(checkToken(token))
            {
                try 
                {  
                    PushNotificationPayload payload = PushNotificationPayload.complex();
                    payload.addCustomAlertActionLocKey(action_loc_key);
                    payload.addCustomAlertLocKey(loc_key);

                    if(loc_args.size()>0) {
                        payload.addCustomAlertLocArgs(loc_args);
                    }
                    notifications = javapns.Push.payload(payload, cert_path, cert_password, true, token);
                } 
                catch (CommunicationException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                } 
                catch (KeystoreException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                }
                catch (JSONException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                }
            }
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String action_loc_key, String loc_key, List<String> loc_args, String sound, int badge, String token", desc="sends apple localized complex only")
    public Push localized_combined(String action_loc_key, String loc_key, List<String> loc_args, String sound, int badge, String token) throws javapns.org.json.JSONException {
        if(device_type==0)
        {
            if(checkToken(token))
            {
                try 
                {  
                    PushNotificationPayload payload = PushNotificationPayload.complex();
                    payload.addCustomAlertActionLocKey(action_loc_key);
                    payload.addCustomAlertLocKey(loc_key);

                    if(loc_args.size()>0) {
                        payload.addCustomAlertLocArgs(loc_args);
                    }

                    payload.addBadge(badge);
                    payload.addSound(sound);

                    notifications = javapns.Push.payload(payload, cert_path, cert_password, true, token);
                } 
                catch (CommunicationException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                } 
                catch (KeystoreException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                }
                catch (JSONException ex) 
                {
                    LOG.error("trouble to send: "+token+", exception: "+ex.getMessage());
                }
            }
        }
        
        return this;
    }
}
