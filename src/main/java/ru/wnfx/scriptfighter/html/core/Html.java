/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.html.core;

import com.itextpdf.text.pdf.PdfWriter;
import ru.wnfx.scriptfighter.http.core.*;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.*;
import java.util.zip.GZIPInputStream;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import sun.org.mozilla.javascript.internal.NativeObject;
import sun.org.mozilla.javascript.internal.NativeArray;
import org.jsoup.*;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
//import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import static com.itextpdf.text.pdf.PdfFileSpecification.url;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import gui.ava.html.image.generator.HtmlImageGenerator;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import org.jsoup.nodes.Attributes;
import static ru.wnfx.scriptfighter.http.core.Http.LOG;
import ru.wnfx.scriptfighter.utils.StringUtils;


/**
 *
 * @author sergk
 */
public class Html  extends Library
{
    Document doc;
    Elements elements;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(Html.class);
    private String last_url;
    public Html(CommonEngine g)
    {
        doc=null;
        elements=null;
        prefix="HTML";
        desc="HTML Parse API";
        last_url=null;
        
        if(g!=null)
        {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String url", desc="save HTML as img")
    public void saveToJpeg(String URL)
    {
        //URL url = MyClass.class.getResource("myhtml.html");
        /*try
        {
            Dimension size = new Dimension(1024, 768);
            Image img = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
            JFrame frame = new JFrame();
            frame.setUndecorated(true);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            JEditorPane pane = new JEditorPane(URL) {
                @Override
                protected void paintComponent(Graphics g) {
                    super.paintComponent(g);
                }
            };
            frame.setSize(size);
            frame.add(pane);
            frame.setVisible(true);
            Graphics g = img.getGraphics();
            pane.paintAll(g);
            ImageIO.write((RenderedImage) img, "jpg", new FileOutputStream("myimg.jpg"));
            frame.setVisible(false);
            frame.dispose();
        }
        catch(IOException e)
        {
            LOG.error("Error converting html: "+e.getMessage());
        }*/
        HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
        String jopa = getString(URL);
        imageGenerator.loadHtml(jopa);
        imageGenerator.saveAsImage("hello-world.png");
        imageGenerator.saveAsHtmlWithMap("hello-world.html", "hello-world.png");
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String url", desc="save HTML as PDF")
    public void saveToPdf(String URL)
    {
        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        // step 2
        try
        {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("pdf.pdf"));

            // step 3
            document.open();
            // step 4
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, Get(URL)); 
            //step 5
            document.close();
        }
        catch(IOException e)
        {
            LOG.error("Error converting html: "+e.getMessage());
        }
        catch(DocumentException e)
        {
            LOG.error("Error converting html: "+e.getMessage());
        }
        System.out.println( "PDF Created!" );
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String url", desc="open HTML from URL")
    public Html openUrl(String URL)
    {
        //check URL
        try 
        {
            URL url=new URL(URL);
        } 
        catch (MalformedURLException ex) 
        {
            g.error("Malformed URL: "+URL+", exception: "+ex.getMessage());
        }
        
        try 
        {
            doc = Jsoup.connect(URL).userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36").get();
        }
        catch (IOException ex) 
        {
            g.error("Can't open URL: "+URL+", exception: "+ex.getMessage());
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String page", desc="open HTML from parameter")
    public Html openHTML(String page)
    {

        doc = Jsoup.parse(page);
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String selector", desc="applies selector to")
    public Html useSelector(String selector)
    {
        if(elements==null)
        {
            elements=doc.select(selector);
        }
        else
        {
            elements=elements.select(selector);
        }
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="", desc="returns all of nodes")
    public Object getResult()
    {   
        if(elements!=null)
        {
            ArrayList<Object> tags_list=new ArrayList();
            for (Element e: elements) 
            {
                
                HashMap<String, Object> element_obj = new HashMap();
                element_obj.put("class", e.className());
                element_obj.put("id", e.id());
                element_obj.put("name", e.nodeName());
                element_obj.put("tag", e.tagName());
                element_obj.put("val", e.val());
                element_obj.put("html", e.html());
                element_obj.put("text", e.text());
                Map<String, String> attrs = new HashMap();
                Iterator<Attribute> itr = e.attributes().iterator();
      
                while(itr.hasNext()) {
                   Attribute elt = itr.next();
                   attrs.put(elt.getKey(), elt.getValue());
                }
                element_obj.put("attrs", attrs);
                
                tags_list.add(ObjectUtils.convertNative(element_obj));
                //nobj.
            }
            elements = null;
            return ObjectUtils.convertNative(tags_list);
            /*NativeArray nobj;// = new NativeArray(elements.size());
            Object[] objs=new Object[elements.size()];
            int i=0;
            for (Element e: elements) 
            {
                NativeObject element_nobj = new NativeObject();
                element_nobj.defineProperty("class", e.className(), NativeObject.READONLY);
                element_nobj.defineProperty("id", e.id(), NativeObject.READONLY);
                element_nobj.defineProperty("name", e.nodeName(), NativeObject.READONLY);
                element_nobj.defineProperty("tag", e.tagName(), NativeObject.READONLY);
                element_nobj.defineProperty("val", e.val(), NativeObject.READONLY);
                element_nobj.defineProperty("html", e.html(), NativeObject.READONLY);
                element_nobj.defineProperty("text", e.text(), NativeObject.READONLY);

                //element_nobj.defineProperty("class", e.className(), NativeObject.READONLY);
                
                
                NativeObject attributes_nobj = new NativeObject();
                for(Attribute a: e.attributes())
                {
                    attributes_nobj.defineProperty(a.getKey(), a.getValue(), NativeObject.READONLY);
                }
                element_nobj.defineProperty("attributes", attributes_nobj, NativeObject.READONLY);
                
                //nobj.put(i, nobj, element_nobj);
                objs[i]=element_nobj;
                i++;
                //nobj.
            }
            nobj = new NativeArray(objs);
            return nobj;*/
        }
        return null;
    }
    
    public InputStream Get(String Url)
    {
      
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        URL url;
        try 
        {
            url = new URL(Url);
            HashMap<String, String> headers=new HashMap();
            int status=0;
            URLConnection conn = Url.startsWith("https:")?(HttpsURLConnection)url.openConnection():(HttpURLConnection)url.openConnection();
            
            for (int i = 1; conn.getHeaderFieldKey(i)!= null; i++) 
            {
                headers.put(conn.getHeaderFieldKey(i), conn.getHeaderField(i));
            }
            

            
        
            InputStream is = null;
            try 
            {
                is = conn.getInputStream();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            Map<String, List<String>> gogno = conn.getHeaderFields();
            
            if ("gzip".equals(conn.getContentEncoding())) 
            {
                try 
                {
                    is = new GZIPInputStream(is);
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            String contentType = conn.getHeaderField("Content-Type");
            String charset = null;

            if(contentType!=null)
            {
                for (String param : contentType.replace(" ", "").split(";")) 
                {
                    if (param.startsWith("charset=")) 
                    {
                        charset = param.split("=", 2)[1];
                        break;
                    }
                }
            }

            BufferedReader rd;
            
            if (charset != null) 
            {
                try 
                {
                    rd = new BufferedReader(new InputStreamReader(is, charset));
                } 
                catch (UnsupportedEncodingException ex) 
                {
                    LOG.error("Wrong encoding: "+ex.getMessage());
                    rd = new BufferedReader(new InputStreamReader(is));
                }
            }
            else
            {
                rd = new BufferedReader(new InputStreamReader(is));
            }
            //GZIPInputStream  gzip = new GZIPInputStream (new ByteArrayInputStream (tBytes));
            
            String line;
            StringBuffer response = new StringBuffer(); 
            try 
            {
                //java.io.By
                while((line = rd.readLine()) != null) 
                {
                  response.append(line);
                  //response.append('\r');
                }
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            try 
            {
                rd.close();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            final Document document = Jsoup.parse(response.toString());
            document.select("script").remove();
            document.outputSettings().syntax( Document.OutputSettings.Syntax.xml );
            
            return new ByteArrayInputStream(document.html().getBytes(StandardCharsets.UTF_8));
            //return response.toString();
        } 
        catch (MalformedURLException ex) 
        {
            LOG.error("Wrong URL: "+ex.getMessage());
        }
        catch (IOException ex)
        {
            LOG.error("Some IO trouble: "+ex.getMessage());
        }

        return null;
    }
    
    public String getString(String Url)
    {
      
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        URL url;
        try 
        {
            url = new URL(Url);
            HashMap<String, String> headers=new HashMap();
            int status=0;
            URLConnection conn = Url.startsWith("https:")?(HttpsURLConnection)url.openConnection():(HttpURLConnection)url.openConnection();
            
            for (int i = 1; conn.getHeaderFieldKey(i)!= null; i++) 
            {
                headers.put(conn.getHeaderFieldKey(i), conn.getHeaderField(i));
            }
            

            
        
            InputStream is = null;
            try 
            {
                is = conn.getInputStream();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            Map<String, List<String>> gogno = conn.getHeaderFields();
            
            if ("gzip".equals(conn.getContentEncoding())) 
            {
                try 
                {
                    is = new GZIPInputStream(is);
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            String contentType = conn.getHeaderField("Content-Type");
            String charset = null;

            if(contentType!=null)
            {
                for (String param : contentType.replace(" ", "").split(";")) 
                {
                    if (param.startsWith("charset=")) 
                    {
                        charset = param.split("=", 2)[1];
                        break;
                    }
                }
            }

            BufferedReader rd;
            
            if (charset != null) 
            {
                try 
                {
                    rd = new BufferedReader(new InputStreamReader(is, charset));
                } 
                catch (UnsupportedEncodingException ex) 
                {
                    LOG.error("Wrong encoding: "+ex.getMessage());
                    rd = new BufferedReader(new InputStreamReader(is));
                }
            }
            else
            {
                rd = new BufferedReader(new InputStreamReader(is));
            }
            //GZIPInputStream  gzip = new GZIPInputStream (new ByteArrayInputStream (tBytes));
            
            String line;
            StringBuffer response = new StringBuffer(); 
            try 
            {
                //java.io.By
                while((line = rd.readLine()) != null) 
                {
                  response.append(line);
                  //response.append('\r');
                }
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            try 
            {
                rd.close();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return response.toString();
            //return response.toString();
        } 
        catch (MalformedURLException ex) 
        {
            LOG.error("Wrong URL: "+ex.getMessage());
        }
        catch (IOException ex)
        {
            LOG.error("Some IO trouble: "+ex.getMessage());
        }

        return "";
    }
  
}
