/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.email.core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.*;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.html.core.Html;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
/**
 *
 * @author sergk
 */
public class Email   extends Library {
     Document doc;
    Elements elements;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(Html.class);
    Session mailSession;
    Session pop3Session;
    Session imapSession;
    Store store;
    
    public Email(CommonEngine g)
    {
        doc=null;
        elements=null;
        prefix="Email";
        desc="Email send API";
        
        if(g!=null)
        {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String smtp_server, boolean smtp_auth, boolean ssl_enable, final String user, final String password", desc="open pdf doc")
    public Email Init(String smtp_server, String smtp_port, boolean smtp_auth, boolean ssl_enable, final String user, final String password)
    {
        Properties mailProps=new Properties();
        mailProps.put("mail.smtp.host",smtp_server);
        mailProps.put("mail.smtp.auth", smtp_auth?"true":"false");
        mailProps.put("mail.smtp.ssl.enable", ssl_enable?"true":"false");
        if(ssl_enable)
        {
            mailProps.put("mail.smtp.starttls.enable", "true");
            mailProps.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
            mailProps.put("mail.smtp.socketFactory.port", smtp_port);
        }
        
        
        if(smtp_port.length()>0)
        {
            mailProps.put("mail.smtp.port", smtp_port);
        }
        
        if(smtp_auth)
        {
        //LOG.debug("Adress sender: "+ConfigUtils.getConfigString ("email.address",""));
        mailSession=Session.getDefaultInstance(
                                                        mailProps,new Authenticator()
                                                                     {
                @Override
                                                                        protected PasswordAuthentication getPasswordAuthentication()
                                                                        {
                                                                                return(new PasswordAuthentication(user,password));
                                                                        }
                                                                     }
                                                      );
        }
        else
        {
            mailSession=Session.getDefaultInstance(mailProps);
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String smtp_server, boolean smtp_auth, boolean ssl_enable, final String user, final String password", desc="open pdf doc")
    public Email InitPOP3(String pop3_server, String pop3_port, final String user, final String password)
    {
        Properties mailProps=new Properties();
        
        mailProps.put("mail.pop3.host",pop3_server);
        mailProps.put("mail.pop3.port", pop3_port);
        mailProps.put("mail.pop3.starttls.enable", "true");
        pop3Session = Session.getDefaultInstance(mailProps);

        //create the POP3 store object and connect with the pop server
        try
        {
            store = pop3Session.getStore("pop3s");

            store.connect(pop3_server, user, password);
        }
        catch(Exception ex)
        {
            LOG.error(ex.getMessage()+". Проблема с подключением");
        }
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="String smtp_server, boolean smtp_auth, boolean ssl_enable, final String user, final String password", desc="open pdf doc")
    public Email InitImap(String imap_server, String pop3_port, final String user, final String password)
    {
        //Properties mailProps=new Properties();
        
        Properties props = System.getProperties();
        props.setProperty("mail.store.protocol", "imaps");

        imapSession = Session.getDefaultInstance(props, null);
        // session.setDebug(true);
        try
        {
            store = imapSession.getStore("imaps");
            store.connect(imap_server,user, password);
        }
        catch(Exception ex)
        {
            LOG.error(ex.getMessage()+". Проблема с подключением");
        }
        //create the POP3 store object and connect with the pop server
        
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="List<String> address, String from, String subject, String text, String encoding", desc="sends email")
    public Email send(List<String> address, String from, String subject, String text, String encoding)
    {
        MimeMessage message=new MimeMessage(mailSession);
                
        try
        {
            try
            {

                String[] emails=address.toArray(new String[0]); //адреса получателей
                InternetAddress dests[]=new InternetAddress[emails.length];
                for(int i=0; i<emails.length; i++)
                {
                 dests[i]=new InternetAddress(emails[i].trim().toLowerCase());
                }
                message.setFrom(new InternetAddress(from));
                message.setRecipients(Message.RecipientType.TO, dests);
                //message.setRecipients(Message.RecipientType.TO, dests);
                message.setSubject(subject,encoding);
                Multipart mp=new MimeMultipart();
                MimeBodyPart mbp1=new MimeBodyPart();
                //mbp1.setText("Здравствуйте, "+firstname+" "+lastname+"!\n Для активации аккаунта positimer.ru, скопируйте ссылку "+GenerateActivationLink()+" в адресную строку браузера и перейдите по ней (нажмите Enter)","KOI8-R");
                mbp1.setText(text,encoding); //"KOI8-R"
                //mbp1.setHeader("Content-Type", "text/plain; charset=utf-8");
                //mbp1.setHeader("Content-Transfer-Encoding", "7bit");
                mp.addBodyPart(mbp1);
                message.setContent(mp);
                message.setSentDate(new java.util.Date());
                Transport.send(message);
            }
            catch(AddressException AdEx)
            {
               LOG.error(AdEx.getMessage()+". Проблема с адресом"); 
            }
        }
        catch(javax.mail.MessagingException MEx)
        {
            LOG.error(MEx.getMessage()+". Проблема с сообщением");
        }        

        LOG.info("Message has been sent: "+text+" to "+address);
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="List<String> address, String from, String subject, String text, String file, String filename, String encoding", desc="sends email")
    public Email sendAttach(List<String> address, String from, String subject, String text, String file, String filename, String encoding)
    {
        MimeMessage message=new MimeMessage(mailSession);
                
        try
        {
            try
            {

                String[] emails=address.toArray(new String[0]); //адреса получателей
                InternetAddress dests[]=new InternetAddress[emails.length];
                for(int i=0; i<emails.length; i++)
                {
                 dests[i]=new InternetAddress(emails[i].trim().toLowerCase());
                }
                message.setFrom(new InternetAddress(from));
                message.setRecipients(Message.RecipientType.TO, dests);
                //message.setRecipients(Message.RecipientType.TO, dests);
                message.setSubject(subject,encoding);
                Multipart multipart = new MimeMultipart();
                MimeBodyPart mbp1=new MimeBodyPart();
                //mbp1.setHeader("Content-Type", "text/plain; charset=koi-8");
                //mbp1.setHeader("Content-Transfer-Encoding", "7bit");
                //mbp1.setText("Здравствуйте, "+firstname+" "+lastname+"!\n Для активации аккаунта positimer.ru, скопируйте ссылку "+GenerateActivationLink()+" в адресную строку браузера и перейдите по ней (нажмите Enter)","KOI8-R");
                mbp1.setText(text,encoding); //"KOI8-R"
                multipart.addBodyPart(mbp1);
                
                MimeBodyPart messageBodyPart = new MimeBodyPart();

                

                /*String file = "path of file to be attached";
                String fileName = "attachmentName";*/
                DataSource source = new FileDataSource(file);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(filename);
                multipart.addBodyPart(messageBodyPart);

                message.setContent(multipart);

                System.out.println("Sending");
                /*Multipart mp=new MimeMultipart();
                MimeBodyPart mbp1=new MimeBodyPart();
                //mbp1.setText("Здравствуйте, "+firstname+" "+lastname+"!\n Для активации аккаунта positimer.ru, скопируйте ссылку "+GenerateActivationLink()+" в адресную строку браузера и перейдите по ней (нажмите Enter)","KOI8-R");
                mbp1.setText(text,encoding); //"KOI8-R"
                mp.addBodyPart(mbp1);
                message.setContent(mp);*/
                message.setSentDate(new java.util.Date());
                Transport.send(message);
            }
            catch(AddressException AdEx)
            {
               LOG.error(AdEx.getMessage()+". Проблема с адресом"); 
            }
        }
        catch(javax.mail.MessagingException MEx)
        {
            LOG.error(MEx.getMessage()+". Проблема с сообщением");
            MEx.printStackTrace();
        }        

        LOG.info("Message has been sent: "+text+" to "+address);
        return this;
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="List<String> address, String from, String subject, String text, String file, String filename, String encoding", desc="sends email")
    public String readMessages()
    {
        try
        {
            Folder inbox = store.getFolder("Inbox");
            inbox.open(Folder.READ_ONLY);

            // get the list of inbox messages
            Message[] messages = inbox.getMessages();

            if (messages.length == 0) System.out.println("No messages found.");

            for (int i = 0; i < messages.length; i++) {
              // stop after listing ten messages
              if (i > 10) {
                System.exit(0);
                inbox.close(true);
                store.close();
              }

              System.out.println("Message " + (i + 1));
              System.out.println("From : " + messages[i].getFrom()[0]);
              System.out.println("Subject : " + messages[i].getSubject());
              System.out.println("Sent Date : " + messages[i].getSentDate());
              System.out.println();

            }

            inbox.close(true);
            store.close();
        }
        catch(javax.mail.MessagingException MEx)
        {
            LOG.error(MEx.getMessage()+". Проблема с сообщением");
        } 
        
        return "";
    }
    
    @JavaScriptMethod(type="common", autocomplete=true, params="List<String> address, String from, String subject, String text, String file, String filename, String encoding", desc="sends email")
    public Object readMessagesIMAP(String folder)
    {
        List<Map> result = new ArrayList();
        try
        {
            Folder inbox = store.getFolder("Inbox");
            inbox.open(Folder.READ_WRITE);

            // get the list of inbox messages
            Message[] messages = inbox.getMessages();

            if (messages.length == 0) System.out.println("No messages found.");

            for (int i = 0; i < messages.length; i++) 
            {
              // stop after listing ten messages
              System.out.println("Message " + (i + 1));
              System.out.println("From : " + messages[i].getFrom()[0]);
              System.out.println("Subject : " + messages[i].getSubject());
              System.out.println("Sent Date : " + messages[i].getSentDate());
              System.out.println();
              Map<String, Object> tmp=new HashMap();
              tmp.put("from", messages[i].getFrom()[0]);
              tmp.put("subject", messages[i].getSubject());
              SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
              tmp.put("date", f.format(messages[i].getSentDate()));
              List<String> attachments = new ArrayList();
              
              try
              {
                  Object o = messages[i].getContent();
                  messages[i].setFlag(Flags.Flag.DELETED, true);
                  if(o instanceof String)
                  {
                     tmp.put("text", o.toString());
                  }
                  else
                  {

                    Multipart multipart = (Multipart) messages[i].getContent();
                    // System.out.println(multipart.getCount());

                    for (int j = 0; j < multipart.getCount(); j++) 
                    {
                       BodyPart bodyPart = multipart.getBodyPart(j);
                       if(!bodyPart.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())/* &&
                           !StringUtils.isNotBlank(bodyPart.getFileName())*/) 
                       {
                           continue; // dealing with attachments only
                       } 
                       InputStream is = bodyPart.getInputStream();
                       String real_name = MimeUtility.decodeText(bodyPart.getFileName());
                       File file = new File(folder + System.getProperty("file.separator") + real_name);
                       FileOutputStream fos = new FileOutputStream(file);
                       byte[] buf = new byte[4096];
                       int bytesRead;
                       while((bytesRead = is.read(buf))!=-1) 
                       {
                           fos.write(buf, 0, bytesRead);
                       }
                       fos.close();
                       attachments.add(file.getAbsolutePath());
                    }
                  }
              }
              catch(IOException e)
              {
                  LOG.error(e.getMessage()+". Проблема с сообщением");
              }
              
              tmp.put("attachments", attachments);
              
              result.add(tmp);
            }
           

            inbox.close(true);
            store.close();
        }
        catch(javax.mail.MessagingException MEx)
        {
            LOG.error(MEx.getMessage()+". Проблема с сообщением");
        } 
        
        return ObjectUtils.listToNativeArray(result);
    }
    
}
