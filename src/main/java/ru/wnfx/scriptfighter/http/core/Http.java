/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.http.core;

import java.awt.image.BufferedImage;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.*;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.HttpsURLConnection;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import ru.wnfx.scriptfighter.utils.StringUtils;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.OutputStream; 
import javax.imageio.ImageIO;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import ru.wnfx.scriptfighter.utils.Base64;


/**
 *
 * @author sergk
 */
public class Http  extends Library
{
    private HashMap<String, List<String>> list_of_cookies;
    private HashMap<String, HashMap<String, String>> list_of_headers;
    private HashMap<String, Integer> list_of_statuses;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(Http.class);
    private String last_url;
    public Http(CommonEngine g)
    {
        prefix="HTTP";
        desc="HTTP API";
        last_url=null;
        list_of_cookies=new HashMap();
        list_of_headers=new HashMap();
        list_of_statuses=new HashMap();
        
        if(g!=null)
        {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String url", desc="gets HTTP headers of request")
    public Object getHeaders(String url)
    {
        HashMap<String, String> headers=list_of_headers.get(url);
        
        return ObjectUtils.convertNative(headers);
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String url", desc="gets cookies of request")    
    public Object getCookies(String url)
    {
        
        List<String> cookies=list_of_cookies.get(url);
        
        return ObjectUtils.listToNativeArray(cookies);
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String url", desc="gets status of request")
    public int getStatus(String url)
    {
        return list_of_statuses.get(url);
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="", desc="gets HTTP headers for last request")
    public Object getHeaders()
    {
        return getHeaders(last_url);
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="", desc="gets HTTP cookies for last request")
    public Object getCookies()
    {
        return getCookies(last_url);
    }
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="", desc="gets status for last request")
    public int getStatus()
    {
        return getStatus(last_url);
    }
    
    public String extractDomain(URL url)
    {
        String result="";
        String[] url_parts=url.getHost().split("\\.");
        if(url_parts.length>1)
        {
            result=url_parts[url_parts.length-2]+"."+url_parts[url_parts.length-1];
        }
        else
        {
            result=url.getHost();
        }
        
        return result;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String source, String regexp", desc="regexp implementation")
    public String regexp(String source, String regexp)
    {
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(source);
        if (matcher.find())
        {
            String groups="";
            return matcher.group(0);
        }
        return "";
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String host, int port, int timeout", desc="pings http url")
    public boolean pingHost(String host, int port, int timeout) 
    {
        Socket socket = new Socket();
    try{
        socket.connect(new InetSocketAddress(host, port), timeout);
        return true;
    } catch (IOException e) {
        return false; // Either timeout or unreachable or failed DNS lookup.
    }
}
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String url", desc="makes HTTP get request")
    public String Get(String Url)
    {
      try
       {
        //CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        URL url;
        try 
        {
            url = new URL(Url);
            List<String> cookies=list_of_cookies.get(extractDomain(url));
            HashMap<String, String> headers=new HashMap();
            int status=0;
            
            if(Url.startsWith("https:"))
            {
                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {

                            public java.security.cert.X509Certificate[] getAcceptedIssuers()
                            {
                                return null;
                            }
                            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
                            {
                                //No need to implement.
                            }
                            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
                            {
                                //No need to implement.
                            }
                        }
                };

                // Install the all-trusting trust manager
                try 
                {
                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCerts, new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                } 
                catch (Exception e) 
                {
                    System.out.println(e);
                }
            }
            
            URLConnection conn = Url.startsWith("https:")?(HttpsURLConnection)url.openConnection():(HttpURLConnection)url.openConnection();
            
            if(cookies!=null)
            {
                ArrayList<String> coo = new ArrayList();
                for (String cookie : cookies) 
                {
                    String s=cookie.split(";", 2)[0];
                    coo.add(s);
                    //conn.addRequestProperty("Cookie", cookie.split(";", 2)[0]);
                }
                conn.addRequestProperty("Cookie", StringUtils.join(coo, ";"));
            }
            for (int i = 1; conn.getHeaderFieldKey(i)!= null; i++) 
            {
                headers.put(conn.getHeaderFieldKey(i), conn.getHeaderField(i));
            }
            conn.setConnectTimeout(3000);
            

            
        
            InputStream is = null;
            try 
            {
                is = conn.getInputStream();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            Map<String, List<String>> gogno = conn.getHeaderFields();


            cookies = conn.getHeaderFields().get("Set-Cookie");

            last_url=extractDomain(url);
            list_of_headers.put(extractDomain(url), headers);
            list_of_cookies.put(extractDomain(url), cookies);
            list_of_statuses.put(extractDomain(url), status);
            
            if ("gzip".equals(conn.getContentEncoding())) 
            {
                try 
                {
                    is = new GZIPInputStream(is);
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            String contentType = conn.getHeaderField("Content-Type");
            String charset = null;

            for (String param : contentType.replace(" ", "").split(";")) 
            {
                if (param.startsWith("charset=")) 
                {
                    charset = param.split("=", 2)[1];
                    break;
                }
            }

            BufferedReader rd;
            
            if (charset != null) 
            {
                try 
                {
                    rd = new BufferedReader(new InputStreamReader(is, charset));
                } 
                catch (UnsupportedEncodingException ex) 
                {
                    LOG.error("Wrong encoding: "+ex.getMessage());
                    rd = new BufferedReader(new InputStreamReader(is));
                }
            }
            else
            {
                rd = new BufferedReader(new InputStreamReader(is));
            }
            //GZIPInputStream  gzip = new GZIPInputStream (new ByteArrayInputStream (tBytes));
            
            String line;
            StringBuffer response = new StringBuffer(); 
            try 
            {
                //java.io.By
                while((line = rd.readLine()) != null) 
                {
                  response.append(line);
                  //response.append('\r');
                }
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            try 
            {
                rd.close();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }

            return response.toString();
        } 
        catch (MalformedURLException ex) 
        {
            LOG.error("Wrong URL: "+ex.getMessage());
        }
        catch (IOException ex)
        {
            LOG.error("Some IO trouble: "+ex.getMessage());
        }

        return "";
        }
       catch(Exception ex)
       {
           LOG.error("Some trouble!!!: "+ex.getMessage());
           return "";
       }
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String url", desc="makes HTTP get request")
    public String GetExample(String Url, int length)
    {
       try
       {
        //CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        URL url;
        try 
        {
            url = new URL(Url);
            List<String> cookies=list_of_cookies.get(extractDomain(url));
            HashMap<String, String> headers=new HashMap();
            int status=0;
            
            if(Url.startsWith("https:"))
            {
                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {

                            public java.security.cert.X509Certificate[] getAcceptedIssuers()
                            {
                                return null;
                            }
                            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
                            {
                                //No need to implement.
                            }
                            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
                            {
                                //No need to implement.
                            }
                        }
                };

                // Install the all-trusting trust manager
                try 
                {
                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCerts, new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                } 
                catch (Exception e) 
                {
                    System.out.println(e);
                }
            }
            
            URLConnection conn = Url.startsWith("https:")?(HttpsURLConnection)url.openConnection():(HttpURLConnection)url.openConnection();
            
            if(cookies!=null)
            {
                ArrayList<String> coo = new ArrayList();
                for (String cookie : cookies) 
                {
                    String s=cookie.split(";", 2)[0];
                    coo.add(s);
                    //conn.addRequestProperty("Cookie", cookie.split(";", 2)[0]);
                }
                conn.addRequestProperty("Cookie", StringUtils.join(coo, ";"));
            }
            for (int i = 1; conn.getHeaderFieldKey(i)!= null; i++) 
            {
                headers.put(conn.getHeaderFieldKey(i), conn.getHeaderField(i));
            }
            

            conn.setConnectTimeout(3000);
        
            InputStream is = null;
            try 
            {
                is = conn.getInputStream();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            Map<String, List<String>> gogno = conn.getHeaderFields();


            cookies = conn.getHeaderFields().get("Set-Cookie");

            last_url=extractDomain(url);
            list_of_headers.put(extractDomain(url), headers);
            list_of_cookies.put(extractDomain(url), cookies);
            list_of_statuses.put(extractDomain(url), status);
            
            if ("gzip".equals(conn.getContentEncoding())) 
            {
                try 
                {
                    is = new GZIPInputStream(is);
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            String contentType = conn.getHeaderField("Content-Type");
            String charset = null;

            for (String param : contentType.replace(" ", "").split(";")) 
            {
                if (param.startsWith("charset=")) 
                {
                    charset = param.split("=", 2)[1];
                    break;
                }
            }

            BufferedReader rd;
            
            if (charset != null) 
            {
                try 
                {
                    rd = new BufferedReader(new InputStreamReader(is, charset));
                } 
                catch (UnsupportedEncodingException ex) 
                {
                    LOG.error("Wrong encoding: "+ex.getMessage());
                    rd = new BufferedReader(new InputStreamReader(is));
                }
            }
            else
            {
                rd = new BufferedReader(new InputStreamReader(is));
            }
            //GZIPInputStream  gzip = new GZIPInputStream (new ByteArrayInputStream (tBytes));
            
            String line;
            StringBuffer response = new StringBuffer(); 
            try 
            {
                //java.io.By
                while((line = rd.readLine()) != null && response.length()<length) 
                {
                  response.append(line);
                  //response.append('\r');
                }
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            try 
            {
                rd.close();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }

            return response.toString();
        } 
        catch (MalformedURLException ex) 
        {
            LOG.error("Wrong URL: "+ex.getMessage());
        }
        catch (IOException ex)
        {
            LOG.error("Some IO trouble: "+ex.getMessage());
        }

        return "";
       }
       catch(Exception ex)
       {
           LOG.error("Some trouble!!!: "+ex.getMessage());
           return "";
       }
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String url, Object request_headers, String data", desc="makes HTTP post request")
    public String Post(String url, Object request_headers, String data)
    {
        System.setProperty("https.protocols", "TLSv1.1");
        Map<String,Object> request_headers_map=(Map<String,Object>)ObjectUtils.convert(request_headers);
        //CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        URL targetUrl=null;
        List<String> cookies=null;
        HashMap<String, String> headers=new HashMap();
        int status=0;
        try 
        {
            targetUrl = new URL(url);
            cookies=list_of_cookies.get(extractDomain(targetUrl));
        } 
        catch (MalformedURLException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        URLConnection connection = null;  
        try 
        {
            connection=url.startsWith("https:")?(HttpsURLConnection)targetUrl.openConnection():(HttpURLConnection)targetUrl.openConnection();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        try 
        {
            if(url.startsWith("https:"))
            {
                ((HttpsURLConnection)connection).setRequestMethod("POST");
            }
            else
            {
                ((HttpURLConnection)connection).setRequestMethod("POST");
            }
        } 
        catch (ProtocolException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(cookies!=null)
        {
             ArrayList<String> coo = new ArrayList();
                for (String cookie : cookies) 
                {
                    String s=cookie.split(";", 2)[0];
                    coo.add(s);
                    //conn.addRequestProperty("Cookie", cookie.split(";", 2)[0]);
                }
                connection.addRequestProperty("Cookie", StringUtils.join(coo, ";"));
        }

        for (Map.Entry<String, Object> entry : request_headers_map.entrySet())
        {
            connection.setRequestProperty(entry.getKey(),(String)entry.getValue());
            System.out.println(entry.getKey() + "/" + entry.getValue());
        }
        connection.setRequestProperty("Content-Length", "" + Integer.toString(data.getBytes().length));
        //connection.setRequestProperty("Content-Language", "en-US");  
        
        connection.setUseCaches (false);
        connection.setDoInput(true);
        connection.setDoOutput(true);

        //Send request
        DataOutputStream wr;
        try 
        {
            wr = new DataOutputStream (connection.getOutputStream ());
            wr.writeBytes (data);
            wr.flush ();
            wr.close ();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }

                try {
            status = url.startsWith("https:")?((HttpsURLConnection)connection).getResponseCode():((HttpURLConnection)connection).getResponseCode();
        } catch (IOException ex) {
            Logger.getLogger(Http.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for (int i = 1; connection.getHeaderFieldKey(i)!= null; i++) 
        {
            headers.put(connection.getHeaderFieldKey(i), connection.getHeaderField(i));
        }
        
        Map<String, List<String>> gogno = connection.getHeaderFields();
        cookies = connection.getHeaderFields().get("Set-Cookie");
        last_url=extractDomain(targetUrl);
        list_of_headers.put(extractDomain(targetUrl), headers);
        list_of_cookies.put(extractDomain(targetUrl), cookies);
        list_of_statuses.put(extractDomain(targetUrl), status);

        InputStream is = null;
        try 
        {
            is = connection.getInputStream();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Get Response	
        if ("gzip".equals(connection.getContentEncoding())) 
        {
            try 
            {
                is = new GZIPInputStream(is);
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        //GZIPInputStream  gzip = new GZIPInputStream (new ByteArrayInputStream (tBytes));
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuffer response = new StringBuffer(); 
        try 
        {
            //java.io.By
            while((line = rd.readLine()) != null) 
            {
              response.append(line);
              //response.append('\r');
            }
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        try 
        {
            rd.close();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
        }

        //System.out.println("result is bytes:"+response.toString().getBytes());
        LOG.debug("result is:"+response.toString());

        return response.toString();// new String(Gzip.decompress(response.toString().getBytes()));
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String url, Object request_headers, String data", desc="makes HTTP post request")
    public String PostObject(String url, Object request_headers, Object data)
    {
        Map<String,Object> object_map=(Map<String,Object>)ObjectUtils.convert(data);
        return Post(url, request_headers, StringUtils.urlEncodeUTF8(object_map));
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String url, Object request_headers, String data", desc="makes HTTP post request")
    public String PostForm(String url, Object request_fields)
    {
        System.setProperty("https.protocols", "TLSv1.1");
        Map<String,Object> req_fields=(Map<String,Object>)ObjectUtils.convert(request_fields);
        try
        {
            CloseableHttpClient  httpClient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            for(String key: req_fields.keySet())
            {
                params.add(new BasicNameValuePair(key, req_fields.get(key).toString()));
            }
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            /*params.add(new BasicNameValuePair("username", "John"));
            params.add(new BasicNameValuePair("password", "pass"));*/
            CloseableHttpResponse response = httpClient.execute(httpPost);
            //assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
            httpClient.close();


            LOG.debug("result is:"+response.toString());

            return response.toString();// new String(Gzip.decompress(response.toString().getBytes()));
        }
        catch(IOException ie)
        {
            LOG.error("result is:"+ie.toString());
        }
        
        return null;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String url, Object request_headers, String data", desc="makes HTTP post request")
    public String PostFormMultipart(String url, Object request_fields)
    {
        System.setProperty("https.protocols", "TLSv1.1");
        Map<String,Object> req_fields=(Map<String,Object>)ObjectUtils.convert(request_fields);
        try
        {
            List<String> cookies=null;
            HashMap<String, String> headers=new HashMap();
            int status=0;
            try 
            {
                URL targetUrl = new URL(url);
                cookies=list_of_cookies.get(extractDomain(targetUrl));
            } 
            catch (MalformedURLException ex) 
            {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpPost uploadFile = new HttpPost(url);
            
            
            if(cookies!=null)
            {
                ArrayList<String> coo = new ArrayList();
                for (String cookie : cookies) 
                {
                    String s=cookie.split(";", 2)[0];
                    coo.add(s);
                    //conn.addRequestProperty("Cookie", cookie.split(";", 2)[0]);
                }
                uploadFile.addHeader("Cookie", StringUtils.join(coo, ";"));
            }
    
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            
            for(String key: req_fields.keySet())
            {
                builder.addTextBody(key, req_fields.get(key).toString(), ContentType.MULTIPART_FORM_DATA);
            }

            CloseableHttpResponse response = httpClient.execute(uploadFile);
            HttpEntity responseEntity = response.getEntity();

            return responseEntity.toString();
        }
        catch(IOException ie)
        {
            LOG.error("result is:"+ie.toString());
        }
        
        return null;
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String url, Object request_headers, String data", desc="makes HTTP post request")
    public String getSourceBase64(String url)
    {
        String encoded = null;
        try
        {
            System.setProperty("https.protocols", "TLSv1.1");
            URL imageURL = new URL(url);
            BufferedImage originalImage=ImageIO.read(imageURL);
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            ImageIO.write(originalImage, "jpg", baos );
            
            encoded = Base64.encode(baos.toByteArray());
        }
        catch(MalformedURLException ex)
        {
            LOG.error("result file download is:"+ex.toString());
        }
        catch(IOException ex)
        {
            LOG.error("result file download is:"+ex.toString());
        }
        
        return encoded;
        //Persist - in this case to a file
    }
}
