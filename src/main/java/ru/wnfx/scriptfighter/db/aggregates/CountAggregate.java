/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.aggregates;

/**
 *
 * @author sergk
 */
public class CountAggregate extends Aggregate 
{
    public CountAggregate(String keyfield)
    {
        this.keyfield = keyfield;
    }
    
    @Override
    public String toSQL()
    {
        String select_clause="";
        if(keyfield.length()>0)
        {
            select_clause+=" count("+keyfield+") as "+keyfield;
        }
        return select_clause;
    }
}
