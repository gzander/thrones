/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.sorts;

/**
 *
 * @author sergk
 */
public class SortDesc extends Sort 
{
    public String value;
    public SortDesc(String keyfield)
    {
        this.keyfield = keyfield;
    }
    
    public String toSQL()
    {
        String select_clause="";
        
        if(keyfield.length()>0)
        {
            select_clause+=" "+keyfield+" desc";
        }
        return select_clause;
    }
}
