/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.sorts;


/**
 *
 * @author sergk
 */
public class SortAsc extends Sort 
{
    public String value;
    public SortAsc(String keyfield)
    {
        this.keyfield = keyfield;
    }
    
    public String toSQL()
    {
        String select_clause="";
        
        if(keyfield.length()>0)
        {
            select_clause+=" "+keyfield+" asc";
        }
        return select_clause;
    }
}
