/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.nosql;

import ru.wnfx.scriptfighter.db.sorts.SortDesc;
import ru.wnfx.scriptfighter.db.sorts.Sort;
import ru.wnfx.scriptfighter.db.sorts.SortAsc;
import ru.wnfx.scriptfighter.db.aggregates.SumAggregate;
import ru.wnfx.scriptfighter.db.aggregates.MaxAggregate;
import ru.wnfx.scriptfighter.db.aggregates.MinAggregate;
import ru.wnfx.scriptfighter.db.aggregates.Aggregate;
import ru.wnfx.scriptfighter.db.aggregates.AvgAggregate;
import ru.wnfx.scriptfighter.db.conditions.MoreCondition;
import ru.wnfx.scriptfighter.db.conditions.EqualsCondition;
import ru.wnfx.scriptfighter.db.conditions.LessEqualsCondition;
import ru.wnfx.scriptfighter.db.conditions.LikeCondition;
import ru.wnfx.scriptfighter.db.conditions.Condition;
import ru.wnfx.scriptfighter.db.conditions.LessCondition;
import ru.wnfx.scriptfighter.db.conditions.MoreEqualsCondition;
import ru.wnfx.scriptfighter.db.core.DBHelper;
import ru.wnfx.scriptfighter.db.core.Param;
import ru.wnfx.scriptfighter.db.core.Serializer;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import ru.wnfx.scriptfighter.db.conditions.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import ru.wnfx.scriptfighter.db.aggregates.*;
import ru.wnfx.scriptfighter.db.sorts.*;
import ru.wnfx.scriptfighter.db.core.*;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.utils.ConfigUtils;
import ru.wnfx.scriptfighter.utils.StringUtils;

/**
 *
 * @author sergk
 */
public class NOSQL extends Library
{
    private static String query_body;
    private ArrayList<Condition> conditions;
    private ArrayList<Param> pars;
    private ArrayList<Aggregate> aggs;
    private ArrayList<Sort> sorts;
    private ArrayList<Over> overs;
    public String relation;
    private boolean next_not;
    private boolean next_or;
    private boolean join;
    private int pagesize;
    private int pageno;
    private NOSQL subquery;
    private ArrayList<Join> joins;
    
    
    //private static volatile NOSQL instance;
    public static final Logger LOG=Logger.getLogger(NOSQL.class);
    
    /*public static NOSQL initInstance() 
    {
        NOSQL localInstance = instance;
        if (localInstance == null) {
            synchronized (NOSQL.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new NOSQL();
                }
            }
        }
        return localInstance;
    }
    
    public static NOSQL getInstance() 
    {
        NOSQL localInstance = instance;
        if (localInstance == null) {
            synchronized (NOSQL.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new NOSQL();
                }
            }
        }
        return localInstance;
    }*/
    
    private NOSQL(String cortej)
    {
        conditions=new ArrayList();
        pars=new ArrayList();
        aggs=new ArrayList();
        sorts=new ArrayList();
        joins=new ArrayList();
        LOG.info("First init menu");
        relation=cortej;
         
        InnerDatabase h=null;
        
        
        if(ConfigUtils.getConfigBoolean("thrones.db.use_trans", false))
        {
            h=MemoryDb.initInstance("");
        }
        else
        {
            h=new DBHelper();
        }
        ResultSet rs=h.runSelect("select column_name,data_type from metadata where table_name='"+relation+"'");

        try 
        {
           for (; rs.next();)
           {
               pars.add(new Param(rs.getString(1),"",rs.getString(2)));
           }
           rs.close();
           h.closeDatabase();
        } 
        catch (SQLException ex) 
        {
           java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        next_not=false;
        next_or=false;
        join=false;
        pagesize=0;
        pageno=0;
    }
    
    public NOSQL(CommonEngine g)
    {
        prefix="DB";
        desc="inline database API";
        if(g!=null)
        {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
        conditions=new ArrayList();
        pars=new ArrayList();
        aggs=new ArrayList();
        sorts=new ArrayList();
        joins=new ArrayList();
        LOG.info("First init menu");
    }
    
    /*private NOSQL(NOSQL subq)
    {
        subquery=subq;
        conditions=new ArrayList();
        pars=new ArrayList();
        aggs=new ArrayList();
        sorts=new ArrayList();
        LOG.info("First init menu");
    }*/
    
    /*private String formatClause(ArrayList<Condition> conditions, ArrayList<Param> pars)
    {
        String where_clause=" where ";
        for(Condition c: conditions)
        {
            for(Param p: pars)
            {
                if(p.name.equalsIgnoreCase(c.keyfield))
                {
                    where_clause+=c.toSQL(p.type)+" and ";
                }
            }
            
        }
        where_clause+=" 1=1 ";
        return where_clause;
    }*/
    
    /*public static NOSQL getInstance()
    {
        NOSQL localInstance = instance;
        if (localInstance == null) {
            synchronized (NOSQL.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new NOSQL();
                }
            }
        }
        return localInstance;
    }*/
    
    public static String identifyType(String param_type)
    {
        if(param_type.equalsIgnoreCase("integer")||param_type.equalsIgnoreCase("numeric")||param_type.equalsIgnoreCase("float"))
        {
            return "DOUBLE";
        }
        else
        {
            return "UNKNOWN";
        }
    }
    
    public void refineParams()
    {
        for(int i=0; i< pars.size(); i++)
        {
            Param p  = pars.get(i);
            
            if(p.type.equalsIgnoreCase("UNKNOWN"))
            {
                DBHelper h=new DBHelper();
                ResultSet rs=h.runSelect("select "+p.name+" from "+relation);
                
                try
                {
                    for (; rs.next();)
                    {
                        if(rs.getString(1)!=null)
                        {
                            if(rs.getString(1).length()>0)
                            {
                                if(!StringUtils.isJSONValid(rs.getString(1)))
                                {
                                    p.type="TEXT";
                                    rs.close();
                                    h.closeDatabase();
                                    break;
                                }
                            }
                        }
                    }
                    rs.close();
                    h.closeDatabase();
                }
                catch (SQLException ex) 
                {
                   LOG.info("Error checking column: "+ex.getMessage());
                }
            }
            
            if(p.type.equalsIgnoreCase("UNKNOWN"))
            {
                p.type="COMPLEX";
            }
            
            pars.set(i, p);
        }
        
        DBHelper h=new DBHelper();
        h.runInsertUpdate("delete from metadata where table_name='"+relation+"'");
        h.closeDatabase();
        
        for(Param p:pars)
        {
            h.runInsertUpdate("insert into metadata(table_name,column_name,data_type) values('"+relation+"','"+p.name+"','"+p.type+"')");
            h.closeDatabase();
        }
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String relation", desc="inits interaction with table")
    public NOSQL get(String cortej)
    {
        /*NOSQL localInstance = instance;
        if (localInstance == null) {
            synchronized (NOSQL.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new NOSQL(cortej);
                }
            }
        }
        return localInstance;*/
        conditions=new ArrayList();
        pars=new ArrayList();
        aggs=new ArrayList();
        sorts=new ArrayList();
        joins=new ArrayList();
        LOG.info("First init menu");
        relation=cortej;
         
        InnerDatabase h=null;
        
        
        if(ConfigUtils.getConfigBoolean("thrones.db.use_trans", false))
        {
            h=MemoryDb.initInstance("");
        }
        else
        {
            h=new DBHelper();
        }
        //MemoryDb h=MemoryDb.initInstance("");
        ResultSet rs=h.runSelect("select column_name,data_type from metadata where table_name='"+relation+"'");
        boolean has_metadata=false;
        try 
        {
           for (; rs.next();)
           {
               Param p=new Param(rs.getString(1),"",rs.getString(2));
               p.relation=relation;
               pars.add(p);
               has_metadata=true;
           }
           rs.close();
           h.closeDatabase();
        } 
        catch (SQLException ex) 
        {
           java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(!has_metadata)
        {
            rs=h.runSelect("PRAGMA table_info('"+relation+"');");
            try 
            {
               for (; rs.next();)
               {
                   Param p=new Param(rs.getString(2),"",identifyType(rs.getString(3)));
                   p.relation=relation;
                   pars.add(p);
               }
               rs.close();
               h.closeDatabase();
            } 
            catch (SQLException ex) 
            {
               java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            refineParams();
        }
        
        next_not=false;
        join=false;
        next_or=false;
        pagesize=0;
        pageno=0;
        
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="", desc="pushes result as subquery")
    public NOSQL push()
    {
        NOSQL tmp=new NOSQL(this.g).get(this.relation);
        tmp.subquery=this;
        return tmp;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="DB relation", desc="joins with result")
    public NOSQL join(NOSQL rel)
    {
        join=true;
        joins.add(new InnerJoin(rel));
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String relation", desc="joins with relation")
    public NOSQL join(String rel)
    {
        join=true;
        joins.add(new InnerJoin(rel));
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="DB relation", desc="makes outer join with result")
    public NOSQL outerjoin(NOSQL rel)
    {
        join=true;
        joins.add(new OuterJoin(rel));
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String relation", desc="makes outer join with relation")
    public NOSQL outerjoin(String rel)
    {
        join=true;
        joins.add(new OuterJoin(rel));
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="Param key, String value", desc="query 'equals' condition")
    public NOSQL equals(Param key, String value)
    {
        Condition c=(new EqualsCondition(key,value)).setNot(next_not);
        if(!join)
        {
            conditions.add(c);
        }
        else
        {
            if(joins.size()>0)
            {
                joins.get(joins.size()-1).addCondition(c);
            }
        }
        next_not=false;
        next_or=false;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="Param key, String value", desc="more condition")
    public NOSQL more(Param key, String value)
    {
        conditions.add((new MoreCondition(key,value)).setNot(next_not));
        next_not=false;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="Param key, String value", desc="less condition")
    public NOSQL less(Param key, String value)
    {
        conditions.add((new LessCondition(key,value)).setNot(next_not));
        next_not=false;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="Param key, String value", desc="less equals condition")
    public NOSQL lessequals(Param key, String value)
    {
        conditions.add((new LessEqualsCondition(key,value)).setNot(next_not));
        next_not=false;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="Param key, String value", desc="more equals condition")
    public NOSQL moreequals(Param key, String value)
    {
        conditions.add((new MoreEqualsCondition(key,value)).setNot(next_not));
        next_not=false;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="Param key, String value", desc="like condition")
    public NOSQL like(Param key, String value)
    {
        conditions.add((new LikeCondition(key,value)).setNot(next_not));
        next_not=false;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String key", desc="summary aggregate function")
    public NOSQL sum(String key)
    {
        aggs.add(new SumAggregate(key));
        next_not=false;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String key", desc="average aggregate function")
    public NOSQL avg(String key)
    {
        aggs.add(new AvgAggregate(key));
        next_not=false;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String key", desc="minimum aggregate function")
    public NOSQL min(String key)
    {
        aggs.add(new MinAggregate(key));
        next_not=false;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String key", desc="maximum aggregate function")
    public NOSQL max(String key)
    {
        aggs.add(new MaxAggregate(key));
        next_not=false;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String key", desc="negative function")
    public NOSQL not()
    {
        next_not=true;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String key", desc="paging function, sets size of page")
    public NOSQL pagesize(int pgs)
    {
        pagesize=pgs;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String key", desc="paging function, sets number of page")
    public NOSQL pageno(int pgs)
    {
        pageno=pgs;
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String key", desc="sort column ascending")
    public NOSQL asc(String field)
    {
        sorts.add(new SortAsc(field));
        return this;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="String key", desc="sort column descending")
    public NOSQL desc(String field)
    {
        sorts.add(new SortDesc(field));
        return this;
    }
    
    public String formatQuery()
    {
        String select_clause="select ";
        int i=1;

        String keyfield_type="";
        
        for(Join j: joins)
        {
            NOSQL ns;
            if(j.relation == null)
            {
                ns=j.view;
            }
            else
            {
                ns=(new NOSQL(this.g)).get(j.relation);                
            }
            
            pars.addAll(ns.pars);
        }

        for(Param p:pars)
        {
           boolean is_agg=false;
           
           for(Aggregate agg:aggs)
           {
               if(agg.keyfield.equalsIgnoreCase(p.name))
               {
                   select_clause+=agg.toSQL();
                   if(i!=pars.size()) 
                   {
                       select_clause+=",\n";
                       is_agg=true;
                       break;
                   }
               }
           }
           
           if(!is_agg)
           {
              select_clause+=p.relation+"."+p.name;
              if(i!=pars.size()) 
              {
                  select_clause+=",\n";
              }
           }

           i++;
        }
         
         select_clause+=" from "+((subquery!=null)?"("+subquery.formatQuery()+") "+relation.toLowerCase():relation.toLowerCase());
         
         String join_clause="";
         
         for(Join j: joins)
         {
             join_clause+=j.toSQL(relation);
         }
         
         select_clause+=join_clause;
         
         boolean has_cond=false;
         
         String where_clause="";
         for(Condition c: conditions)
         {
            for(Param p: pars)
            {
                boolean is_agg=false;
                for(Aggregate agg:aggs)
                {
                    if(agg.keyfield.equalsIgnoreCase(p.name))
                    {
                            is_agg=true;
                            break;
                    }
                }
                
                if(!is_agg)
                {
                    if(p.name.equalsIgnoreCase(c.keyfield.name))
                    {
                        where_clause+=c.getOr()+c.getNot()+c.toSQL();
                        has_cond=true;
                    }
                }
            }
            
         }
         if(where_clause.startsWith(" or "))
         {
            where_clause=where_clause.replaceFirst(" or ", "where");
         }
         else
         {
             where_clause=where_clause.replaceFirst(" and ", "where");
         }
         
         if(has_cond)
         {
            select_clause+=where_clause;
         }
         
         String group_clause=" group by ";
         
        i=1; 
        
        boolean has_group=false;
        
        for(Param p:pars)
        {
           boolean is_agg=false;
           
           for(Aggregate agg:aggs)
           {
               if(agg.keyfield.equalsIgnoreCase(p.name))
               {
                       is_agg=true;
                       has_group=true;
                       break;
               }
           }
           
           if(!is_agg)
           {
              group_clause+=p.name;
              if(i!=pars.size()) 
              {
                  group_clause+=",\n";
              }
              
           }

           i++;
        }
        
        if(has_group)
        {
            select_clause+=group_clause;
        }
        
        String sort_clause=" order by ";
         
        i=1; 
        
        boolean has_sort=false;
         
        for(Param p:pars)
        {
           
           for(Sort st:sorts)
           {
               if(p.name.equalsIgnoreCase(st.keyfield))
               {
                    boolean is_agg=false;

                    for(Aggregate agg:aggs)
                    {
                         if(agg.keyfield.equalsIgnoreCase(p.name))
                         {
                                 is_agg=true;
                                 break;
                         }
                    }

                    if(!is_agg)
                    {
                        sort_clause+=st.toSQL();
                        if(i!=sorts.size()) 
                        {
                            sort_clause+=",\n";
                        }
                        has_sort=true;
                    }

                    i++;
               }
           }
        }
        
        if(has_sort)
        {
            select_clause+=sort_clause;
        }
        
        LOG.info(select_clause);
        
        return select_clause;
    }
    
    @JavaScriptMethod(type="database", autocomplete=true, params="", desc="applies ")
    public String apply()
    {
        
        String result="";
        
        String select_clause=formatQuery();
        
        //DBHelper h=new DBHelper();
        InnerDatabase h=null;
        
        
        if(ConfigUtils.getConfigBoolean("thrones.db.use_trans", false))
        {
            h=MemoryDb.initInstance("");
        }
        else
        {
            h=new DBHelper();
        }
        ResultSet rs=null;
        
        Gson gson=new Gson();
        StringWriter sw=new StringWriter();
        JsonWriter jw=new JsonWriter(sw);
        
        try //make single object
        {
            jw.beginArray();
            rs=h.runSelect(select_clause);

            try 
            {
                for (; rs.next();)
                {
                    jw.beginObject();
                    int j=1;
                    for(Param p:pars)
                    {
                        if(p.type.equalsIgnoreCase("TEXT"))
                        {
                            gson.toJson(rs.getString(j), String.class, jw.name(p.name));
                        }
                        else if(p.type.equalsIgnoreCase("COMPLEX"))
                        {
                            Map<String, Object> RootMapObject = new Gson().fromJson(rs.getString(j), Map.class);
                            gson.toJson(RootMapObject, Map.class, jw.name(p.name));
                        }
                        else if(p.type.equalsIgnoreCase("LIST"))
                        {
                            List<Object> RootMapObject = new Gson().fromJson(rs.getString(j), List.class);
                            gson.toJson(RootMapObject, List.class, jw.name(p.name));
                        }                        
                        else
                        {
                            gson.toJson(rs.getDouble(j), Double.class, jw.name(p.name));
                        }
                        j++;
                    }
                    jw.endObject();
                }

                rs.close();
                h.closeDatabase();
            } 
            catch (SQLException ex) 
            {
                java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
            } 
            jw.endArray();
            jw.flush();
            jw.close();

            result=sw.toString();
        }
        catch (IOException ex) 
        {
            java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
        }    
        LOG.info("result of query is "+result);
        return result;
    }
    /**public static Class less()
    {
        return NOSQL.class;
    }*/
    
    /*public static NOSQL get_all()
    {
        return get("a").equals("b","10").asc("");
    }*/
}
