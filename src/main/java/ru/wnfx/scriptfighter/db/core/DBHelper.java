/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.wnfx.scriptfighter.db.core;

/**
 *
 * @author dompc
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;
import thrones.*;
/**
 *
 * @author sergk
 */
public class DBHelper extends InnerDatabase
{
    public static final Logger LOG=Logger.getLogger(DBHelper.class);
    
    public DBHelper()
    {
        // load the sqlite-JDBC driver using the current class loader
        try
        {
            Class.forName("org.sqlite.JDBC");
        }
        catch(ClassNotFoundException Ex)
        {

        }

        String fullPath = PathHolder.getInstance().path;
        connectionString="jdbc:sqlite:"+fullPath+System.getProperty("file.separator")+"thrones.db3";//System.getProperty("slave.connection.string");
        
        LOG.debug("DBHelper created: "+connectionString);
                //"jdbc:sqlite:sample.db";
    }
    
    public DBHelper(String filename)
    {
        if(filename!=null)
        {
            // load the sqlite-JDBC driver using the current class loader
            try
            {
                Class.forName("org.sqlite.JDBC");
            }
            catch(ClassNotFoundException Ex)
            {

            }

            String fullPath = PathHolder.getInstance().path;
            connectionString="jdbc:sqlite:"+fullPath+System.getProperty("file.separator")+filename;//System.getProperty("slave.connection.string");

            LOG.debug("DBHelper created: "+connectionString);
        }
        else
        {
            try
            {
                Class.forName("org.sqlite.JDBC");
            }
            catch(ClassNotFoundException Ex)
            {

            }

            String fullPath = PathHolder.getInstance().path;
            connectionString="jdbc:sqlite:"+fullPath+System.getProperty("file.separator")+"thrones.db3";//System.getProperty("slave.connection.string");

            LOG.debug("DBHelper created: "+connectionString);
        }
                //"jdbc:sqlite:sample.db";
    }
    
    /*
     * Connection connection = DriverManager.getConnection("jdbc:sqlite::memory:");
     * Connection connection = DriverManager.getConnection("jdbc:sqlite::memory:");
     * Statement s = connection.createStatement(); 
     *   s.execute("ATTACH 'myDB.db' AS fs");
     *   s.executeUpdate("CREATE VIRTUAL TABLE coordinates USING rtree(id, min_longitude, max_longitude, min_latitude, max_latitude)");
     *   s.executeUpdate("INSERT INTO coordinates (id, min_longitude, max_longitude, min_latitude, max_latitude)
     *   SELECT id, min_longitude, max_longitude, min_latitude, max_latitude FROM fs.coordinates");
     *   s.executeUpdate("CREATE TABLE locations AS SELECT * from fs.locations");
     *   s.execute("DETACH DATABASE fs");
     */
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public ResultSet runSelect(String select_text)
    {
        LOG.debug("try to query: "+select_text);
        ResultSet result=null;
        try
        {
            // create a database connection
            connection = DriverManager.getConnection(connectionString);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            result = statement.executeQuery(select_text);
            /*while(rs.next())
            {
              // read the result set
              System.out.println("name = " + rs.getString("name"));
              System.out.println("id = " + rs.getInt("id"));
            }*/
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory", 
          // it probably means no database file is found
            e.printStackTrace();
          LOG.error("Error running select:"+e.getMessage());
        }
        finally
        {
          /*  try
            {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e)
            {
              // connection close failed.
              LOG.error("Error closing: "+e);
            }*/
        }
        return result;
    }
    
    
    @Override
    public void closeDatabase()
    {
        try
            {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e)
            {
              // connection close failed.
              LOG.error("Error closing: "+e);
            }
    }
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int runInsertUpdate(String insert_text)
    {
        int result=0;
        LOG.debug("try to run: "+insert_text);
        try
        {
            // create a database connection
            connection = DriverManager.getConnection(connectionString);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            statement.executeUpdate(insert_text);
            result=statement.getUpdateCount();
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory", 
          // it probably means no database file is found
          LOG.error("Error running statement:"+e.getMessage());
        }
        finally
        {
          /*  try
            {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e)
            {
              // connection close failed.
              LOG.error("Error closing: "+e);
            }*/
        }
        return result;
    }
    
    public int runInsertUpdateBunch(String[] bunch, int[] checksums)
    {
        int result=0;

        LOG.debug("try to run batch");
        int[] tmp_result;
        try
        {
            // create a database connection
            connection = DriverManager.getConnection(connectionString);
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            for(int i=0; i< bunch.length;i++)
            {
                if(bunch[i]!=null)
                {
                    LOG.debug("try to run: "+bunch[i]);
                    statement.addBatch(bunch[i]);
                }
            }
            tmp_result=statement.executeBatch();
            
            connection.commit();
            
            for(int i=0; i<tmp_result.length;i++)
            {
                result+=tmp_result[i];
                /*if(tmp_result[i]!=checksums[i])
                {
                    LOG.error("checksum not match for: "+bunch[i]+", difference is: memory checksum is "+checksums[i]+", disk checksum is "+tmp_result[i]);
                    connection.rollback();
                    //connection.close();
                    MemoryDb.getInstance().backupFull();
                }*/
            }
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory", 
          // it probably means no database file is found  
          LOG.error("Error running statement: "+e.getMessage());
          result=-1;
          
          try
          {
              connection.rollback();
              //connection.close();
              MemoryDb.getInstance().backupFull();
          }
          catch(SQLException ex)
          {
              LOG.error("Error running backup: "+ex.getMessage());
          }
        }
        finally
        {
          /*  try
            {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e)
            {
              // connection close failed.
              LOG.error("Error closing: "+e);
            }*/
        }
        return result;
    }
    
    @Override
    public boolean isTableExists(String tableName)
    {
        LOG.debug("try to check_table: "+tableName);
        boolean res=false;
        ResultSet result=null;
        try
        {
            // create a database connection
            connection = DriverManager.getConnection(connectionString);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            result = statement.executeQuery("SELECT * FROM sqlite_master WHERE type = 'table' AND name = '"+tableName+"'");
            while(result.next())
            {
              // read the result set
              res=true;
            }
            
            result.close();
            connection.close();
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory", 
          // it probably means no database file is found
          LOG.error("Error running select:"+e.getMessage());
        }
        finally
        {
            try
            {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e)
            {
              // connection close failed.
              LOG.error("Error closing: "+e);
            }
        }
        return res;
    }
}
