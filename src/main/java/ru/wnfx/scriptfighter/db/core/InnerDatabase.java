/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.wnfx.scriptfighter.db.core;

/**
 *
 * @author dompc
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;
import thrones.*;
/**
 *
 * @author sergk
 */
public abstract class InnerDatabase 
{
    Connection connection = null;
    String connectionString;
    
    public abstract ResultSet runSelect(String select_text);
       
    public abstract void closeDatabase();
    ////////////////////////////////////////////////////////////////////////////
    public abstract int runInsertUpdate(String insert_text);

    public abstract boolean isTableExists(String tableName);
}
