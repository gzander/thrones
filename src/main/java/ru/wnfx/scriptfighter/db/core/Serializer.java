/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.core;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import ru.wnfx.scriptfighter.utils.ConfigUtils;
import ru.wnfx.scriptfighter.utils.StringUtils;

/**
 *
 * @author sergk
 */
public class Serializer 
{
    /*TODO отэкранировать все кавычки, проверять набор параметров из таблицы и метаданных и если что добавлять колонки*/
    public static void Serialize(String db_name, String table_name, ArrayList<Param> pars, String key_field)
    {
        //DBHelper h=new DBHelper();
        InnerDatabase h=null;
        
        
        if(ConfigUtils.getConfigBoolean("thrones.db.use_trans", false))
        {
            h=MemoryDb.initInstance("");
        }
        else
        {
            h=new DBHelper(db_name);
        }
        
        if(db_name!=null)
        {
            if(!h.isTableExists("metadata"))
            {
                String create_clause="create table if not exists metadata(table_name  VARCHAR, column_name VARCHAR, data_type   VARCHAR);";
                int result=h.runInsertUpdate(create_clause);
                h.closeDatabase();
            }
        }
        
        
        if(!h.isTableExists(table_name))
        {
            String create_clause="create table if not exists "+table_name+"(";
            int i=1;
            for(Param p:pars)
            {
                create_clause+=p.name+" "+p.castType()+(key_field.equalsIgnoreCase(p.name)?" PRIMARY KEY":"");
                if(i!=pars.size()) create_clause+=",\n";
                i++;
            }

            create_clause+=");";

            
            int result=h.runInsertUpdate(create_clause);

            h.closeDatabase();
            
            for(Param p:pars)
            {
                h.runInsertUpdate("insert into metadata(table_name,column_name,data_type) values('"+table_name+"','"+p.name+"','"+p.type+"')");
                h.closeDatabase();
            }
        }
        else
        {
            ArrayList<Param> existing_pars=new ArrayList();
         
            ResultSet rs=h.runSelect("select column_name,data_type from metadata where table_name='"+table_name+"'");
           
            String select_clause="select ";

            try 
            {
               for (; rs.next();)
               {
                   existing_pars.add(new Param(rs.getString(1),"",rs.getString(2)));
               }
               rs.close();
               h.closeDatabase();
            } 
            catch (SQLException ex) 
            {
               Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            for(Param p:pars)
            {
                boolean already_exists=false;
                for(Param pp:existing_pars)
                {
                    if(pp.name.equalsIgnoreCase(p.name))
                    {
                        already_exists=true;
                        break;
                    }
                }
                
                if(!already_exists)
                {
                    String add_clause="alter table "+table_name+" add column "+p.name+" "+p.castType()+";";
                    int result=h.runInsertUpdate(add_clause);
                    h.closeDatabase();
                    h.runInsertUpdate("insert into metadata(table_name,column_name,data_type) values('"+table_name+"','"+p.name+"','"+p.type+"')");
                    h.closeDatabase();
                }
            }
        }
        
        ///////////////////////////////////////////
        // CHECK PARAMS SET
        //////////////////////////////////////////
        
        
        boolean has_same=false;
        String select_clause="select 1 from "+table_name+" where "+key_field+"=";
        for(Param p:pars)
        {
            String comma=(p.type.equalsIgnoreCase("TEXT")||p.type.equalsIgnoreCase("COMPLEX")||p.type.equalsIgnoreCase("LIST"))?"'":"";
            select_clause+=(key_field.equalsIgnoreCase(p.name)?(comma+p.val+comma):"");
        }

        ResultSet rs=h.runSelect(select_clause);
        try 
        {
            for (; rs.next();)
            {
                has_same=true;
                break;
            }
            rs.close();
            h.closeDatabase();
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(has_same)
        {
            String update_clause="update "+table_name+" set ";
            int i=1;
            for(Param p:pars)
            {
                if(!key_field.equalsIgnoreCase(p.name))
                {
                    String comma=(p.type.equalsIgnoreCase("TEXT")||p.type.equalsIgnoreCase("COMPLEX")||p.type.equalsIgnoreCase("LIST"))?"'":"";
                    update_clause+=p.name+"="+comma+p.val+comma;
                    if(i!=pars.size()) update_clause+=",\n";
                }
                i++;
            }
            if(update_clause.lastIndexOf(",\n")==update_clause.length()-2)
            {
                update_clause=update_clause.substring(0, update_clause.length()-2);
            }
            
            update_clause+=" where ";
            i=1;
            for(Param p:pars)
            {
                if(key_field.equalsIgnoreCase(p.name))
                {
                    String comma=(p.type.equalsIgnoreCase("TEXT")||p.type.equalsIgnoreCase("COMPLEX")||p.type.equalsIgnoreCase("LIST"))?"'":"";
                    update_clause+=p.name+"="+comma+p.val+comma;
                }
            }

            int result=h.runInsertUpdate(update_clause);

            h.closeDatabase();
            
        }
        else
        {
            String insert_clause="insert into "+table_name+" (";
            int i=1;
            for(Param p:pars)
            {
                String comma=(p.type.equalsIgnoreCase("TEXT")||p.type.equalsIgnoreCase("COMPLEX")||p.type.equalsIgnoreCase("LIST"))?"'":"";
                insert_clause+=p.name;
                if(i!=pars.size()) insert_clause+=",\n";
                i++;
            }
            
            insert_clause+=") values (";
            i=1;
            for(Param p:pars)
            {
                String comma=(p.type.equalsIgnoreCase("TEXT")||p.type.equalsIgnoreCase("COMPLEX")||p.type.equalsIgnoreCase("LIST"))?"'":"";
                insert_clause+=comma+p.val+comma;
                if(i!=pars.size()) insert_clause+=",\n";
                i++;
            }
            
            insert_clause+=")";
            
            int result=h.runInsertUpdate(insert_clause);

            h.closeDatabase();
            
        }
        
        //h.closeDatabase();
    }
    
    public static String Deserialize(String db_name, String table_name, String key_field, String key_value)
    {
         String result="{}";
         
        InnerDatabase h=null;
        
        if(ConfigUtils.getConfigBoolean("thrones.db.use_trans", false))
        {
            h=MemoryDb.initInstance("");
        }
        else
        {
            h=new DBHelper(db_name);
        }
        
        if(!h.isTableExists(table_name))
        {
            return null;
        }
        
         ResultSet rs=h.runSelect("select column_name,data_type from metadata where table_name='"+table_name+"'");
         Gson gson=new Gson();
         StringWriter sw=new StringWriter();
         JsonWriter jw=new JsonWriter(sw);
         
         ArrayList<Param> pars=new ArrayList();
         
         String select_clause="select ";
         
         try 
         {
            for (; rs.next();)
            {
                pars.add(new Param(rs.getString(1),"",rs.getString(2)));
            }
            rs.close();
            h.closeDatabase();
         } 
         catch (SQLException ex) 
         {
            Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
         }
         
         int i=1;
         
         String keyfield_type="";
         
         for(Param p:pars)
         {
            if(key_field.equalsIgnoreCase(p.name))
            {
                keyfield_type=p.type;
            }
            select_clause+=p.name;
            if(i!=pars.size()) select_clause+=",\n";

            i++;
         }
         
         select_clause+=" from "+table_name+" ";
         
         if(key_field!=null)
         {
             if(key_field.length()>0)
             {
                 String comma=(keyfield_type.equalsIgnoreCase("TEXT")||keyfield_type.equalsIgnoreCase("COMPLEX")||keyfield_type.equalsIgnoreCase("LIST"))?"'":"";
                 
                 if(StringUtils.isNumeric(key_value)&&StringUtils.isInteger(key_value))
                 {
                     comma="'";
                 }
                 select_clause+=" where "+key_field+"="+comma+key_value+comma;
             }
         }
         
         select_clause+=";";
         
         rs=h.runSelect(select_clause);
         
         int count_records=0;
         
         try 
         {
            for (; rs.next();)
            {
                count_records++;
            }
            rs.close();
            h.closeDatabase();
         } 
         catch (SQLException ex) 
         {
            Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
         }
         
         if(count_records>1) // make array
         {
            try //make single object
            {
                rs=h.runSelect(select_clause);
                jw.beginArray();
                try 
                {
                    for (; rs.next();)
                    {
                        jw.beginObject();
                        int j=1;
                        for(Param p:pars)
                        {
                            if(p.type.equalsIgnoreCase("TEXT"))
                            {
                                gson.toJson(rs.getString(j), String.class, jw.name(p.name));
                            }
                            else if(p.type.equalsIgnoreCase("COMPLEX"))
                            {
                                try
                                {
                                    Map<String, Object> RootMapObject = new Gson().fromJson(rs.getString(j), Map.class);
                                    gson.toJson(RootMapObject, Map.class, jw.name(p.name));
                                }
                                catch(Exception ex)
                                {
                                    ex.printStackTrace();
                                    gson.toJson(rs.getString(j), String.class, jw.name(p.name));
                                }
                            }
                            else if(p.type.equalsIgnoreCase("LIST"))
                            {
                                try
                                {
                                    List<Object> RootMapObject = new Gson().fromJson(rs.getString(j), List.class);
                                    gson.toJson(RootMapObject, List.class, jw.name(p.name));
                                }
                                catch(Exception ex)
                                {
                                    ex.printStackTrace();
                                    gson.toJson(rs.getString(j), String.class, jw.name(p.name));
                                }
                            }                        
                            else
                            {
                                try
                                {
                                    gson.toJson(rs.getDouble(j), Double.class, jw.name(p.name));
                                }
                                catch(Exception ex)
                                {
                                    ex.printStackTrace();
                                    gson.toJson(rs.getString(j), String.class, jw.name(p.name));
                                }
                            }
                            j++;
                        }
                        jw.endObject();
                    }
                    
                    rs.close();
                    h.closeDatabase();
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
                } 
                jw.endArray();
                
                result=sw.toString();
                jw.flush();
                sw.flush();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
            }             
         }
         else if(count_records==0) // make empty object
         {
             result="{}";
         }
         else//make single object
         {
            try //make single object
            {
                rs=h.runSelect(select_clause);
                jw.beginObject();
                try 
                {
                    for (; rs.next();)
                    {
                        int j=1;
                        for(Param p:pars)
                        {
                            if(p.type.equalsIgnoreCase("TEXT"))
                            {
                                try
                                {
                                    gson.toJson(rs.getString(j), String.class, jw.name(p.name));
                                }
                                catch(Exception ex)
                                {
                                    ex.printStackTrace();
                                    gson.toJson(rs.getString(j), String.class, jw.name(p.name));
                                }
                            }
                            else if(p.type.equalsIgnoreCase("COMPLEX"))
                            {
                                try
                                {
                                    Map<String, Object> RootMapObject = new Gson().fromJson(rs.getString(j), Map.class);
                                    gson.toJson(RootMapObject, Map.class, jw.name(p.name));
                                }
                                catch(Exception ex)
                                {
                                    ex.printStackTrace();
                                    gson.toJson(rs.getString(j), String.class, jw.name(p.name));
                                }
                            }
                            else if(p.type.equalsIgnoreCase("LIST"))
                            {
                                try
                                {
                                    List<Object> RootMapObject = new Gson().fromJson(rs.getString(j), List.class);
                                    gson.toJson(RootMapObject, List.class, jw.name(p.name));
                                }
                                catch(Exception ex)
                                {
                                    ex.printStackTrace();
                                    gson.toJson(rs.getString(j), String.class, jw.name(p.name));
                                }
                            }                        
                            else
                            {
                                try
                                {  
                                    gson.toJson(rs.getDouble(j), Double.class, jw.name(p.name));
                                }
                                catch(Exception ex)
                                {
                                    ex.printStackTrace();
                                    gson.toJson(rs.getString(j), String.class, jw.name(p.name));
                                }
                            }
                            j++;
                        }
                    }
                    
                    rs.close();
                    h.closeDatabase();
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
                } 

                jw.endObject();
                
                result=sw.toString();
                jw.flush();
                sw.flush();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
            }
         }

         return result;
    }
    
    public static Object DeserializeMap(String db_name, String table_name, String key_field, String key_value)
    {
        Object result = new HashMap();
         
        InnerDatabase h=null;
        
        if(ConfigUtils.getConfigBoolean("thrones.db.use_trans", false))
        {
            h=MemoryDb.initInstance("");
        }
        else
        {
            h=new DBHelper(db_name);
        }
        
        if(!h.isTableExists(table_name))
        {
            return null;
        }
        
         ResultSet rs=h.runSelect("select column_name,data_type from metadata where table_name='"+table_name+"'");
         
         ArrayList<Param> pars=new ArrayList();
         
         String select_clause="select ";
         
         try 
         {
            for (; rs.next();)
            {
                pars.add(new Param(rs.getString(1),"",rs.getString(2)));
            }
            rs.close();
            h.closeDatabase();
         } 
         catch (SQLException ex) 
         {
            Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
         }
         
         int i=1;
         
         String keyfield_type="";
         
         for(Param p:pars)
         {
            if(key_field.equalsIgnoreCase(p.name))
            {
                keyfield_type=p.type;
            }
            select_clause+=p.name;
            if(i!=pars.size()) select_clause+=",\n";

            i++;
         }
         
         select_clause+=" from "+table_name+" ";
         
         if(key_field!=null)
         {
             if(key_field.length()>0)
             {
                 String comma=(keyfield_type.equalsIgnoreCase("TEXT")||keyfield_type.equalsIgnoreCase("COMPLEX")||keyfield_type.equalsIgnoreCase("LIST"))?"'":"";
                 
                 if(StringUtils.isNumeric(key_value)&&StringUtils.isInteger(key_value))
                 {
                     comma="'";
                 }
                 select_clause+=" where "+key_field+"="+comma+key_value+comma;
             }
         }
         
         select_clause+=";";
         
         rs=h.runSelect(select_clause);
         
         int count_records=0;
         
         try 
         {
            for (; rs.next();)
            {
                count_records++;
            }
            rs.close();
            h.closeDatabase();
         } 
         catch (SQLException ex) 
         {
            Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
         }
         
         if(count_records>1) // make array
         {
            try //make single object
            {
                rs=h.runSelect(select_clause);
                List<Object> tmp_array=new ArrayList();
                try 
                {
                    for (; rs.next();)
                    {
                        Map<String, Object> tmp_map = new HashMap();
                        int j=1;
                        for(Param p:pars)
                        {
                            if(p.type.equalsIgnoreCase("TEXT"))
                            {
                                tmp_map.put(p.name, rs.getString(j));
                            }
                            else if(p.type.equalsIgnoreCase("COMPLEX"))
                            {
                                Map<String, Object> RootMapObject = new Gson().fromJson(rs.getString(j), Map.class);
                                tmp_map.put(p.name, RootMapObject);
                            }
                            else if(p.type.equalsIgnoreCase("LIST"))
                            {
                                List<Object> RootMapObject = new Gson().fromJson(rs.getString(j), List.class);
                                tmp_map.put(p.name, RootMapObject);
                            }                        
                            else
                            {
                                tmp_map.put(p.name, rs.getDouble(j));
                            }
                            j++;
                        }
                        tmp_array.add(tmp_map);
                    }
                    
                    result = tmp_array;
                    rs.close();
                    h.closeDatabase();
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
                } 
            } 
            catch (Exception ex) 
            {
                Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
            }  
         }
         else if(count_records==0) // make empty object
         {
             //result="{}";
             result = new HashMap();
         }
         else//make single object
         {
            try //make single object
            {
                rs=h.runSelect(select_clause);
                Map<String, Object> tmp_map = new HashMap();
                //jw.beginObject();
                try 
                {
                    for (; rs.next();)
                    {
                        int j=1;
                        for(Param p:pars)
                        {
                            if(p.type.equalsIgnoreCase("TEXT"))
                            {
                                tmp_map.put(p.name, rs.getString(j));
                            }
                            else if(p.type.equalsIgnoreCase("COMPLEX"))
                            {
                                Map<String, Object> RootMapObject = new Gson().fromJson(rs.getString(j), Map.class);
                                tmp_map.put(p.name, RootMapObject);
                            }
                            else if(p.type.equalsIgnoreCase("LIST"))
                            {
                                List<Object> RootMapObject = new Gson().fromJson(rs.getString(j), List.class);
                                tmp_map.put(p.name, RootMapObject);
                            }                        
                            else
                            {
                                tmp_map.put(p.name, rs.getDouble(j));
                                //gson.toJson(rs.getDouble(j), Double.class, jw.name(p.name));
                            }
                            j++;
                        }
                    }
                    
                    
                    rs.close();
                    h.closeDatabase();
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
                } 
                result = tmp_map;
            } 
            catch (Exception ex) 
            {
                Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
            }
         }

         return result;
    }
    
    public static void Kill(String db_name, String table_name, String key_field, String key_value)
    {
        //DBHelper h=new DBHelper();
        InnerDatabase h=null;
        
        
        if(ConfigUtils.getConfigBoolean("thrones.db.use_trans", false))
        {
            h=MemoryDb.initInstance("");
        }
        else
        {
            h=new DBHelper(db_name);
        }
        
        if((key_field==null?"":key_field).length()>0)
        {
            
                ResultSet rs=h.runSelect("select column_name,data_type from metadata where table_name='"+table_name+"' and column_name='"+key_field+"'");

                Param pars=null;

                try 
                {
                    for (; rs.next();)
                    {
                        pars = new Param(rs.getString(1),"",rs.getString(2));
                    }
                    rs.close();
                    h.closeDatabase();
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                if(pars!=null)
                {
                    String comma=(pars.type.equalsIgnoreCase("TEXT")||pars.type.equalsIgnoreCase("COMPLEX")||pars.type.equalsIgnoreCase("COMPLEX"))?"'":"";
                    String delete_clause="delete from "+table_name+" where "+key_field+"="+comma+key_value+comma+";";
                    h.runInsertUpdate(delete_clause);
                    h.closeDatabase();
                }  
        }
        else
        {
            String delete_clause="delete from "+table_name+";";
            h.runInsertUpdate(delete_clause);
            h.closeDatabase();
        }
        
    }
    
}
