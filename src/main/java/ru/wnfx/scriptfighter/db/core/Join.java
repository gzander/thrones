/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.core;

import java.util.ArrayList;
import ru.wnfx.scriptfighter.db.conditions.Condition;
import ru.wnfx.scriptfighter.db.nosql.NOSQL;

/**
 *
 * @author sergk
 */
public abstract class Join 
{
    public NOSQL view;
    public String relation;
    ArrayList<Condition> conditions;
    
    public Join()
    {
        conditions=new ArrayList();
        relation=null;
    }
    
    protected String formatConditions(String rel)
    {
        String result="";
        for(Condition c:conditions)
        {
            result+=c.getNot()+c.toJoin(rel, (this.relation==null)?this.view.relation:this.relation);
        }
        if(result.startsWith(" or "))
        {
            result=result.replaceFirst(" or ", "");
        }
        else
        {
             result=result.replaceFirst(" and ", "");
        }
        
        return result;
    }
    
    public Join addCondition(Condition c)
    {
        conditions.add(c);
        return this;
    }
    
    public abstract String toSQL(String rel);
}
