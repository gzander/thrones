/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.core;

/**
 *
 * @author sergk
 */
import java.io.*;
import java.nio.*;
import java.net.*;
import java.util.Arrays;
//import java.util.concurrent.ConcurrentLinkedQueue;
//import java.util.concurrent.ArrayBlockingQueue;
import com.google.gson.*;
import com.google.gson.stream.*;
import org.apache.log4j.Logger;
import ru.wnfx.scriptfighter.utils.MyBase64;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.UUID.*;
import java.util.ArrayList;
import java.util.logging.Level;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.db.persistent.queue.PersistentQueue;
import ru.wnfx.scriptfighter.db.persistent.queue.Transaction;
import ru.wnfx.scriptfighter.utils.ConfigUtils;


/**
 *
 * @author sergk
 */
public class TransactThread implements Runnable 
{
    public static final Logger LOG=Logger.getLogger(TransactThread.class);

    private volatile PersistentQueue<Transaction> transact_queue;
    public final String notifyer;
    public int mode;

    public TransactThread(PersistentQueue statements_queue) 
    {
        Thread t = new Thread(this);
        notifyer="notifyme";
        t.start();
        this.transact_queue = statements_queue;
    }

    @Override
     public void run() 
     {   
         DBHelper standby=new DBHelper();
         int batch_size=100;
         
         for(;;)
         {
             int min_batch_setting=ConfigUtils.getConfigInt("thrones.db.min_batch_size", 10);
             int batch_divider=ConfigUtils.getConfigInt("thrones.db.batch_divider", 1000);
             String[] stmt_list=null;
             int[] checksums=null;
             int real_count=0;
             batch_size=transact_queue.size()/batch_divider;
             if(batch_size<min_batch_setting)
             {
                 batch_size=min_batch_setting;
             }
             
             try
             {
                Transaction tr=null;
                try 
                {
                    stmt_list=new String[batch_size];
                    checksums=new int[batch_size];
                    real_count=0;
                    for(int i=0; i<batch_size; i++)
                    {
                        tr = transact_queue.peek(i);
                        stmt_list[i]=tr.statementText;
                        checksums[i]=tr.update_count;
                        real_count++;
                    }
                    
                    if(stmt_list.length>0)
                    {      
                        if(standby.runInsertUpdateBunch(stmt_list,checksums)>=0)
                        {
                            for(int i=0; i<batch_size; i++)
                            {
                                tr = transact_queue.remove();
                                tr=null;
                            }
                        }
                    }
                    else
                    {
                        try 
                        {
                            Thread.sleep(100);
                        } 
                        catch (InterruptedException ex) 
                        {
                            LOG.error("interrupted waiting of statements");
                        }
                    }
                } 
                catch (IOException ex) 
                {
                     LOG.error("error appliying statement: "+ex.getMessage());
                }
                //standby.runInsertUpdate(tr.statementText);
                standby.closeDatabase();
             }
             catch(NullPointerException npe)
             {
                 if(stmt_list!=null)
                 {
                    if(standby.runInsertUpdateBunch(stmt_list,checksums)>=0)
                    {
                       for(int i=0; i<real_count; i++)
                       {
                           try
                           {
                               Transaction tr = transact_queue.remove();
                               tr=null;
                           }
                           catch (IOException ex) 
                           {
                                LOG.error("error appliying statement: "+ex.getMessage());
                           }
                       }
                    }
                 }
                 
                 try 
                 {
                     //Thread.sleep(100);
                     synchronized (notifyer) 
                     {
                        notifyer.wait();
                     }

                 } 
                 catch (InterruptedException ex) 
                 {
                     LOG.error("interrupted waiting of statements");
                 }
             }
         }
     }
}