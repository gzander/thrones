/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.core;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import thrones.*;
import org.apache.log4j.Logger;
import ru.wnfx.scriptfighter.db.persistent.queue.*;
import ru.wnfx.scriptfighter.utils.ConfigUtils;

/**
 *
 * @author sergk
 */
public class MemoryDb extends InnerDatabase
{
    public static final Logger LOG=Logger.getLogger(MemoryDb.class);
    private static volatile PersistentQueue<Transaction> statementsQueue; 
    private static TransactThread thread;
    public volatile boolean blocker;
    
    private static volatile MemoryDb instance;
    
    private int window_size=ConfigUtils.getConfigInt("thrones.db.window_size", 10000);
    
    private synchronized void block(String statement, boolean need_check_queue)
    {
        boolean logged = false;
        for(;blocker;)
        {
            if(!logged)
            {
                LOG.debug("statement execution blocked: "+statement);
            }
            
            logged=true;
            
            try 
            {
                Thread.sleep(1);
            } 
            catch (InterruptedException ex) 
            {
                LOG.error("Error blocking select interrupted:"+ex.getMessage());
            }
        }
        
        if(need_check_queue)
        {
            for(;statementsQueue.size()>window_size;) 
            {
                if(!logged)
                {
                    LOG.debug("statement execution blocked: "+statement+" caused by trans_queue overflow: "+statementsQueue.size()+" vs. window_size "+window_size);
                }
                logged=true;

                try 
                {
                    Thread.sleep(1);
                } 
                catch (InterruptedException ex) 
                {
                    LOG.error("Error blocking select interrupted:"+ex.getMessage());
                }
            }
        }
        
    }


    public static MemoryDb initInstance(String s) 
    {
        MemoryDb localInstance = instance;
        if (localInstance == null) {
            synchronized (PathHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new MemoryDb();
                }
            }
        }
        return localInstance;
    }
    
    public static MemoryDb getInstance() 
    {
        MemoryDb localInstance = instance;
        if (localInstance == null) {
            synchronized (PathHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new MemoryDb();
                }
            }
        }
        return localInstance;
    }
    
    private MemoryDb()
    {
        
        blocker=false;
        
        try 
        {
            statementsQueue = new PersistentQueue();
        } 
        catch (IOException ex) 
        {
            LOG.error("error init persistent queue: "+ex.getMessage());
        }
        
        thread=new TransactThread(statementsQueue);
        
        try
        {
            Class.forName("org.sqlite.JDBC");
        }
        catch(ClassNotFoundException Ex)
        {

        }
        connectionString="jdbc:sqlite::memory:";
        
        LOG.debug("DBHelper created: "+connectionString);
        try
        {
            connection = DriverManager.getConnection(connectionString);
            String fullPath = PathHolder.getInstance().path;
            String LoadString=fullPath+System.getProperty("file.separator")+"thrones.db3";
            File dbFile = new File(LoadString);
            if (dbFile.exists()) 
            {
                Statement s=connection.createStatement();
                LOG.debug("File exists.");
                s.executeUpdate("restore from " + dbFile.getAbsolutePath());
                s.close();
            }
            
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory", 
          // it probably means no database file is found
          LOG.error("Error running select:"+e.getMessage());
        }
    }  
    
    @Override
    public  ResultSet runSelect(String select_text)
    {
        LOG.debug("try to query: "+select_text);
        
        block(select_text, false);
        
        ResultSet result=null;
        try
        {
            // create a database connection
            
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            result = statement.executeQuery(select_text);
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory", 
          // it probably means no database file is found
          LOG.error("Error running select:"+e.getMessage());
        }
        finally
        {

        }
        return result;
    }
    
    
    @Override
    public void closeDatabase()
    {
        /*try
            {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e)
            {
              // connection close failed.
              LOG.error("Error closing: "+e);
            }*/
        
    }
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int runInsertUpdate(String insert_text)
    {
        int result=0;
        LOG.debug("try to run: "+insert_text);
        
        block(insert_text, true);
        
        try 
        {
            try
            {
                // create a database connection
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(30);  // set timeout to 30 sec.
                statement.executeUpdate(insert_text);
                result=statement.getUpdateCount();
                statement.close();
                statementsQueue.add(new Transaction(insert_text, result));
                /*if(result>0)
                {
                    
                }*/
            }
            catch(SQLException e)
            {
              // if the error message is "out of memory", 
              // it probably means no database file is found
              LOG.error("Statement:"+insert_text+"Error running statement:"+e.getMessage());
            }
            finally
            {
            }
        } 
        catch (IOException ex) 
        {
            LOG.error("Error putting transaction into queue"+ex.getMessage());
        }
        
        synchronized (thread.notifyer) 
        {
           thread.notifyer.notifyAll();
        }
        
        return result;
    }
    
    public void backupFull()
    {
        blocker=true;
        LOG.info("full backup to disk");
        try
        {
            // create a database connection
            Statement statement = connection.createStatement();
            String fullPath = PathHolder.getInstance().path;
            String LoadString=fullPath+System.getProperty("file.separator")+"thrones.db3";
            statement.executeUpdate("backup to "+LoadString);
            statement.close();
            try 
            {
                statementsQueue.clear();
            } 
            catch (IOException ex) 
            {
                LOG.error("Error clear the queue: "+ex.getMessage());
            }
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory", 
          // it probably means no database file is found
          LOG.error("Error running statement:"+e.getMessage());
        }
        finally
        {
            blocker=false;
        }
    }
    
    @Override
    public boolean isTableExists(String tableName)
    {
        LOG.debug("try to check_table: "+tableName);
        
        block("check table "+tableName, false);
        
        boolean res=false;
        ResultSet result=null;
        try
        {
            // create a database connection
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            result = statement.executeQuery("SELECT * FROM sqlite_master WHERE type = 'table' AND name = '"+tableName+"'");
            while(result.next())
            {
              // read the result set
              res=true;
            }
            
            result.close();
        }
        catch(/*SQL*/Exception e)
        {
          // if the error message is "out of memory", 
          // it probably means no database file is found
          LOG.error("Error running select:"+e.getMessage());
        }
        finally
        {
            /*try
            {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e)
            {
              // connection close failed.
              LOG.error("Error closing: "+e);
            }*/
        }
        return res;
    }
}
