/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.core;

/**
 *
 * @author sergk
 */
public class Param 
{
    public String name;
    public String val;
    public String type;
    public String relation;
    
    public Param(String name,String val,String type){this.name=name; this.type=type; this.val=val; relation="";}
    public Param(String name,Integer val,String type){this.name=name; this.type=type; this.val=val.toString();  relation="";}
    public Param(String name,Float val,String type){this.name=name; this.type=type; this.val=val.toString();  relation="";}
    public Param(String name,Double val,String type){this.name=name; this.type=type; this.val=val.toString();  relation="";}
    
    public String castType()
    {
        if(type.equalsIgnoreCase("STRING"))
        {
            return "TEXT";
        }
        else if(type.equalsIgnoreCase("FLOAT")||type.equalsIgnoreCase("INT")||type.equalsIgnoreCase("DOUBLE"))
        {
            return "NUMERIC";
        }
        else
        {
            return "TEXT";
        }
    }

}
