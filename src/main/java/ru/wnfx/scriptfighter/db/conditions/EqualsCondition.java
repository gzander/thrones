/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.conditions;

import ru.wnfx.scriptfighter.db.core.Param;
import ru.wnfx.scriptfighter.utils.StringUtils;

/**
 *
 * @author sergk
 */
public class EqualsCondition extends Condition 
{
    public String value;
    public EqualsCondition(Param keyfield, String value)
    {
        this.keyfield = keyfield;
        this.value=value;
        
    }
    
    public String toSQL()
    {
        String select_clause="";
        if(value==null)
        {
            select_clause+=" "+keyfield+" is null";
        }
        else
        {
            if(keyfield.name.length()>0)
            {
                String comma=(keyfield.type.equalsIgnoreCase("TEXT")||keyfield.type.equalsIgnoreCase("COMPLEX")||keyfield.type.equalsIgnoreCase("COMPLEX"))?"'":"";

                if(StringUtils.isNumeric(value)&&StringUtils.isInteger(value))
                {
                    comma="'";
                }
                select_clause+=" "+keyfield+"="+comma+value+comma;
            }
        }
        return select_clause;
    }
    
    @Override
    public String toJoin(String relation1, String relation2)
    {
        String select_clause="";
        if(value==null)
        {
            select_clause+=" "+relation1+"."+keyfield+" is null";
        }
        else
        {
            if(keyfield.name.length()>0)
            {
                select_clause+=" "+relation1+"."+keyfield+"="+relation2+"."+value;
            }
        }
        return select_clause;
    }
}
