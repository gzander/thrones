/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thrones;

import java.io.File;

/**
 *
 * @author sergk
 */
public class JavaScripts 
{
    
    public static String getJavascript(String scriptname, String app_path)
    {
        //String app_path=".";
        /*try
            {
                ApplicationStartUpPath pth=new ApplicationStartUpPath();
                app_path=pth.getApplicationStartUp();
                LOG.debug("app_path : "+app_path);
            }
        catch(UnsupportedEncodingException UEE)
        {
            LOG.error(UEE.getMessage());
        }*/
        
        return FileReader.readPage(app_path+scriptname, "UTF8");
    }
    public static String getJquery(String app_path)
    {
        
        return FileReader.readPage(app_path+"/jquery.js", "UTF8");
    }
    public static String getTabs(String app_path)
    {
        
        return FileReader.readPage(app_path+"/bootstrap-tab.js", "UTF8");
    }
    public static String getTransition(String app_path)
    {
        
        return FileReader.readPage(app_path+"/bootstrap-transition.js", "UTF8");
    }
    public static String getModal(String app_path)
    {
        
        return FileReader.readPage(app_path+"/bootstrap-modal.js", "UTF8");
    }
    public static String getKeybinder(String app_path)
    {
        
        return FileReader.readPage(app_path+"/Keybinder.js", "UTF8");
    }
    public static String getParser(String app_path)
    {
        
        return FileReader.readPage(app_path+"/Parser.js", "UTF8");
    }
    public static String getTextareaDecorator(String app_path)
    {
        
        return FileReader.readPage(app_path+"/TextareaDecorator.js", "UTF8");
    }
    public static String getSelectHelper(String app_path)
    {
        
        return FileReader.readPage(app_path+"/SelectHelper.js", "UTF8");
    }
    public static String getTextCompleteHelper(String app_path)
    {
        
        return FileReader.readPage(app_path+"/jquery.textcomplete.js", "UTF8");
    }
    public static String getTableTree(String app_path)
    {
        
        return FileReader.readPage(app_path+"/jquery.treetable.js", "UTF8");
    }
   
    public static String getHighLight(String app_path)
    {
        
        return FileReader.readPage(app_path+"/highlight.js", "UTF8");
    }
    
    public static String getLayout(String app_path)
    {
        
        return FileReader.readPage(app_path+"/layout.js", "UTF8");
    }
    
    
}
