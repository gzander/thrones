/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thrones;

import org.apache.log4j.Logger;

/**
 *
 * @author sergk
 */
public class PathHolder 
{
private static volatile PathHolder instance;
    public static final Logger LOG=Logger.getLogger(PathHolder.class);
    public String path;

    public static PathHolder initInstance(String s) 
    {
        PathHolder localInstance = instance;
        if (localInstance == null) {
            synchronized (PathHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new PathHolder(s);
                }
            }
        }
        return localInstance;
    }
    
    public static PathHolder getInstance() 
    {
        PathHolder localInstance = instance;
        if (localInstance == null) {
            synchronized (PathHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new PathHolder(null);
                }
            }
        }
        return localInstance;
    }
    
    private PathHolder(String s)
    {
        LOG.info("First init app_path");
        path=s;
    }    
}
