/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thrones;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import java.io.*;
import java.net.*;
import org.apache.log4j.Logger;
import ru.wnfx.scriptfighter.config.Config;
import ru.wnfx.scriptfighter.dispatcher.DispatcherThread;
import ru.wnfx.scriptfighter.engine.core.LibraryHolder;
import ru.wnfx.scriptfighter.engine.core.ScriptThread;
import ru.wnfx.scriptfighter.engine.core.ThreadsHolder;
import ru.wnfx.scriptfighter.ide.IdeHandler;
import ru.wnfx.scriptfighter.utils.ConfigUtils;
import ru.wnfx.scriptfighter.utils.SourceUtils;


/**
 *
 * @author sergk
 */
public class Thrones
{

    /**
     * @param args the command line arguments
     */
    static String app_path;
    public static final Logger LOG=Logger.getLogger(Thrones.class);
    //public static volatile ArrayList<ScriptThread> threads;
    
    public static DispatcherThread dispatcher;
    
    public static void main(String[] args)  throws Exception
    {   
        //threads=new ArrayList<ScriptThread>();
        
        app_path=".";
        LOG.info("main::Started");
        /*try
            {
                ApplicationStartUpPath pth=new ApplicationStartUpPath();
                app_path=pth.getApplicationStartUp();
                LOG.debug("app_path : "+app_path);
            }
        catch(UnsupportedEncodingException UEE)
        {
            LOG.error(UEE.getMessage());
        }*/
        
        PathHolder.getInstance().path=app_path;
        
        LOG.info("main::with app_path:"+app_path);
        String config_path="";
        String list[] = new File(app_path).list();
        for(int i = 0; i < list.length; i++)
        {
            if(list[i].contains("config_slave.properties"))
            {
                config_path=list[i];
            }
        }
        if(config_path.length()==0)
        {
            LOG.error("main::No config found");
            return;
        }
        
        System.setProperty (Config.FILE_PROP_NAME, config_path);
        Config.load();
        // TODO code application logic here
        
        
        
        ThreadsHolder.initInstance();
        LibraryHolder.initInstance();
        dispatcher=new DispatcherThread();
        
        String startup_script=ConfigUtils.getConfigString("thrones.autorun", "");//System.getProperty("thrones.autorun");
        
        if(startup_script!=null)
        {
            if(startup_script.length()>=0)
            {
                String startup_script_param=ConfigUtils.getConfigString("thrones.autorun.param", "");//System.getProperty("thrones.autorun.param");
                ScriptThread st=new ScriptThread(SourceUtils.formatScriptCall(startup_script,startup_script_param), 0, dispatcher.events_queue);
                System.out.println("StartupScript: "+startup_script+" started with param: "+startup_script_param);
            }
        }
        if(ConfigUtils.getConfigBoolean("thrones.ide", true))
        {
            HttpServer server = HttpServer.create(new InetSocketAddress(ConfigUtils.getConfigInt("thrones.port", 8088)), 10);
            HttpContext context = server.createContext("/"+ConfigUtils.getConfigString("thrones.context", "Thrones"), new IdeHandler(dispatcher));
            context.getFilters().add(new ParameterFilter());
            server.start();
            System.out.println("Server started...");
        }
    }
    
  
  
  
}
