/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thrones;

/**
 *
 * @author sergk
 */
public class StyleSheet 
{
    
    public static String getStyleSheet(String app_path)
    {       
        return FileReader.readPage(app_path+"/layout.css", "UTF8");
    }
    public static String getBootstrap(String app_path)
    {       
        return FileReader.readPage(app_path+"/bootstrap.css", "UTF8");
    }
    public static String getEditedTextArea(String app_path)
    {       
        return FileReader.readPage(app_path+"/TextareaDecorator.css", "UTF8");
    }
    public static String getTextComplete(String app_path)
    {       
        return FileReader.readPage(app_path+"/jquery.textcomplete.css", "UTF8");
    }
    public static String getTreeTable(String app_path)
    {       
        return FileReader.readPage(app_path+"/jquery.treetable.css", "UTF8");
    }
    public static String getTreeTableStyle(String app_path)
    {       
        return FileReader.readPage(app_path+"/jquery.treetable.theme.default.css", "UTF8");
    }
    public static String getHighlightStyle(String app_path)
    {       
        return FileReader.readPage(app_path+"/highlight.css", "UTF8");
    }

}
