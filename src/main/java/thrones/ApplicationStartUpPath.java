/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thrones;

/**
 *
 * @author sergk
 */
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import org.apache.log4j.*;

/**
 * http://www.dxgames.narod.ru.

 * Класс для определения пути, из которого была стартована программа.
 * @author Урванов Ф. В.
 *
 */
public class ApplicationStartUpPath 
{
    public static final Logger LOG=Logger.getLogger(ApplicationStartUpPath.class);
    
    public String getApplicationStartUp() throws UnsupportedEncodingException
    {
            String strPath=getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
            try
            {
                    URL url=new URL(strPath);
                    strPath=url.getPath();
            }
            catch(MalformedURLException malformedUrlException)
            {

            }

            File f=new File(strPath);
            String strResult=URLDecoder.decode(f.getParent(),"UTF-8");
            return strResult;
    }
}