/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thrones;
/**
 *
 * @author sergk
 */

public class MasterPage 
{
    public static String getMasterPage(String app_path)
    {
        
        return "<!DOCTYPE HTML>"+
"<html lang=\"en\">"+
    "<head>"+
        //"<meta charset=\""+System.getProperty("tank_runner.charset").toString()+"\">"+
        "<meta charset=\"UTF-8\">"+
        "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"+
        "<meta name=\"description\" content=\"\">"+
        "<meta name=\"author\" content=\"\">"+
        "<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">"+
        "<title>"+
            "SCRIPTFIGHTER"+
        "</title>"+
        "<style>"+
                StyleSheet.getBootstrap(app_path)+
        "</style>"+                  
        "<style>"+
                StyleSheet.getStyleSheet(app_path)+
        "</style>"+   
        "<style>"+
                StyleSheet.getEditedTextArea(app_path)+
        "</style>"+   
        "<style>"+
                StyleSheet.getTextComplete(app_path)+
        "</style>"+    
        "<style>"+
                StyleSheet.getTreeTable(app_path)+
        "</style>"+    
        "<style>"+
                StyleSheet.getTreeTableStyle(app_path)+
        "</style>"+  
        "<style>"+
                StyleSheet.getHighlightStyle(app_path)+
        "</style>"+                 
        "<script>"+
                JavaScripts.getJquery(app_path)+
        "</script>"+
        "<script>"+
                JavaScripts.getTabs(app_path)+
        "</script>"+
        "<script>"+
                JavaScripts.getTransition(app_path)+
        "</script>"+                
        "<script>"+
                JavaScripts.getModal(app_path)+
        "</script>"+ 
        "<script>"+
                JavaScripts.getKeybinder(app_path)+
        "</script>"+ 
        "<script>"+
                JavaScripts.getParser(app_path)+
        "</script>"+ 
        "<script>"+
                JavaScripts.getTextareaDecorator(app_path)+
        "</script>"+ 
        "<script>"+
                JavaScripts.getSelectHelper(app_path)+
        "</script>"+ 
        "<script>"+
                JavaScripts.getTextCompleteHelper(app_path)+
        "</script>"+                 
        "<script>"+
                JavaScripts.getLayout(app_path)+
        "</script>"+    
        "<script>"+
                JavaScripts.getTableTree(app_path)+
        "</script>"+  
        "<script>"+
                JavaScripts.getHighLight(app_path)+
        "</script>"+                 
        "</head>"+
        "<http>"+
        "<body>"+
                FileReader.readPage("index.html", "utf-8")+
        "</body>"+        
        "</http>"
                ;
    }
}
